This repository contains sources and formalisation for the draft paper "Shallow Embedding of Type Theory is Morally Correct", by Ambrus Kaposi, Andr�s Kov�cs and Nicolai Kraus.

Pdf of paper [can be found here](paper/paper.pdf).

The [Agda](Agda) directory contains the formalization. You can start from [Readme.agda](Agda/Readme.agda). We used Agda 2.6.0 to check the repository. 
