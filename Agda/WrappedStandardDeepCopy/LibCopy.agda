module WrappedStandardDeepCopy.LibCopy where

open import Agda.Primitive

infix 4 _≡_
infixl 4 _◾_
infix 5 _⁻¹
infixl 5 _,Σ_
infixl 3 _×_

data _≡_ {ℓ}{A : Set ℓ} (x : A) : A → Set ℓ where
  refl : x ≡ x

coe : ∀{ℓ}{A B : Set ℓ} → A ≡ B → A → B
coe refl a = a

ap : ∀{ℓ ℓ'}{A : Set ℓ}{B : Set ℓ'}(f : A → B){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    → f a₀ ≡ f a₁
ap f refl = refl

tr : ∀{ℓ ℓ'}{A : Set ℓ}(P : A → Set ℓ'){x y : A}(p : x ≡ y) → P x → P y
tr P p a = coe (ap P p) a

_◾_ : ∀{ℓ}{A : Set ℓ}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ◾ refl = refl

_⁻¹ : ∀{ℓ}{A : Set ℓ}{x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl

J : ∀{ℓ ℓ'}{A : Set ℓ}{x : A}(P : {y : A} → x ≡ y → Set ℓ') → P refl → {y : A} → (w : x ≡ y) → P w
J P pr refl = pr

record ⊤ : Set where -- \b1

tt : ⊤
tt = record {}

record Σ {ℓ ℓ'} (A : Set ℓ) (B : A → Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,Σ_
  field
    fst : A
    snd : B fst
open Σ public

_×_ : ∀{ℓ ℓ'} → Set ℓ → Set ℓ' → Set (ℓ ⊔ ℓ')
A × B = Σ A λ _ → B

data ⊥ : Set where -- \b0

exfalso : ∀{i}{C : Set i} → ⊥ → C
exfalso ()

data Bool : Set where -- \b2
  true false : Bool

if : ∀{i}(P : Bool → Set i) → P true → P false → (b : Bool) → P b
if P u v true  = u
if P u v false = v

record Lift {ℓ ℓ'}(A : Set ℓ) : Set (ℓ ⊔ ℓ') where
  constructor lift
  field
    unlift : A

open Lift public
