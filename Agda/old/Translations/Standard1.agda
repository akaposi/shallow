{-# OPTIONS --no-pattern-matching --prop #-}

module Translations.Standard1 where

open import Agda.Primitive
open import MLTT.Lib
open import DefnEq

import WrappedStandard as S


infixl 5 _▶_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixl 6 _,Σ_

-- substitution calculus

Con   : ∀ i → Set (lsuc i)
Con i = S.Tm S.∙ (S.U i)

Ty    : ∀ {i}(Γ : Con i)(j : Level) → Set (i ⊔ lsuc j)
Ty Γ j = S.Tm S.∙ (S.El Γ S.⇒ S.U j)

∙     : Con lzero
∙ = S.c S.Unit

_▶_   : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j)
Γ ▶ A = S.c (S.Σ (S.El Γ) (S.El (S.app A)))

Sub   : ∀ {i}(Γ : Con i){j}(Δ : Con j) → Set (i ⊔ j)
Sub Γ Δ = S.Tm S.∙ (S.El Γ S.⇒ S.El Δ)

Tm    : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Set (i ⊔ j)
Tm Γ A = S.Tm S.∙ (S.Π (S.El Γ) (S.El (S.app A)))

_[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
A [ σ ]T = S.lam (S.app A S.[ S.ε S., S.app (σ S.[ S.ε ]t) ]t)

id : ∀{i}{Γ : Con i} → Sub Γ Γ
id = S.lam S.v⁰

_∘_ :
  ∀{i}{Θ : Con i}{j}{Δ : Con j}(σ : Sub Θ Δ){k}{Γ : Con k}(δ : Sub Γ Θ)
  → Sub Γ Δ
σ ∘ δ = S.lam (σ S.[ S.ε ]t S.$ (δ S.[ S.ε ]t S.$ S.v⁰))

ε : ∀{i}{Γ : Con i} → Sub Γ ∙
ε = S.lam S.tt

∙η : ∀{i}{Γ : Con i}{σ : Sub Γ ∙} → σ =? ε
∙η = yes

_,_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : Ty Δ k}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▶ A)
σ , t = S.lam (σ S.[ S.ε ]t S.$ S.v⁰ S.,Σ t S.[ S.ε ]t S.$ S.v⁰)

π₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}(σ : Sub Γ (Δ ▶ A)) → Sub Γ Δ
π₁ σ = S.lam (S.pr₁ (σ S.[ S.ε ]t S.$ S.v⁰))

π₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}(σ : Sub Γ (Δ ▶ A)) → Tm Γ (A [ π₁ σ ]T)
π₂ σ = S.lam (S.pr₂ (σ S.[ S.ε ]t S.$ S.v⁰))

_[_]t : ∀{i}{Δ : Con i}{j}{A : Ty Δ j}(t : Tm Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = S.lam (t S.[ S.ε ]t S.$ (σ S.[ S.ε ]t S.$ S.v⁰))

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T =? A
[id]T = yes

[∘]T : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
        {l}{A : Ty Δ l} → A [ σ ]T [ δ ]T =? A [ σ ∘ δ ]T
[∘]T = yes

idl : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ =? σ
idl = yes

idr : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id =? σ
idr = yes

ass : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Ξ : Con k}{δ : Sub Ξ Θ}{l}{Γ : Con l}{ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν =? σ ∘ (δ ∘ ν)
ass = yes

▶β₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → π₁ {A = A}(σ , t) =? σ
▶β₁ = yes

▶β₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → π₂ {A = A} (σ , t) =? t
▶β₂ = yes

▶η : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}{σ : Sub Γ (Δ ▶ A)} → (π₁ σ , π₂ σ) =? σ
▶η = yes

,∘ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)}{l}{θ : Con l}{δ : Sub θ Γ} →
  (_,_ σ {A = A} t) ∘ δ =? (σ ∘ δ) , (t [ δ ]t)
,∘ = yes

-- abbreviations

wk : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Sub (Γ ▶ A) Γ
wk = π₁ id

wk² :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▶ A) k} →
   Sub (Γ ▶ A ▶ B) Γ
wk² = wk ∘ wk

wk³ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▶ A) k}
   {l}{C : Ty (Γ ▶ A ▶ B) l} →
   Sub (Γ ▶ A ▶ B ▶ C) Γ
wk³ = wk ∘ (wk ∘ wk)

wk⁴ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▶ A) k}
   {l}{C : Ty (Γ ▶ A ▶ B) l}
   {m}{D : Ty (Γ ▶ A ▶ B ▶ C) m} →
   Sub (Γ ▶ A ▶ B ▶ C ▶ D) Γ
wk⁴ = wk ∘ (wk ∘ (wk ∘ wk))

wk⁵ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▶ A) k}
   {l}{C : Ty (Γ ▶ A ▶ B) l}
   {m}{D : Ty (Γ ▶ A ▶ B ▶ C) m}
   {n}{E : Ty (Γ ▶ A ▶ B ▶ C ▶ D) n} →
   Sub (Γ ▶ A ▶ B ▶ C ▶ D ▶ E) Γ
wk⁵ = wk ∘ (wk ∘ (wk ∘ (wk ∘ wk)))

v⁰ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▶ A) (A [ wk ]T)
v⁰ = π₂ id

v¹ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▶ A) k} →
   Tm (Γ ▶ A ▶ B) (A [ wk² ]T)
v¹ = v⁰ [ wk ]t

v² :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▶ A) k}
   {l}{C : Ty (Γ ▶ A ▶ B) l} →
   Tm (Γ ▶ A ▶ B ▶ C) (A [ wk³ ]T)
v² = v⁰ [ wk² ]t

v³ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▶ A) k}
   {l}{C : Ty (Γ ▶ A ▶ B) l}
   {m}{D : Ty (Γ ▶ A ▶ B ▶ C) m} →
   Tm (Γ ▶ A ▶ B ▶ C ▶ D) (A [ wk⁴ ]T)
v³ = v⁰ [ wk³ ]t

_^_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}(A : Ty Δ k) →
  Sub (Γ ▶ A [ σ ]T) (Δ ▶ A)
_^_ {i}{Γ}{j}{Δ} σ {k} A = σ ∘ wk , v⁰ {i}{Γ}{_}{A [ σ ]T}

-- Π

Π : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▶ A) k) → Ty Γ (j ⊔ k)
Π A B = S.lam (S.c (S.Π (S.El (S.app A)) (S.El (S.app B S.[ S.ε S., S.v¹ S.,Σ S.v⁰ ]t))))

Π[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}{l}{Θ : Con l}{σ : Sub Θ Γ} →
  Π A B [ σ ]T =? Π (A [ σ ]T) (B [ (σ ∘ π₁ id) , π₂ id ]T)
Π[] = yes

lam : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}(t : Tm (Γ ▶ A) B) → Tm Γ (Π A B)
lam t = S.lam (S.lam (t S.[ S.ε ]t S.$ (S.v¹ S.,Σ S.v⁰)))

app : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}(t : Tm Γ (Π A B)) → Tm (Γ ▶ A) B
app t = S.lam (t S.[ S.ε ]t S.$ S.pr₁ S.v⁰ S.$ S.pr₂ S.v⁰)

_$_ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}(t : Tm Γ (Π A B))(u : Tm Γ A) → Tm Γ (B [ id , u ]T)
t $ u = app t [ id , u ]t

app[] :
  ∀{i}{Γ : Con i}{l}{Δ : Con l}{σ : Sub Γ Δ}{j}{A : Ty Δ j}{k}{B : Ty (Δ ▶ A) k}{t : Tm Δ (Π A B)}
  → app t [ σ ∘ π₁ id , π₂ id ]t =? app (t [ σ ]t)
app[] = yes

Πβ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}{t : Tm (Γ ▶ A) B} → app (lam t) =? t
Πβ = yes

Πη : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}{t : Tm Γ (Π A B)} → lam (app t) =? t
Πη = yes

-- U

U : ∀{i}{Γ : Con i} j → Ty Γ (lsuc j)
U j = S.lam (S.c (S.U j))

U[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} {k} → U k [ σ ]T =? U k
U[] = yes

El : ∀{i}{Γ : Con i}{j}(a : Tm Γ (U j)) → Ty Γ j
El a = a

El[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{a : Tm Δ (U k)}
       → El a [ σ ]T =? El (a [ σ ]t)
El[] = yes

c : ∀{i}{Γ : Con i}{j}(A : Ty Γ j) → Tm Γ (U j)
c A = A

Elc : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → El (c A) =? A
Elc = yes

cEl : ∀{i}{Γ : Con i}{j}{a : Tm Γ (U j)} → c (El a) =? a
cEl = yes

Russell : ∀{i}{Γ : Con i}{j} → Tm Γ (U j) =? Ty Γ j
Russell = yes

-- Bool

Bool    : ∀{i}{Γ : Con i} → Ty Γ lzero
Bool = S.lam (S.c S.Bool)

Bool[]  : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → Bool [ σ ]T =? Bool
Bool[] = yes
true    : ∀{i}{Γ : Con i} → Tm Γ Bool
true = S.lam S.true
true[]  : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → true [ σ ]t =? true
true[] = yes
false   : ∀{i}{Γ : Con i} → Tm Γ Bool
false = S.lam S.false
false[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → false [ σ ]t =? false
false[] = yes

ite :
  ∀{i}{Γ : Con i}{j}(C : Ty (Γ ▶ Bool) j)
      → Tm Γ (C [ (id , true) ]T)
      → Tm Γ (C [ (id , false) ]T)
      → (t : Tm Γ Bool)
      → Tm Γ (C [ (id , t) ]T)
ite C u v t = S.lam (S.ite (S.El (C S.[ S.ε ]t S.$ (S.v¹ S.,Σ S.v⁰)))
                           (S.app u) (S.app v) (S.app t))

ite-true :
  ∀{i}{Γ : Con i}{j}{C : Ty (Γ ▶ Bool) j}
      → {u : Tm Γ (C [ (id , true) ]T)}
      → {v : Tm Γ (C [ (id , false) ]T)}
      → ite C u v true =? u
ite-true = yes

ite-false :
  ∀{i}{Γ : Con i}{j}{C : Ty (Γ ▶ Bool) j}
      → {u : Tm Γ (C [ (id , true) ]T)}
      → {v : Tm Γ (C [ (id , false) ]T)}
      → ite C u v false =? v
ite-false = yes

ite[]   : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}
          → {C  : Ty (Δ ▶ Bool) j}
          → {u : Tm Δ (C [ (id , true) ]T)}
          → {v : Tm Δ (C [ (id , false) ]T)}
          → {t  : Tm Δ Bool}
          → ite C u v t [ σ ]t =? ite (C [ σ ^ Bool ]T) (u [ σ ]t) (v [ σ ]t) (t [ σ ]t)
ite[] = yes

-- unit

Unit : {i : Level}{Γ : Con i} → Ty Γ lzero
Unit = S.lam (S.c S.Unit)

tt : ∀{i}{Γ : Con i} → Tm Γ Unit
tt = S.lam S.tt

-- Σ

Σ : {i j k : Level}{Γ : Con i}(A : Ty Γ j)(B : Ty (Γ ▶ A) k) → Ty Γ (j ⊔ k)
Σ A B = S.lam (S.c (S.Σ (S.El (S.app A)) (S.El (S.app B S.[ S.ε S., S.v¹ S.,Σ S.v⁰ ]t))))

_,Σ_ : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▶ A) k}(u : Tm Γ A)(v : Tm Γ (B [ id , u ]T)) → Tm Γ (Σ A B)
u ,Σ v = S.lam (S.app u S.,Σ S.app v)

pr₁ : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▶ A) k} → Tm Γ (Σ A B) → Tm Γ A
pr₁ t = S.lam (S.pr₁ (S.app t))

pr₂ : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▶ A) k}(t : Tm Γ (Σ A B)) → Tm Γ (B [ id , pr₁ t ]T)
pr₂ t = S.lam (S.pr₂ (S.app t))

-- Identity

Id : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(u v : Tm Γ A) → Ty Γ j
Id A u v = S.lam (S.c (S.Id (S.El (S.app A)) (S.app u) (S.app v)))

refl : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}(u : Tm Γ A) → Tm Γ (Id A u u)
refl u = S.lam (S.refl (S.app u))

J :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
   {k}(C : Ty (Γ ▶ A ▶ Id (A [ wk ]T) (u [ wk ]t) v⁰) k)
   (w : Tm Γ (C [ id , u , refl u ]T))
   {v : Tm Γ A}(t : Tm Γ (Id A u v)) → Tm Γ (C [ id , v , t ]T)
J C w {v} t = S.lam (S.J (S.El (C S.[ S.ε ]t S.$ (S.v² S.,Σ S.v¹ S.,Σ S.v⁰))) (S.app w) (S.app t))

Idβ :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
   {k}{C : Ty (Γ ▶ A ▶ Id (A [ wk ]T) (u [ wk ]t) v⁰) k}
   {w : Tm Γ (C [ id , u , refl u ]T)} →
   J C w (refl u) =? w
Idβ = yes

tr :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}
   {k}(C : Ty (Γ ▶ A) k)
   {u v : Tm Γ A}(t : Tm Γ (Id A u v))
   (w : Tm Γ (C [ id , u ]T)) → Tm Γ (C [ id , v ]T)
tr C t w = J (C [ wk ]T) w t
