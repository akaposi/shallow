{-# OPTIONS --no-pattern-matching --prop #-}

module Translations.Graph2 where

open import Agda.Primitive
open import DefnEq
import WrappedStandard as S

infixl 5 _▶_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixl 6 _,Σ_

-- substitution calculus

record Con i : Set (lsuc i) where
  field
    ∣_∣C : S.Ty S.∙ i
    _~C  : S.Ty (S.∙ S.▶ ∣_∣C S.▶ ∣_∣C S.[ S.ε ]T) i
open Con

record Ty {i}(Γ : Con i)(j : Level) : Set (i ⊔ lsuc j) where
  field
    ∣_∣T : S.Ty (S.∙ S.▶ ∣ Γ ∣C) j
    _~T  : S.Ty (S.∙ S.▶ ∣ Γ ∣C S.▶ ∣ Γ ∣C S.[ S.ε ]T S.▶ Γ ~C S.▶ ∣_∣T S.[ S.ε S., S.v² ]T S.▶ ∣_∣T S.[ S.ε S., S.v² ]T) j
open Ty

∙ : Con lzero
∙ = record {
  ∣_∣C = S.Unit ;
  _~C = S.Unit }

_▶_ : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j)
Γ ▶ A = record {
  ∣_∣C = S.Σ ∣ Γ ∣C ∣ A ∣T ;
  _~C  = S.Σ (Γ ~C S.[ S.ε S., S.pr₁ S.v¹ S., S.pr₁ S.v⁰ ]T)
             (A ~T S.[ S.ε S., S.pr₁ S.v² S., S.pr₁ S.v¹ S., S.v⁰ S., S.pr₂ S.v² S., S.pr₂ S.v¹ ]T) }

record Sub {i}(Γ : Con i){j}(Δ : Con j) : Set (i ⊔ j) where
  field
    ∣_∣s : S.Tm (S.∙ S.▶ ∣ Γ ∣C) (∣ Δ ∣C S.[ S.ε ]T)
    _~s  : S.Tm (S.∙ S.▶ ∣ Γ ∣C S.▶ ∣ Γ ∣C S.[ S.wk ]T S.▶ Γ ~C)
                (Δ ~C S.[ S.ε S., ∣_∣s S.[ S.ε S., S.v² ]t S., ∣_∣s S.[ S.ε S., S.v¹ ]t ]T)
open Sub

record Tm {i}(Γ : Con i){j}(A : Ty Γ j) : Set (i ⊔ j) where
  field
    ∣_∣t : S.Tm (S.∙ S.▶ ∣ Γ ∣C) ∣ A ∣T
    _~t  : S.Tm (S.∙ S.▶ ∣ Γ ∣C S.▶ ∣ Γ ∣C S.[ S.wk ]T S.▶ Γ ~C)
                (A ~T S.[ S.id S., ∣_∣t S.[ S.ε S., S.v² ]t S., ∣_∣t S.[ S.ε S., S.v¹ ]t ]T)
open Tm

_[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
A [ σ ]T = record {
  ∣_∣T = ∣ A ∣T S.[ S.ε S., ∣ σ ∣s ]T ;
  _~T  = A ~T S.[ S.ε S., ∣ σ ∣s S.[ S.ε S., S.v⁴ ]t S., ∣ σ ∣s S.[ S.ε S., S.v³ ]t S., σ ~s S.[ S.wk² ]t S., S.v¹ S., S.v⁰ ]T }

id : ∀{i}{Γ : Con i} → Sub Γ Γ
id = record {
  ∣_∣s = S.v⁰ ;
  _~s = S.v⁰ }

-- ...

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T =? A
[id]T = yes

-- ...
