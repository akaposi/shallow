{-# OPTIONS --no-pattern-matching --prop #-}

module Translations.Pred2 where

open import Agda.Primitive
open import DefnEq
import WrappedStandard as S

infixl 5 _▶_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixl 6 _,Σ_

-- substitution calculus

record Con i : Set (lsuc i) where
  field
    ∣_∣C : S.Ty S.∙ i
    _ᴾC  : S.Ty (S.∙ S.▶ ∣_∣C) i
open Con

record Ty {i}(Γ : Con i)(j : Level) : Set (i ⊔ lsuc j) where
  field
    ∣_∣T : S.Ty (S.∙ S.▶ ∣ Γ ∣C) j
    _ᴾT  : S.Ty (S.∙ S.▶ ∣ Γ ∣C S.▶ Γ ᴾC S.▶ ∣_∣T S.[ S.wk ]T) j
open Ty

∙ : Con lzero
∙ = record {
  ∣_∣C = S.Unit ;
  _ᴾC = S.Unit }

_▶_ : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j)
Γ ▶ A = record {
  ∣_∣C = S.Σ ∣ Γ ∣C ∣ A ∣T ;
  _ᴾC  = S.Σ (Γ ᴾC S.[ S.ε S., S.pr₁ S.v⁰ ]T)
             (A ᴾT S.[ S.ε S., S.pr₁ S.v¹ S., S.v⁰ S., S.pr₂ S.v¹ ]T) }

record Sub {i}(Γ : Con i){j}(Δ : Con j) : Set (i ⊔ j) where
  field
    ∣_∣s : S.Tm (S.∙ S.▶ ∣ Γ ∣C) (∣ Δ ∣C S.[ S.ε ]T)
    _ᴾs  : S.Tm (S.∙ S.▶ ∣ Γ ∣C S.▶ Γ ᴾC)
                (Δ ᴾC S.[ S.ε S., ∣_∣s S.[ S.wk ]t ]T)
open Sub

record Tm {i}(Γ : Con i){j}(A : Ty Γ j) : Set (i ⊔ j) where
  field
    ∣_∣t : S.Tm (S.∙ S.▶ ∣ Γ ∣C) ∣ A ∣T
    _ᴾt  : S.Tm (S.∙ S.▶ ∣ Γ ∣C S.▶ Γ ᴾC)
                (A ᴾT S.[ S.id S., ∣_∣t S.[ S.wk ]t ]T)
open Tm

_[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
A [ σ ]T = record {
  ∣_∣T = ∣ A ∣T S.[ S.ε S., ∣ σ ∣s ]T ;
  _ᴾT  = A ᴾT S.[ S.ε S., ∣ σ ∣s S.[ S.wk² ]t S., σ ᴾs S.[ S.wk ]t S., S.v⁰ ]T }

id : ∀{i}{Γ : Con i} → Sub Γ Γ
id = record {
  ∣_∣s = S.v⁰ ;
  _ᴾs = S.v⁰ }

-- ...

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T =? A
[id]T = yes

-- ...
