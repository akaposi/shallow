{-# OPTIONS --no-pattern-matching --prop #-}

module Translations.Setoid3b where

open import Agda.Primitive
open import DefnEq
import WrappedStandard as S

infixl 5 _▶_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixl 6 _,Σ_

-- substitution calculus

record Con i : Setω where
  field
    ∣_∣C : S.Con i
    _~C  : {j : Level}{Ω : S.Con j} → S.Sub Ω ∣_∣C → S.Sub Ω ∣_∣C → S.Tm Ω (S.P i)
    ~C[] : {j : Level}{Ω : S.Con j}{ρ₀ ρ₁ : S.Sub Ω ∣_∣C}{k : Level}{Ψ : S.Con k}{ν : S.Sub Ψ Ω} →
           (_~C ρ₀ ρ₁ S.[ ν ]t) =? _~C (ρ₀ S.∘ ν) (ρ₁ S.∘ ν)
    RC   : {j : Level}{Ω : S.Con j}(ρ : S.Sub Ω ∣_∣C) → S.Tm Ω (S.ElP (_~C ρ ρ))
    SC   : {j : Level}{Ω : S.Con j}{ρ₀ ρ₁ : S.Sub Ω ∣_∣C}(ρ₀₁ : S.Tm Ω (S.ElP (_~C ρ₀ ρ₁))) → S.Tm Ω (S.ElP (_~C ρ₁ ρ₀))
    TC   : {j : Level}{Ω : S.Con j}{ρ₀ ρ₁ ρ₂ : S.Sub Ω ∣_∣C}(ρ₀₁ : S.Tm Ω (S.ElP (_~C ρ₀ ρ₁)))(ρ₁₂ : S.Tm Ω (S.ElP (_~C ρ₁ ρ₂))) →
           S.Tm Ω (S.ElP (_~C ρ₀ ρ₂))
open Con

record Ty {i}(Γ : Con i)(j : Level) : Setω where
  field
    ∣_∣T : S.Ty ∣ Γ ∣C j
    _~T  : {j : Level}{Ω : S.Con j}{ρ₀ ρ₁ : S.Sub Ω ∣ Γ ∣C}(ρ₀₁ : S.Tm Ω (S.ElP ((Γ ~C) ρ₀ ρ₁)))
           (t₀ : S.Tm Ω (∣_∣T S.[ ρ₀ ]T))(t₁ : S.Tm Ω (∣_∣T S.[ ρ₁ ]T)) → S.Tm Ω (S.P j)
{-
    ~C[] : {j : Level}{Ω : S.Con j}{ρ₀ ρ₁ : S.Sub Ω ∣_∣C}{k : Level}{Ψ : S.Con k}{ν : S.Sub Ψ Ω} →
           (_~C ρ₀ ρ₁ S.[ ν ]t) =? _~C (ρ₀ S.∘ ν) (ρ₁ S.∘ ν)
    RC   : {j : Level}{Ω : S.Con j}(ρ : S.Sub Ω ∣_∣C) → S.Tm Ω (S.ElP (_~C ρ ρ))
    SC   : {j : Level}{Ω : S.Con j}{ρ₀ ρ₁ : S.Sub Ω ∣_∣C}(ρ₀₁ : S.Tm Ω (S.ElP (_~C ρ₀ ρ₁))) → S.Tm Ω (S.ElP (_~C ρ₁ ρ₀))
-}
