{-# OPTIONS --no-pattern-matching --prop #-}

module Translations.Pred3a where

open import Agda.Primitive
open import DefnEq
import WrappedStandard as S

infixl 5 _▶_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixl 6 _,Σ_

-- substitution calculus

record Con i : Set (lsuc i) where
  field
    ∣_∣C : S.Con i
    _ᴾC  : S.Con i
    prC  : S.Sub _ᴾC ∣_∣C
open Con

record Ty {i}(Γ : Con i)(j : Level) : Set (i ⊔ lsuc j) where
  field
    ∣_∣T : S.Ty ∣ Γ ∣C j
    _ᴾT  : S.Ty (Γ ᴾC S.▶ ∣_∣T S.[ prC Γ ]T) j
open Ty

∙ : Con lzero
∙ = record {
  ∣_∣C = S.∙ ;
  _ᴾC = S.∙ }

_▶_ : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j)
Γ ▶ A = record {
  ∣_∣C = ∣ Γ ∣C S.▶ ∣ A ∣T ;
  _ᴾC  = Γ ᴾC S.▶ ∣ A ∣T S.[ prC Γ ]T S.▶ A ᴾT ;
  prC  = prC Γ S.∘ S.wk² S., S.v¹ }

record Sub {i}(Γ : Con i){j}(Δ : Con j) : Set (i ⊔ j) where
  field
    ∣_∣s : S.Sub ∣ Γ ∣C ∣ Δ ∣C
    _ᴾs  : S.Sub (Γ ᴾC) (Δ ᴾC)
    prs  : prC Δ S.∘ _ᴾs =? ∣_∣s S.∘ prC Γ
open Sub

record Tm {i}(Γ : Con i){j}(A : Ty Γ j) : Set (i ⊔ j) where
  field
    ∣_∣t : S.Tm ∣ Γ ∣C ∣ A ∣T
    _ᴾt  : S.Tm (Γ ᴾC) (A ᴾT S.[ S.id S., ∣_∣t S.[ prC Γ ]t ]T)
open Tm

_[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
A [ σ ]T = record {
  ∣_∣T = ∣ A ∣T S.[ ∣ σ ∣s ]T ;
  _ᴾT  = A ᴾT S.[ σ ᴾs S.∘ S.wk S., {!!} ]T } -- here we need to use the equality...

id : ∀{i}{Γ : Con i} → Sub Γ Γ
id = record {
  ∣_∣s = S.id ;
  _ᴾs  = S.id ;
  prs  = yes }

-- ...

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T =? A
[id]T = yes

-- ...
