{-# OPTIONS --no-pattern-matching --prop #-}

module Translations.Pred1 where

open import Agda.Primitive
open import DefnEq
import WrappedStandard as S

infixl 5 _▶_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixl 6 _,Σ_

-- substitution calculus

record Con i : Set (lsuc i) where
  field
    ∣_∣C : S.Tm S.∙ (S.U i)
    _ᴾC  : S.Tm S.∙ (S.El ∣_∣C S.⇒ S.U i)
open Con

record Ty {i}(Γ : Con i)(j : Level) : Set (i ⊔ lsuc j) where
  field
    ∣_∣T : S.Tm S.∙ (S.El ∣ Γ ∣C S.⇒ S.U j)
    _ᴾT  : S.Tm S.∙ (S.Π (S.El ∣ Γ ∣C) (S.El (S.app (Γ ᴾC)) S.⇒ S.El (S.app ∣_∣T) S.⇒ S.U j))
open Ty

∙ : Con lzero
∙ = record {
  ∣_∣C = S.c S.Unit ;
  _ᴾC = S.lam (S.c S.Unit) }

_▶_ : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j)
Γ ▶ A = record {
  ∣_∣C = S.c (S.Σ (S.El ∣ Γ ∣C) (S.El (S.app ∣ A ∣T))) ;
  _ᴾC = S.lam (S.c (S.Σ (S.El (Γ ᴾC S.[ S.ε ]t S.$ S.pr₁ S.v⁰))
                        (S.El (A ᴾT S.[ S.ε ]t S.$ S.pr₁ S.v¹ S.$ S.v⁰ S.$ S.pr₂ S.v¹)))) }

record Sub {i}(Γ : Con i){j}(Δ : Con j) : Set (i ⊔ j) where
  field
    ∣_∣s : S.Tm S.∙ (S.El ∣ Γ ∣C S.⇒ S.El ∣ Δ ∣C)
    _ᴾs  : S.Tm S.∙ (S.Π (S.El ∣ Γ ∣C) (S.El (S.app (Γ ᴾC)) S.⇒
                     S.El (Δ ᴾC S.[ S.ε ]t S.$ (∣_∣s S.[ S.ε ]t S.$ S.v⁰))))
open Sub

record Tm {i}(Γ : Con i){j}(A : Ty Γ j) : Set (i ⊔ j) where
  field
    ∣_∣t : S.Tm S.∙ (S.Π (S.El ∣ Γ ∣C) (S.El (S.app ∣ A ∣T)))
    _ᴾt  : S.Tm S.∙ (S.Π (S.El ∣ Γ ∣C) (S.Π (S.El (S.app (Γ ᴾC)))
                     (S.El (A ᴾT S.[ S.ε ]t S.$ S.v¹ S.$ S.v⁰ S.$ (∣_∣t S.[ S.ε ]t S.$ S.v¹)))))
open Tm

_[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
A [ σ ]T = record {
  ∣_∣T = S.lam (S.app ∣ A ∣T S.[ S.ε S., S.app (∣ σ ∣s S.[ S.ε ]t) ]t) ;
  _ᴾT = S.lam (S.lam (S.lam (A ᴾT S.[ S.ε ]t S.$ (∣ σ ∣s S.[ S.ε ]t S.$ S.v²) S.$ (σ ᴾs S.[ S.ε ]t S.$ S.v² S.$ S.v¹) S.$ S.v⁰))) }

id : ∀{i}{Γ : Con i} → Sub Γ Γ
id = record {
  ∣_∣s = S.lam S.v⁰ ;
  _ᴾs = S.lam (S.lam S.v⁰) }

-- ...

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T =? A
[id]T = yes

-- ...
