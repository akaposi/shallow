{-# OPTIONS --rewriting #-}

module TTM where

open import Agda.Primitive
open import Lib
open import TT

record TTM (M N : TT) : Setω where
  private module M = TT.TT M
  private module N = TT.TT N
  field
    Con   : ∀ {i} → M.Con i → N.Con i
    Ty    : ∀ {j}{Γ : M.Con j}{i} → M.Ty Γ i → N.Ty (Con Γ) i
    ∙     : Con M.∙ ≡ N.∙
    _▶_   : ∀ {i}(Γ : M.Con i){j}(A : M.Ty Γ j) → Con (Γ M.▶ A) ≡ Con Γ N.▶ Ty A
    Sub   : ∀ {i}{Γ : M.Con i}{j}{Δ : M.Con j} → M.Sub Γ Δ → N.Sub (Con Γ) (Con Δ)
    Tm    : ∀ {i}{Γ : M.Con i}{j}{A : M.Ty Γ j} → M.Tm Γ A → N.Tm (Con Γ) (Ty A)
    _[_]T : ∀ {i}{Δ : M.Con i}{j}(A : M.Ty Δ j){k}{Γ : M.Con k}(σ : M.Sub Γ Δ) →
      Ty (A M.[ σ ]T) ≡ Ty A N.[ Sub σ ]T
    id    : ∀{i}{Γ : M.Con i} → Sub (M.id {i}{Γ}) ≡ N.id

    -- ...

    Bool  : ∀{i}{Γ : M.Con i} → Ty (M.Bool {i}{Γ}) ≡ N.Bool


{-
record TT : Setω where
  field
    Con   : ∀ i → Set (lsuc i)
    Ty    : ∀ {j}(Γ : Con j) i → Set (lsuc i ⊔ j)
    ∙     : Con lzero
    _▶_   : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j)
    Sub   : ∀ {i}(Γ : Con i){j}(Δ : Con j) → Set (i ⊔ j)
    Tm    : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Set (i ⊔ j)
    _[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j


    id : ∀{i}{Γ : Con i} → Sub Γ Γ

    _∘_ :
      ∀{i}{Θ : Con i}{j}{Δ : Con j}(σ : Sub Θ Δ){k}{Γ : Con k}(δ : Sub Γ Θ)
      → Sub Γ Δ

    ε : ∀{i}{Γ : Con i} → Sub Γ ∙

    _,s_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : Ty Δ k}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▶ A)


    π₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}(σ : Sub Γ (Δ ▶ A)) → Sub Γ Δ


    π₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}(σ : Sub Γ (Δ ▶ A)) → Tm Γ (A [ π₁ σ ]T)


    _[_]t : ∀{i}{Δ : Con i}{j}{A : Ty Δ j}(t : Tm Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)


    Π : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▶ A) k) → Ty Γ (j ⊔ k)

    lam : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}(t : Tm (Γ ▶ A) B) → Tm Γ (Π A B)

    app : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}(t : Tm Γ (Π A B)) → Tm (Γ ▶ A) B

    U : ∀{i}{Γ : Con i} j → Ty Γ (lsuc j)

    El : ∀{i}{Γ : Con i}{j}(a : Tm Γ (U j)) → Ty Γ j

    c : ∀{i}{Γ : Con i}{j}(A : Ty Γ j) → Tm Γ (U j)

    Bool    : ∀{i}{Γ : Con i} → Ty Γ lzero
    true    : ∀{i}{Γ : Con i} → Tm Γ Bool
    false   : ∀{i}{Γ : Con i} → Tm Γ Bool
  
    ite :
      ∀{i}{Γ : Con i}{j}(P : Ty (Γ ▶ Bool) j)
          → Tm Γ (P [ (id ,s tr (Tm Γ) (Bool[] ⁻¹) true) ]T)
          → Tm Γ (P [ (id ,s tr (Tm Γ) (Bool[] ⁻¹) false) ]T)
          → (t : Tm Γ Bool)
          → Tm Γ (P [ (id ,s tr (Tm Γ) (Bool[] ⁻¹) t) ]T)
  
  infixl 5 _▶_
  infixl 7 _[_]T
  infixl 5 _,s_
  infix  6 _∘_
  infixl 8 _[_]t
  infixl 5 _^_

  -- abbreviations

  wk : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Sub (Γ ▶ A) Γ
  wk = π₁ id

  vz : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▶ A) (A [ wk ]T)
  vz = π₂ id

  _^_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}(A : Ty Δ k) →
    Sub (Γ ▶ A [ σ ]T) (Δ ▶ A)
  _^_ {i}{Γ}{j}{Δ} σ {k} A = σ ∘ wk ,s tr (Tm (Γ ▶ A [ σ ]T)) [∘]T
                                          (vz {i}{Γ}{_}{A [ σ ]T})
-}
