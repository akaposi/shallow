{-# OPTIONS --rewriting #-}

module Lib where

open import Agda.Primitive renaming (lsuc to suc; lzero to zero) public

infix 4 _≡_
infixl 4 _◾_
infix 5 _⁻¹
infixl 5 _,Σ'_

data _≡_ {ℓ}{A : Set ℓ} (x : A) : A → Set ℓ where
  refl' : x ≡ x

{-# BUILTIN REWRITE _≡_ #-}

coe : ∀{ℓ}{A B : Set ℓ} → A ≡ B → A → B
coe refl' a = a

ap : ∀{ℓ ℓ'}{A : Set ℓ}{B : Set ℓ'}(f : A → B){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    → f a₀ ≡ f a₁
ap f refl' = refl'

tr' : ∀{ℓ ℓ'}{A : Set ℓ}(P : A → Set ℓ'){x y : A}(p : x ≡ y) → P x → P y
tr' P p a = coe (ap P p) a

tr : ∀{ℓ ℓ'}{A : Set ℓ}(P : A → Set ℓ'){x y : A}(p : x ≡ y) → P x → P y
tr P p a = coe (ap P p) a

_◾_ : ∀{ℓ}{A : Set ℓ}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl' ◾ refl' = refl'

_⁻¹ : ∀{ℓ}{A : Set ℓ}{x y : A} → x ≡ y → y ≡ x
refl' ⁻¹ = refl'

J' : ∀{ℓ ℓ'}{A : Set ℓ}{x : A}(P : {y : A} → x ≡ y → Set ℓ') → P refl' → {y : A} → (w : x ≡ y) → P w
J' P pr refl' = pr

record 𝟙 : Set where -- \b1

* : 𝟙
* = record {}

record Σ' {ℓ ℓ'} (A : Set ℓ) (B : A → Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,Σ'_
  field
    pr₁' : A
    pr₂' : B pr₁'

open Σ' public

data 𝟘 : Set where -- \b0

magic : ∀{i}{C : Set i} → 𝟘 → C
magic ()

data 𝟚 : Set where -- \b2
  b0 : 𝟚
  b1 : 𝟚

𝟚Elim : ∀{i}(P : 𝟚 → Set i) → P b0 → P b1 → (b : 𝟚) → P b
𝟚Elim P u v b0 = u
𝟚Elim P u v b1 = v
