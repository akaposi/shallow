{-# OPTIONS --rewriting #-}

module Syntax where

open import Agda.Primitive
open import Lib
open import TT
open import TTM

postulate
  S : TT

module S = TT.TT S

module _ (M : TT) where
  postulate
    r : TTM S M

  module r = TTM.TTM r

  {-# REWRITE r.∙ #-}
  {-# REWRITE r._▶_ #-}
  {-# REWRITE r._[_]T #-}
  {-# REWRITE r.id #-}

  -- ...

  {-# REWRITE r.Bool #-}
