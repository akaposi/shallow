{-# OPTIONS --rewriting #-}

module TT where

open import Agda.Primitive
open import Lib

record TT : Setω where
  field
    Con   : ∀ i → Set (lsuc i)
    Ty    : ∀ {j}(Γ : Con j) i → Set (lsuc i ⊔ j)
    ∙     : Con lzero
    _▶_   : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j)
    Sub   : ∀ {i}(Γ : Con i){j}(Δ : Con j) → Set (i ⊔ j)
    Tm    : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Set (i ⊔ j)
    _[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j


    id : ∀{i}{Γ : Con i} → Sub Γ Γ

    _∘_ :
      ∀{i}{Θ : Con i}{j}{Δ : Con j}(σ : Sub Θ Δ){k}{Γ : Con k}(δ : Sub Γ Θ)
      → Sub Γ Δ

    ε : ∀{i}{Γ : Con i} → Sub Γ ∙

    ∙η : ∀{i}{Γ : Con i}{σ : Sub Γ ∙} → σ ≡ ε

    _,s_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : Ty Δ k}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▶ A)


    π₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}(σ : Sub Γ (Δ ▶ A)) → Sub Γ Δ


    π₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}(σ : Sub Γ (Δ ▶ A)) → Tm Γ (A [ π₁ σ ]T)


    _[_]t : ∀{i}{Δ : Con i}{j}{A : Ty Δ j}(t : Tm Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)


    [id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T ≡ A


    [∘]T : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
            {l}{A : Ty Δ l} → A [ σ ]T [ δ ]T ≡ A [ σ ∘ δ ]T


    idl : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ ≡ σ


    idr : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id ≡ σ


    ass : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Ξ : Con k}{δ : Sub Ξ Θ}{l}{Γ : Con l}{ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)


    ▶β₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → π₁ {A = A}(σ ,s t) ≡ σ

    ▶β₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → coe (ap (λ x → Tm Γ (A [ x ]T)) ▶β₁) (π₂ (σ ,s t)) ≡ t

    ▶η : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}{σ : Sub Γ (Δ ▶ A)} → (π₁ σ ,s π₂ σ) ≡ σ

    ,∘ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)}{l}{θ : Con l}{δ : Sub θ Γ} → (_,s_ σ {A = A} t) ∘ δ ≡ ((σ ∘ δ) ,s coe (ap (Tm θ) [∘]T) (t [ δ ]t))

    Π : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▶ A) k) → Ty Γ (j ⊔ k)

    Π[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}{l}{Θ : Con l}{σ : Sub Θ Γ} →
      Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ∘ π₁ id ,s tr (Tm (Θ ▶ A [ σ ]T)) [∘]T (π₂ id) ]T)

    lam : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}(t : Tm (Γ ▶ A) B) → Tm Γ (Π A B)

    app : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}(t : Tm Γ (Π A B)) → Tm (Γ ▶ A) B

    app[] :
      ∀{i}{Γ : Con i}{l}{Δ : Con l}{σ : Sub Γ Δ}{j}{A : Ty Δ j}{k}{B : Ty (Δ ▶ A) k}{t : Tm Δ (Π A B)}
      → app t [ σ ∘ π₁ id ,s tr (Tm (Γ ▶ A [ σ ]T)) [∘]T (π₂ id) ]t ≡ app (tr (Tm Γ) Π[] (t [ σ ]t))
      
    Πβ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}{t : Tm (Γ ▶ A) B} → app (lam t) ≡ t

    Πη : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▶ A) k}{t : Tm Γ (Π A B)} → lam (app t) ≡ t

    U : ∀{i}{Γ : Con i} j → Ty Γ (lsuc j)
    U[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} {k} → U k [ σ ]T ≡ U k

    El : ∀{i}{Γ : Con i}{j}(a : Tm Γ (U j)) → Ty Γ j

    El[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}(a : Tm Δ (U k))
           → El a [ σ ]T ≡ El (tr (Tm Γ) U[] (a [ σ ]t))

    c : ∀{i}{Γ : Con i}{j}(A : Ty Γ j) → Tm Γ (U j)

    Elc : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → El (c A) ≡ A

    cEl : ∀{i}{Γ : Con i}{j}{a : Tm Γ (U j)} → c (El a) ≡ a
    
    Bool    : ∀{i}{Γ : Con i} → Ty Γ lzero
    Bool[]  : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → Bool [ σ ]T ≡ Bool
    true    : ∀{i}{Γ : Con i} → Tm Γ Bool
    true[]  : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → true [ σ ]t ≡ tr (Tm Γ) (Bool[] ⁻¹) true
    false   : ∀{i}{Γ : Con i} → Tm Γ Bool
    false[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → false [ σ ]t ≡ tr (Tm Γ) (Bool[] ⁻¹) false
  
    ite :
      ∀{i}{Γ : Con i}{j}(P : Ty (Γ ▶ Bool) j)
          → Tm Γ (P [ (id ,s tr (Tm Γ) (Bool[] ⁻¹) true) ]T)
          → Tm Γ (P [ (id ,s tr (Tm Γ) (Bool[] ⁻¹) false) ]T)
          → (t : Tm Γ Bool)
          → Tm Γ (P [ (id ,s tr (Tm Γ) (Bool[] ⁻¹) t) ]T)

    ite-true :
      ∀{i}{Γ : Con i}{j}(P : Ty (Γ ▶ Bool) j)
          → (t : Tm Γ (P [ (id ,s tr (Tm Γ) (Bool[] ⁻¹) true) ]T))
          → (f : Tm Γ (P [ (id ,s tr (Tm Γ) (Bool[] ⁻¹) false) ]T))
          → ite P t f true ≡ t

    ite-false :
      ∀{i}{Γ : Con i}{j}(P : Ty (Γ ▶ Bool) j)
          → (t : Tm Γ (P [ (id ,s tr (Tm Γ) (Bool[] ⁻¹) true) ]T))
          → (f : Tm Γ (P [ (id ,s tr (Tm Γ) (Bool[] ⁻¹) false) ]T))
          → ite P t f false ≡ f

    -- -- ugly
    -- ite[]   : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}
    --           → (P  : Ty (Δ ▶ Bool) j)
    --           → (tᴹ : Tm Δ (P [ (id ,s tr (Tm Δ) (Bool[] ⁻¹) true) ]T))
    --           → (fᴹ : Tm Δ (P [ (id ,s tr (Tm Δ) (Bool[] ⁻¹) false) ]T))
    --           → (b  : Tm Δ Bool)
    --           → coe {!!} (ite P tᴹ fᴹ b [ σ ]t) ≡ ite (coe {!(λ x → Ty (Γ ▶ x)) & ?!} (P [ σ ^ Bool ]T)) {!!} {!!} {!!}
  
  infixl 5 _▶_
  infixl 7 _[_]T
  infixl 5 _,s_
  infix  6 _∘_
  infixl 8 _[_]t
  infixl 5 _^_

  -- abbreviations

  wk : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Sub (Γ ▶ A) Γ
  wk = π₁ id

  vz : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▶ A) (A [ wk ]T)
  vz = π₂ id

  _^_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}(A : Ty Δ k) →
    Sub (Γ ▶ A [ σ ]T) (Δ ▶ A)
  _^_ {i}{Γ}{j}{Δ} σ {k} A = σ ∘ wk ,s tr (Tm (Γ ▶ A [ σ ]T)) [∘]T
                                          (vz {i}{Γ}{_}{A [ σ ]T})
