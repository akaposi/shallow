module WrappedStandard.Lib where

open import Agda.Primitive

-- wrappers

record Con' i : Set (lsuc i) where
  constructor mkC
  field
    ∣_∣C : Set i
open Con' public

record Ty' {i}(Γ : Con' i)(j : Level) : Set (i ⊔ lsuc j) where
  constructor mkT
  field
    ∣_∣T : ∣ Γ ∣C → Set j
open Ty' public

record Sub' {i}(Γ : Con' i){j}(Δ : Con' j) : Set (i ⊔ j) where
  constructor mks
  field
    ∣_∣s : ∣ Γ ∣C → ∣ Δ ∣C
open Sub' public

record Tm' {i}(Γ : Con' i){j}(A : Ty' Γ j) : Set (i ⊔ j) where
  constructor mkt
  field
    ∣_∣t : (γ : ∣ Γ ∣C) → ∣ A ∣T γ
open Tm' public

record Π' {i}{j}(A : Set i)(B : A → Set j) : Set (i ⊔ j) where
  constructor lam'
  field
    app' : ∀ x → B x
open Π' public

record K' {i}(Γ : Set i) : Set i where
  constructor mkK'
  field
    unK' : Γ
open K' public

record Wrap {i}(A : Set i) : Set i where
  constructor mk
  field
    ∣_∣ : A
open Wrap public
