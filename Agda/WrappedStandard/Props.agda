{-# OPTIONS --prop #-}

module WrappedStandard.Props where

open import Agda.Primitive
import Standard.Lib.Props as m
open import WrappedStandard.Lib
import Lib as l
open import WrappedStandard public

infixr 6 _⇒P_
infixl 7 _$P_
infixl 6 _,ΣP_

-- Prop

P : ∀{i}{Γ : Con i} j → Ty Γ (lsuc j)
P j = mkT λ _ → Prop j

ElP : ∀{i}{Γ : Con i}{j}(a : Tm Γ (P j)) → Ty Γ j
ElP a = mkT λ γ → m.ElP (∣ a ∣t γ)

P[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} {k} → U k [ σ ]T l.≡ U k
P[] = l.refl

ElP[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{a : Tm Δ (P k)}
       → ElP a [ σ ]T l.≡ ElP (a [ σ ]t)
ElP[] = l.refl

irr : ∀{i}{Γ : Con i}{j}(a : Tm Γ (P j))(u v : Tm Γ (ElP a)) → u l.≡ v
irr a u v = l.refl

⊤P : ∀{i}{Γ : Con i} → Tm Γ (P lzero)
⊤P = mkt λ _ → m.⊤P

⊤P[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ} → ⊤P [ σ ]t l.≡ ⊤P
⊤P[] = l.refl

ttP : ∀{i}{Γ : Con i} → Tm Γ (ElP ⊤P)
ttP = mkt λ _ → m.mkElP m.ttP

ttP[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ} → ttP [ σ ]t l.≡ ttP
ttP[] = l.refl

⊥P : ∀ {i}{Γ : Con i} → Tm Γ (P lzero)
⊥P = mkt λ _ → m.⊥P

⊥P[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ} → ⊥P [ σ ]t l.≡ ⊥P
⊥P[] = l.refl

exfalsoP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(t : Tm Γ (ElP ⊥P)) → Tm Γ A
exfalsoP A t = mkt λ γ → m.exfalsoP (m.unElP (∣ t ∣t γ))

exfalsoP[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ}{k}{A : Ty Γ k}{t : Tm Γ (ElP ⊥P)} →
  exfalsoP A t [ σ ]t l.≡ exfalsoP (A [ σ ]T) (t [ σ ]t)
exfalsoP[] = l.refl

ΠP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(b : Tm (Γ ▷ A) (P k)) → Tm Γ (P (j ⊔ k))
ΠP A b = mkt λ γ → (x : ∣ A ∣T γ) → (∣ b ∣t (γ m.,Σ x))

lamp : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ A) (P k)} → Tm (Γ ▷ A) (ElP b) → Tm Γ (ElP (ΠP A b))
lamp t = mkt λ γ → m.mkElP λ α → m.unElP (∣ t ∣t (γ m.,Σ α))

appp : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ A) (P k)} → Tm Γ (ElP (ΠP A b)) → Tm (Γ ▷ A) (ElP b)
appp t = mkt λ γ → m.mkElP (m.unElP (∣ t ∣t (m.fst γ)) (m.snd γ))

ΠP[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ A) (P k)}{l}{Θ : Con l}{σ : Sub Θ Γ} →
  ΠP A b [ σ ]t m.≡ ΠP (A [ σ ]T) (b [ σ ^ A ]t)
ΠP[] = m.refl

_⇒P_ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(b : Tm Γ (P k)) → Tm Γ (P (j ⊔ k))
A ⇒P b = ΠP A (b [ p ]t)

_$P_ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ A) (P k)}(t : Tm Γ (ElP (ΠP A b)))(u : Tm Γ A) → Tm Γ (ElP b [ id , u ]T)
t $P u = appp t [ id , u ]t

ΣP : ∀{i}{Γ : Con i}{j}(a : Tm Γ (P j)){k}(b : Tm (Γ ▷ ElP a) (P k)) → Tm Γ (P (j ⊔ k))
ΣP a b = mkt λ γ → m.ΣP (∣ a ∣t γ) λ α → ∣ b ∣t (γ m.,Σ m.mkElP α)

_,ΣP_ : ∀{i}{Γ : Con i}{j}{a : Tm Γ (P j)}{k}{b : Tm (Γ ▷ ElP a) (P k)}(u : Tm Γ (ElP a))(v : Tm Γ (ElP b [ id , u ]T)) → Tm Γ (ElP (ΣP a b))
u ,ΣP v = mkt λ γ → m.mkElP (m.unElP (∣ u ∣t γ) m.,ΣP m.unElP (∣ v ∣t γ))

fstP : ∀{i}{Γ : Con i}{j}{a : Tm Γ (P j)}{k}{b : Tm (Γ ▷ ElP a) (P k)}
  (t : Tm Γ (ElP (ΣP a b))) → Tm Γ (ElP a)
fstP t = mkt λ γ → m.mkElP (m.fstP (m.unElP (∣ t ∣t γ)))

sndP : ∀{i}{Γ : Con i}{j}{a : Tm Γ (P j)}{k}{b : Tm (Γ ▷ ElP a) (P k)}
  (t : Tm Γ (ElP (ΣP a b))) → Tm Γ (ElP b [ id , fstP {a = a}{b = b} t ]T)
sndP t = mkt λ γ → m.mkElP (m.sndP (m.unElP (∣ t ∣t γ)))

ΣP[] : ∀{i}{Γ : Con i}{j}{a : Tm Γ (P j)}{k}{b : Tm (Γ ▷ ElP a) (P k)}{l}{Θ : Con l}{σ : Sub Θ Γ} →
  ΣP a b [ σ ]t m.≡ ΣP (a [ σ ]t) (b [ σ ^ ElP a ]t)
ΣP[] = m.refl
