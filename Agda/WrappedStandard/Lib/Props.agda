{-# OPTIONS --no-pattern-matching --prop #-}

module WrappedStandard.Lib.Props where

open import Agda.Primitive
open import WrappedStandard.Lib public

record TyP' {i}(Γ : Con' i)(j : Level) : Set (i ⊔ lsuc j) where
  constructor mkTP
  field
    ∣_∣TP : ∣ Γ ∣C → Prop j
open TyP' public

record TmP' {i}(Γ : Con' i){j}(A : TyP' Γ j) : Prop (i ⊔ j) where
  constructor mktP
  field
    ∣_∣tP : (γ : ∣ Γ ∣C) → ∣ A ∣TP γ
open TmP' public
