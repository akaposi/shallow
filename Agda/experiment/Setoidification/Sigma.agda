{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification.Sigma where

open import Agda.Primitive
import Lib as m 
import Lib.Props as m 
import WrappedAnticatStandard.Props as S 

open import experiment.Setoidification.Category
open import experiment.Setoidification.Families
open import experiment.Setoidification.Comprehension

open import experiment.Setoidification.Sigma.Typeformer public
open import experiment.Setoidification.Sigma.Constructor public
open import experiment.Setoidification.Sigma.Eliminators public
open import experiment.Setoidification.Sigma.Equalities public
