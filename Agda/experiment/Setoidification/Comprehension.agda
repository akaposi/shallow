{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification.Comprehension where

open import Agda.Primitive
import Lib as m 
import Lib.Props as m 
import WrappedAnticatStandard.Props as S 

open import experiment.Setoidification.Category
open import experiment.Setoidification.Families

infixl 5 _▷_
infixl 5 _,_

_▷_   : {Γₛ : S.Con i}   (Γ : Con Γₛ)
        {Aₛ : S.Ty Γₛ j} (A : Ty Γ Aₛ)
        → Con (Γₛ S.▷ Aₛ)
(Γ ▷ A) = record
  { Γ~ = S.ΣP (Γ ~ S.[ S.p S.∘ S.p S., S.p S.∘ S.q ]TP) (A ~ S.[ S.p S.∘ S.p S.∘ S.pP S., S.p S.∘ S.q S.∘ S.pP S.,P S.qP S., S.q S.[ S.p S.∘ S.pP ]t S., S.q S.[ S.q S.∘ S.pP ]t ]TP)
  ; RΓ = (R Γ S.[ S.p ]tP) S.,ΣP (R A S.[ S.p S., S.q ]tP)
  }

_,_ :
  {Γₛ : S.Con i}     {Γ : Con Γₛ}
  {Δₛ : S.Con j}     {Δ : Con Δₛ}
  {σₛ : S.Sub Γₛ Δₛ} (σ : Sub Γ Δ σₛ)
  {Aₛ : S.Ty Δₛ k}   {A : Ty Δ Aₛ}
  {tₛ : S.Tm Γₛ (Aₛ S.[ σₛ ]T)} (t : Tm Γ (_[_]T {Aₛ = Aₛ} A {σₛ = σₛ} σ) tₛ)
  → Sub Γ (Δ ▷ A) (σₛ S., tₛ)
(σ , t) = σ S.,ΣP t

p :
  {Γₛ : S.Con i}   {Γ : Con Γₛ}
  {Aₛ : S.Ty Γₛ k} {A : Ty Γ Aₛ} →
  Sub (Γ ▷ A) Γ S.p
p = S.fstP S.qP

q :
  {Γₛ : S.Con i}   {Γ : Con Γₛ}
  {Aₛ : S.Ty Γₛ k} {A : Ty Γ Aₛ} →
   Tm (Γ ▷ A) (A [ p {A = A} ]T) S.q
q = S.sndP S.qP

▷β₁ :
  {Γₛ : S.Con i}                {Γ : Con Γₛ}
  {Δₛ : S.Con j}                {Δ : Con Δₛ}
  {σₛ : S.Sub Γₛ Δₛ}            {σ : Sub Γ Δ σₛ}
  {Aₛ : S.Ty Δₛ k}              {A : Ty Δ Aₛ}
  {tₛ : S.Tm Γₛ (Aₛ S.[ σₛ ]T)} {t : Tm Γ (_[_]T {Aₛ = Aₛ} A {σₛ = σₛ} σ) tₛ}
  → m._≡P_ {A = Sub Γ Δ σₛ}
          (_∘_ {Θ = _▷_ Δ {Aₛ = Aₛ} A}{Δ = Δ}
               {σₛ = S.p}(p {Aₛ = Aₛ}{A = A}) {Γ = Γ}
               {δₛ = σₛ S., tₛ}(_,_ {Γ = Γ} σ {Aₛ = Aₛ}{A = A}{tₛ = tₛ} t))
          σ
▷β₁ = m.reflP

▷β₂ :
  {Γₛ : S.Con i}                {Γ : Con Γₛ}
  {Δₛ : S.Con j}                {Δ : Con Δₛ}
  {σₛ : S.Sub Γₛ Δₛ}            {σ : Sub Γ Δ σₛ}
  {Aₛ : S.Ty Δₛ k}              {A : Ty Δ Aₛ}
  {tₛ : S.Tm Γₛ (Aₛ S.[ σₛ ]T)} {t : Tm Γ (_[_]T {Aₛ = Aₛ} A {σₛ = σₛ} σ) tₛ}
  → m._≡P_ {A = Tm Γ (_[_]T {Aₛ = Aₛ} A {σₛ = σₛ} σ) tₛ}
          (_[_]t {A = _[_]T {Aₛ = Aₛ} A {Γ = _▷_ Δ {Aₛ = Aₛ} A} {σₛ = S.p {A = Aₛ}} (p {Aₛ = Aₛ}{A = A})}
                 {tₛ = S.q {A = Aₛ}}(q {Aₛ = Aₛ}{A = A}) {Γ = Γ}
                 {σₛ = σₛ S., tₛ}(_,_ {Γ = Γ} σ {Aₛ = Aₛ}{A = A}{tₛ = tₛ} t))
          t
▷β₂ = m.reflP

▷η :
  {Γₛ : S.Con i}                {Γ : Con Γₛ}
  {Aₛ : S.Ty Γₛ k}              {A : Ty Γ Aₛ}
  → m._≡P_ {A = Sub (_▷_ Γ {Aₛ = Aₛ} A) (_▷_ Γ {Aₛ = Aₛ} A) (S.p S., S.q)}
          (_,_ {Γ = Γ ▷ A}{σₛ = S.p}(p {Aₛ = Aₛ}{A = A}){Aₛ = Aₛ}{A = A}{tₛ = S.q}(q {Aₛ = Aₛ}{A = A}))
          (id {Γ = _▷_ Γ {Aₛ = Aₛ} A})
▷η = m.reflP

,∘ :
  {Γₛ : S.Con i}                {Γ : Con Γₛ}
  {Δₛ : S.Con j}                {Δ : Con Δₛ}
  {σₛ : S.Sub Γₛ Δₛ}            {σ : Sub Γ Δ σₛ}
  {Aₛ : S.Ty Δₛ k}              {A : Ty Δ Aₛ}
  {tₛ : S.Tm Γₛ (Aₛ S.[ σₛ ]T)} {t : Tm Γ (_[_]T {Aₛ = Aₛ} A {σₛ = σₛ} σ) tₛ}
  {Θₛ : S.Con l}                {Θ : Con Θₛ}
  {δₛ : S.Sub Θₛ Γₛ}            {δ : Sub Θ Γ δₛ} →
  m._≡P_ {A = Sub Θ (_▷_ Δ {Aₛ = Aₛ} A) ((σₛ S., tₛ) S.∘ δₛ)}
        (_∘_ {Θ = Γ}{Δₛ = Δₛ S.▷ Aₛ}{Δ = Δ ▷ A}{σₛ = σₛ S., tₛ}(_,_ {Γ = Γ}{σₛ = σₛ} σ {Aₛ = Aₛ}{A = A}{tₛ = tₛ} t){Γ = Θ}{δₛ = δₛ} δ)
        (_,_ {Γ = Θ} (_∘_ {Θ = Γ}{Δ = Δ}{σₛ = σₛ} σ {Γ = Θ} {δₛ = δₛ} δ){Aₛ = Aₛ}{A = A}
             {tₛ = tₛ S.[ δₛ ]t}(_[_]t {Δ = Γ} {A = _[_]T {Aₛ = Aₛ} A {σₛ = σₛ} σ}{tₛ = tₛ} t {Γ = Θ} {σₛ = δₛ} δ))
,∘ = m.reflP
{-
_^ : 
  {Γₛ : S.Con i}                {Γ : Con Γₛ}
  {Δₛ : S.Con j}                {Δ : Con Δₛ}
  {σₛ : S.Sub Γₛ Δₛ}            (σ : Sub Γ Δ σₛ)
  {Aₛ : S.Ty Δₛ k}              {A : Ty Δ Aₛ} →
  Sub (Γ ▷ (A [ σ ]T)) (Δ ▷ A) (σₛ S.^)
-- σ ^ = {!   !}
_^ {Γₛ = Γₛ}{Γ}{Δₛ}{Δ}{σₛ} σ {Aₛ}{A} = _,_ (_∘_ {Θ = Γ} {Δ = Δ} σ {Γ = Γ ▷ (A [ σ ]T)} (p {A = {!!}})) q
-}
