{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module Setoidification.Tmp where

open import Agda.Primitive
import Lib as m
import Lib.Props as m
import WrappedAnticatStandard.Props as S

open import Setoidification.Category
open import Setoidification.Families
open import Setoidification.Comprehension
open import Setoidification.Sigma

