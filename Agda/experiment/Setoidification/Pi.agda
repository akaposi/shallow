{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification.Pi where

open import Agda.Primitive
import Lib as m 
import Lib.Props as m 
import WrappedAnticatStandard.Props as S 

open import experiment.Setoidification.Category
open import experiment.Setoidification.Families
open import experiment.Setoidification.Comprehension

Π :
  {Γₛ : S.Con i}            {Γ : Con Γₛ}
  {Aₛ : S.Ty Γₛ j}          (A : Ty Γ Aₛ)
  {Bₛ : S.Ty (Γₛ S.▷ Aₛ) k} (B : Ty (Γ ▷ A) Bₛ)
   → Ty Γ (S.Π Aₛ Bₛ)
(Π A B) = {!   !}

--                                                                     ez nem mukodik:
-- (Π A B) ~ = Π x₀:A[γ₀] Π x₁:A[γ₁] Π(A~) (B~(f₀$x₀)(f₁$x₁))           (Π A B)~ = Π (x:A) (B~ (f₀$x) (f₁$x))
--   : TyP (γ₁:Γ,γ₂:Γ,Γ~,f₀:Π A B[γ₀],f₁:Π A B[γ₁])                       TyP(Γ₀,Γ₁,Γ~,f₀:ΠA₀ B₀,f₁:ΠA₁ B₁)

-- R(Π A B) := lam (lam (lam (coe0~ B [ RB [f$x₀] ])))                                     R(Π A B) := lam (RB(f$x))
--   : TmP (γ:Γ,f:Π A B, x₀:A , x₁:A , A~) (B~(f$x₀)(f$x₁))              TmP (γ:Γ,f:Π A B,x:A) (B~(f$x)(f$x))

-- coe(B~(f$x₀)(f$_)) : B~(f$x₀)(f$x₀) -> B~(f$x₀)(f$x₁)               coe0(Π A B) := lam (coe0 B (Γ~,coh1 A x₁) (f$(coe1 A x₁)))
--                                                                       Tm(Γ₀,Γ₁,Γ~,f:ΠA₀ B₀,x₁:A₁) B₁
--                                                                     coh0(Π A B) := 
--                                                                       Tm(Γ₀,Γ₁,Γ~,f:ΠA₀ B₀,x:) ((Π A B)~ f (coe0(Π A B)f))


--                                                                     coe1(Π A B) := lam (coe1 B (f$(coe0 A x₀)))


-- Ty A :=
--   A~
--   RA
--   coe0A  : Tm(Γ₀,Γ₁,Γ~,A₀) A₁
--   ...
--   coe0A~ : TmP(Γ₀₀,Γ₁₀,Γ~₀,A₀₀,A₁₀,Γ₀₁,Γ₁₁,Γ~₁,A₀₁,A₁₁,Γ₀~,Γ₁~,_,A₀~,A₁~ ,A~₀) A~₁      = coe0B, ahol B=(A~)
--                \_________________/ \_________________/ \_______________/
--                  Δ₀                  Δ₁                  Δ~                B₀   B₁
--   coe1A~

-- A₀ --> B₀
-- ^      | 
-- |      v
-- A₁ --> B₁

-- funext : ((x:A) → f x = g x) → (f ≡ g)
-- funext : ((x y : A)(e : x = y) → transport B e (f x) = (g y)) → f = g
