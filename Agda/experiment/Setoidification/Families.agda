{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification.Families where

open import Agda.Primitive
import Lib as m 
import Lib.Props as m 
import WrappedAnticatStandard.Props as S 

open import experiment.Setoidification.Category

infixl 7 _[_]T
infixl 8 _[_]t

record Ty {Γₛ : S.Con j}(Γ : Con Γₛ)(Aₛ : S.Ty Γₛ i) : Set (lsuc i ⊔ j) where
  field
    -- A~ : (γ γ' : Γₛ) → Γ~ γ γ' → Aₛ γ → Aₛ γ' → Prop
    A~    : S.TyP (Γₛ S.▷ Γₛ S.[ S.ε ]T S.▷P Γ ~ S.▷ Aₛ S.[ S.p S.∘ S.pP ]T S.▷ Aₛ S.[ S.q S.[ S.pP S.∘ S.p ]t ]T) i

    -- RA : (γ : Γ)(α : A γ) → A~ γ γ (RΓ γ) α α
    RA    : S.TmP (Γₛ S.▷ Aₛ) (A~ S.[ S.p S., S.p S.,P R Γ S.[ S.p ]tP S., S.q S., S.q ]TP)

    -- coe0A : (γ γ' : Γₛ) → Γ~ γ γ' → Aₛ γ → Aₛ γ'
    coe0A : S.Tm (Γₛ S.▷ Γₛ S.[ S.ε ]T S.▷P Γ ~ S.▷ Aₛ S.[ S.p S.∘ S.pP ]T) (Aₛ S.[ S.q S.[ S.pP S.∘ S.p ]t ]T)

    -- coh0A : (γ γ' : Γₛ)(γ~ : Γ~ γ γ')(α : Aₛ γ) → A~ γ γ' γ~ α (coe0A γ γ' γ~ α)
    coh0A : S.TmP (Γₛ S.▷ Γₛ S.[ S.ε ]T S.▷P Γ ~ S.▷ Aₛ S.[ S.p S.∘ S.pP ]T) (A~ S.[ S.id S., coe0A ]TP)

    -- coe1A : (γ γ' : Γₛ) → Γ~ γ γ' → Aₛ γ' → Aₛ γ
    coe1A : S.Tm (Γₛ S.▷ Γₛ S.[ S.ε ]T S.▷P Γ ~ S.▷ Aₛ S.[ S.q S.[ S.pP ]t ]T) (Aₛ S.[ S.p S.∘ S.pP S.∘ S.p ]T)

    -- coh1A : (γ γ' : Γₛ)(γ~ : Γ~ γ γ')(α : Aₛ γ') → A~ γ γ' γ~ (coe1A γ γ' γ~ α) α
    coh1A : S.TmP (Γₛ S.▷ Γₛ S.[ S.ε ]T S.▷P Γ ~ S.▷ Aₛ S.[ S.q S.[ S.pP ]t ]T) (A~ S.[ S.p S., coe1A S., S.q ]TP)

open Ty renaming
  ( A~ to _~
  ; RA to R_
  ; coe0A to coe0
  ; coh0A to coh0
  ; coe1A to coe1
  ; coh1A to coh1
  ) public

    -- coe0A~ : TmP(Γ₀₀,Γ₁₀,Γ~₀,A₀₀,A₁₀,Γ₀₁,Γ₁₁,Γ~₁,A₀₁,A₁₁,Γ₀~,Γ₁~,_,A₀~,A₁~ ,A~₀) A~₁ 
    -- coe0A~ :
        --  TmP (∙ ▷ 
        --    line Γ ▷ (a₀₀ : A [start q]) ▷ A [end q[p]] ▷
        --    line Γ ▷ (a₀₁ : A [start q]) ▷ A [end q[p]] ▷
        --    Γ ~ [start q[ppppppppp], start q[pppp]] 
        --    Γ ~ [end q[ppppppppp], end q[pppp]] 
        --    A ~ [a₀₀ , a₀₁]
        --    A ~ [a₁₀ , a₁₁]
        --    A ~ [a₀₀ , a₁₀]
        --  )
        --    A ~ [a₀₁ , a₁₁]
    -- coe0~ :
    --   {Γₛ₀ : S.Con i}    {Γ₀ : Con Γₛ₀}
    --   {Γₛ₁ : S.Con j}    {Γ₁ : Con Γₛ₁}
    --   {Aₛ₀ : S.Ty Γₛ₀ k} {A₀ : Ty Γ₀ Aₛ₀}
    --   {Aₛ₁ : S.Ty Γₛ₁ l} {A₁ : Ty Γ₁ Aₛ₁} →
    --   -- TmP(
    --   --   Γ₀₀,Γ₁₀,Γ~₀,A₀₀,A₁₀,
    --   --   Γ₀₁,Γ₁₁,Γ~₁,A₀₁,A₁₁,
    --   --   Γ₀~,Γ₁~,_,A₀~,A₁~,
    --   --   A~₀
    --   -- ) A~₁ 
    --   S.TmP (
    --     Γₛ₀ S.▷ Γₛ₀ S.[ S.ε ]T S.▷P Γ₀ ~ S.▷ Aₛ₀ S.[ S.p S.∘ S.pP ]T S.▷ Aₛ₀ S.[ S.p S.∘ S.pP S.∘ S.p ]T S.▷
    --     Γₛ₁ S.[ S.ε ]T S.▷ Γₛ₁ S.[ S.ε ]T S.▷P Γ₁ ~ S.▷ Aₛ₁ S.[ S.p S.∘ S.pP ]T S.▷ Aₛ₁ S.[ S.p S.∘ S.pP S.∘ S.p ]T S.▷
    --     Γ₀ ~ S.[ ? ]TP S.▷ Γ₁ ~ S.[ ? ]TP S.▷P Γ₁ ~ S.▷ A₀ ~ S.[ ? ]TP
    --     )
    --     -- (A₁ ~ S.[ ? ]TP)
    --     ?
    --     -- (A₁ ~ S.[ S.id S., ? ]TP)
    -- coe0~ = {!   !}


Tm : {Γₛ : S.Con i}(Γ : Con Γₛ){Aₛ : S.Ty Γₛ j}(A : Ty Γ Aₛ) → S.Tm Γₛ Aₛ → Prop (i ⊔ j)
Tm {Γₛ = Γₛ} Γ {Aₛ = Aₛ} A tₛ = S.TmP (Γₛ S.▷ Γₛ S.[ S.ε ]T S.▷P Γ ~) (A ~ S.[ S.id S., tₛ S.[ S.p S.∘ S.pP ]t S., tₛ S.[ S.q S.∘ S.pP ]t ]TP)

_[_]T :
  {Δₛ : S.Con i}     {Δ : Con Δₛ}
  {Aₛ : S.Ty Δₛ j}   (A : Ty Δ Aₛ)
  {Γₛ : S.Con k}     {Γ : Con Γₛ}
  {σₛ : S.Sub Γₛ Δₛ} (σ : Sub Γ Δ σₛ)
  → Ty Γ (Aₛ S.[ σₛ ]T)
_[_]T A {σₛ = σₛ} σ = record
  { A~ = A ~ S.[ σₛ S.[ S.p S.∘ S.pP S.∘ S.p² ]t S., σₛ S.[ S.q S.∘ S.pP S.∘ S.p² ]t S.,P σ S.[ S.p² ]tP S., S.q S.[ S.p ]t S., S.q ]TP
  ; RA = R A S.[ σₛ S.[ S.p ]t S., S.q ]tP
  ; coe0A = coe0 A S.[ σₛ S.[ S.p S.∘ S.pP S.∘ S.p ]t S., σₛ S.[ S.q S.∘ S.pP S.∘ S.p ]t S.,P σ S.[ S.p ]tP S., S.q ]t
  ; coh0A = coh0 A S.[ σₛ S.[ S.p S.∘ S.pP S.∘ S.p ]t S., σₛ S.[ S.q S.∘ S.pP S.∘ S.p ]t S.,P σ S.[ S.p ]tP S., S.q ]tP
  ; coe1A = coe1 A S.[ σₛ S.[ S.p S.∘ S.pP S.∘ S.p ]t S., σₛ S.[ S.q S.∘ S.pP S.∘ S.p ]t S.,P σ S.[ S.p ]tP S., S.q ]t
  ; coh1A = coh1 A S.[ σₛ S.[ S.p S.∘ S.pP S.∘ S.p ]t S., σₛ S.[ S.q S.∘ S.pP S.∘ S.p ]t S.,P σ S.[ S.p ]tP S., S.q ]tP
  }

_[_]t :
  {Δₛ : S.Con i}     {Δ : Con Δₛ}
  {Aₛ : S.Ty Δₛ j}   {A : Ty Δ Aₛ}
  {tₛ : S.Tm Δₛ Aₛ}  (t : Tm Δ A tₛ)
  {Γₛ : S.Con k}     {Γ : Con Γₛ}
  {σₛ : S.Sub Γₛ Δₛ} (σ : Sub Γ Δ σₛ)
  → Tm Γ (A [ σ ]T) (tₛ S.[ σₛ ]t)
_[_]t t {σₛ = σₛ} σ = t S.[ σₛ S.[ S.p S.∘ S.pP ]t S., σₛ S.[ S.q S.∘ S.pP ]t S.,P σ ]tP

[id]T :
  ∀{i}{Γₛ : S.Con i}   {Γ : Con Γₛ}
   {j}{Aₛ : S.Ty Γₛ j} {A : Ty Γ Aₛ}
  → m._≡_ {A = Ty Γ Aₛ} (_[_]T {Aₛ = Aₛ} A {σₛ = S.id} (id {Γ = Γ})) A
[id]T = m.refl

[∘]T :
  {Θₛ : S.Con i}     {Θ : Con Θₛ}
  {Δₛ : S.Con j}     {Δ : Con Δₛ}
  {σₛ : S.Sub Θₛ Δₛ} {σ : Sub Θ Δ σₛ}
  {Γₛ : S.Con k}     {Γ : Con Γₛ}
  {δₛ : S.Sub Γₛ Θₛ} {δ : Sub Γ Θ δₛ}
  {Aₛ : S.Ty Δₛ l}   {A : Ty Δ Aₛ}
   → m._≡_ {A = Ty Γ (Aₛ S.[ σₛ S.∘ δₛ ]T)}
          (_[_]T {Δ = Θ} {Aₛ = Aₛ S.[ σₛ ]T} (_[_]T {Aₛ = Aₛ} A {σₛ = σₛ} σ) {Γ = Γ} {σₛ = δₛ} δ)
          (_[_]T {Aₛ = Aₛ} A {σₛ = σₛ S.∘ δₛ} (_∘_ {Θ = Θ} {Δ = Δ}{σₛ = σₛ} σ {Γ = Γ} {δₛ = δₛ} δ))
[∘]T = m.refl

[id]t :
  {Γₛ : S.Con i}   {Γ : Con Γₛ}
  {Aₛ : S.Ty Γₛ j} {A : Ty Γ Aₛ}
  {tₛ : S.Tm Γₛ Aₛ}{t : Tm Γ A tₛ}
  → m._≡P_ {A = Tm Γ A tₛ}
          (_[_]t {A = A} {tₛ = tₛ} t {Γ = Γ} {σₛ = S.id} (id {Γ = Γ}))
          t
[id]t = m.reflP

[∘]t :
  {Θₛ : S.Con i}     {Θ : Con Θₛ}
  {Δₛ : S.Con j}     {Δ : Con Δₛ}
  {σₛ : S.Sub Θₛ Δₛ} {σ : Sub Θ Δ σₛ}
  {Γₛ : S.Con k}     {Γ : Con Γₛ}
  {δₛ : S.Sub Γₛ Θₛ} {δ : Sub Γ Θ δₛ}
  {Aₛ : S.Ty Δₛ l}   {A : Ty Δ Aₛ}
  {tₛ : S.Tm Δₛ Aₛ}{t : Tm Δ A tₛ}
   → m._≡P_ {A = Tm Γ (_[_]T {Δ = Θ} {Aₛ = Aₛ S.[ σₛ ]T} (_[_]T {Aₛ = Aₛ} A {σₛ = σₛ} σ) {σₛ = δₛ} δ) (tₛ S.[ σₛ ]t S.[ δₛ ]t)}
          (_[_]t {Δ = Θ} {Aₛ = Aₛ S.[ σₛ ]T}{A = _[_]T {Aₛ = Aₛ} A {σₛ = σₛ} σ}
                 {tₛ = tₛ S.[ σₛ ]t}(_[_]t {Aₛ = Aₛ} {A = A} {tₛ = tₛ} t {Γ = Θ} {σₛ = σₛ} σ) {Γ = Γ}
                 {σₛ = δₛ} δ)
          (_[_]t {Aₛ = Aₛ}{A = A}
                 {tₛ = tₛ} t {Γ = Γ}
                 {σₛ = σₛ S.∘ δₛ} (_∘_ {Θ = Θ} {Δ = Δ}{σₛ = σₛ} σ {Γ = Γ} {δₛ = δₛ} δ))
[∘]t = m.reflP
