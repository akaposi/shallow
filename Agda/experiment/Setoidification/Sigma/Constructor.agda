{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification.Sigma.Constructor where

open import Agda.Primitive
import Lib as m 
import Lib.Props as m 
import WrappedAnticatStandard.Props as S 

open import experiment.Setoidification.Category
open import experiment.Setoidification.Families
open import experiment.Setoidification.Comprehension

open import experiment.Setoidification.Sigma.Typeformer

_,Σ_ :
  {Γₛ : S.Con i}            {Γ : Con Γₛ}
  {Aₛ : S.Ty Γₛ j}          {A : Ty Γ Aₛ}
  {Bₛ : S.Ty (Γₛ S.▷ Aₛ) k} {B : Ty (Γ ▷ A) Bₛ}
  {uₛ : S.Tm Γₛ Aₛ}(u : Tm Γ A uₛ)
  {vₛ : S.Tm Γₛ (Bₛ S.[ S.id S., uₛ ]T)}
  (v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aₛ = Bₛ} B {σₛ = S.id S., uₛ}(_,_ {Γ = Γ} {σₛ = S.id} (id {Γ = Γ}) {A = A}{tₛ = uₛ} u)) vₛ)
  → Tm Γ (Σ {Γₛ = Γₛ}{Γ = Γ}{Aₛ = Aₛ} A {Bₛ = Bₛ} B) (S._,Σ_ {B = Bₛ} uₛ vₛ)
(u ,Σ v) = u S.,ΣP v
