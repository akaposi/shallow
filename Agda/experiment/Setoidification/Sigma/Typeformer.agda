{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification.Sigma.Typeformer where

open import Agda.Primitive
import Lib as m 
import Lib.Props as m 
import WrappedAnticatStandard.Props as S 

open import experiment.Setoidification.Category
open import experiment.Setoidification.Families
open import experiment.Setoidification.Comprehension

Σ :
  {Γₛ : S.Con i}            {Γ : Con Γₛ}
  {Aₛ : S.Ty Γₛ j}          (A : Ty Γ Aₛ)
  {Bₛ : S.Ty (Γₛ S.▷ Aₛ) k} (B : Ty (Γ ▷ A) Bₛ)
   → Ty Γ (S.Σ Aₛ Bₛ)
(Σ A B) = record
  { A~ = S.ΣP ((A ~) S.[ S.p² S., S.fst (S.q S.[ S.p ]t) S., S.fst S.q ]TP) ((B ~) S.[ (S.p S.∘ S.pP S.∘ S.p² S.∘ S.pP) S.,Σ (S.fst (S.q S.[ S.p S.∘ S.pP ]t )) S., (S.q S.[ S.pP S.∘ S.p² S.∘ S.pP ]t) S.,Σ (S.fst (S.q S.[ S.pP ]t )) S.,P ( (S.qP S.[ S.p² S.∘ S.pP ]tP) S.,ΣP S.qP ) S., S.snd (S.q S.[ S.p S.∘ S.pP ]t) S., S.snd S.q S.[ S.pP ]t ]TP) 
  ; RA = ((R A) S.[ S.p S., S.fst S.q ]tP) S.,ΣP ((R B) S.[ S.p S., S.fst S.q S., S.snd S.q ]tP) 
  ; coe0A = ((coe0 A) S.[ S.p S., S.fst S.q ]t)  S.,Σ  ((coe0 B) S.[ (S.p S.∘ S.pP S.∘ S.p) S.,Σ S.fst S.q S., S.q S.[ S.pP S.∘ S.p ]t S.,Σ (coe0 A) S.[ S.p S., S.fst S.q ]t S.,P (S.qP S.[ S.p ]tP) S.,ΣP (coh0 A) S.[ S.p S., S.fst S.q ]tP S., S.snd S.q ]t)
  ; coh0A = ((coh0 A) S.[ S.p S., S.fst S.q ]tP) S.,ΣP ((coh0 B) S.[ (S.p S.∘ S.pP S.∘ S.p) S.,Σ S.fst S.q S., S.q S.[ S.pP S.∘ S.p ]t S.,Σ (coe0 A) S.[ S.p S., S.fst S.q ]t S.,P (S.qP S.[ S.p ]tP) S.,ΣP (coh0 A) S.[ S.p S., S.fst S.q ]tP S., S.snd S.q ]tP)
  ; coe1A = ((coe1 A) S.[ S.p S., S.fst S.q ]t)  S.,Σ  ((coe1 B) S.[ (S.p S.∘ S.pP S.∘ S.p) S.,Σ (coe1 A) S.[ S.p S., S.fst S.q ]t S., S.q S.[ S.pP S.∘ S.p ]t S.,Σ S.fst S.q S.,P (S.qP S.[ S.p ]tP) S.,ΣP (coh1 A) S.[ S.p S., S.fst S.q ]tP S., S.snd S.q ]t)
  ; coh1A = ((coh1 A) S.[ S.p S., S.fst S.q ]tP) S.,ΣP ((coh1 B) S.[ (S.p S.∘ S.pP S.∘ S.p) S.,Σ (coe1 A) S.[ S.p S., S.fst S.q ]t S., S.q S.[ S.pP S.∘ S.p ]t S.,Σ S.fst S.q S.,P (S.qP S.[ S.p ]tP) S.,ΣP (coh1 A) S.[ S.p S., S.fst S.q ]tP S., S.snd S.q ]tP)
  }
