{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification.Sigma.Equalities where

open import Agda.Primitive
import Lib as m
import Lib.Props as m
import WrappedAnticatStandard.Props as S

open import experiment.Setoidification.Category
open import experiment.Setoidification.Families
open import experiment.Setoidification.Comprehension

open import experiment.Setoidification.Sigma.Typeformer
open import experiment.Setoidification.Sigma.Constructor
open import experiment.Setoidification.Sigma.Eliminators

Σβ₁ :
  {Γₛ : S.Con i}            {Γ : Con Γₛ}
  {Aₛ : S.Ty Γₛ j}          {A : Ty Γ Aₛ}
  {Bₛ : S.Ty (Γₛ S.▷ Aₛ) k} {B : Ty (Γ ▷ A) Bₛ}
  {uₛ : S.Tm Γₛ Aₛ}{u : Tm Γ A uₛ}
  {vₛ : S.Tm Γₛ (Bₛ S.[ S.id S., uₛ ]T)}
  {v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aₛ = Bₛ} B {σₛ = S.id S., uₛ}(_,_ {Γ = Γ} {σₛ = S.id} (id {Γ = Γ}) {A = A}{tₛ = uₛ} u)) vₛ} →
  m._≡P_ {A = Tm Γ A uₛ}
        (fst {Aₛ = Aₛ}{A = A}{Bₛ = Bₛ}{B = B}{tₛ = uₛ S.,Σ vₛ}
             (_,Σ_ {Aₛ = Aₛ}{A = A}{Bₛ = Bₛ}{B = B}{uₛ = uₛ} u {vₛ = vₛ} v))
        u
Σβ₁ = m.reflP

Σβ₂ :
  ∀{i}{Γₛ : S.Con i}            {Γ : Con Γₛ}
   {j}{Aₛ : S.Ty Γₛ j}          {A : Ty Γ Aₛ}
   {k}{Bₛ : S.Ty (Γₛ S.▷ Aₛ) k} {B : Ty (Γ ▷ A) Bₛ}
      {uₛ : S.Tm Γₛ Aₛ}{u : Tm Γ A uₛ}
      {vₛ : S.Tm Γₛ (Bₛ S.[ S.id S., uₛ ]T)}
      {v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aₛ = Bₛ} B {σₛ = S.id S., uₛ}(_,_ {Γ = Γ} {σₛ = S.id} (id {Γ = Γ}) {A = A}{tₛ = uₛ} u)) vₛ} →
  m._≡P_ {A = Tm Γ (_[_]T {Δ = Γ ▷ A}{Aₛ = Bₛ} B {σₛ = S.id S., uₛ}(_,_ {Γ = Γ} {σₛ = S.id} (id {Γ = Γ}) {A = A}{tₛ = uₛ} u)) vₛ}
        (snd {Aₛ = Aₛ}{A = A}{Bₛ = Bₛ}{B = B}{tₛ = uₛ S.,Σ vₛ}
             (_,Σ_ {Aₛ = Aₛ}{A = A}{Bₛ = Bₛ}{B = B}{uₛ = uₛ} u {vₛ = vₛ} v))
        v
Σβ₂ = m.reflP

Ση :
  ∀{i}{Γₛ : S.Con i}            {Γ : Con Γₛ}
   {j}{Aₛ : S.Ty Γₛ j}          {A : Ty Γ Aₛ}
   {k}{Bₛ : S.Ty (Γₛ S.▷ Aₛ) k} {B : Ty (Γ ▷ A) Bₛ}
      {tₛ : S.Tm Γₛ (S.Σ Aₛ Bₛ)}{t : Tm Γ (Σ {Γₛ = Γₛ}{Γ = Γ}{Aₛ = Aₛ} A {Bₛ = Bₛ} B) tₛ} →
  m._≡P_ {A = Tm Γ (Σ {Γₛ = Γₛ}{Γ = Γ}{Aₛ = Aₛ} A {Bₛ = Bₛ} B) tₛ}
        (_,Σ_ {Aₛ = Aₛ}{A = A}{Bₛ = Bₛ}{B = B}
              {uₛ = S.fst tₛ}(fst {Aₛ = Aₛ}{A = A}{Bₛ = Bₛ}{B = B}{tₛ = tₛ} t)
              {vₛ = S.snd tₛ}(snd {Aₛ = Aₛ}{A = A}{Bₛ = Bₛ}{B = B}{tₛ = tₛ} t))
        t
Ση = m.reflP

-- Σ[] :
--   ∀{i}{Γₛ : S.Con i}           {Γ : Con Γₛ}
--    {j}{Aₛ : S.Ty Γₛ j}         {A : Ty Γ Aₛ}
--    {k}{Bₛ : S.Ty (Γₛ S.▷ Aₛ) k}{B : Ty (Γ ▷ A) Bₛ}
--    {l}{Θₛ : S.Con l}           {Θ : Con Θₛ}
--       {σₛ : S.Sub Θₛ Γₛ}       {σ : Sub Θ Γ σₛ}
--   → m._≡_ {A = Ty Θ (S.Σ Aₛ Bₛ S.[ σₛ ]T)}
--          (_[_]T {Δ = Γ}{Aₛ = S.Σ Aₛ Bₛ}(Σ A {Bₛ = Bₛ} B){σₛ = σₛ} σ)
--          (Σ (_[_]T {Aₛ = Aₛ} A {σₛ = σₛ} σ)
--             {Bₛ = Bₛ S.[ σₛ S.^ ]T}
--             -- (_[_]T {Aₛ = Bₛ} B {σₛ = σₛ S.^ }(_^_ {σₛ = σₛ} σ {Aₛ = Aₛ} A)))
--             -- (_[_]T {Aₛ = Bₛ} B {σₛ = σₛ S.^ }(σ ∘ p , q)))
-- Σ[] = m.refl

-- ,Σ[] :
--   ∀{i}{Γₛ : S.Con i}            {Γ : Con Γₛ}
--    {j}{Aₛ : S.Ty Γₛ j}          {A : Ty Γ Aₛ}
--    {k}{Bₛ : S.Ty (Γₛ S.▷ Aₛ) k} {B : Ty (Γ ▷ A) Bₛ}
--       {uₛ : S.Tm Γₛ Aₛ}{u : Tm Γ A uₛ}
--       {vₛ : S.Tm Γₛ (Bₛ S.[ S.id S., uₛ ]T)}
--       {v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aₛ = Bₛ} B {σₛ = S.id S., uₛ}(_,_ {σₛ = S.id} (id {Γ = Γ}) {A = A}{tₛ = uₛ} u)) vₛ}
--    {l}{Θₛ : S.Con l}            {Θ : Con Θₛ}
--       {σₛ : S.Sub Θₛ Γₛ}        {σ : Sub Θ Γ σₛ} →
--   m._≡_ {A = Tm Θ
--                 {Aₛ = S.Σ Aₛ Bₛ S.[ σₛ ]T}
--                 (_[_]T {Aₛ = S.Σ Aₛ Bₛ} (Σ {Aₛ = Aₛ} A {Bₛ = Bₛ} B) {σₛ = σₛ} σ)
--                 ((S._,Σ_ {A = Aₛ}{B = Bₛ} uₛ vₛ) S.[ σₛ ]t)}
--         (_[_]t {Aₛ = S.Σ Aₛ Bₛ}{A = Σ {Aₛ = Aₛ} A {Bₛ = Bₛ} B}{tₛ = S._,Σ_ {A = Aₛ}{B = Bₛ} uₛ vₛ}(_,Σ_ {Aₛ = Aₛ}{A = A}{Bₛ = Bₛ}{B = B}{uₛ = uₛ} u {vₛ = vₛ} v) {σₛ = σₛ} σ)
--         (_,Σ_ {Aₛ = Aₛ S.[ σₛ ]T}
--               {A = _[_]T {Aₛ = Aₛ} A {σₛ = σₛ} σ}
--               {Bₛ = Bₛ S.[ σₛ S.^ Aₛ ]T}
--               {B = _[_]T {Aₛ = Bₛ} B {σₛ = σₛ S.^ Aₛ} (_^_ {σₛ = σₛ} σ {Aₛ = Aₛ} A)}
--               {uₛ = uₛ S.[ σₛ ]t}
--               (_[_]t {Aₛ = Aₛ}{A = A}{tₛ = uₛ} u {σₛ = σₛ} σ)
--               {vₛ = vₛ S.[ σₛ ]t}
--               (_[_]t {Aₛ = Bₛ S.[ S._,_ S.id {A = Aₛ} uₛ ]T}{A = _[_]T {Aₛ = Bₛ} B {σₛ = S.id S., uₛ} (_,_ (id {Γₛ = Γₛ}{Γ = Γ}){Aₛ = Aₛ}{A = A}{tₛ = uₛ} u)}{tₛ = vₛ} v {σₛ = σₛ} σ))
-- ,Σ[] = m.refl
