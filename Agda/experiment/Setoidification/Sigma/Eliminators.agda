{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification.Sigma.Eliminators where

open import Agda.Primitive
import Lib as m 
import Lib.Props as m 
import WrappedAnticatStandard.Props as S 

open import experiment.Setoidification.Category
open import experiment.Setoidification.Families
open import experiment.Setoidification.Comprehension

open import experiment.Setoidification.Sigma.Typeformer
open import experiment.Setoidification.Sigma.Constructor

fst :
  {Γₛ : S.Con i}            {Γ : Con Γₛ}
  {Aₛ : S.Ty Γₛ j}          {A : Ty Γ Aₛ}
  {Bₛ : S.Ty (Γₛ S.▷ Aₛ) k} {B : Ty (Γ ▷ A) Bₛ}
  {tₛ : S.Tm Γₛ (S.Σ Aₛ Bₛ)}(t : Tm Γ (Σ {Γₛ = Γₛ}{Γ = Γ}{Aₛ = Aₛ} A {Bₛ = Bₛ} B) tₛ)
  → Tm Γ A (S.fst tₛ)
(fst t) = S.fstP t

snd :
  {Γₛ : S.Con i}            {Γ : Con Γₛ}
  {Aₛ : S.Ty Γₛ j}          {A : Ty Γ Aₛ}
  {Bₛ : S.Ty (Γₛ S.▷ Aₛ) k} {B : Ty (Γ ▷ A) Bₛ}
  {tₛ : S.Tm Γₛ (S.Σ Aₛ Bₛ)}(t : Tm Γ (Σ {Γₛ = Γₛ}{Γ = Γ}{Aₛ = Aₛ} A {Bₛ = Bₛ} B) tₛ)
  → Tm Γ (_[_]T {Δ = Γ ▷ A}
                 {Aₛ = Bₛ}
                 B {σₛ = S.id S., S.fst tₛ}
                 (_,_ {Γ = Γ} {σₛ = S.id} (id {Γ = Γ}) {A = A}{tₛ = S.fst tₛ} (fst {A = A}{Bₛ = Bₛ}{B = B}{tₛ = tₛ} t)))
          (S.snd tₛ)
(snd t) = S.sndP t
