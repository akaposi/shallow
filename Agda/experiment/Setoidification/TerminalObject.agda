{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification.TerminalObject where

open import Agda.Primitive public
import Lib as m 
import Lib.Props as m 
import WrappedAnticatStandard.Props as S 

open import experiment.Setoidification.Category

∙ : Con S.∙
∙ = record { Γ~ = S.⊤P ; RΓ = S.ttP }

ε : {Γₛ : S.Con i}{Γ : Con Γₛ} → Sub Γ ∙ S.ε
ε = S.ttP

•η :
   {Γₛ : S.Con i}      {Γ : Con Γₛ}
   {σₛ : S.Sub Γₛ S.∙} {σ : Sub Γ ∙ σₛ}
   → m._≡P_ {A = Sub Γ ∙ σₛ} σ (ε {Γ = Γ})
•η = m.reflP
