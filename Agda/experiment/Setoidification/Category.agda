{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification.Category where

open import Agda.Primitive
import Lib as m 
import Lib.Props as m 
import WrappedAnticatStandard.Props as S 

variable
  i j k l : Level

infixr 6 _∘_

record Con (Γₛ : S.Con i) : Set (lsuc i) where
  field
    Γ~ : S.TyP (Γₛ S.▷ Γₛ S.[ S.ε ]T) i              -- Γ~ : Γ → Γ → Prop
    RΓ : S.TmP Γₛ (Γ~ S.[ S.id S., S.id ]TP)         -- RΓ : (γ : Γ) → Γ~ γ γ
open Con renaming (Γ~ to _~; RΓ to R_) public

Sub : {Γₛ : S.Con i}(Γ : Con Γₛ){Δₛ : S.Con j}(Δ : Con Δₛ) → S.Sub Γₛ Δₛ → Prop (i ⊔ j)
Sub {Γₛ = Γₛ} Γ {Δₛ = Δₛ} Δ σₛ = S.TmP (Γₛ S.▷ Γₛ S.[ S.ε ]T S.▷P Γ ~) (Δ ~ S.[ σₛ S.∘ S.p S.∘ S.pP S., σₛ S.∘ S.q S.∘ S.pP ]TP)

_∘_ :
  {Θₛ : S.Con i}     {Θ : Con Θₛ}
  {Δₛ : S.Con j}     {Δ : Con Δₛ}
  {σₛ : S.Sub Θₛ Δₛ} (σ : Sub Θ Δ σₛ)
  {Γₛ : S.Con k}     {Γ : Con Γₛ}
  {δₛ : S.Sub Γₛ Θₛ} (δ : Sub Γ Θ δₛ)
  → Sub Γ Δ (σₛ S.∘ δₛ)
_∘_ σ {δₛ = δₛ} δ = σ S.[ δₛ S.∘ S.p S.∘ S.pP S., δₛ S.∘ S.q S.∘ S.pP S.,P δ ]tP

id : {Γₛ : S.Con i}{Γ : Con Γₛ} → Sub Γ Γ S.id
id = S.qP

ass :
  {Θₛ : S.Con i}     {Θ : Con Θₛ}
  {Δₛ : S.Con j}     {Δ : Con Δₛ}
  {σₛ : S.Sub Θₛ Δₛ} {σ : Sub Θ Δ σₛ}
  {Ξₛ : S.Con k}     {Ξ : Con Ξₛ}
  {δₛ : S.Sub Ξₛ Θₛ} {δ : Sub Ξ Θ δₛ}
  {Γₛ : S.Con l}     {Γ : Con Γₛ}
  {νₛ : S.Sub Γₛ Ξₛ} {ν : Sub Γ Ξ νₛ}
  → m._≡P_ {A = Sub Γ Δ ((σₛ S.∘ δₛ) S.∘ νₛ)}
           (_∘_ {Θ = Ξ}{Δ = Δ}{σₛ = σₛ S.∘ δₛ}(_∘_ {Θ = Θ}{Δ = Δ}{σₛ = σₛ} σ {Γ = Ξ} {δₛ = δₛ} δ) {Γ = Γ} {δₛ = νₛ} ν)
           (_∘_ {Θ = Θ}{Δ = Δ}{σₛ = σₛ} σ {Γ = Γ} {δₛ = δₛ S.∘ νₛ}(_∘_ {Θ = Ξ}{Δ = Θ}{σₛ = δₛ} δ {Γ = Γ} {δₛ = νₛ} ν))
ass = m.reflP

idl :
  {Γₛ : S.Con i}    {Γ : Con Γₛ}
  {Δₛ : S.Con j}    {Δ : Con Δₛ}
  {σₛ : S.Sub Γₛ Δₛ} {σ : Sub Γ Δ σₛ}
  → m._≡P_ {A = Sub Γ Δ σₛ} (_∘_ {Θ = Δ}{Δ = Δ}{σₛ = S.id}(id {Γ = Δ}){Γ = Γ}{δₛ = σₛ} σ) σ
idl = m.reflP

idr :
  {Γₛ : S.Con i}     {Γ : Con Γₛ}
  {Δₛ : S.Con j}     {Δ : Con Δₛ}
  {σₛ : S.Sub Γₛ Δₛ}  {σ : Sub Γ Δ σₛ}
  → m._≡P_ {A = Sub Γ Δ σₛ} (_∘_ {Θ = Γ}{Δ = Δ}{σₛ = σₛ} σ {Γ = Γ} {δₛ = S.id} (id {Γₛ = Γₛ}{Γ = Γ})) σ
idr = m.reflP
