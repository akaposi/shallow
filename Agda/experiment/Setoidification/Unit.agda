{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification.Unit where

open import Agda.Primitive
import Lib as m 
import Lib.Props as m 
import WrappedAnticatStandard.Props as S 

open import experiment.Setoidification.Category
open import experiment.Setoidification.Families

⊤ : {Γₛ : S.Con i}{Γ : Con Γₛ} → Ty Γ S.⊤
⊤ = record
  { A~ = S.⊤P 
  ; RA = S.ttP 
  ; coe0A = S.tt 
  ; coh0A = S.ttP 
  ; coe1A = S.tt 
  ; coh1A = S.ttP
  }

tt : {Γₛ : S.Con i}{Γ : Con Γₛ} → Tm Γ ⊤ S.tt
tt = S.ttP

⊤η : ∀{i}{Γₛ : S.Con i}{Γ : Con Γₛ}{tₛ : S.Tm Γₛ S.⊤}{t : Tm Γ (⊤ {Γₛ = Γₛ}{Γ = Γ}) tₛ} →
  m._≡P_ {A = Tm Γ (⊤ {Γₛ = Γₛ}{Γ = Γ}) tₛ} t (tt {Γₛ = Γₛ}{Γ = Γ})
⊤η = m.reflP

⊤[] : ∀{i}{Γₛ : S.Con i}{Γ : Con Γₛ}{j}{Δₛ : S.Con j}{Δ : Con Δₛ}{σₛ : S.Sub Γₛ Δₛ}{σ : Sub Γ Δ σₛ} →
  m._≡_ {A = Ty Γ S.⊤} (_[_]T {Δ = Δ}{Aₛ = S.⊤}(⊤ {Γₛ = Δₛ}{Γ = Δ}){σₛ = σₛ} σ) (⊤ {Γₛ = Γₛ}{Γ = Γ})
⊤[] = m.refl

tt[] : ∀{i}{Γₛ : S.Con i}{Γ : Con Γₛ}{j}{Δₛ : S.Con j}{Δ : Con Δₛ}{σₛ : S.Sub Γₛ Δₛ}{σ : Sub Γ Δ σₛ} →
  m._≡P_ {A = Tm Γ (⊤ {Γₛ = Γₛ}{Γ = Γ}) S.tt}
        (_[_]t {Δ = Δ}{Aₛ = S.⊤}{A = ⊤ {Γₛ = Δₛ}{Γ = Δ}}{tₛ = S.tt}(tt {Γₛ = Δₛ}{Γ = Δ}){Γ = Γ}{σₛ = σₛ} σ)
        (tt {Γₛ = Γₛ}{Γ = Γ})
tt[] = m.reflP
