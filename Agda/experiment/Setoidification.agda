{-# OPTIONS --no-pattern-matching --prop --rewriting #-}

module experiment.Setoidification where

open import Agda.Primitive
import Lib as m 
import Lib.Props as m 
import WrappedAnticatStandard.Props as S 

{- Category -}
open import experiment.Setoidification.Category

{- Terminal Object -}
open import experiment.Setoidification.TerminalObject

{- Families -}
open import experiment.Setoidification.Families

{- Comprehension -}
open import experiment.Setoidification.Comprehension

{- Unit -}
open import experiment.Setoidification.Unit

{- Sigma -}
open import experiment.Setoidification.Sigma

{- Pi -}
open import experiment.Setoidification.Pi

-- CwF, ⊤, Σ, Π<-(érdekes lesz)

{-

displayed MLTT-model

eredeti setoidification:
                                                    Prop
Con := (∣Γ∣ : S.Con) × (Γ~ : ∀Ω.Sub Ω ∣Γ∣→Sub Ω ∣Γ∣→S.TyP Ω) × Γ~[] × RΓ × SΓ × TΓ
Con Γ := (Γ~ : S.TyP (Γ S.▹ Γ[ε])) × S.TmP Γ (Γ~ S.[ id S., id ]T)

mostani setoidification heterogen valtozata:
 A~ : Γ~ ρ₀ ρ₁ → M.Tm Ω (A[ρ₀]) → M.Tm Ω (A[ρ₁]) → M.TyP Ω

Ty (Γ~ , RΓ) A :=
  (A~  : TyP (Γ▷Γ▷Γ~▷A[1. Γ]▷A[2. Γ])) ×                A~ : Γ~ γ₀ γ₁ → A γ₀ → A γ₁ → Prop
  (RA  : TmP (x:Γ▷y:A) (A~[x,x,RΓ[x],y,y]))             RA : (x : A γ) → A~ (RΓ γ) x x
  (coe0A : Tm (x₀:Γ▷x₁:Γ▷x₀₁:Γ~▷A[x₀]) (A[x₁])
  (coh0A : TmP (x₀:Γ▷x₁:Γ▷x₀₁:Γ~▷y₀:A[x₀]) (A~[x₀,x₁,x₀₁,y₀,coe0 A x₀₁ y₀])
  (coe1A : Tm (x₀:Γ▷x₁:Γ▷x₀₁:Γ~▷A[x₁]) (A[x₀])
  (coh1A : ...
  ((coe0~A : TmP                                            ...  → A~ γ₀₁ₐ α₀ₐ α₁ₐ → A~ γ₀₁b α₀b α₁b))
  ((coe1~A : TmP ))

Sub Γ Δ σₛ := TmP (Γ▷Γ▷Γ~) (Δ~[σₛ∘(1. Γ) , σₛ∘(2. Γ)])

Tm Γ tₛ := TmP (Γ▷Γ▷Γ~) (A~[id,tₛ[1. Γ],tₛ[2. Γ]])          ∀γ₀,γ₁,γ₀₁:Γ~ γ₀ γ₁ → A~ {γ₀}{γ₁} γ₀₁ (t γ₀) (t γ₁)

-}
