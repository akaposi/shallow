{-# OPTIONS --no-pattern-matching #-}

module Inj where

open import Agda.Primitive
import Lib as m
import WrappedStandard as S
import Termification as T
-- if you use these two instead of the above two for S, T, the contents of hole ?0 are accepted
-- import Standard as S
-- import UnwrappedTermification as T

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixl 6 _,Σ_

-- substitution calculus

record Con i (Γˢ : S.Con i) : Set (lsuc i) where
  field
    ⟦_⟧ : T.Con i
    _₁  : S.Sub Γˢ (S.∙ S.▷ S.El ⟦_⟧)
    _₂  : S.Sub (S.∙ S.▷ S.El ⟦_⟧) Γˢ
    _₁₂ : _₁ S.∘ _₂ m.≡ S.id
    _₂₁ : _₂ S.∘ _₁ m.≡ S.id
open Con

record Ty {i}{Γˢ : S.Con i}(Γ : Con i Γˢ) j (Aˢ : S.Ty Γˢ j) : Set (i ⊔ lsuc j) where
  field
    ⟦_⟧ : T.Ty ⟦ Γ ⟧ j
    _⁼  : Aˢ m.≡ S.El (S.app ⟦_⟧ S.[ Γ ₁ ]t)
open Ty

∙ : Con lzero S.∙
∙ = record {
  ⟦_⟧ = T.∙ ;
  _₁  = S.ε S., S.tt ;
  _₂  = S.ε ;
  _₁₂ = m.refl ;
  _₂₁ = m.refl }

import WrappedStandard.Lib as L
{-
_▷_ : ∀ {i}{Γˢ : S.Con i}(Γ : Con i Γˢ)
        {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ j Aˢ) →
        Con (i ⊔ j) (Γˢ S.▷ Aˢ)
_▷_ {i}{Γˢ} Γ {j}{Aˢ} A = record {
  ⟦_⟧ = ⟦ Γ ⟧ T.▷ ⟦ A ⟧ ;
  _₁  = S.ε S., (S._,Σ_ {A = S.El ⟦ Γ ⟧ S.[ S.ε ]T}
                        {B = S.El (S.app ⟦ A ⟧) S.[ S.ε S., S.v⁰ ]T}
                        (S.v⁰ S.[ Γ ₁ S.∘ S.p {A = Aˢ} ]t)
                        (m.tr (λ z → S.Tm (Γˢ S.▷ Aˢ) (z S.[ S.p ]T)) (A ⁼) S.v⁰)) ;
  _₂  = Γ ₂ S.∘ (S.ε S., S.fst S.v⁰) S.,
        m.tr (λ z → S.Tm (S.∙ S.▷ S.Σ (S.El ⟦ Γ ⟧) (S.El (S.app ⟦ A ⟧))) (z S.[ S.p S., S.fst S.v⁰ ]T))
             ((m.ap S._[ Γ ₂ ]T (A ⁼) m.◾ {!m.ap (λ z → S.El (S.app ⟦ A ⟧) S.[ z ]T) (Γ ₁₂)!}) m.⁻¹) --
             (S.snd S.v⁰) ;
  _₁₂ = {!!} ;
  _₂₁ = {!!} }
-}
-- Goal: L.mkT (λ γ → L.app' (L.∣ ⟦ A ⟧ ∣t (L.pr₁' (L.∣ Γ ₁ ∣s (L.∣ Γ ₂ ∣s γ)))) (L.pr₂' (L.∣ Γ ₁ ∣s (L.∣ Γ ₂ ∣s γ)))) ≡ L.mkT (λ z → L.app' (L.∣ ⟦ A ⟧ ∣t (L.pr₁' z)) (L.pr₂' z))
-- Have: L.mkT (λ γ → L.app' (L.∣ ⟦ A ⟧ ∣t (L.pr₁' (L.∣ Γ ₁ ∣s (L.∣ Γ ₂ ∣s γ)))) (L.pr₂' (L.∣ Γ ₁ ∣s (L.∣ Γ ₂ ∣s γ)))) ≡ L.mkT (λ γ → L.app' (L.∣ ⟦ A ⟧ ∣t (L.pr₁' γ)) (L.pr₂' γ))



record Sub {i}{Γˢ : S.Con i}(Γ : Con i Γˢ){j}{Δˢ : S.Con j}(Δ : Con j Δˢ)(σˢ : S.Sub Γˢ Δˢ) : Set (i ⊔ j) where
  field
    ⟦_⟧ : T.Sub ⟦ Γ ⟧ ⟦ Δ ⟧
    _⁼  : σˢ m.≡ (Δ ₂ S.∘ (S.p S., S.app ⟦_⟧)) S.∘ Γ ₁
open Sub

record Tm {i}{Γˢ : S.Con i}(Γ : Con i Γˢ){j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ j Aˢ)(tˢ : S.Tm Γˢ Aˢ) : Set (i ⊔ j) where
  field
    ⟦_⟧ : T.Tm ⟦ Γ ⟧ ⟦ A ⟧
    _⁼  : tˢ m.≡ m.tr (S.Tm Γˢ) (A ⁼ m.⁻¹) (S.app ⟦_⟧ S.[ Γ ₁ ]t)
{-
_[_]T :
  ∀ {i}{Δˢ : S.Con i}   {Δ : Con i Δˢ}
    {j}{Aˢ : S.Ty Δˢ j} (A : Ty Δ j Aˢ)
    {k}{Γˢ : S.Con k}   {Γ : Con k Γˢ}
    {σˢ : S.Sub Γˢ Δˢ}  (σ : Sub Γ Δ σˢ)
    → Ty Γ j (Aˢ S.[ σˢ ]T)
(A [ σ ]T) = record { ⟦_⟧ = ⟦ A ⟧ T.[ ⟦ σ ⟧ ]T ; _⁼ = {!A ⁼!} }
-}
id : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ} → Sub Γ Γ S.id
id {Γ = Γ} = record { ⟦_⟧ = T.id ; _⁼ = Γ ₂₁ m.⁻¹ }
{-
_∘_ :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con _ Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con _ Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} (σ : Sub Θ Δ σˢ)
   {k}{Γˢ : S.Con k}     {Γ : Con _ Γˢ}
      {δˢ : S.Sub Γˢ Θˢ} (δ : Sub Γ Θ δˢ)
  → Sub Γ Δ (σˢ S.∘ δˢ)
(σ ∘ δ) ρ̂ = σ (δ ρ̂)
-}
ε : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
    → Sub Γ ∙ S.ε
ε = record { ⟦_⟧ = T.ε ; _⁼ = m.refl }
