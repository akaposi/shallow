{-# OPTIONS --no-pattern-matching --prop #-}

module Standard.PropsNewSorts where

open import Agda.Primitive
import Standard.Lib.Props as m
import Lib as l
import Lib.Props as l
open import Standard public

infixl 5 _▷P_
infixl 7 _[_]TP
infixl 5 _,P_
infixl 8 _[_]tP
infixl 5 _^P_
infixr 6 _⇒P_
infixl 7 _$P_
infixl 6 _,ΣP_
infixl 6 _,ΣSP_

-- two new sorts

TyP : ∀{i}(Γ : Con i)(j : Level) → Set (i ⊔ lsuc j)
TyP Γ j = Γ → Prop j

TmP : ∀{i}(Γ : Con i){j}(A : TyP Γ j) → Prop (i ⊔ j)
TmP Γ A = (γ : Γ) → A γ

irr : ∀{i}{Γ : Con i}{j}(A : TyP Γ j)(u v : TmP Γ A) → u l.≡P v
irr a u v = l.reflP

-- new substitutions for propositions

_[_]TP : ∀ {i}{Δ : Con i}{j}(A : TyP Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → TyP Γ j
A [ σ ]TP = λ γ → A (σ γ)

_[_]tP : ∀{i}{Δ : Con i}{j}{A : TyP Δ j}(t : TmP Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → TmP Γ (A [ σ ]TP)
t [ σ ]tP = λ γ → t (σ γ)

[id]TP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → A [ id ]TP l.≡ A
[id]TP = l.refl

[∘]TP : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
  {l}{A : TyP Δ l} → A [ σ ]TP [ δ ]TP l.≡ A [ σ ∘ δ ]TP
[∘]TP = l.refl

[id]tP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{t : TmP Γ A} → t [ id ]tP l.≡P t
[id]tP = l.reflP

[∘]tP : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
  {l}{A : TyP Δ l}{t : TmP Δ A} → t [ σ ]tP [ δ ]tP l.≡P t [ σ ∘ δ ]tP
[∘]tP = l.reflP

-- new comprehension for propositions

_▷P_   : ∀ {i}(Γ : Con i){j}(A : TyP Γ j) → Con (i ⊔ j)
Γ ▷P A = m.ΣSP Γ λ γ → A γ

_,P_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : TyP Δ k}(t : TmP Γ (A [ σ ]TP)) → Sub Γ (Δ ▷P A)
σ ,P t = λ γ → (σ γ m.,ΣSP t γ)

pP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → Sub (Γ ▷P A) Γ
pP = m.fstSP

qP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → TmP (Γ ▷P A) (A [ pP ]TP)
qP = m.sndSP

▷Pβ₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : TyP Δ k}{t : TmP Γ (A [ σ ]TP)} → pP ∘ (_,P_ σ {A = A} t) l.≡ σ
▷Pβ₁ = l.refl

▷Pβ₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : TyP Δ k}{t : TmP Γ (A [ σ ]TP)} → qP [ _,P_ σ {A = A} t ]tP l.≡P t
▷Pβ₂ = l.reflP

▷Pη : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → (pP {A = A} ,P qP {A = A}) l.≡ id
▷Pη = l.refl

,P∘ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)}{l}{θ : Con l}{δ : Sub θ Γ} →
  (_,_ σ {A = A} t) ∘ δ l.≡ (σ ∘ δ) , (t [ δ ]t)
,P∘ = l.refl

-- abbreviations

pP² : 
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : TyP (Γ ▷P A) k} →
   Sub (Γ ▷P A ▷P B) Γ
pP² = pP ∘ pP

pP³ : 
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : TyP (Γ ▷P A) k}
   {l}{C : TyP (Γ ▷P A ▷P B) l} →
   Sub (Γ ▷P A ▷P B ▷P C) Γ
pP³ = pP ∘ pP ∘ pP

pᵖᵖᴾ : 
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : TyP (Γ ▷ A ▷ B) l} →
   Sub (Γ ▷ A ▷ B ▷P C) Γ
pᵖᵖᴾ = p ∘ p ∘ pP

pP⁴ : 
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : TyP (Γ ▷P A) k}
   {l}{C : TyP (Γ ▷P A ▷P B) l}
   {m}{D : TyP (Γ ▷P A ▷P B ▷P C) m} →
   Sub (Γ ▷P A ▷P B ▷P C ▷P D) Γ
pP⁴ = pP ∘ pP ∘ pP ∘ pP

pP⁵ : 
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : TyP (Γ ▷P A) k}
   {l}{C : TyP (Γ ▷P A ▷P B) l}
   {m}{D : TyP (Γ ▷P A ▷P B ▷P C) m}
   {n}{E : TyP (Γ ▷P A ▷P B ▷P C ▷P D) n} →
   Sub (Γ ▷P A ▷P B ▷P C ▷P D ▷P E) Γ
pP⁵ = pP ∘ pP ∘ pP ∘ pP ∘ pP

vᴾ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : TyP (Γ ▷ A) k} →
   Tm (Γ ▷ A ▷P B) (A [ p ∘ pP ]T)
vᴾ = q [ pP ]t

vPᴾ :
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : TyP (Γ ▷P A) k} →
   TmP (Γ ▷P A ▷P B) (A [ pP ∘ pP ]TP)
vPᴾ = qP [ pP ]tP

vPᵖ :
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : Ty (Γ ▷P A) k} →
   TmP (Γ ▷P A ▷ B) (A [ pP ∘ p ]TP)
vPᵖ = qP [ p ]tP

vPᵖᵖ :
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : Ty (Γ ▷P A) k}
   {l}{C : Ty (Γ ▷P A ▷ B) l} →
   TmP (Γ ▷P A ▷ B ▷ C) (A [ pP ∘ p ∘ p ]TP)
vPᵖᵖ = qP [ p ∘ p ]tP

vPᵖᴾ :
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : Ty (Γ ▷P A) k}
   {l}{C : TyP (Γ ▷P A ▷ B) l} →
   TmP (Γ ▷P A ▷ B ▷P C) (A [ pP ∘ p ∘ pP ]TP)
vPᵖᴾ = qP [ p ∘ pP ]tP

vPᴾᵖ :
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : TyP (Γ ▷P A) k}
   {l}{C : Ty (Γ ▷P A ▷P B) l} →
   TmP (Γ ▷P A ▷P B ▷ C) (A [ pP ∘ pP ∘ p ]TP)
vPᴾᵖ = qP [ pP ∘ p ]tP

vPᴾᴾ :
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : TyP (Γ ▷P A) k}
   {l}{C : TyP (Γ ▷P A ▷P B) l} →
   TmP (Γ ▷P A ▷P B ▷P C) (A [ pP ∘ pP ∘ pP ]TP)
vPᴾᴾ = qP [ pP ∘ pP ]tP

vᵖᴾ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : TyP (Γ ▷ A ▷ B) l} →
   Tm (Γ ▷ A ▷ B ▷P C) (A [ p ∘ p ∘ pP ]T)
vᵖᴾ = q [ p ∘ pP ]t

vᴾᵖ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : TyP (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷P B) l} →
   Tm (Γ ▷ A ▷P B ▷ C) (A [ p ∘ pP ∘ p ]T)
vᴾᵖ = q [ pP ∘ p ]t

vᴾᴾ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : TyP (Γ ▷ A) k}
   {l}{C : TyP (Γ ▷ A ▷P B) l} →
   Tm (Γ ▷ A ▷P B ▷P C) (A [ p ∘ pP ∘ pP ]T)
vᴾᴾ = q [ pP ∘ pP ]t

vᵖᴾᵖᴾ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : TyP (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷P C) m}
   {n}{E : TyP (Γ ▷ A ▷ B ▷P C ▷ D) n} →
   Tm (Γ ▷ A ▷ B ▷P C ▷ D ▷P E) (A [ p ∘ p ∘ pP ∘ p ∘ pP ]T)
vᵖᴾᵖᴾ = v⁰ [ p ∘ pP ∘ p ∘ pP ]t

vᴾᵖᴾᵖ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : TyP (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷P B) l}
   {m}{D : TyP (Γ ▷ A ▷P B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷P B ▷ C ▷P D) n} →
   Tm (Γ ▷ A ▷P B ▷ C ▷P D ▷ E) (A [ p ∘ pP ∘ p ∘ pP ∘ p ]T)
vᴾᵖᴾᵖ = v⁰ [ pP ∘ p ∘ pP ∘ p ]t

vᴾᵖᵖᴾᵖ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : TyP (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷P B) l}
   {m}{D : Ty (Γ ▷ A ▷P B ▷ C) m}
   {n}{E : TyP (Γ ▷ A ▷P B ▷ C ▷ D) n}
   {o}{F : Ty (Γ ▷ A ▷P B ▷ C ▷ D ▷P E) o} →
   Tm (Γ ▷ A ▷P B ▷ C ▷ D ▷P E ▷ F) (A [ p ∘ pP ∘ p ∘ p ∘ pP ∘ p ]T)
vᴾᵖᵖᴾᵖ = v⁰ [ pP ∘ p ∘ p ∘ pP ∘ p ]t

vᴾᵖᴾᵖᴾ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : TyP (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷P B) l}
   {m}{D : TyP (Γ ▷ A ▷P B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷P B ▷ C ▷P D) n}
   {o}{F : TyP (Γ ▷ A ▷P B ▷ C ▷P D ▷ E) o} →
   Tm (Γ ▷ A ▷P B ▷ C ▷P D ▷ E ▷P F) (A [ p ∘ pP ∘ p ∘ pP ∘ p ∘ pP ]T)
vᴾᵖᴾᵖᴾ = v⁰ [ pP ∘ p ∘ pP ∘ p ∘ pP ]t

vPᵖᵖᴾᵖᴾᵖᴾ :
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : Ty (Γ ▷P A) k}
   {l}{C : Ty (Γ ▷P A ▷ B) l}
   {m}{D : TyP (Γ ▷P A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷P A ▷ B ▷ C ▷P D) n}
   {o}{F : TyP (Γ ▷P A ▷ B ▷ C ▷P D ▷ E) o}
   {r}{G : Ty (Γ ▷P A ▷ B ▷ C ▷P D ▷ E ▷P F) r}
   {s}{H : TyP (Γ ▷P A ▷ B ▷ C ▷P D ▷ E ▷P F ▷ G) s} →
   TmP (Γ ▷P A ▷ B ▷ C ▷P D ▷ E ▷P F ▷ G ▷P H) (A [ pP ∘ p ∘ p ∘ pP ∘ p ∘ pP ∘ p ∘ pP ]TP)
vPᵖᵖᴾᵖᴾᵖᴾ = qP [ p ∘ p ∘ pP ∘ p ∘ pP ∘ p ∘ pP ]tP

_^P_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}(A : TyP Δ k) →
  Sub (Γ ▷P A [ σ ]TP) (Δ ▷P A)
_^P_ {i}{Γ}{j}{Δ} σ {k} A = σ ∘ pP ,P qP {i}{Γ}{_}{A [ σ ]TP}

-- Π

ΠP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : TyP (Γ ▷ A) k) → TyP Γ (j ⊔ k)
ΠP A B = λ γ → (x : A γ) → (B (γ m.,Σ x))

lamP : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : TyP (Γ ▷ A) k} → TmP (Γ ▷ A) B → TmP Γ (ΠP A B)
lamP t = λ γ α → t (γ m.,Σ α)

appP : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : TyP (Γ ▷ A) k} → TmP Γ (ΠP A B) → TmP (Γ ▷ A) B
appP t = λ γ → t (m.fst γ) (m.snd γ)

ΠP[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : TyP (Γ ▷ A) k}{l}{Θ : Con l}{σ : Sub Θ Γ} →
  ΠP A B [ σ ]TP m.≡ ΠP (A [ σ ]T) (B [ σ ^ A ]TP)
ΠP[] = m.refl

-- abbreviations

_⇒P_ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : TyP Γ k) → TyP Γ (j ⊔ k)
A ⇒P B = ΠP A (B [ p ]TP)

_$P_ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : TyP (Γ ▷ A) k}(t : TmP Γ (ΠP A B))(u : Tm Γ A) → TmP Γ (B [ id , u ]TP)
t $P u = appP t [ id , u ]tP

-- Σ

ΣP : ∀{i}{Γ : Con i}{j}(A : TyP Γ j){k}(B : TyP (Γ ▷P A) k) → TyP Γ (j ⊔ k)
ΣP A B = λ γ → m.ΣP (A γ) λ α → B (γ m.,ΣSP α)

_,ΣP_ : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{B : TyP (Γ ▷P A) k}(u : TmP Γ A)(v : TmP Γ (B [ id ,P u ]TP)) → TmP Γ (ΣP A B)
u ,ΣP v = λ γ → (u γ m.,ΣP v γ)

fstP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{B : TyP (Γ ▷P A) k}
  (t : TmP Γ (ΣP A B)) → TmP Γ A
fstP t = λ γ → m.fstP (t γ)

sndP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{B : TyP (Γ ▷P A) k}
  (t : TmP Γ (ΣP A B)) → TmP Γ (B [ id ,P fstP {B = B} t ]TP)
sndP t = λ γ → m.sndP (t γ)

ΣP[] : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{B : TyP (Γ ▷P A) k}{l}{Θ : Con l}{σ : Sub Θ Γ} →
  ΣP A B [ σ ]TP l.≡ ΣP (A [ σ ]TP) (B [ σ ^P A ]TP)
ΣP[] = l.refl

-- another Σ

ΣSP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : TyP (Γ ▷ A) k) → Ty Γ (j ⊔ k)
ΣSP A B = λ γ → m.ΣSP (A γ) λ α → B (γ m.,Σ α)

_,ΣSP_ : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}(u : Tm Γ A)(v : TmP Γ (B [ id , u ]TP)) → Tm Γ (ΣSP A B)
u ,ΣSP v = λ γ → u γ m.,ΣSP v γ

fstSP : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k} → Tm Γ (ΣSP A B) → Tm Γ A
fstSP t = λ γ → m.fstSP (t γ)

sndSP : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}(t : Tm Γ (ΣSP A B)) → TmP Γ (B [ id , fstSP t ]TP)
sndSP t = λ γ → m.sndSP (t γ)

ΣSPβ : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}{u : Tm Γ A}{v : TmP Γ (B [ id , u ]TP)} →
  fstSP (_,ΣSP_ {A = A}{B = B} u v) l.≡ u
ΣSPβ = l.refl

ΣSPη : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}{t : Tm Γ (ΣSP A B)} →
  fstSP t ,ΣSP sndSP t l.≡ t
ΣSPη = l.refl

ΣSP[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{l}{B : TyP (Δ ▷ A) l} →
  ΣSP A B [ σ ]T l.≡ ΣSP (A [ σ ]T) (B [ σ ^ A ]TP)
ΣSP[] = l.refl

,ΣSP[] : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}{u : Tm Γ A}{v : TmP Γ (B [ _,_ id {A = A} u ]TP)}{l}{Ω : Con l}{ν : Sub Ω Γ} →
  (_,ΣSP_ {A = A}{B = B} u v) [ ν ]t l.≡ _,ΣSP_ {A = A [ ν ]T}{B = B [ ν ^ A ]TP} (u [ ν ]t) (v [ ν ]tP)
,ΣSP[] = l.refl

-- unit

⊤P : ∀{i}{Γ : Con i} → TyP Γ lzero
⊤P = λ _ → m.⊤P

⊤P[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ} → ⊤P [ σ ]TP l.≡ ⊤P
⊤P[] = l.refl

ttP : ∀{i}{Γ : Con i} → TmP Γ ⊤P
ttP = λ _ → m.ttP

-- universe of propositions

P : ∀{i}{Γ : Con i} j → Ty Γ (lsuc j)
P j = λ _ → Prop j

ElP : ∀{i}{Γ : Con i}{j}(a : Tm Γ (P j)) → TyP Γ j
ElP a = a

cP : ∀{i}{Γ : Con i}{j}(A : TyP Γ j) → Tm Γ (P j)
cP A = A

Pβ : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → ElP (cP A) l.≡ A
Pβ = l.refl

Pη : ∀{i}{Γ : Con i}{j}{a : Tm Γ (P j)} → cP (ElP a) l.≡ a
Pη = l.refl

P[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} {k} → U k [ σ ]T l.≡ U k
P[] = l.refl

ElP[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{a : Tm Δ (P k)}
       → ElP a [ σ ]TP l.≡ ElP (a [ σ ]t)
ElP[] = l.refl

-- extra equalities

RussellP : ∀{i}{Γ : Con i}{j} → Tm Γ (P j) l.≡ TyP Γ j
RussellP = l.refl

[]TtP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{Θ : Con k}{σ : Sub Θ Γ} → A [ σ ]TP l.≡ A [ σ ]t
[]TtP = l.refl

-- empty

⊥P : ∀ {i}{Γ : Con i} → TyP Γ lzero
⊥P = λ _ → m.⊥P

⊥P[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ} → ⊥P [ σ ]TP l.≡ ⊥P
⊥P[] = l.refl

exfalsoP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(t : TmP Γ ⊥P) → Tm Γ A
exfalsoP A t = λ γ → m.exfalsoP (t γ)

exfalsoP[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ}{k}{A : Ty Γ k}{t : TmP Γ ⊥P} →
  exfalsoP A t [ σ ]t l.≡ exfalsoP (A [ σ ]T) (t [ σ ]tP)
exfalsoP[] = l.refl
