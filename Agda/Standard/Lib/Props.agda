{-# OPTIONS --prop #-}

module Standard.Lib.Props where

open import Agda.Primitive
open import Standard.Lib public

-- this is not needed in PropsNewSorts:
record ElP {ℓ}(A : Prop ℓ) : Set ℓ where
  constructor mkElP
  field
    unElP : A
open ElP public

record ⊤P : Prop where

ttP : ⊤P   -- \st
ttP = record {}

record ΣP {ℓ ℓ'}(A : Prop ℓ)(B : A → Prop ℓ') : Prop (ℓ ⊔ ℓ') where
  constructor _,ΣP_
  field
    fstP : A
    sndP : B fstP
open ΣP public

record ΣSP {ℓ ℓ'}(A : Set ℓ)(B : A → Prop ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,ΣSP_
  field
    fstSP : A
    sndSP : B fstSP
open ΣSP public

data ⊥P : Prop where

exfalsoP : ∀{i}{C : Set i} → ⊥P → C
exfalsoP ()
