{-# OPTIONS --no-pattern-matching #-}

module Slice where

open import Agda.Primitive
import Lib as m
import WrappedStandard as S

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixr 6 _⇒_
infixl 7 _$_
infixl 6 _,Σ_

module Slice {l}(Ω : S.Con l) where

  Con : (i : Level) → Set (l ⊔ lsuc i)
  Con i = m.Σ (S.Con i) (λ Γ → S.Sub Γ Ω)

  Ty : ∀{i}(Γ : Con i)(j : Level) → Set (i ⊔ lsuc j)
  Ty Γ j = S.Ty (m.fst Γ) j

  Sub : ∀{i}(Γ : Con i){j}(Δ : Con j) → Set _
  Sub Γ Δ = m.Σ (S.Sub (m.fst Γ) (m.fst Δ)) λ σ → m.snd Γ m.≡ m.snd Δ S.∘ σ

  -- TODO
