{-# OPTIONS --prop --rewriting #-}

open import Poset hiding (_∶_⇒_)

module Kripke (ℙ : Poset) where

open import Agda.Primitive
open import Lib using (_≡_)
import Lib.Props as l

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_

Ob = ∣ ℙ ∣
_⇒_ = Poset._∶_⇒_ ℙ

record Con (i : Level) : Set (lsuc i) where
  field
    ∣_∣    : Ob → Set i
    _∶_⟨_⟩ : {I J : Ob} → ∣_∣ I → J ⇒ I → ∣_∣ J
open Con public

record Ty {i}(Γ : Con i)(j : Level) : Set (i ⊔ lsuc j) where
  field
    ∣_∣    : {I : Ob} → ∣ Γ ∣ I → Set j
    _∶_⟨_⟩ : {I J : Ob}{γ : Con.∣ Γ ∣ I} → ∣_∣ γ → (f : J ⇒ I) → ∣_∣ (Γ ∶ γ ⟨ f ⟩)
open Ty public

record Sub {i}(Γ : Con i){j}(Δ : Con j) : Set (i ⊔ j) where
  field
    ∣_∣    : {I : Ob} → ∣ Γ ∣ I → ∣ Δ ∣ I
    _∶_⟨_⟩ : {I J : Ob}(γ : Con.∣ Γ ∣ I)(f : J ⇒ I) → Δ ∶ ∣_∣ γ ⟨ f ⟩ l.≡p ∣_∣ (Γ ∶ γ ⟨ f ⟩) 
open Sub public

record Tm {i}(Γ : Con i){j}(A : Ty Γ j) : Set (i ⊔ j) where
  field
    ∣_∣    : {I : Ob}(γ : ∣ Γ ∣ I) → ∣ A ∣ γ
    _∶_⟨_⟩ : {I J : Ob}(γ : Con.∣ Γ ∣ I)(f : J ⇒ I) → A ∶ ∣_∣ γ ⟨ f ⟩ l.≡p ∣_∣ (Γ ∶ γ ⟨ f ⟩) 
open Tm public
    
id : ∀{i}{Γ : Con i} → Sub Γ Γ
id = record { ∣_∣ = λ γ → γ ; _∶_⟨_⟩ = λ _ _ → l.reflp }

_∘_ : ∀{i}{Θ : Con i}{j}{Δ : Con j}(σ : Sub Θ Δ){k}{Γ : Con k}(δ : Sub Γ Θ) → Sub Γ Δ
σ ∘ δ = record { ∣_∣ = λ γ → ∣ σ ∣ (∣ δ ∣ γ) ; _∶_⟨_⟩ = λ γ f → σ ∶ (∣ δ ∣ γ) ⟨ f ⟩ l.◾p l.ap-p ∣ σ ∣ (δ ∶ γ ⟨ f ⟩) }

ass : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Ξ : Con k}{δ : Sub Ξ Θ}{l}{Γ : Con l}{ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν l.≡ σ ∘ (δ ∘ ν)
ass = l.refl

idl : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → l._≡_ {A = Sub Γ Δ} (_∘_ {_}{Δ}{_}{Δ} (id {Γ = Δ}) {_}{Γ} σ) σ
idl = l.refl

idr : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → l._≡_ {A = Sub Γ Δ} (_∘_ {_}{Γ}{_}{Δ} σ {_}{Γ} (id {Γ = Γ})) σ
idr = l.refl

_[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
A [ σ ]T = record {
  ∣_∣    = λ γ → ∣ A ∣ (∣ σ ∣ γ) ;
  _∶_⟨_⟩ = λ α f → l.tr-p ∣ A ∣ (σ ∶ _ ⟨ f ⟩) (A ∶ α ⟨ f ⟩)  }

_[_]t : ∀{i}{Δ : Con i}{j}{A : Ty Δ j}(t : Tm Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
_[_]t {A = A} t σ = record {
  ∣_∣ = λ γ → ∣ t ∣ (∣ σ ∣ γ) ;
  _∶_⟨_⟩ = λ γ f → l.ap-p (l.tr-p ∣ A ∣ _) (t ∶ ∣ σ ∣ γ ⟨ f ⟩) l.◾p l.apd-p ∣ t ∣ (σ ∶ γ ⟨ f ⟩) }

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T l.≡ A
[id]T = l.refl
{-
[∘]T : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
  {l}{A : Ty Δ l} → A [ σ ]T [ δ ]T l.≡ A [ σ ∘ δ ]T
[∘]T = {!l.refl!}

[id]t : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{t : Tm Γ A} → t [ id ]t l.≡ t
[∘]t : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
  {l}{A : Ty Δ l}{t : Tm Δ A} → t [ σ ]t [ δ ]t l.≡ t [ σ ∘ δ ]t
∙ : Con lzero
ε : ∀{i}{Γ : Con i} → Sub Γ ∙
∙η : ∀{i}{Γ : Con i}{σ : Sub Γ ∙} → σ l.≡ ε
_▷_   : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j)
_,_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : Ty Δ k}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▷ A)
p : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Sub (Γ ▷ A) Γ
q : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
▷β₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → p ∘ (_,_ σ {A = A} t) l.≡ σ

▷β₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → q [ _,_ σ {A = A} t ]t l.≡ t
▷η : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → (p {A = A} , q {A = A}) l.≡ id
,∘ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)}{l}{θ : Con l}{δ : Sub θ Γ} →
  (_,_ σ {A = A} t) ∘ δ l.≡ (σ ∘ δ) , (t [ δ ]t)

 abbreviations
 :
∀{i}{Γ : Con i}
 {j}{A : Ty Γ j}
 {k}{B : Ty (Γ ▷ A) k} →
 Sub (Γ ▷ A ▷ B) Γ
 = p ∘ p

 :
∀{i}{Γ : Con i}
 {j}{A : Ty Γ j}
 {k}{B : Ty (Γ ▷ A) k}
 {l}{C : Ty (Γ ▷ A ▷ B) l} →
 Sub (Γ ▷ A ▷ B ▷ C) Γ
 = p ∘ p ∘ p

 :
∀{i}{Γ : Con i}
 {j}{A : Ty Γ j}
 {k}{B : Ty (Γ ▷ A) k}
 {l}{C : Ty (Γ ▷ A ▷ B) l}
 {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
 Sub (Γ ▷ A ▷ B ▷ C ▷ D) Γ
 = p ∘ p ∘ p ∘ p

 :
∀{i}{Γ : Con i}
 {j}{A : Ty Γ j}
 {k}{B : Ty (Γ ▷ A) k}
 {l}{C : Ty (Γ ▷ A ▷ B) l}
 {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
 {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n} →
 Sub (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) Γ
 = p ∘ p ∘ p ∘ p ∘ p

 : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
 = q

 :
∀{i}{Γ : Con i}
 {j}{A : Ty Γ j}
 {k}{B : Ty (Γ ▷ A) k} →
 Tm (Γ ▷ A ▷ B) (A [ p² ]T)
 = v⁰ [ p ]t

 :
∀{i}{Γ : Con i}
 {j}{A : Ty Γ j}
 {k}{B : Ty (Γ ▷ A) k}
 {l}{C : Ty (Γ ▷ A ▷ B) l} →
 Tm (Γ ▷ A ▷ B ▷ C) (A [ p³ ]T)
 = v⁰ [ p² ]t

 :
∀{i}{Γ : Con i}
 {j}{A : Ty Γ j}
 {k}{B : Ty (Γ ▷ A) k}
 {l}{C : Ty (Γ ▷ A ▷ B) l}
 {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
 Tm (Γ ▷ A ▷ B ▷ C ▷ D) (A [ p⁴ ]T)
 = v⁰ [ p³ ]t

_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}(A : Ty Δ k) →
Sub (Γ ▷ A [ σ ]T) (Δ ▷ A)
_ {i}{Γ}{j}{Δ} σ {k} A = σ ∘ p , v⁰ {i}{Γ}{_}{A [ σ ]T}

 Identity
stulate
Id : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(u v : Tm Γ A) → Ty Γ j
Id[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u v : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
  Id A u v [ σ ]T l.≡ Id (A [ σ ]T) (u [ σ ]t) (v [ σ ]t)

stulate
refl : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}(u : Tm Γ A) → Tm Γ (Id A u u)
refl[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
  refl u [ σ ]t l.≡ refl (u [ σ ]t)

J :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
   {k}(C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k)
   (w : Tm Γ (C [ id , u , refl u ]T))
   {v : Tm Γ A}(t : Tm Γ (Id A u v)) → Tm Γ (C [ id , v , t ]T)

Idβ :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
   {k}{C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k}
   {w : Tm Γ (C [ id , u , refl u ]T)} →
   J C w (refl u) l.≡ w
-}
