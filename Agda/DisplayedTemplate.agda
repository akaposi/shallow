{-# OPTIONS --rewriting #-}

module DisplayedTemplate where

open import Agda.Primitive

open import Lib using (coe; ap; tr; _≡_; _⁻¹; _◾_)
import Lib as m
import ModelTemplate as S

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_

-- substitution calculus

postulate
  Con : ∀ {i} → S.Con i → Set (lsuc i)

  Ty  : ∀ {j}{Γˢ : S.Con j}(Γ : Con Γˢ){i} (Aˢ : S.Ty Γˢ i) → Set (lsuc i ⊔ j)

  Sub   : ∀ {i}{Γˢ : S.Con i}(Γ : Con Γˢ)
            {j}{Δˢ : S.Con j}(Δ : Con Δˢ)
            → S.Sub Γˢ Δˢ → Set (i ⊔ j)

  Tm  : ∀ {i}{Γˢ : S.Con i}(Γ : Con Γˢ)
            {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ Aˢ) → S.Tm Γˢ Aˢ → Set (i ⊔ j)

  id : ∀{i}{Γˢ : S.Con i}{Γ : Con Γˢ} → Sub Γ Γ S.id

  _∘_ :
    ∀{i}{Θˢ : S.Con i}     {Θ : Con Θˢ}
     {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
        {σˢ : S.Sub Θˢ Δˢ} (σ : Sub Θ Δ σˢ)
     {k}{Γˢ : S.Con k}     {Γ : Con Γˢ}
        {δˢ : S.Sub Γˢ Θˢ} (δ : Sub Γ Θ δˢ)
    → Sub Γ Δ (σˢ S.∘ δˢ)

  ass :
    ∀{i}{Θˢ : S.Con i}     {Θ : Con Θˢ}
     {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
        {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
     {k}{Ξˢ : S.Con k}     {Ξ : Con Ξˢ}
        {δˢ : S.Sub Ξˢ Θˢ} {δ : Sub Ξ Θ δˢ}
     {l}{Γˢ : S.Con l}     {Γ : Con Γˢ}
        {νˢ : S.Sub Γˢ Ξˢ} {ν : Sub Γ Ξ νˢ}
     → m._≡_ {A = Sub Γ Δ ((σˢ S.∘ δˢ) S.∘ νˢ)}
             (_∘_ {Θ = Ξ}{Δ = Δ}{σˢ = σˢ S.∘ δˢ}(_∘_ {Θ = Θ}{Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ} δ){δˢ = νˢ} ν)
             (_∘_ {Θ = Θ}{Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ S.∘ νˢ}(_∘_ {Θ = Ξ}{Δ = Θ}{σˢ = δˢ} δ {δˢ = νˢ} ν))

  idl :
    ∀{i}{Γˢ : S.Con i}     {Γ : Con Γˢ}
     {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
        {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
    → m._≡_ {A = Sub Γ Δ σˢ} (_∘_ {Θ = Δ}{Δ = Δ}{σˢ = S.id}(id {Γ = Δ}){δˢ = σˢ} σ) σ

  idr :
    ∀{i}{Γˢ : S.Con i}     {Γ : Con Γˢ}
     {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
        {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
    → m._≡_ {A = Sub Γ Δ σˢ} (_∘_ {Θ = Γ}{Δ = Δ}{σˢ = σˢ} σ {δˢ = S.id} (id {Γˢ = Γˢ}{Γ = Γ})) σ

  _[_]T :
    ∀ {i}{Δˢ : S.Con i}   {Δ : Con Δˢ}
      {j}{Aˢ : S.Ty Δˢ j} (A : Ty Δ Aˢ)
      {k}{Γˢ : S.Con k}   {Γ : Con Γˢ}
      {σˢ : S.Sub Γˢ Δˢ}  (σ : Sub Γ Δ σˢ)
      → Ty Γ (Aˢ S.[ σˢ ]T)

  _[_]t :
    ∀{i}{Δˢ : S.Con i}     {Δ : Con Δˢ}
     {j}{Aˢ : S.Ty Δˢ j}   {A : Ty Δ Aˢ}
        {tˢ : S.Tm Δˢ Aˢ}  (t : Tm Δ A tˢ)
     {k}{Γˢ : S.Con k}     {Γ : Con Γˢ}
        {σˢ : S.Sub Γˢ Δˢ} (σ : Sub Γ Δ σˢ)
    → Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) (tˢ S.[ σˢ ]t)

  [id]T :
    ∀{i}{Γˢ : S.Con i}   {Γ : Con Γˢ}
     {j}{Aˢ : S.Ty Γˢ j} {A : Ty Γ Aˢ}
    → m._≡_ {A = Ty Γ Aˢ} (_[_]T {Aˢ = Aˢ} A {σˢ = S.id} (id {Γ = Γ})) A

  [∘]T :
    ∀{i}{Θˢ : S.Con i}     {Θ : Con Θˢ}
     {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
        {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
     {k}{Γˢ : S.Con k}     {Γ : Con Γˢ}
        {δˢ : S.Sub Γˢ Θˢ} {δ : Sub Γ Θ δˢ}
     {l}{Aˢ : S.Ty Δˢ l}   {A : Ty Δ Aˢ}
     → m._≡_ {A = Ty Γ (Aˢ S.[ σˢ S.∘ δˢ ]T)}
            (_[_]T {Aˢ = Aˢ S.[ σˢ ]T} (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) {σˢ = δˢ} δ)
            (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ S.∘ δˢ} (_∘_ {Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ} δ))
{-# REWRITE ass idl idr [id]T [∘]T #-}

postulate
  [id]t :
    ∀{i}{Γˢ : S.Con i}   {Γ : Con Γˢ}
     {j}{Aˢ : S.Ty Γˢ j} {A : Ty Γ Aˢ}
     {tˢ : S.Tm Γˢ Aˢ}{t : Tm Γ A tˢ}
    → m._≡_ {A = Tm Γ A tˢ}
            (_[_]t {A = A} {tˢ = tˢ} t {σˢ = S.id} (id {Γ = Γ}))
            t

  [∘]t :
    ∀{i}{Θˢ : S.Con i}     {Θ : Con Θˢ}
     {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
        {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
     {k}{Γˢ : S.Con k}     {Γ : Con Γˢ}
        {δˢ : S.Sub Γˢ Θˢ} {δ : Sub Γ Θ δˢ}
     {l}{Aˢ : S.Ty Δˢ l}   {A : Ty Δ Aˢ}
     {tˢ : S.Tm Δˢ Aˢ}{t : Tm Δ A tˢ}
     → m._≡_
            {A = Tm Γ (_[_]T {Aˢ = Aˢ S.[ σˢ ]T}(_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ)
                {σˢ = δˢ} δ) (tˢ S.[ σˢ ]t S.[ δˢ ]t)}
            (_[_]t {Aˢ = Aˢ S.[ σˢ ]T}{A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}
                   {tˢ = tˢ S.[ σˢ ]t}(_[_]t {Aˢ = Aˢ}{A = A} {tˢ = tˢ} t {σˢ = σˢ} σ)
                   {σˢ = δˢ} δ)
            (_[_]t {Aˢ = Aˢ}{A = A}
                   {tˢ = tˢ} t
                   {σˢ = σˢ S.∘ δˢ} (_∘_ {Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ} δ))

  ∙ : Con S.∙

  ε : ∀{i}{Γˢ : S.Con i}{Γ : Con Γˢ}
      → Sub Γ ∙ S.ε

  -- •η :
  --    ∀  {i}{Γˢ : S.Con i}      {Γ : Con Γˢ}
  --          {σˢ : S.Sub Γˢ S.∙} {σ : Sub Γ ∙ σˢ}
  --    → m._≡_ {A = Sub Γ ∙ σˢ} σ (ε {Γ = Γ})

  _▷_   : ∀ {i}{Γˢ : S.Con i}(Γ : Con Γˢ)
            {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ Aˢ)
            → Con (Γˢ S.▷ Aˢ)

  _,_ :
    ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
     {j}{Δˢ : S.Con j}                {Δ : Con Δˢ}
        {σˢ : S.Sub Γˢ Δˢ}            (σ : Sub Γ Δ σˢ)
     {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ Aˢ}
        {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} (t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ)
     → Sub Γ (_▷_ Δ {Aˢ = Aˢ} A) (σˢ S., tˢ)

  p :
    ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
     {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ Aˢ} →
     Sub (_▷_ Γ {Aˢ = Aˢ} A) Γ S.p

  q :
    ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
     {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ Aˢ} →
     Tm (_▷_ Γ {Aˢ = Aˢ} A)
        {Aˢ = Aˢ S.[ S.p {A = Aˢ} ]T}
        (_[_]T {Aˢ = Aˢ} A {Γ = _▷_ Γ {Aˢ = Aˢ} A}{σˢ = S.p} (p {Aˢ = Aˢ}{A = A}))
        S.q

  ▷β₁ :
    ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
     {j}{Δˢ : S.Con j}                {Δ : Con Δˢ}
        {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
     {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ Aˢ}
        {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ}
    → m._≡_ {A = Sub Γ Δ σˢ}
            (_∘_ {Θ = _▷_ Δ {Aˢ = Aˢ} A}{Δ = Δ}
                 {σˢ = S.p}(p {Aˢ = Aˢ}{A = A})
                 {δˢ = σˢ S., tˢ}(_,_ σ {Aˢ = Aˢ}{A = A}{tˢ = tˢ} t))
            σ
{-# REWRITE [id]t [∘]t ▷β₁ #-}

postulate
  ▷β₂ :
    ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
     {j}{Δˢ : S.Con j}                {Δ : Con Δˢ}
        {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
     {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ Aˢ}
        {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ}
    → q {j}{Δˢ}{Δ}{k}{Aˢ}{A}[ σ , t ]t ≡ t

  ▷η :
    ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
     {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ Aˢ}
    → (p {i}{Γˢ}{Γ}{_}{Aˢ}{A} , q) ≡ id

  ,∘ :
    ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
     {j}{Δˢ : S.Con j}                {Δ : Con Δˢ}
        {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
     {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ Aˢ}
        {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ}
     {l}{Θˢ : S.Con l}                {Θ : Con Θˢ}
        {δˢ : S.Sub Θˢ Γˢ}            {δ : Sub Θ Γ δˢ} →
    m._≡_ {A = Sub Θ (_▷_ Δ {Aˢ = Aˢ} A) ((σˢ S., tˢ) S.∘ δˢ)} (_∘_ {Θ = Γ}{Δˢ =
          Δˢ S.▷ Aˢ}{Δ = Δ ▷ A}{σˢ = σˢ S., tˢ}(_,_ {σˢ = σˢ} σ {Aˢ = Aˢ}{A =
          A}{tˢ = tˢ} t){δˢ = δˢ} δ) (_,_ (_∘_ {Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ}
          δ){Aˢ = Aˢ}{A = A}{tˢ = tˢ S.[ δˢ ]t}(_[_]t {A = _[_]T {Aˢ = Aˢ} A {σˢ
          = σˢ} σ}{tˢ = tˢ} t {σˢ = δˢ} δ))

{-# REWRITE ▷β₂ ,∘ ▷η #-}


-- abbreviations
p² :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ} →
   Sub (_▷_ (_▷_ Γ {Aˢ = Aˢ} A){Aˢ = Bˢ} B) Γ S.p²
p² {Γˢ = Γˢ}{Γ}{Aˢ = Aˢ}{A}{Bˢ = Bˢ}{B} =
  _∘_ {Θˢ = Γˢ S.▷ Aˢ}{Θ = _▷_ Γ {Aˢ = Aˢ} A}{Δˢ = Γˢ}{Δ = Γ}
      {σˢ = S.p {Γ = Γˢ}{A = Aˢ}}(p {Γˢ = Γˢ}{Γ}{Aˢ = Aˢ}{A})
      {Γˢ = Γˢ S.▷ Aˢ S.▷ Bˢ}{Γ = _▷_ (_▷_ Γ {Aˢ = Aˢ} A){Aˢ = Bˢ} B}
      {δˢ = S.p {Γ = Γˢ S.▷ Aˢ}{A = Bˢ}}(p {Γˢ = Γˢ S.▷ Aˢ}{Γ = _▷_ Γ {Aˢ = Aˢ} A}{Aˢ = Bˢ}{A = B})

v⁰ :
    ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
     {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ Aˢ}
    → Tm (_▷_ Γ {Aˢ = Aˢ} A)
         ((_[_]T {Aˢ = Aˢ} A {Γ = _▷_ Γ {Aˢ = Aˢ} A}{σˢ = S.p {A = Aˢ}}
         (p {Γ = Γ}{Aˢ = Aˢ}{A = A}))) S.v⁰
v⁰ {Γˢ = Γˢ}{Γ}{Aˢ = Aˢ}{A} = q {Aˢ = Aˢ}{A = A}

_^_ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            (σ : Sub Γ Δ σˢ)
   {k}{Aˢ : S.Ty Δˢ k}              (A : Ty Δ Aˢ)
  → Sub (Γ ▷ _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) (Δ ▷ A) (σˢ S.^ Aˢ)
_^_ {i} {Γˢ} {Γ} {j} {Δˢ} {Δ} {σˢ} σ {k} {Aˢ} A =
  _,_ {Γˢ = Γˢ S.▷ Aˢ S.[ σˢ ]T}{_▷_ Γ {Aˢ = Aˢ S.[ σˢ ]T}(_[_]T {Aˢ = Aˢ} A {σˢ
      = σˢ} σ)} {σˢ = σˢ S.∘ S.p}(_∘_ {Θˢ = Γˢ}{Θ = Γ}{Δˢ = Δˢ}{Δ = Δ}{σˢ = σˢ}
      σ {Γˢ = Γˢ S.▷ Aˢ S.[ σˢ ]T}{Γ ▷ _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ} {δˢ = S.p
      {Γ = Γˢ}{A = Aˢ S.[ σˢ ]T}} (p {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ S.[ σˢ ]T}{A =
      _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ})) {Aˢ = Aˢ}{A = A} {tˢ = S.v⁰}(v⁰ {Γˢ =
      Γˢ}{Γ}{Aˢ = Aˢ S.[ σˢ ]T}{A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ})

postulate
  Id :
    ∀ {i}{Γˢ : S.Con i}   {Γ : Con Γˢ}
      {j}{Aˢ : S.Ty Γˢ j} (A : Ty Γ Aˢ)
         {uˢ : S.Tm Γˢ Aˢ}(u : Tm Γ A uˢ)
         {vˢ : S.Tm Γˢ Aˢ}(v : Tm Γ A vˢ) →
      Ty Γ {j} (S.Id Aˢ uˢ vˢ)

  Id[] :
   {i : Level} {Γˢ : S.Con i}     {Γ : Con Γˢ}
   {j : Level} {Aˢ : S.Ty Γˢ j}   {A : Ty Γ Aˢ}
               {uˢ : S.Tm Γˢ Aˢ}  {u : Tm Γ A uˢ}
               {vˢ : S.Tm Γˢ Aˢ}  {v : Tm Γ A vˢ}
   {k : Level} {θˢ : S.Con k}     {θ : Con θˢ}
               {σˢ : S.Sub θˢ Γˢ} {σ : Sub θ Γ σˢ}
   → Id A u v [ σ ]T ≡ Id (A [ σ ]T) (u [ σ ]t) (v [ σ ]t)
{-# REWRITE Id[] #-}

postulate
  refl :
    ∀ {i}{Γˢ : S.Con i}    {Γ : Con Γˢ}
      {j}{Aˢ : S.Ty Γˢ j}  {A : Ty Γ Aˢ}
         {uˢ : S.Tm Γˢ Aˢ} (u : Tm Γ A uˢ) →
      Tm Γ (Id A {uˢ = uˢ} u {vˢ = uˢ} u) (S.refl uˢ)

  refl[] :
    ∀ {i}{Γˢ : S.Con i}    {Γ : Con Γˢ}
      {j}{Δˢ : S.Con j}    {Δ : Con Δˢ}
         {σˢ : S.Sub Γˢ Δˢ}{σ : Sub Γ Δ σˢ}
      {k}{Aˢ : S.Ty Δˢ k}  {A : Ty Δ Aˢ}
         {tˢ : S.Tm Δˢ Aˢ} {t : Tm Δ A tˢ}
    → refl t [ σ ]t ≡ refl (t [ σ ]t)
{-# REWRITE refl[] #-}

postulate
  J :
    ∀ {i}{Γˢ : S.Con i}    {Γ : Con Γˢ}
      {j}{Aˢ : S.Ty Γˢ j}  {A : Ty Γ Aˢ}
         {uˢ : S.Tm Γˢ Aˢ} {u : Tm Γ A uˢ}
      {k}
        {Cˢ : S.Ty (Γˢ S.▷ Aˢ S.▷ S.Id (Aˢ S.[ S.p ]T) (uˢ S.[ S.p ]t) S.v⁰) k}
          (C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) Cˢ )
      {wˢ : S.Tm Γˢ (Cˢ S.[ S.id S., uˢ S., S.refl uˢ ]T)}
        (w : Tm Γ (C [ id , u , refl u ]T) wˢ)

      {vˢ : S.Tm Γˢ Aˢ}              {v : Tm Γ A vˢ}
      {tˢ : S.Tm Γˢ (S.Id Aˢ uˢ vˢ)} (t : Tm Γ (Id A u v) tˢ)
    → Tm Γ (C [ id , v , t ]T) (S.J Cˢ wˢ tˢ)

  Idβ :
    ∀{i}{Γˢ : S.Con i}    {Γ : Con Γˢ}
     {j}{Aˢ : S.Ty Γˢ j}  {A : Ty Γ Aˢ}
        {uˢ : S.Tm Γˢ Aˢ} {u : Tm Γ A uˢ}
     {k}
       {Cˢ : S.Ty (Γˢ S.▷ Aˢ S.▷ S.Id (Aˢ S.[ S.p ]T) (uˢ S.[ S.p ]t) S.v⁰) k}
         {C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) Cˢ}
     {wˢ : S.Tm Γˢ (Cˢ S.[ S.id S., uˢ S., S.refl uˢ ]T)}
       {w : Tm Γ (C [ id , u , refl u ]T) wˢ}
     →
     J C w (refl u) ≡ w
