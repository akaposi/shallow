{-# OPTIONS --no-pattern-matching --prop #-}

module WrappedAnticatStandard.Props where

-- The "wrapped" anticategorified standard model

open import Agda.Primitive
import Standard.Lib as m
import Standard.Lib.Props as m
open import WrappedAnticatStandard public
open import WrappedAnticatStandard.Lib
open import WrappedAnticatStandard.Props.Lib
import Lib as l

infixl 7 _[_]TP
infixl 8 _[_]tP
infixl 5 _▷P_
infixl 5 _,P_
infixl 6 _,ΣP_

-- substitution calculus

TyP : ∀{i}(Γ : Con i)(j : Level) → Set (i ⊔ lsuc j)
TyP Γ j = Wrap 4 (∣ Γ ∣ _ → Prop j)

Lift : ∀{i}{Γ : Con i}{j : Level}(A : TyP Γ j) → Ty Γ j
Lift A = mk λ γ → LiftP (∣ A ∣ γ)

TmP : ∀{i}(Γ : Con i){j}(A : TyP Γ j) → Set (i ⊔ j)
TmP Γ A = Tm Γ (Lift A)

irr : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{u v : TmP Γ A} → u l.≡ v
irr = l.refl

_[_]TP : ∀ {i}{Δ : Con i}{j}(A : TyP Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → TyP Γ j
A [ σ ]TP = mk λ γ → ∣ A ∣ (∣ σ ∣ γ)

_[_]tP : ∀{i}{Δ : Con i}{j}{A : TyP Δ j}(t : TmP Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → TmP Γ (A [ σ ]TP)
_[_]tP = _[_]t

_▷P_   : ∀ {i}(Γ : Con i){j}(A : TyP Γ j) → Con (i ⊔ j)
Γ ▷P A = Γ ▷ Lift A

_,P_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : TyP Δ k}(t : TmP Γ (A [ σ ]TP)) → Sub Γ (Δ ▷P A)
σ ,P t = σ , t
-- _,P_ = _,_  -- TODO report Agda Bug

pP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → Sub (Γ ▷P A) Γ
pP = p

qP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → TmP (Γ ▷P A) (A [ pP ]TP)
qP = q

⊤P : ∀{i}{Γ : Con i} → TyP Γ lzero
⊤P = mk λ _ → m.⊤P

ttP : ∀{i}{Γ : Con i} → TmP Γ ⊤P
ttP = mk λ _ → liftP m.ttP

-- ΣP

ΣP : {i j k : Level}{Γ : Con i}(A : TyP Γ j)(B : TyP (Γ ▷P A) k) → TyP Γ (j ⊔ k)
ΣP A B = mk λ γ → m.ΣP ((∣ A ∣ γ)) λ α → ∣ B ∣ (γ m.,Σ liftP α)

_,ΣP_ : {i j k : Level}{Γ : Con i}{A : TyP Γ j}{B : TyP (Γ ▷P A) k}(u : TmP Γ A)(v : TmP Γ (B [ id ,P u ]TP)) → TmP Γ (ΣP A B)
u ,ΣP v = mk λ γ → liftP (unliftP (∣ u ∣ γ) m.,ΣP unliftP (∣ v ∣ γ))

fstP : {i j k : Level}{Γ : Con i}{A : TyP Γ j}{B : TyP (Γ ▷P A) k} → TmP Γ (ΣP A B) → TmP Γ A
fstP t = mk λ γ → liftP (m.fstP (unliftP (∣ t ∣ γ)))

sndP : {i j k : Level}{Γ : Con i}{A : TyP Γ j}{B : TyP (Γ ▷P A) k}(t : TmP Γ (ΣP A B)) → TmP Γ (B [ id ,P fstP t ]TP)
sndP t = mk λ γ → liftP (m.sndP (unliftP (∣ t ∣ γ)))

-- ΣSP

ΣSP : {i j k : Level}{Γ : Con i}(A : Ty Γ j)(B : TyP (Γ ▷ A) k) → Ty Γ (j ⊔ k)
ΣSP A B = Σ A (Lift B)

_,ΣSP_ : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}(u : Tm Γ A)(v : TmP Γ (B [ id , u ]TP)) → Tm Γ (ΣSP A B)
_,ΣSP_ = _,Σ_

fstSP : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k} → Tm Γ (ΣSP A B) → Tm Γ A
fstSP = fst

sndSP : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}(t : Tm Γ (ΣSP A B)) → TmP Γ (B [ id , fstSP t ]TP)
sndSP = snd