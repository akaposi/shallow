module WrappedAnticatStandard.Lib where

open import Agda.Primitive

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}

-- wrappers

record Wrap (n : ℕ){i}(A : Set i) : Set i where
  constructor mk
  field
    ∣_∣ : A
open Wrap public
