{-# OPTIONS --prop #-}

module WrappedAnticatStandard.Props.Lib where

open import Agda.Primitive
open import WrappedAnticatStandard.Lib

record WrapP (n : ℕ){i}(A : Prop i) : Prop i where
  constructor mk
  field
    ∣_∣ : A
open WrapP public

record LiftP {ℓ}(P : Prop ℓ) : Set ℓ where
  constructor liftP
  field unliftP : P
open LiftP public
