
module Definability where

open import Agda.Primitive

open import Lib using (coe; ap; tr; _≡_; _⁻¹; _◾_; _×_)
import Lib as m
import WrappedStandard as S
-- import Standard as SET
import StandardInterpretation as SI

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixr 6 _⇒_
infixl 6 _,Σ_

-- substitution calculus

record Con {i}(Γˢ : S.Con i) : Setω where
  field
    PC    : ∀{j}{Ωˢ : S.Con j}(f : SI.Con Ωˢ → SI.Con Γˢ) → Set (i ⊔ j)
    reifC : ∀{j}{Ωˢ : S.Con j}{f : SI.Con Ωˢ → SI.Con Γˢ} → PC f → m.Σ (S.Sub Ωˢ Γˢ) λ ρˢ → SI.Sub ρˢ m.≡ f
    reflC : ∀{j}{Ωˢ : S.Con j}(ρˢ : S.Sub Ωˢ Γˢ)(f : SI.Con Ωˢ → SI.Con Γˢ) → SI.Sub ρˢ m.≡ f → PC f
open Con

record Ty {j}{Γˢ : S.Con j}(Γ : Con Γˢ){i}(Aˢ : S.Ty Γˢ i) : Setω where
  field
    PA    : ∀{j}{Ωˢ : S.Con j}{f : SI.Con Ωˢ → SI.Con Γˢ}(f̂ : PC Γ f)(g : (γ : SI.Con Ωˢ) → SI.Ty Aˢ (f γ)) → Set (i ⊔ j)
--    reifA : ∀{j}{Ωˢ : S.Con j}{f : SI.Con Ωˢ → SI.Con Γˢ}{f̂ : PC Γ f}{g : (γ : SI.Con Ωˢ) → SI.Ty Aˢ (f γ)} → PA f̂ g → m.Σ (S.Tm Ωˢ (Aˢ S.[ m.fst (reifC Γ f̂) ]T)) λ tˢ → SI.Tm tˢ m.≡ {!g!}
{-
Sub   : ∀ {i}{Γˢ : S.Con i}(Γ : Con Γˢ)
          {j}{Δˢ : S.Con j}(Δ : Con Δˢ)
          → S.Sub Γˢ Δˢ → Set (i ⊔ j)
Sub {i}{Γˢ} Γ {j}{Δˢ} Δ σˢ = {ρˢ : S.Sub S.∙ Γˢ}(ρ̂ : Γ ρˢ) → Δ (σˢ S.∘ ρˢ)

Tm  : ∀ {i}{Γˢ : S.Con i}(Γ : Con Γˢ)
          {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ Aˢ) → S.Tm Γˢ Aˢ → Set (i ⊔ j)
Tm {i}{Γˢ} Γ {j}{Aˢ} A tˢ = {ρˢ : S.Sub S.∙ Γˢ}(ρ̂ : Γ ρˢ) → A ρ̂ (tˢ S.[ ρˢ ]t)

id : ∀{i}{Γˢ : S.Con i}{Γ : Con Γˢ} → Sub Γ Γ S.id
id ρ̂ = ρ̂

_∘_ :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} (σ : Sub Θ Δ σˢ)
   {k}{Γˢ : S.Con k}     {Γ : Con Γˢ}
      {δˢ : S.Sub Γˢ Θˢ} (δ : Sub Γ Θ δˢ)
  → Sub Γ Δ (σˢ S.∘ δˢ)
(σ ∘ δ) ρ̂ = σ (δ ρ̂)

ass :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
   {k}{Ξˢ : S.Con k}     {Ξ : Con Ξˢ}
      {δˢ : S.Sub Ξˢ Θˢ} {δ : Sub Ξ Θ δˢ}
   {l}{Γˢ : S.Con l}     {Γ : Con Γˢ}
      {νˢ : S.Sub Γˢ Ξˢ} {ν : Sub Γ Ξ νˢ}
   → m._≡_ {A = Sub Γ Δ ((σˢ S.∘ δˢ) S.∘ νˢ)}
           (_∘_ {Θ = Ξ}{Δ = Δ}{σˢ = σˢ S.∘ δˢ}(_∘_ {Θ = Θ}{Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ} δ){δˢ = νˢ} ν)
           (_∘_ {Θ = Θ}{Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ S.∘ νˢ}(_∘_ {Θ = Ξ}{Δ = Θ}{σˢ = δˢ} δ {δˢ = νˢ} ν))
ass = m.refl

idl :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
  → m._≡_ {A = Sub Γ Δ σˢ} (_∘_ {Θ = Δ}{Δ = Δ}{σˢ = S.id}(id {Γ = Δ}){δˢ = σˢ} σ) σ
idl = m.refl

idr :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
  → m._≡_ {A = Sub Γ Δ σˢ} (_∘_ {Θ = Γ}{Δ = Δ}{σˢ = σˢ} σ {δˢ = S.id} (id {Γˢ = Γˢ}{Γ = Γ})) σ
idr = m.refl

_[_]T :
  ∀ {i}{Δˢ : S.Con i}   {Δ : Con Δˢ}
    {j}{Aˢ : S.Ty Δˢ j} (A : Ty Δ Aˢ)
    {k}{Γˢ : S.Con k}   {Γ : Con Γˢ}
    {σˢ : S.Sub Γˢ Δˢ}  (σ : Sub Γ Δ σˢ)
    → Ty Γ (Aˢ S.[ σˢ ]T)
(A [ σ ]T) ρ̂ tˢ = A (σ ρ̂) tˢ

_[_]t :
  ∀{i}{Δˢ : S.Con i}     {Δ : Con Δˢ}
   {j}{Aˢ : S.Ty Δˢ j}   {A : Ty Δ Aˢ}
      {tˢ : S.Tm Δˢ Aˢ}  (t : Tm Δ A tˢ)
   {k}{Γˢ : S.Con k}     {Γ : Con Γˢ}
      {σˢ : S.Sub Γˢ Δˢ} (σ : Sub Γ Δ σˢ)
  → Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) (tˢ S.[ σˢ ]t)
(t [ σ ]t) ρ̂ = t (σ ρ̂)

[id]T :
  ∀{i}{Γˢ : S.Con i}   {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j} {A : Ty Γ Aˢ}
  → m._≡_ {A = Ty Γ Aˢ} (_[_]T {Aˢ = Aˢ} A {σˢ = S.id} (id {Γ = Γ})) A
[id]T = m.refl

[∘]T :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
   {k}{Γˢ : S.Con k}     {Γ : Con Γˢ}
      {δˢ : S.Sub Γˢ Θˢ} {δ : Sub Γ Θ δˢ}
   {l}{Aˢ : S.Ty Δˢ l}   {A : Ty Δ Aˢ}
   → m._≡_ {A = Ty Γ (Aˢ S.[ σˢ S.∘ δˢ ]T)}
          (_[_]T {Aˢ = Aˢ S.[ σˢ ]T} (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) {σˢ = δˢ} δ)
          (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ S.∘ δˢ} (_∘_ {Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ} δ))
[∘]T = m.refl

[id]t :
  ∀{i}{Γˢ : S.Con i}   {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j} {A : Ty Γ Aˢ}
   {tˢ : S.Tm Γˢ Aˢ}{t : Tm Γ A tˢ}
  → m._≡_ {A = Tm Γ A tˢ}
          (_[_]t {A = A} {tˢ = tˢ} t {σˢ = S.id} (id {Γ = Γ}))
          t
[id]t = m.refl

[∘]t :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
   {k}{Γˢ : S.Con k}     {Γ : Con Γˢ}
      {δˢ : S.Sub Γˢ Θˢ} {δ : Sub Γ Θ δˢ}
   {l}{Aˢ : S.Ty Δˢ l}   {A : Ty Δ Aˢ}
   {tˢ : S.Tm Δˢ Aˢ}{t : Tm Δ A tˢ}
   → m._≡_ {A = Tm Γ (_[_]T {Aˢ = Aˢ S.[ σˢ ]T}(_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) {σˢ = δˢ} δ) (tˢ S.[ σˢ ]t S.[ δˢ ]t)}
          (_[_]t {Aˢ = Aˢ S.[ σˢ ]T}{A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}
                 {tˢ = tˢ S.[ σˢ ]t}(_[_]t {Aˢ = Aˢ}{A = A} {tˢ = tˢ} t {σˢ = σˢ} σ)
                 {σˢ = δˢ} δ)
          (_[_]t {Aˢ = Aˢ}{A = A}
                 {tˢ = tˢ} t
                 {σˢ = σˢ S.∘ δˢ} (_∘_ {Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ} δ))
[∘]t = m.refl

∙ : Con S.∙
∙ ρˢ = m.⊤

ε : ∀{i}{Γˢ : S.Con i}{Γ : Con Γˢ}
    → Sub Γ ∙ S.ε
ε ρ̂ = m.tt

•η :
   ∀  {i}{Γˢ : S.Con i}      {Γ : Con Γˢ}
         {σˢ : S.Sub Γˢ S.∙} {σ : Sub Γ ∙ σˢ}
   → m._≡_ {A = Sub Γ ∙ σˢ} σ (ε {Γ = Γ})
•η = m.refl

_▷_   : ∀ {i}{Γˢ : S.Con i}(Γ : Con Γˢ)
          {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ Aˢ)
          → Con (Γˢ S.▷ Aˢ)
(Γ ▷ A) ρˢ = m.Σ (Γ (S.p S.∘ ρˢ)) λ ρ̂ → A ρ̂ (S.q S.[ ρˢ ]t)

_,_ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            (σ : Sub Γ Δ σˢ)
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} (t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ)
   → Sub Γ (_▷_ Δ {Aˢ = Aˢ} A) (σˢ S., tˢ)
(σ , t) ρ̂ = σ ρ̂ m.,Σ t ρ̂

p :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ Aˢ} →
   Sub (_▷_ Γ {Aˢ = Aˢ} A) Γ S.p
p ρ̂ = m.fst ρ̂

q :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ Aˢ} →
   Tm (_▷_ Γ {Aˢ = Aˢ} A)
      {Aˢ = Aˢ S.[ S.p {A = Aˢ} ]T}
      (_[_]T {Aˢ = Aˢ} A {Γ = _▷_ Γ {Aˢ = Aˢ} A}{σˢ = S.p} (p {Aˢ = Aˢ}{A = A}))
      S.q
q ρ̂ = m.snd ρ̂

▷β₁ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ}
  → m._≡_ {A = Sub Γ Δ σˢ}
          (_∘_ {Θ = _▷_ Δ {Aˢ = Aˢ} A}{Δ = Δ}
               {σˢ = S.p}(p {Aˢ = Aˢ}{A = A})
               {δˢ = σˢ S., tˢ}(_,_ σ {Aˢ = Aˢ}{A = A}{tˢ = tˢ} t))
          σ
▷β₁ = m.refl

▷β₂ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ}
  → m._≡_ {A = Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ}
          (_[_]t {A = _[_]T {Aˢ = Aˢ} A {Γ = _▷_ Δ {Aˢ = Aˢ} A} {σˢ = S.p {A = Aˢ}} (p {Aˢ = Aˢ}{A = A})}
                 {tˢ = S.q {A = Aˢ}}(q {Aˢ = Aˢ}{A = A})
                 {σˢ = σˢ S., tˢ}(_,_ σ {Aˢ = Aˢ}{A = A}{tˢ = tˢ} t))
          t
▷β₂ = m.refl

▷η :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ Aˢ}
  → m._≡_ {A = Sub (_▷_ Γ {Aˢ = Aˢ} A) (_▷_ Γ {Aˢ = Aˢ} A) (S.p S., S.q)}
          (_,_ {σˢ = S.p}(p {Aˢ = Aˢ}{A = A}){Aˢ = Aˢ}{A = A}{tˢ = S.q}(q {Aˢ = Aˢ}{A = A}))
          (id {Γ = _▷_ Γ {Aˢ = Aˢ} A})
▷η = m.refl

,∘ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ}
   {l}{Θˢ : S.Con l}                {Θ : Con Θˢ}
      {δˢ : S.Sub Θˢ Γˢ}            {δ : Sub Θ Γ δˢ} →
  m._≡_ {A = Sub Θ (_▷_ Δ {Aˢ = Aˢ} A) ((σˢ S., tˢ) S.∘ δˢ)}
        (_∘_ {Θ = Γ}{Δˢ = Δˢ S.▷ Aˢ}{Δ = Δ ▷ A}{σˢ = σˢ S., tˢ}(_,_ {σˢ = σˢ} σ {Aˢ = Aˢ}{A = A}{tˢ = tˢ} t){δˢ = δˢ} δ)
        (_,_ (_∘_ {Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ} δ){Aˢ = Aˢ}{A = A}{tˢ = tˢ S.[ δˢ ]t}(_[_]t {A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}{tˢ = tˢ} t {σˢ = δˢ} δ))
,∘ = m.refl

-- abbreviations

p² :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ} →
   Sub (_▷_ (_▷_ Γ {Aˢ = Aˢ} A){Aˢ = Bˢ} B) Γ S.p²
p² {Γˢ = Γˢ}{Γ}{Aˢ = Aˢ}{A}{Bˢ = Bˢ}{B} =
  _∘_ {Θˢ = Γˢ S.▷ Aˢ}{Θ = _▷_ Γ {Aˢ = Aˢ} A}{Δˢ = Γˢ}{Δ = Γ}
      {σˢ = S.p {Γ = Γˢ}{A = Aˢ}}(p {Γˢ = Γˢ}{Γ}{Aˢ = Aˢ}{A})
      {Γˢ = Γˢ S.▷ Aˢ S.▷ Bˢ}{Γ = _▷_ (_▷_ Γ {Aˢ = Aˢ} A){Aˢ = Bˢ} B}
      {δˢ = S.p {Γ = Γˢ S.▷ Aˢ}{A = Bˢ}}(p {Γˢ = Γˢ S.▷ Aˢ}{Γ = _▷_ Γ {Aˢ = Aˢ} A}{Aˢ = Bˢ}{A = B})

v⁰ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ Aˢ}
  → Tm (_▷_ Γ {Aˢ = Aˢ} A)
       ((_[_]T {Aˢ = Aˢ} A {Γ = _▷_ Γ {Aˢ = Aˢ} A}{σˢ = S.p {A = Aˢ}}
       (p {Γ = Γ}{Aˢ = Aˢ}{A = A}))) S.v⁰
v⁰ {Γˢ = Γˢ}{Γ}{Aˢ = Aˢ}{A} = q {Aˢ = Aˢ}{A = A}

_^_ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            (σ : Sub Γ Δ σˢ)
   {k}{Aˢ : S.Ty Δˢ k}              (A : Ty Δ Aˢ)
  → Sub (Γ ▷ _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) (Δ ▷ A) (σˢ S.^ Aˢ)
_^_ {i} {Γˢ} {Γ} {j} {Δˢ} {Δ} {σˢ} σ {k} {Aˢ} A =
  _,_ {Γˢ = Γˢ S.▷ Aˢ S.[ σˢ ]T}{_▷_ Γ {Aˢ = Aˢ S.[ σˢ ]T}(_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ)}
      {σˢ = σˢ S.∘ S.p}(_∘_ {Θˢ = Γˢ}{Θ = Γ}{Δˢ = Δˢ}{Δ = Δ}{σˢ = σˢ} σ
                             {Γˢ = Γˢ S.▷ Aˢ S.[ σˢ ]T}{Γ ▷ _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}
                             {δˢ = S.p {Γ = Γˢ}{A = Aˢ S.[ σˢ ]T}}
                             (p {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ S.[ σˢ ]T}{A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}))
      {Aˢ = Aˢ}{A = A}
      {tˢ = S.v⁰}(v⁰ {Γˢ = Γˢ}{Γ}{Aˢ = Aˢ S.[ σˢ ]T}{A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ})

-- Π
--------------------------------------------------------------------------------

Π :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          (A : Ty Γ Aˢ)
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} (B : Ty (Γ ▷ A) Bˢ)
   → Ty Γ (S.Π Aˢ Bˢ)
(Π {Aˢ = Aˢ} A {Bˢ = Bˢ} B){ρˢ = ρˢ} ρ̂ tˢ =
  {uˢ : S.Tm S.∙ (Aˢ S.[ ρˢ ]T)}(û : A ρ̂ uˢ) → B (ρ̂ m.,Σ û) (tˢ S.$ uˢ)

lam :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ}
      {tˢ : S.Tm (Γˢ S.▷ Aˢ) Bˢ}(t : Tm (Γ ▷ A) B tˢ)
  → Tm Γ (Π {Aˢ = Aˢ} A {Bˢ = Bˢ} B) (S.lam tˢ)
(lam t) ρ̂ û = t (ρ̂ m.,Σ û)

app :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ}
      {tˢ : S.Tm Γˢ (S.Π Aˢ Bˢ)}(t : Tm Γ (Π {Aˢ = Aˢ} A {Bˢ = Bˢ} B) tˢ)
  → Tm (Γ ▷ A) B (S.app tˢ)
(app t) ρ̂ = t (m.fst ρ̂) (m.snd ρ̂)

Πβ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ}
      {tˢ : S.Tm (Γˢ S.▷ Aˢ) Bˢ}{t : Tm (Γ ▷ A) B tˢ}
  → m._≡_ {A = Tm (Γ ▷ A) B tˢ}
         (app {Bˢ = Bˢ}{B}{tˢ = S.lam tˢ}(lam {Aˢ = Aˢ}{A}{Bˢ = Bˢ}{B}{tˢ = tˢ} t))
         t
Πβ = m.refl

Πη :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ}
      {tˢ : S.Tm Γˢ (S.Π Aˢ Bˢ)}{t : Tm Γ (Π A {Bˢ = Bˢ} B) tˢ}
  → m._≡_ {A = Tm Γ (Π A {Bˢ = Bˢ} B) tˢ}
         (lam {Aˢ = Aˢ}{A}{Bˢ = Bˢ}{B}{tˢ = S.app tˢ}(app {Aˢ = Aˢ}{A}{Bˢ = Bˢ}{B}{tˢ = tˢ} t))
         t
Πη = m.refl

Π[] :
  ∀{i}{Γˢ : S.Con i}           {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}         {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k}{B : Ty (Γ ▷ A) Bˢ}
   {l}{Θˢ : S.Con l}           {Θ : Con Θˢ}
      {σˢ : S.Sub Θˢ Γˢ}       {σ : Sub Θ Γ σˢ}
  → m._≡_ {A = Ty Θ (S.Π Aˢ Bˢ S.[ σˢ ]T)}
         (_[_]T {Δ = Γ}{Aˢ = S.Π Aˢ Bˢ}(Π A {Bˢ = Bˢ} B){σˢ = σˢ} σ)
         (Π (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ)
            {Bˢ = Bˢ S.[ σˢ S.^ Aˢ ]T}
            (_[_]T {Aˢ = Bˢ} B {σˢ = σˢ S.^ Aˢ}(_^_ {σˢ = σˢ} σ {Aˢ = Aˢ} A)))
Π[] = m.refl

lam[] :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {l}{Δˢ : S.Con l}            {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}        {σ : Sub Γ Δ σˢ}
   {j}{Aˢ : S.Ty Δˢ j}          {A : Ty Δ Aˢ}
   {k}{Bˢ : S.Ty (Δˢ S.▷ Aˢ) k} {B : Ty (Δ ▷ A) Bˢ}
      {tˢ : S.Tm (Δˢ S.▷ Aˢ) Bˢ}{t : Tm (_▷_ Δ {Aˢ = Aˢ} A) {Aˢ = Bˢ} B tˢ}
  → m._≡_ {A = Tm Γ
                  {Aˢ = S.Π Aˢ Bˢ S.[ σˢ ]T}
                  (_[_]T {Aˢ = S.Π Aˢ Bˢ} (Π {Aˢ = Aˢ} A {Bˢ = Bˢ} B) {σˢ = σˢ} σ)
                  (S.lam tˢ S.[ σˢ ]t)}
         (_[_]t {A = Π {Aˢ = Aˢ} A {Bˢ = Bˢ} B}{tˢ = S.lam tˢ}(lam {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{tˢ = tˢ} t){σˢ = σˢ} σ)
         (lam {Aˢ = Aˢ S.[ σˢ ]T}
              {_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}
              {Bˢ = Bˢ S.[ σˢ S.^ Aˢ ]T}
              {_[_]T {Aˢ = Bˢ} B {σˢ = σˢ S.^ Aˢ}(_^_ {σˢ = σˢ} σ {Aˢ = Aˢ} A)}
              {tˢ = tˢ S.[ σˢ S.^ Aˢ ]t}
              (_[_]t {Aˢ = Bˢ}{A = B}{tˢ = tˢ} t {σˢ = σˢ S.^ Aˢ} (_^_ {σˢ = σˢ} σ {Aˢ = Aˢ} A)))
lam[] = m.refl

-- abbreviations

_⇒_ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          (A : Ty Γ Aˢ)
   {k}{Bˢ : S.Ty Γˢ k}          (B : Ty Γ Bˢ) →
   Ty Γ (Aˢ S.⇒ Bˢ)
_⇒_ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B =
  Π {Aˢ = Aˢ} A
    {Bˢ = Bˢ S.[ S.p {A = Aˢ} ]T}
    (_[_]T {Aˢ = Bˢ} B
           {Γˢ = Γˢ S.▷ Aˢ}{_▷_ Γ {Aˢ = Aˢ} A}
           {σˢ = S.p {A = Aˢ}} (p {Aˢ = Aˢ}{A = A}))

-- Σ
--------------------------------------------------------------------------------

Σ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          (A : Ty Γ Aˢ)
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} (B : Ty (Γ ▷ A) Bˢ)
   → Ty Γ (S.Σ Aˢ Bˢ)
(Σ A B) ρ̂ tˢ = m.Σ (A ρ̂ (S.fst tˢ)) λ û → B (ρ̂ m.,Σ û) (S.snd tˢ)


_,Σ_ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}(u : Tm Γ A uˢ)
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      (v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aˢ = Bˢ} B {σˢ = S.id S., uˢ}(_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = uˢ} u)) vˢ)
   → Tm Γ (Σ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B) (S._,Σ_ {B = Bˢ} uˢ vˢ)
(u ,Σ v) ρ̂ = u ρ̂ m.,Σ v ρ̂

fst :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ}
      {tˢ : S.Tm Γˢ (S.Σ Aˢ Bˢ)}(t : Tm Γ (Σ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B) tˢ)
   → Tm Γ A (S.fst tˢ)
(fst t) ρ̂ = m.fst (t ρ̂)

snd :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ}
      {tˢ : S.Tm Γˢ (S.Σ Aˢ Bˢ)}(t : Tm Γ (Σ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B) tˢ)
   → Tm Γ (_[_]T {Δ = Γ ▷ A}
                 {Aˢ = Bˢ}
                 B {σˢ = S.id S., S.fst tˢ}
                 (_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = S.fst tˢ} (fst {A = A}{Bˢ = Bˢ}{B = B}{tˢ = tˢ} t)))
          (S.snd tˢ)
(snd t) ρ̂ = m.snd (t ρ̂)

Σβ₁ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      {v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aˢ = Bˢ} B {σˢ = S.id S., uˢ}(_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = uˢ} u)) vˢ} →
  m._≡_ {A = Tm Γ A uˢ}
        (fst {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{tˢ = uˢ S.,Σ vˢ}
             (_,Σ_ {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{uˢ = uˢ} u {vˢ = vˢ} v))
        u
Σβ₁ = m.refl

Σβ₂ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      {v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aˢ = Bˢ} B {σˢ = S.id S., uˢ}(_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = uˢ} u)) vˢ} →
  m._≡_ {A = Tm Γ (_[_]T {Δ = Γ ▷ A}{Aˢ = Bˢ} B {σˢ = S.id S., uˢ}(_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = uˢ} u)) vˢ}
        (snd {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{tˢ = uˢ S.,Σ vˢ}
             (_,Σ_ {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{uˢ = uˢ} u {vˢ = vˢ} v))
        v
Σβ₂ = m.refl

Ση :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ}
      {tˢ : S.Tm Γˢ (S.Σ Aˢ Bˢ)}{t : Tm Γ (Σ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B) tˢ} →
  m._≡_ {A = Tm Γ (Σ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B) tˢ}
        (_,Σ_ {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}
              {uˢ = S.fst tˢ}(fst {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{tˢ = tˢ} t)
              {vˢ = S.snd tˢ}(snd {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{tˢ = tˢ} t))
        t
Ση = m.refl

Σ[] :
  ∀{i}{Γˢ : S.Con i}           {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}         {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k}{B : Ty (Γ ▷ A) Bˢ}
   {l}{Θˢ : S.Con l}           {Θ : Con Θˢ}
      {σˢ : S.Sub Θˢ Γˢ}       {σ : Sub Θ Γ σˢ}
  → m._≡_ {A = Ty Θ (S.Σ Aˢ Bˢ S.[ σˢ ]T)}
         (_[_]T {Δ = Γ}{Aˢ = S.Σ Aˢ Bˢ}(Σ A {Bˢ = Bˢ} B){σˢ = σˢ} σ)
         (Σ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ)
            {Bˢ = Bˢ S.[ σˢ S.^ Aˢ ]T}
            (_[_]T {Aˢ = Bˢ} B {σˢ = σˢ S.^ Aˢ}(_^_ {σˢ = σˢ} σ {Aˢ = Aˢ} A)))
Σ[] = m.refl

,Σ[] :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      {v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aˢ = Bˢ} B {σˢ = S.id S., uˢ}(_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = uˢ} u)) vˢ}
   {l}{Θˢ : S.Con l}            {Θ : Con Θˢ}
      {σˢ : S.Sub Θˢ Γˢ}        {σ : Sub Θ Γ σˢ} →
  m._≡_ {A = Tm Θ
                {Aˢ = S.Σ Aˢ Bˢ S.[ σˢ ]T}
                (_[_]T {Aˢ = S.Σ Aˢ Bˢ} (Σ {Aˢ = Aˢ} A {Bˢ = Bˢ} B) {σˢ = σˢ} σ)
                ((S._,Σ_ {A = Aˢ}{B = Bˢ} uˢ vˢ) S.[ σˢ ]t)}
        (_[_]t {Aˢ = S.Σ Aˢ Bˢ}{A = Σ {Aˢ = Aˢ} A {Bˢ = Bˢ} B}{tˢ = S._,Σ_ {A = Aˢ}{B = Bˢ} uˢ vˢ}(_,Σ_ {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{uˢ = uˢ} u {vˢ = vˢ} v) {σˢ = σˢ} σ)
        (_,Σ_ {Aˢ = Aˢ S.[ σˢ ]T}
              {A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}
              {Bˢ = Bˢ S.[ σˢ S.^ Aˢ ]T}
              {B = _[_]T {Aˢ = Bˢ} B {σˢ = σˢ S.^ Aˢ} (_^_ {σˢ = σˢ} σ {Aˢ = Aˢ} A)}
              {uˢ = uˢ S.[ σˢ ]t}
              (_[_]t {Aˢ = Aˢ}{A = A}{tˢ = uˢ} u {σˢ = σˢ} σ)
              {vˢ = vˢ S.[ σˢ ]t}
              (_[_]t {Aˢ = Bˢ S.[ S._,_ S.id {A = Aˢ} uˢ ]T}{A = _[_]T {Aˢ = Bˢ} B {σˢ = S.id S., uˢ} (_,_ (id {Γˢ = Γˢ}{Γ = Γ}){Aˢ = Aˢ}{A = A}{tˢ = uˢ} u)}{tˢ = vˢ} v {σˢ = σˢ} σ))
,Σ[] = m.refl

-- unit
--------------------------------------------------------------------------------

⊤ : ∀ {i}{Γˢ : S.Con i}{Γ : Con Γˢ} → Ty Γ S.⊤
⊤ ρ̂ tˢ = m.⊤

tt : ∀ {i}{Γˢ : S.Con i}{Γ : Con Γˢ} → Tm Γ (⊤ {Γ = Γ}) S.tt
tt ρ̂ = m.tt

⊤η : ∀{i}{Γˢ : S.Con i}{Γ : Con Γˢ}{tˢ : S.Tm Γˢ S.⊤}{t : Tm Γ (⊤ {Γˢ = Γˢ}{Γ = Γ}) tˢ} →
  m._≡_ {A = Tm Γ (⊤ {Γˢ = Γˢ}{Γ = Γ}) tˢ} t (tt {Γˢ = Γˢ}{Γ = Γ})
⊤η = m.refl

⊤[] : ∀{i}{Γˢ : S.Con i}{Γ : Con Γˢ}{j}{Δˢ : S.Con j}{Δ : Con Δˢ}{σˢ : S.Sub Γˢ Δˢ}{σ : Sub Γ Δ σˢ} →
  m._≡_ {A = Ty Γ S.⊤} (_[_]T {Δ = Δ}{Aˢ = S.⊤}(⊤ {Γˢ = Δˢ}{Γ = Δ}){σˢ = σˢ} σ) (⊤ {Γˢ = Γˢ}{Γ = Γ})
⊤[] = m.refl

tt[] : ∀{i}{Γˢ : S.Con i}{Γ : Con Γˢ}{j}{Δˢ : S.Con j}{Δ : Con Δˢ}{σˢ : S.Sub Γˢ Δˢ}{σ : Sub Γ Δ σˢ} →
  m._≡_ {A = Tm Γ (⊤ {Γˢ = Γˢ}{Γ = Γ}) S.tt}
        (_[_]t {Δ = Δ}{Aˢ = S.⊤}{A = ⊤ {Γˢ = Δˢ}{Γ = Δ}}{tˢ = S.tt}(tt {Γˢ = Δˢ}{Γ = Δ}){σˢ = σˢ} σ)
        (tt {Γˢ = Γˢ}{Γ = Γ})
tt[] = m.refl

-- U
--------------------------------------------------------------------------------

U :
  ∀{i}{Γˢ : S.Con i}{Γ : Con Γˢ} j → Ty Γ (S.U j)
(U j) ρ̂ aˢ = S.Tm S.∙ (S.El aˢ) → Set j

El :
  ∀{i}{Γˢ : S.Con i}         {Γ : Con Γˢ}
   {j}{aˢ : S.Tm Γˢ (S.U j)} (a : Tm Γ (U {Γ = Γ} j) aˢ)
   → Ty Γ (S.El aˢ)
(El a) ρ̂ tˢ = a ρ̂ tˢ

c :
  ∀{i}{Γˢ : S.Con i}  {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ Aˢ)
  → Tm Γ (U {Γ = Γ} j) (S.c Aˢ)
(c A) ρ̂ = A ρ̂

Uβ :
  ∀{i}{Γˢ : S.Con i}   {Γ : Con Γˢ}
   {j}{Aˢ : S.Ty Γˢ j} {A : Ty Γ Aˢ}
   → m._≡_ {A = Ty Γ Aˢ}(El {aˢ = S.c Aˢ}(c {Aˢ = Aˢ} A)) A
Uβ = m.refl

Uη :
  ∀{i}{Γˢ : S.Con i} {Γ : Con Γˢ}
   {j}{aˢ : S.Tm Γˢ (S.U j)}{a : Tm Γ (U {Γ = Γ} j) aˢ}
   → m._≡_ {A = Tm Γ (U {Γ = Γ} j) aˢ}(c {Aˢ = S.El aˢ}(El {aˢ = aˢ} a)) a
Uη = m.refl

U[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   {k} → m._≡_ {A = Ty Γ (S.U k)}
              (_[_]T {Δ = Δ}{Aˢ = S.U k}(U {Γ = Δ} k){σˢ = σˢ} σ)
              (U {Γ = Γ} k)
U[] = m.refl

El[] :
  ∀{i}{Γˢ : S.Con i}        {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}        {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}    {σ : Sub Γ Δ σˢ}
   {k}{aˢ : S.Tm Δˢ (S.U k)}{a : Tm Δ (U {Γ = Δ} k) aˢ}
  → m._≡_ {A = Ty Γ (S.El aˢ S.[ σˢ ]T)}
         (_[_]T {Aˢ = S.El aˢ}(El {aˢ = aˢ} a){σˢ = σˢ} σ)
         (El {aˢ = aˢ S.[ σˢ ]t}(_[_]t {Aˢ = S.U k}{A = U {Γ = Δ} k}{tˢ = aˢ} a {σˢ = σˢ} σ))
El[] = m.refl


-- Bool
--------------------------------------------------------------------------------

Bool : ∀{i}{Γˢ : S.Con i}{Γ : Con Γˢ} → Ty Γ S.Bool
Bool ρ̂ tˢ = m.Σ m.Bool λ β → m.if _ S.true S.false β m.≡ tˢ

true :
  ∀{i}{Γˢ : S.Con i}{Γ : Con Γˢ}
  → Tm Γ (Bool {Γ = Γ}) S.true
true ρ̂ = m.true m.,Σ m.refl

false :
  ∀{i}{Γˢ : S.Con i}{Γ : Con Γˢ}
  → Tm Γ (Bool {Γ = Γ}) S.false
false ρ̂ = m.false m.,Σ m.refl

if :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {j}{Cˢ : S.Ty (Γˢ S.▷ S.Bool) j} (C : Ty (Γ ▷ Bool {Γ = Γ}) Cˢ)
      {uˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.true ]T)}
      (u : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.true}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.true} (true {Γ = Γ})))
                uˢ)
      {vˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.false ]T)}
      (v : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.false}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.false} (false {Γ = Γ})))
                vˢ)
      {tˢ : S.Tm Γˢ S.Bool}
      (t : Tm Γ (Bool {Γ = Γ}) tˢ)
  → Tm Γ
       (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
              {Aˢ = Cˢ} C
              {Γ = Γ}
              {σˢ = S.id S., tˢ}
              (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = tˢ} t))
       (S.if Cˢ uˢ vˢ tˢ)
if {Γ = Γ}{Cˢ = Cˢ} C {uˢ = uˢ} u {vˢ = vˢ} v {tˢ = tˢ} t {ρˢ = ρˢ} ρ̂ =
  m.J (λ {z} e → C (ρ̂ m.,Σ (m.fst (t ρ̂) m.,Σ e)) (S.if (Cˢ S.[ ρˢ S.^ _ ]T) (uˢ S.[ ρˢ ]t) (vˢ S.[ ρˢ ]t) z))
      (m.if (λ β → C (ρ̂ m.,Σ (β m.,Σ m.refl)) (S.if (Cˢ S.[ ρˢ S.^ _ ]T) (uˢ S.[ ρˢ ]t) (vˢ S.[ ρˢ ]t) (m.if _ S.true S.false β)))
            (u ρ̂)
            (v ρ̂)
            (m.fst (t ρ̂)))
      (m.snd (t ρ̂))

Boolβ₁ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {j}{Cˢ : S.Ty (Γˢ S.▷ S.Bool) j} {C : Ty (Γ ▷ Bool {Γ = Γ}) Cˢ}
      {uˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.true ]T)}
      {u : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.true}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.true} (true {Γ = Γ})))
                uˢ}
      {vˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.false ]T)}
      {v : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.false}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.false} (false {Γ = Γ})))
                vˢ}
   → m._≡_ {A = Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                           {Aˢ = Cˢ} C
                           {Γ = Γ}
                           {σˢ = S.id S., S.true}
                           (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.true} (true {Γ = Γ})))
                  uˢ}
          (if {Γ = Γ}{Cˢ = Cˢ} C {uˢ = uˢ} u {vˢ = vˢ} v {tˢ = S.true} (true {Γ = Γ}))
          u
Boolβ₁ = m.refl

Boolβ₂ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con Γˢ}
   {j}{Cˢ : S.Ty (Γˢ S.▷ S.Bool) j} {C : Ty (Γ ▷ Bool {Γ = Γ}) Cˢ}
      {uˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.true ]T)}
      {u : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.true}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.true} (true {Γ = Γ})))
                uˢ}
      {vˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.false ]T)}
      {v : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.false}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.false} (false {Γ = Γ})))
                vˢ}
   → m._≡_ {A = Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                           {Aˢ = Cˢ} C
                           {Γ = Γ}
                           {σˢ = S.id S., S.false}
                           (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.false} (false {Γ = Γ})))
                  vˢ}
          (if {Γ = Γ}{Cˢ = Cˢ} C {uˢ = uˢ} u {vˢ = vˢ} v {tˢ = S.false} (false {Γ = Γ}))
          v
Boolβ₂ = m.refl

Bool[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   → m._≡_ {A = Ty Γ S.Bool}
          (_[_]T {Δ = Δ} (Bool {Γ = Δ}){σˢ = σˢ} σ)
          (Bool {Γ = Γ})
Bool[] = m.refl

true[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   → m._≡_ {A = Tm Γ (Bool {Γ = Γ}) S.true}
          (_[_]t {Δ = Δ}{A = Bool {Γ = Δ}}{tˢ = S.true} (true {Γ = Δ}) {σˢ = σˢ} σ)
          (true {Γ = Γ})
true[] = m.refl

false[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   → m._≡_ {A = Tm Γ (Bool {Γ = Γ}) S.false}
          (_[_]t {Δ = Δ}{A = Bool {Γ = Δ}}{tˢ = S.false} (false {Γ = Δ}) {σˢ = σˢ} σ)
          (false {Γ = Γ})
false[] = m.refl


-- Identity
--------------------------------------------------------------------------------

-- The very large types in this section are generated by the following way: we
-- take the type signatures from DisplayedTemplate, and print them out with
-- implicit parameters shown explicitly. In ModelTemplate and DisplayedTemplate,
-- we try to get a "generic" strict displayed model by using REWRITE rules.

-- This is necessary because otherwise we have a massive amount of manual work figuring
-- out which implicit parameters needs to be made explicit.

-- The J[] component is missing, because Agda's REWRITE rules fail in this case,
-- so we don't get a type for the component.

Id :
  ∀ {i}{Γˢ : S.Con i}   {Γ : Con Γˢ}
    {j}{Aˢ : S.Ty Γˢ j} (A : Ty Γ Aˢ)
       {uˢ : S.Tm Γˢ Aˢ}(u : Tm Γ A uˢ)
       {vˢ : S.Tm Γˢ Aˢ}(v : Tm Γ A vˢ) →
    Ty Γ {j} (S.Id Aˢ uˢ vˢ)
Id {i} {Γˢ} {Γ} {j} {Aˢ} A {uˢ} u {vˢ} v {ρˢ} ρ eˢ =
  m.Σ (uˢ S.[ ρˢ ]t ≡ vˢ S.[ ρˢ ]t) λ eq →
      (tr (λ x → S.Tm S.∙ (S.Id (Aˢ S.[ ρˢ ]T) x (vˢ S.[ ρˢ ]t))) eq eˢ ≡ S.refl _)
        ×
      (tr (A ρ) eq (u ρ) ≡ v ρ)

Id[] :
  {i : Level} {Γˢ : S.Con i} {Γ : Con {i} Γˢ} {j : Level} {Aˢ : S.Ty {i} Γˢ j} {A
  : Ty {i} {Γˢ} Γ {j} Aˢ} {uˢ : S.Tm {i} Γˢ {j} Aˢ} {u : Tm {i} {Γˢ} Γ {j} {Aˢ} A
  uˢ} {vˢ : S.Tm {i} Γˢ {j} Aˢ} {v : Tm {i} {Γˢ} Γ {j} {Aˢ} A vˢ} {k : Level} {θˢ
  : S.Con k} {θ : Con {k} θˢ} {σˢ : S.Sub {k} θˢ {i} Γˢ} {σ : Sub {k} {θˢ} θ {i}
  {Γˢ} Γ σˢ} → _≡_ {lsuc j ⊔ k} {Ty {k} {θˢ} θ {j} (S.Id {k} {θˢ} {j} (S._[_]T {i}
  {Γˢ} {j} Aˢ {k} {θˢ} σˢ) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {k} {θˢ} σˢ) (S._[_]t {i}
  {Γˢ} {j} {Aˢ} vˢ {k} {θˢ} σˢ))} (Id {k} {θˢ} {θ} {j} {S._[_]T {i} {Γˢ} {j} Aˢ
  {k} {θˢ} σˢ} (_[_]T {i} {Γˢ} {Γ} {j} {Aˢ} A {k} {θˢ} {θ} {σˢ} σ) {S._[_]t {i}
  {Γˢ} {j} {Aˢ} uˢ {k} {θˢ} σˢ} (_[_]t {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u {k} {θˢ}
  {θ} {σˢ} σ) {S._[_]t {i} {Γˢ} {j} {Aˢ} vˢ {k} {θˢ} σˢ} (_[_]t {i} {Γˢ} {Γ} {j}
  {Aˢ} {A} {vˢ} v {k} {θˢ} {θ} {σˢ} σ)) (Id {k} {θˢ} {θ} {j} {S._[_]T {i} {Γˢ} {j}
  Aˢ {k} {θˢ} σˢ} (_[_]T {i} {Γˢ} {Γ} {j} {Aˢ} A {k} {θˢ} {θ} {σˢ} σ) {S._[_]t {i}
  {Γˢ} {j} {Aˢ} uˢ {k} {θˢ} σˢ} (_[_]t {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u {k} {θˢ}
  {θ} {σˢ} σ) {S._[_]t {i} {Γˢ} {j} {Aˢ} vˢ {k} {θˢ} σˢ} (_[_]t {i} {Γˢ} {Γ} {j}
  {Aˢ} {A} {vˢ} v {k} {θˢ} {θ} {σˢ} σ))
Id[] = m.refl

refl :
  ∀ {i}{Γˢ : S.Con i}    {Γ : Con Γˢ}
    {j}{Aˢ : S.Ty Γˢ j}  {A : Ty Γ Aˢ}
       {uˢ : S.Tm Γˢ Aˢ} (u : Tm Γ A uˢ) →
    Tm Γ (Id A {uˢ = uˢ} u {vˢ = uˢ} u) (S.refl uˢ)
refl {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u {ρˢ} ρ = m.refl m.,Σ (m.refl m.,Σ m.refl)

refl[] :
  {i : Level} {Γˢ : S.Con i} {Γ : Con {i} Γˢ} {j : Level} {Δˢ : S.Con j} {Δ : Con
  {j} Δˢ} {σˢ : S.Sub {i} Γˢ {j} Δˢ} {σ : Sub {i} {Γˢ} Γ {j} {Δˢ} Δ σˢ} {k :
  Level} {Aˢ : S.Ty {j} Δˢ k} {A : Ty {j} {Δˢ} Δ {k} Aˢ} {tˢ : S.Tm {j} Δˢ {k} Aˢ}
  {t : Tm {j} {Δˢ} Δ {k} {Aˢ} A tˢ} → _≡_ {i ⊔ k} {Tm {i} {Γˢ} Γ {k} {S.Id {i}
  {Γˢ} {k} (S._[_]T {j} {Δˢ} {k} Aˢ {i} {Γˢ} σˢ) (S._[_]t {j} {Δˢ} {k} {Aˢ} tˢ {i}
  {Γˢ} σˢ) (S._[_]t {j} {Δˢ} {k} {Aˢ} tˢ {i} {Γˢ} σˢ)} (Id {i} {Γˢ} {Γ} {k}
  {S._[_]T {j} {Δˢ} {k} Aˢ {i} {Γˢ} σˢ} (_[_]T {j} {Δˢ} {Δ} {k} {Aˢ} A {i} {Γˢ}
  {Γ} {σˢ} σ) {S._[_]t {j} {Δˢ} {k} {Aˢ} tˢ {i} {Γˢ} σˢ} (_[_]t {j} {Δˢ} {Δ} {k}
  {Aˢ} {A} {tˢ} t {i} {Γˢ} {Γ} {σˢ} σ) {S._[_]t {j} {Δˢ} {k} {Aˢ} tˢ {i} {Γˢ} σˢ}
  (_[_]t {j} {Δˢ} {Δ} {k} {Aˢ} {A} {tˢ} t {i} {Γˢ} {Γ} {σˢ} σ)) (S.refl {i} {Γˢ}
  {k} {S._[_]T {j} {Δˢ} {k} Aˢ {i} {Γˢ} σˢ} (S._[_]t {j} {Δˢ} {k} {Aˢ} tˢ {i} {Γˢ}
  σˢ))} (refl {i} {Γˢ} {Γ} {k} {S._[_]T {j} {Δˢ} {k} Aˢ {i} {Γˢ} σˢ} {_[_]T {j}
  {Δˢ} {Δ} {k} {Aˢ} A {i} {Γˢ} {Γ} {σˢ} σ} {S._[_]t {j} {Δˢ} {k} {Aˢ} tˢ {i} {Γˢ}
  σˢ} (_[_]t {j} {Δˢ} {Δ} {k} {Aˢ} {A} {tˢ} t {i} {Γˢ} {Γ} {σˢ} σ)) (refl {i} {Γˢ}
  {Γ} {k} {S._[_]T {j} {Δˢ} {k} Aˢ {i} {Γˢ} σˢ} {_[_]T {j} {Δˢ} {Δ} {k} {Aˢ} A {i}
  {Γˢ} {Γ} {σˢ} σ} {S._[_]t {j} {Δˢ} {k} {Aˢ} tˢ {i} {Γˢ} σˢ} (_[_]t {j} {Δˢ} {Δ}
  {k} {Aˢ} {A} {tˢ} t {i} {Γˢ} {Γ} {σˢ} σ))
refl[] = m.refl

J :
  {i : Level} {Γˢ : S.Con i} {Γ : Con {i} Γˢ} {j : Level} {Aˢ : S.Ty {i} Γˢ j} {A
  : Ty {i} {Γˢ} Γ {j} Aˢ} {uˢ : S.Tm {i} Γˢ {j} Aˢ} {u : Tm {i} {Γˢ} Γ {j} {Aˢ} A
  uˢ} {k : Level} {Cˢ : S.Ty {i ⊔ j} (S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j}
  (S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ}))) k} (C :
  Ty {i ⊔ j} {S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j} (S.Id {i ⊔ j} {S._▷_ {i} Γˢ
  {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ}
  {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i}
  {Γˢ} {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ}))} (_▷_ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ}
  (_▷_ {i} {Γˢ} Γ {j} {Aˢ} A) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T
  {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t
  {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.q
  {i} {Γˢ} {j} {Aˢ})} (Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ}
  A} {j} {S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j}
  {Aˢ})} (_[_]T {i} {Γˢ} {Γ} {j} {Aˢ} A {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i}
  {Γˢ} Γ {j} {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ} {A}))
  {S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j}
  {Aˢ})} (_[_]t {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ}
  {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ}
  {A})) {S.q {i} {Γˢ} {j} {Aˢ}} (q {i} {Γˢ} {Γ} {j} {Aˢ} {A}))) {k} Cˢ) {wˢ : S.Tm
  {i} Γˢ {k} (S._[_]T {i ⊔ j} {S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j} (S.Id {i ⊔
  j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j}
  Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ
  {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ}))} {k} Cˢ {i} {Γˢ}
  (S._,_ {i} {Γˢ} {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S._,_ {i} {Γˢ} {i} {Γˢ} (S.id {i}
  {Γˢ}) {j} {Aˢ} uˢ) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ}
  {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ}
  {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.q {i} {Γˢ}
  {j} {Aˢ})} (S.refl {i} {Γˢ} {j} {Aˢ} uˢ)))} → Tm {i} {Γˢ} Γ {k} {S._[_]T {i ⊔ j}
  {S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j} (S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j}
  (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ}))
  (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j}
  {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ}))} {k} Cˢ {i} {Γˢ} (S._,_ {i} {Γˢ} {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S._,_ {i} {Γˢ} {i} {Γˢ} (S.id {i} {Γˢ}) {j} {Aˢ} uˢ) {j} {S.Id
  {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ
  {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ})} (S.refl {i}
  {Γˢ} {j} {Aˢ} uˢ))} (_[_]T {i ⊔ j} {S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j}
  (S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ}))} {_▷_ {i
  ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (_▷_ {i} {Γˢ} Γ {j} {Aˢ} A) {j} {S.Id {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p
  {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ}
  (S.p {i} {Γˢ} {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ})} (Id {i ⊔ j} {S._▷_ {i} Γˢ {j}
  Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {j} {S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i}
  Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]T {i} {Γˢ} {Γ} {j} {Aˢ} A {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i}
  {Γˢ} {Γ} {j} {Aˢ} {A})) {S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j}
  Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]t {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i}
  {Γˢ} {Γ} {j} {Aˢ} {A})) {S.q {i} {Γˢ} {j} {Aˢ}} (q {i} {Γˢ} {Γ} {j} {Aˢ} {A}))}
  {k} {Cˢ} C {i} {Γˢ} {Γ} {S._,_ {i} {Γˢ} {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S._,_ {i}
  {Γˢ} {i} {Γˢ} (S.id {i} {Γˢ}) {j} {Aˢ} uˢ) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j}
  Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j}
  {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ}
  {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ})} (S.refl {i} {Γˢ} {j} {Aˢ} uˢ)} (_,_ {i} {Γˢ}
  {Γ} {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S._,_ {i} {Γˢ}
  {i} {Γˢ} (S.id {i} {Γˢ}) {j} {Aˢ} uˢ} (_,_ {i} {Γˢ} {Γ} {i} {Γˢ} {Γ} {S.id {i}
  {Γˢ}} (id {i} {Γˢ} {Γ}) {j} {Aˢ} {A} {uˢ} u) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j}
  Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j}
  {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ}
  {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ})} {Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i}
  {Γˢ} Γ {j} {Aˢ} A} {j} {S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ}
  (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]T {i} {Γˢ} {Γ} {j} {Aˢ} A {i ⊔ j} {S._▷_ {i} Γˢ
  {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j}
  {Aˢ} {A})) {S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i}
  {Γˢ} {j} {Aˢ})} (_[_]t {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u {i ⊔ j} {S._▷_ {i} Γˢ
  {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j}
  {Aˢ} {A})) {S.q {i} {Γˢ} {j} {Aˢ}} (q {i} {Γˢ} {Γ} {j} {Aˢ} {A})} {S.refl {i}
  {Γˢ} {j} {Aˢ} uˢ} (refl {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u))) wˢ → {vˢ : S.Tm {i}
  Γˢ {j} Aˢ} {v : Tm {i} {Γˢ} Γ {j} {Aˢ} A vˢ} {tˢ : S.Tm {i} Γˢ {j} (S.Id {i}
  {Γˢ} {j} Aˢ uˢ vˢ)} (t : Tm {i} {Γˢ} Γ {j} {S.Id {i} {Γˢ} {j} Aˢ uˢ vˢ} (Id {i}
  {Γˢ} {Γ} {j} {Aˢ} A {uˢ} u {vˢ} v) tˢ) → Tm {i} {Γˢ} Γ {k} {S._[_]T {i ⊔ j}
  {S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j} (S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j}
  (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ}))
  (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j}
  {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ}))} {k} Cˢ {i} {Γˢ} (S._,_ {i} {Γˢ} {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S._,_ {i} {Γˢ} {i} {Γˢ} (S.id {i} {Γˢ}) {j} {Aˢ} vˢ) {j} {S.Id
  {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ
  {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ})} tˢ)} (_[_]T {i
  ⊔ j} {S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j} (S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j}
  Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j}
  {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ}
  {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ}))} {_▷_ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (_▷_ {i}
  {Γˢ} Γ {j} {Aˢ} A) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ}
  {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ}
  {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.q {i} {Γˢ}
  {j} {Aˢ})} (Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {j}
  {S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})}
  (_[_]T {i} {Γˢ} {Γ} {j} {Aˢ} A {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j}
  {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ} {A})) {S._[_]t {i} {Γˢ}
  {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]t {i}
  {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j}
  {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ} {A})) {S.q {i} {Γˢ} {j}
  {Aˢ}} (q {i} {Γˢ} {Γ} {j} {Aˢ} {A}))} {k} {Cˢ} C {i} {Γˢ} {Γ} {S._,_ {i} {Γˢ} {i
  ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S._,_ {i} {Γˢ} {i} {Γˢ} (S.id {i} {Γˢ}) {j} {Aˢ} vˢ)
  {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i
  ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ})} tˢ}
  (_,_ {i} {Γˢ} {Γ} {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A}
  {S._,_ {i} {Γˢ} {i} {Γˢ} (S.id {i} {Γˢ}) {j} {Aˢ} vˢ} (_,_ {i} {Γˢ} {Γ} {i} {Γˢ}
  {Γ} {S.id {i} {Γˢ}} (id {i} {Γˢ} {Γ}) {j} {Aˢ} {A} {vˢ} v) {j} {S.Id {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ}
  (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j}
  Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.q {i} {Γˢ} {j} {Aˢ})} {Id {i ⊔ j} {S._▷_ {i} Γˢ
  {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {j} {S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]T {i} {Γˢ} {Γ} {j} {Aˢ} A {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i}
  {Γˢ} {Γ} {j} {Aˢ} {A})) {S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j}
  Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]t {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i}
  {Γˢ} {Γ} {j} {Aˢ} {A})) {S.q {i} {Γˢ} {j} {Aˢ}} (q {i} {Γˢ} {Γ} {j} {Aˢ} {A})}
  {tˢ} t)) (S.J {i} {Γˢ} {j} {Aˢ} {uˢ} {k} Cˢ wˢ {vˢ} tˢ)
J {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} {u} {k} {Cˢ} C {wˢ} w {vˢ}{v}{tˢ} t {ρˢ} ρ =
   let p1 m.,Σ (p2 m.,Σ p3) = t ρ
   in m.J (λ {vˢρˢ} p1 →
              ∀ tˢρˢ vρ p2 p3
              → C (ρ m.,Σ vρ m.,Σ (p1 m.,Σ (p2 m.,Σ p3)))
                  (S.J (Cˢ S.[ ρˢ S.^ Aˢ S.^ S.Id (Aˢ S.[ S.p ]T) (uˢ S.[ S.p ]t) S.v⁰ ]T)
                       (wˢ S.[ ρˢ ]t)
                       tˢρˢ))
          (λ tˢρˢ vρ → λ {m.refl m.refl → w ρ})
          (m.fst (t ρ)) (tˢ S.[ ρˢ ]t) (v ρ) p2 p3

Idβ :
  {i : Level} {Γˢ : S.Con i} {Γ : Con {i} Γˢ} {j : Level} {Aˢ : S.Ty {i} Γˢ j} {A
  : Ty {i} {Γˢ} Γ {j} Aˢ} {uˢ : S.Tm {i} Γˢ {j} Aˢ} {u : Tm {i} {Γˢ} Γ {j} {Aˢ} A
  uˢ} {k : Level} {Cˢ : S.Ty {i ⊔ j} (S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j}
  (S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ}))) k} {C
  : Ty {i ⊔ j} {S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j} (S.Id {i ⊔ j} {S._▷_ {i}
  Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i}
  {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p
  {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ}))} (_▷_ {i ⊔ j} {S._▷_ {i} Γˢ {j}
  Aˢ} (_▷_ {i} {Γˢ} Γ {j} {Aˢ} A) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j}
  (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ}))
  (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j}
  {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ})} (Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ}
  Γ {j} {Aˢ} A} {j} {S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p
  {i} {Γˢ} {j} {Aˢ})} (_[_]T {i} {Γˢ} {Γ} {j} {Aˢ} A {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ}
  {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ}
  {A})) {S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ}
  {j} {Aˢ})} (_[_]t {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ}
  {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ}
  {A})) {S.v⁰ {i} {Γˢ} {j} {Aˢ}} (v⁰ {i} {Γˢ} {Γ} {j} {Aˢ} {A}))) {k} Cˢ} {wˢ :
  S.Tm {i} Γˢ {k} (S._[_]T {i ⊔ j} {S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j} (S.Id
  {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ
  {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ}))} {k} Cˢ {i}
  {Γˢ} (S._,_ {i} {Γˢ} {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S._,_ {i} {Γˢ} {i} {Γˢ}
  (S.id {i} {Γˢ}) {j} {Aˢ} uˢ) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j}
  (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ}))
  (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j}
  {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ})} (S.refl {i} {Γˢ} {j} {Aˢ} uˢ)))} {w : Tm {i}
  {Γˢ} Γ {k} {S._[_]T {i ⊔ j} {S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j} (S.Id {i ⊔
  j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j}
  Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ
  {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ}))} {k} Cˢ {i} {Γˢ}
  (S._,_ {i} {Γˢ} {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S._,_ {i} {Γˢ} {i} {Γˢ} (S.id {i}
  {Γˢ}) {j} {Aˢ} uˢ) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ}
  {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ}
  {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i}
  {Γˢ} {j} {Aˢ})} (S.refl {i} {Γˢ} {j} {Aˢ} uˢ))} (_[_]T {i ⊔ j} {S._▷_ {i ⊔ j}
  (S._▷_ {i} Γˢ {j} Aˢ) {j} (S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i}
  {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i}
  {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰
  {i} {Γˢ} {j} {Aˢ}))} {_▷_ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (_▷_ {i} {Γˢ} Γ {j} {Aˢ}
  A) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i
  ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ})}
  (Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {j} {S._[_]T {i}
  {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]T {i}
  {Γˢ} {Γ} {j} {Aˢ} A {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A}
  {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ} {A})) {S._[_]t {i} {Γˢ} {j}
  {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]t {i} {Γˢ}
  {Γ} {j} {Aˢ} {A} {uˢ} u {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ}
  A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ} {A})) {S.v⁰ {i} {Γˢ} {j}
  {Aˢ}} (v⁰ {i} {Γˢ} {Γ} {j} {Aˢ} {A}))} {k} {Cˢ} C {i} {Γˢ} {Γ} {S._,_ {i} {Γˢ}
  {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S._,_ {i} {Γˢ} {i} {Γˢ} (S.id {i} {Γˢ}) {j} {Aˢ}
  uˢ) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i
  ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ})}
  (S.refl {i} {Γˢ} {j} {Aˢ} uˢ)} (_,_ {i} {Γˢ} {Γ} {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ}
  {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S._,_ {i} {Γˢ} {i} {Γˢ} (S.id {i} {Γˢ}) {j} {Aˢ}
  uˢ} (_,_ {i} {Γˢ} {Γ} {i} {Γˢ} {Γ} {S.id {i} {Γˢ}} (id {i} {Γˢ} {Γ}) {j} {Aˢ}
  {A} {uˢ} u) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ
  {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j}
  {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ}
  {j} {Aˢ})} {Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {j}
  {S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})}
  (_[_]T {i} {Γˢ} {Γ} {j} {Aˢ} A {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j}
  {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ} {A})) {S._[_]t {i} {Γˢ}
  {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]t {i}
  {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j}
  {Aˢ} A} {S.p {i} {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ} {A})) {S.v⁰ {i} {Γˢ}
  {j} {Aˢ}} (v⁰ {i} {Γˢ} {Γ} {j} {Aˢ} {A})} {S.refl {i} {Γˢ} {j} {Aˢ} uˢ} (refl
  {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u))) wˢ} → _≡_ {i ⊔ k} {Tm {i} {Γˢ} Γ {k}
  {S._[_]T {i ⊔ j} {S._▷_ {i ⊔ j} (S._▷_ {i} Γˢ {j} Aˢ) {j} (S.Id {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p
  {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ}
  (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ}))} {k} Cˢ {i} {Γˢ} (S._,_ {i}
  {Γˢ} {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S._,_ {i} {Γˢ} {i} {Γˢ} (S.id {i} {Γˢ}) {j}
  {Aˢ} uˢ) {j} {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i
  ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ}
  uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ} {j}
  {Aˢ})} (S.refl {i} {Γˢ} {j} {Aˢ} uˢ))} (_[_]T {i ⊔ j} {S._▷_ {i ⊔ j} (S._▷_ {i}
  Γˢ {j} Aˢ) {j} (S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ
  {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j}
  {Aˢ} uˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ}
  {j} {Aˢ}))} {_▷_ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (_▷_ {i} {Γˢ} Γ {j} {Aˢ} A) {j}
  {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ})} (Id {i
  ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {j} {S._[_]T {i} {Γˢ} {j}
  Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]T {i} {Γˢ} {Γ}
  {j} {Aˢ} A {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i}
  {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ} {A})) {S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔
  j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]t {i} {Γˢ} {Γ} {j} {Aˢ}
  {A} {uˢ} u {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i}
  {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ} {A})) {S.v⁰ {i} {Γˢ} {j} {Aˢ}} (v⁰ {i}
  {Γˢ} {Γ} {j} {Aˢ} {A}))} {k} {Cˢ} C {i} {Γˢ} {Γ} {S._,_ {i} {Γˢ} {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S._,_ {i} {Γˢ} {i} {Γˢ} (S.id {i} {Γˢ}) {j} {Aˢ} uˢ) {j} {S.Id
  {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_ {i} Γˢ
  {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ})} (S.refl {i}
  {Γˢ} {j} {Aˢ} uˢ)} (_,_ {i} {Γˢ} {Γ} {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ}
  Γ {j} {Aˢ} A} {S._,_ {i} {Γˢ} {i} {Γˢ} (S.id {i} {Γˢ}) {j} {Aˢ} uˢ} (_,_ {i}
  {Γˢ} {Γ} {i} {Γˢ} {Γ} {S.id {i} {Γˢ}} (id {i} {Γˢ} {Γ}) {j} {Aˢ} {A} {uˢ} u) {j}
  {S.Id {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {j} (S._[_]T {i} {Γˢ} {j} Aˢ {i ⊔ j} {S._▷_
  {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔ j}
  {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})) (S.v⁰ {i} {Γˢ} {j} {Aˢ})} {Id {i
  ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {j} {S._[_]T {i} {Γˢ} {j}
  Aˢ {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]T {i} {Γˢ} {Γ}
  {j} {Aˢ} A {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i}
  {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ} {A})) {S._[_]t {i} {Γˢ} {j} {Aˢ} uˢ {i ⊔
  j} {S._▷_ {i} Γˢ {j} Aˢ} (S.p {i} {Γˢ} {j} {Aˢ})} (_[_]t {i} {Γˢ} {Γ} {j} {Aˢ}
  {A} {uˢ} u {i ⊔ j} {S._▷_ {i} Γˢ {j} Aˢ} {_▷_ {i} {Γˢ} Γ {j} {Aˢ} A} {S.p {i}
  {Γˢ} {j} {Aˢ}} (p {i} {Γˢ} {Γ} {j} {Aˢ} {A})) {S.v⁰ {i} {Γˢ} {j} {Aˢ}} (v⁰ {i}
  {Γˢ} {Γ} {j} {Aˢ} {A})} {S.refl {i} {Γˢ} {j} {Aˢ} uˢ} (refl {i} {Γˢ} {Γ} {j}
  {Aˢ} {A} {uˢ} u))) wˢ} (J {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} {u} {k} {Cˢ} C {wˢ} w
  {uˢ} {u} {S.refl {i} {Γˢ} {j} {Aˢ} uˢ} (refl {i} {Γˢ} {Γ} {j} {Aˢ} {A} {uˢ} u))
  w
Idβ = m.refl

-- Missing from formalization, because we cannot generate the type of the component:
-- J[] :
-}
