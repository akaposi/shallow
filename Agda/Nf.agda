module Nf where

-- unquotiented terms as codes / normal forms

-- CwF with Bool and Π

open import Agda.Primitive
open import Standard renaming (Con to Con′; Ty to Ty′) hiding (refl; Σ; _,Σ_)

Con : Set₁
Con = Con′ lzero
Ty : Con → Set₁
Ty Γ = Ty′ Γ lzero

data Con' : Con → Set₁
data Sub' : (Δ : Con)(Γ : Con) → Sub Δ Γ → Set₁
data Ty'  : (Γ : Con) → Ty Γ → Set₁
data Tm'  : (Γ : Con)(A : Ty Γ) → Tm Γ A → Set₁

data Con' where
  ∙'      : Con' ∙
  _▷'_    : ∀{Γ} → Con' Γ → ∀{A} → Ty' Γ A → Con' (Γ ▷ A)

data Sub' where
  _∘'_    : ∀{Γ}{_ : Con' Γ}{Δ}{_ : Con' Δ}{γ} → Sub' Δ Γ γ → ∀{Θ}{_ : Con' Θ}{δ} → Sub' Θ Δ δ → Sub' Θ Γ (γ ∘ δ)
  id'     : ∀{Γ}{_ : Con' Γ} → Sub' Γ Γ id
  ε'      : ∀{Γ}{_ : Con' Γ} → Sub' Γ ∙ ε
  _,'_    : ∀{Γ}{_ : Con' Γ}{Δ}{_ : Con' Δ}{γ} → Sub' Δ Γ γ → ∀{A}{_ : Ty' Γ A}{a} → Tm' Δ (A [ γ ]T) a → Sub' Δ (Γ ▷ A) (γ , a)
  p'      : ∀{Γ}{_ : Con' Γ}{A}{_ : Ty' Γ A} → Sub' (Γ ▷ A) Γ p

data Ty' where
  _[_]T'  : ∀{Γ}{_ : Con' Γ}{A} → Ty' Γ A → ∀{Δ}{_ : Con' Δ}{γ} → Sub' Δ Γ γ → Ty' Δ (A [ γ ]T)
  Bool'   : ∀{Γ}{_ : Con' Γ} → Ty' Γ Bool
  Π'      : ∀{Γ}{_ : Con' Γ}{A} → Ty' Γ A → ∀{B} → Ty' (Γ ▷ A) B → Ty' Γ (Π A B)

data Tm' where
  _[_]t'  : ∀{Γ}{_ : Con' Γ}{A}{_ : Ty' Γ A}{a} → Tm' Γ A a → ∀{Δ}{_ : Con' Δ}{γ} → Sub' Δ Γ γ → Tm' Δ (A [ γ ]T) (a [ γ ]t)
  q'      : ∀{Γ}{_ : Con' Γ}{A}{_ : Ty' Γ A} → Tm' (Γ ▷ A) (A [ p ]T) q
  true'   : ∀{Γ}{_ : Con' Γ} → Tm' Γ Bool true
  false'  : ∀{Γ}{_ : Con' Γ} → Tm' Γ Bool false
  if'     : ∀{Γ}{_ : Con' Γ}{C} → Ty' (Γ ▷ Bool) C → ∀{u} → Tm' Γ (C [ id , true ]T) u → ∀{v} → Tm' Γ (C [ id , false ]T) v → ∀{t} → Tm' Γ Bool t → Tm' Γ (C [ id , t ]T) (if C u v t)
  lam'    : ∀{Γ}{_ : Con' Γ}{A}{_ : Ty' Γ A}{B}{_ : Ty' (Γ ▷ A) B}{t} → Tm' (Γ ▷ A) B t → Tm' Γ (Π A B) (lam t)
  app'    : ∀{Γ}{_ : Con' Γ}{A}{_ : Ty' Γ A}{B}{_ : Ty' (Γ ▷ A) B}{t} → Tm' Γ (Π A B) t → Tm' (Γ ▷ A) B (app t)
  
open import Lib

data _⊎_ {i}(A : Set i){j}(B : Set j) : Set (i ⊔ j) where
  inl : A → A ⊎ B
  inr : B → A ⊎ B

Dec : ∀{i} → Set i → Set i
Dec A = A ⊎ (A → ⊥)

Con= : ∀{Γ₀}(Γ'₀ : Con' Γ₀){Γ₁}(Γ'₁ : Con' Γ₁) → Dec ((Γ₀ ,Σ Γ'₀) ≡ (Γ₁ ,Σ Γ'₁))
Sub= : ∀{Γ₀ Δ₀ γ₀}(γ'₀ : Sub' Δ₀ Γ₀ γ₀){Γ₁ Δ₁ γ₁}(γ'₁ : Sub' Δ₁ Γ₁ γ₁) → Dec ((Γ₀ ,Σ Δ₀ ,Σ γ₀ ,Σ γ'₀) ≡ (Γ₁ ,Σ Δ₁ ,Σ γ₁ ,Σ γ'₁))
Ty=  : ∀{Γ₀ A₀}(A'₀ : Ty' Γ₀ A₀){Γ₁ A₁}(A'₁ : Ty' Γ₁ A₁) → Dec ((Γ₀ ,Σ A₀ ,Σ A'₀) ≡ (Γ₁ ,Σ A₁ ,Σ A'₁))
Tm=  : ∀{Γ₀ A₀ a₀}(a'₀ : Tm' Γ₀ A₀ a₀){Γ₁ A₁ a₁}(a'₁ : Tm' Γ₁ A₁ a₁) → Dec ((Γ₀ ,Σ A₀ ,Σ a₀ ,Σ a'₀) ≡ (Γ₁ ,Σ A₁ ,Σ a₁ ,Σ a'₁))

Con= ∙' ∙' = inl refl
Con= ∙' (Γ'₁ ▷' _) = inr λ ()
Con= (Γ'₀ ▷' _) ∙' = inr λ ()
Con= (Γ'₀ ▷' A'₀) (Γ'₁ ▷' A'₁) with Con= Γ'₀ Γ'₁
Con= (Γ'₀ ▷' A'₀) (Γ'₁ ▷' A'₁) | inl refl with Ty= A'₀ A'₁
Con= (Γ'₀ ▷' A'₀) (Γ'₀ ▷' A'₁) | inl refl | inl refl = inl refl
Con= (Γ'₀ ▷' A'₀) (Γ'₀ ▷' A'₁) | inl refl | inr ne   = inr λ { refl → ne refl }
Con= (Γ'₀ ▷' A'₀) (Γ'₁ ▷' A'₁) | inr ne   = inr λ { refl → ne refl }

Sub= (_∘'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{Θ'₀} δ'₀) (_∘'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{Θ'₁} δ'₁) with Con= Γ'₀ Γ'₁
Sub= (_∘'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{Θ'₀} δ'₀) (_∘'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{Θ'₁} δ'₁) | inl refl with Con= Δ'₀ Δ'₁
Sub= (_∘'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{Θ'₀} δ'₀) (_∘'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{Θ'₁} δ'₁) | inl refl | inl refl with Sub= γ'₀ γ'₁
Sub= (_∘'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{Θ'₀} δ'₀) (_∘'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{Θ'₁} δ'₁) | inl refl | inl refl | inl refl with Con= Θ'₀ Θ'₁
Sub= (_∘'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{Θ'₀} δ'₀) (_∘'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{Θ'₁} δ'₁) | inl refl | inl refl | inl refl | inl refl with Sub= δ'₀ δ'₁
Sub= (_∘'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{Θ'₀} δ'₀) (_∘'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{Θ'₁} δ'₁) | inl refl | inl refl | inl refl | inl refl | inl refl = inl refl
Sub= (_∘'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{Θ'₀} δ'₀) (_∘'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{Θ'₁} δ'₁) | inl refl | inl refl | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Sub= (_∘'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{Θ'₀} δ'₀) (_∘'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{Θ'₁} δ'₁) | inl refl | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Sub= (_∘'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{Θ'₀} δ'₀) (_∘'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{Θ'₁} δ'₁) | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Sub= (_∘'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{Θ'₀} δ'₀) (_∘'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{Θ'₁} δ'₁) | inl refl | inr ne   = inr λ { refl → ne refl }
Sub= (_∘'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{Θ'₀} δ'₀) (_∘'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{Θ'₁} δ'₁) | inr ne   = inr λ { refl → ne refl }
Sub= (_ ∘' _) id' = inr λ ()
Sub= (_ ∘' _) ε' = inr λ ()
Sub= (_ ∘' _) (_ ,' _) = inr λ ()
Sub= (_ ∘' _) p' = inr λ ()
Sub= id' (γ'₁ ∘' δ'₁) = inr λ ()
Sub= (id' {_}{Γ'₀}) (id' {_}{Γ'₁}) with Con= Γ'₀ Γ'₁
... | inl refl = inl refl
... | inr ne   = inr λ { refl → ne refl }
Sub= id' ε' = inr λ ()
Sub= id' (_ ,' _) = inr λ ()
Sub= id' p' = inr λ ()
Sub= ε' (_ ∘' _) = inr λ ()
Sub= ε' id' = inr λ ()
Sub= (ε' {_}{Γ'₀}) (ε' {_}{Γ'₁}) with Con= Γ'₀ Γ'₁
... | inl refl = inl refl
... | inr ne   = inr λ { refl → ne refl }
Sub= ε' (_ ,' _) = inr λ ()
Sub= ε' p' = inr λ ()
Sub= (_ ,' _) (_ ∘' _) = inr λ ()
Sub= (_ ,' _) id' = inr λ ()
Sub= (_ ,' _) ε' = inr λ ()
Sub= (_,'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{A'₀} a'₀) (_,'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{A'₁} a'₁) with Con= Γ'₀ Γ'₁
Sub= (_,'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{A'₀} a'₀) (_,'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{A'₁} a'₁) | inl refl with Con= Δ'₀ Δ'₁
Sub= (_,'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{A'₀} a'₀) (_,'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{A'₁} a'₁) | inl refl | inl refl with Sub= γ'₀ γ'₁
Sub= (_,'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{A'₀} a'₀) (_,'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{A'₁} a'₁) | inl refl | inl refl | inl refl with Ty= A'₀ A'₁
Sub= (_,'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{A'₀} a'₀) (_,'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{A'₁} a'₁) | inl refl | inl refl | inl refl | inl refl with Tm= a'₀ a'₁
Sub= (_,'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{A'₀} a'₀) (_,'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{A'₁} a'₁) | inl refl | inl refl | inl refl | inl refl | inl refl = inl refl
Sub= (_,'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{A'₀} a'₀) (_,'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{A'₁} a'₁) | inl refl | inl refl | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Sub= (_,'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{A'₀} a'₀) (_,'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{A'₁} a'₁) | inl refl | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Sub= (_,'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{A'₀} a'₀) (_,'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{A'₁} a'₁) | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Sub= (_,'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{A'₀} a'₀) (_,'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{A'₁} a'₁) | inl refl | inr ne   = inr λ { refl → ne refl }
Sub= (_,'_ {_}{Γ'₀}{_}{Δ'₀} γ'₀ {_}{A'₀} a'₀) (_,'_ {_}{Γ'₁}{_}{Δ'₁} γ'₁ {_}{A'₁} a'₁) | inr ne   = inr λ { refl → ne refl }
Sub= (_ ,' _) p' = inr λ ()
Sub= p' (_ ∘' _) = inr λ ()
Sub= p' id' = inr λ ()
Sub= p' ε' = inr λ ()
Sub= p' (_ ,' _) = inr λ ()
Sub= (p' {_}{Γ'₀}{_}{A'₀}) (p' {_}{Γ'₁}{_}{A'₁}) with Con= Γ'₀ Γ'₁
Sub= (p' {_}{Γ'₀}{_}{A'₀}) (p' {_}{Γ'₁}{_}{A'₁}) | inl refl with Ty= A'₀ A'₁
Sub= (p' {_}{Γ'₀}{_}{A'₀}) (p' {_}{Γ'₁}{_}{A'₁}) | inl refl | inl refl = inl refl
Sub= (p' {_}{Γ'₀}{_}{A'₀}) (p' {_}{Γ'₁}{_}{A'₁}) | inl refl | inr ne   = inr λ { refl → ne refl }
Sub= (p' {_}{Γ'₀}{_}{A'₀}) (p' {_}{Γ'₁}{_}{A'₁}) | inr ne   = inr λ { refl → ne refl }

Ty= (_[_]T' {Γ₀}{Γ'₀} A'₀ {Δ₀}{Δ'₀} γ'₀) (_[_]T' {Γ₁}{Γ'₁} A'₁ {Δ₁}{Δ'₁} γ'₁) with Con= Γ'₀ Γ'₁
Ty= (_[_]T' {Γ₀}{Γ'₀} A'₀ {Δ₀}{Δ'₀} γ'₀) (_[_]T' {Γ₁}{Γ'₁} A'₁ {Δ₁}{Δ'₁} γ'₁) | inl refl with Ty= A'₀ A'₁
Ty= (_[_]T' {Γ₀}{Γ'₀} A'₀ {Δ₀}{Δ'₀} γ'₀) (_[_]T' {Γ₀}{Γ'₀} A'₁ {Δ₁}{Δ'₁} γ'₁) | inl refl | inl refl with Con= Δ'₀ Δ'₁
Ty= (_[_]T' {Γ₀}{Γ'₀} A'₀ {Δ₀}{Δ'₀} γ'₀) (_[_]T' {Γ₀}{Γ'₀} A'₁ {Δ₁}{Δ'₁} γ'₁) | inl refl | inl refl | inl refl with Sub= γ'₀ γ'₁
Ty= (_[_]T' {Γ₀}{Γ'₀} A'₀ {Δ₀}{Δ'₀} γ'₀) (_[_]T' {Γ₀}{Γ'₀} A'₁ {Δ₁}{Δ'₁} γ'₁) | inl refl | inl refl | inl refl | inl refl = inl refl
Ty= (_[_]T' {Γ₀}{Γ'₀} A'₀ {Δ₀}{Δ'₀} γ'₀) (_[_]T' {Γ₀}{Γ'₀} A'₁ {Δ₁}{Δ'₁} γ'₁) | inl refl | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Ty= (_[_]T' {Γ₀}{Γ'₀} A'₀ {Δ₀}{Δ'₀} γ'₀) (_[_]T' {Γ₀}{Γ'₀} A'₁ {Δ₁}{Δ'₁} γ'₁) | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Ty= (_[_]T' {Γ₀}{Γ'₀} A'₀ {Δ₀}{Δ'₀} γ'₀) (_[_]T' {Γ₀}{Γ'₀} A'₁ {Δ₁}{Δ'₁} γ'₁) | inl refl | inr ne   = inr λ { refl → ne refl }
Ty= (_[_]T' {Γ₀}{Γ'₀} A'₀ {Δ₀}{Δ'₀} γ'₀) (_[_]T' {Γ₁}{Γ'₁} A'₁ {Δ₁}{Δ'₁} γ'₁) | inr ne   = inr λ { refl → ne refl }
Ty= (_ [ _ ]T') Bool' = inr λ ()
Ty= (_ [ _ ]T') (Π' _ _) = inr λ ()
Ty= Bool' (_ [ _ ]T') = inr λ ()
Ty= (Bool' {_}{Γ'₀}) (Bool' {_}{Γ'₁}) with Con= Γ'₀ Γ'₁
... | inl refl = inl refl
... | inr ne   = inr λ { refl → ne refl }
Ty= Bool' (Π' _ _) = inr λ ()
Ty= (Π' _ _) (_ [ _ ]T') = inr λ ()
Ty= (Π' _ _) Bool' = inr λ ()
Ty= (Π' {_}{Γ'₀} A'₀ B'₀) (Π' {_}{Γ'₁} A'₁ B'₁) with Con= Γ'₀ Γ'₁
Ty= (Π' {_}{Γ'₀} A'₀ B'₀) (Π' {_}{Γ'₁} A'₁ B'₁) | inl refl with Ty= A'₀ A'₁
Ty= (Π' {_}{Γ'₀} A'₀ B'₀) (Π' {_}{Γ'₁} A'₁ B'₁) | inl refl | inl refl with Ty= B'₀ B'₁
Ty= (Π' {_}{Γ'₀} A'₀ B'₀) (Π' {_}{Γ'₁} A'₁ B'₁) | inl refl | inl refl | inl refl = inl refl
Ty= (Π' {_}{Γ'₀} A'₀ B'₀) (Π' {_}{Γ'₁} A'₁ B'₁) | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Ty= (Π' {_}{Γ'₀} A'₀ B'₀) (Π' {_}{Γ'₁} A'₁ B'₁) | inl refl | inr ne   = inr λ { refl → ne refl }
Ty= (Π' {_}{Γ'₀} A'₀ B'₀) (Π' {_}{Γ'₁} A'₁ B'₁) | inr ne   = inr λ { refl → ne refl }


Tm= (_[_]t' {_}{Γ'₀}{_}{A'₀} a'₀ {_}{Δ'₀} γ'₀) (_[_]t' {_}{Γ'₁}{_}{A'₁} a'₁ {_}{Δ'₁} γ'₁) with Con= Γ'₀ Γ'₁
Tm= (_[_]t' {_}{Γ'₀}{_}{A'₀} a'₀ {_}{Δ'₀} γ'₀) (_[_]t' {_}{Γ'₁}{_}{A'₁} a'₁ {_}{Δ'₁} γ'₁) | inl refl with Ty= A'₀ A'₁
Tm= (_[_]t' {_}{Γ'₀}{_}{A'₀} a'₀ {_}{Δ'₀} γ'₀) (_[_]t' {_}{Γ'₁}{_}{A'₁} a'₁ {_}{Δ'₁} γ'₁) | inl refl | inl refl with Tm= a'₀ a'₁
Tm= (_[_]t' {_}{Γ'₀}{_}{A'₀} a'₀ {_}{Δ'₀} γ'₀) (_[_]t' {_}{Γ'₁}{_}{A'₁} a'₁ {_}{Δ'₁} γ'₁) | inl refl | inl refl | inl refl with Con= Δ'₀ Δ'₁
Tm= (_[_]t' {_}{Γ'₀}{_}{A'₀} a'₀ {_}{Δ'₀} γ'₀) (_[_]t' {_}{Γ'₁}{_}{A'₁} a'₁ {_}{Δ'₁} γ'₁) | inl refl | inl refl | inl refl | inl refl with Sub= γ'₀ γ'₁
Tm= (_[_]t' {_}{Γ'₀}{_}{A'₀} a'₀ {_}{Δ'₀} γ'₀) (_[_]t' {_}{Γ'₁}{_}{A'₁} a'₁ {_}{Δ'₁} γ'₁) | inl refl | inl refl | inl refl | inl refl | inl refl = inl refl
Tm= (_[_]t' {_}{Γ'₀}{_}{A'₀} a'₀ {_}{Δ'₀} γ'₀) (_[_]t' {_}{Γ'₁}{_}{A'₁} a'₁ {_}{Δ'₁} γ'₁) | inl refl | inl refl | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (_[_]t' {_}{Γ'₀}{_}{A'₀} a'₀ {_}{Δ'₀} γ'₀) (_[_]t' {_}{Γ'₁}{_}{A'₁} a'₁ {_}{Δ'₁} γ'₁) | inl refl | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (_[_]t' {_}{Γ'₀}{_}{A'₀} a'₀ {_}{Δ'₀} γ'₀) (_[_]t' {_}{Γ'₁}{_}{A'₁} a'₁ {_}{Δ'₁} γ'₁) | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (_[_]t' {_}{Γ'₀}{_}{A'₀} a'₀ {_}{Δ'₀} γ'₀) (_[_]t' {_}{Γ'₁}{_}{A'₁} a'₁ {_}{Δ'₁} γ'₁) | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (_[_]t' {_}{Γ'₀}{_}{A'₀} a'₀ {_}{Δ'₀} γ'₀) (_[_]t' {_}{Γ'₁}{_}{A'₁} a'₁ {_}{Δ'₁} γ'₁) | inr ne   = inr λ { refl → ne refl }
Tm= (_ [ _ ]t') q' = inr λ ()
Tm= (_ [ _ ]t') true' = inr λ ()
Tm= (_ [ _ ]t') false' = inr λ ()
Tm= (_ [ _ ]t') (if' _ _ _ _) = inr λ ()
Tm= (_ [ _ ]t') (lam' _) = inr λ ()
Tm= (_ [ _ ]t') (app' _) = inr λ ()
Tm= q' (_ [ _ ]t') = inr λ ()
Tm= (q' {_}{Γ'₀}{_}{A'₀}) (q' {_}{Γ'₁}{_}{A'₁}) with Con= Γ'₀ Γ'₁
Tm= (q' {_}{Γ'₀}{_}{A'₀}) (q' {_}{Γ'₁}{_}{A'₁}) | inl refl with Ty= A'₀ A'₁
Tm= (q' {_}{Γ'₀}{_}{A'₀}) (q' {_}{Γ'₁}{_}{A'₁}) | inl refl | inl refl = inl refl
Tm= (q' {_}{Γ'₀}{_}{A'₀}) (q' {_}{Γ'₁}{_}{A'₁}) | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (q' {_}{Γ'₀}{_}{A'₀}) (q' {_}{Γ'₁}{_}{A'₁}) | inr ne   = inr λ { refl → ne refl }
Tm= q' true' = inr λ ()
Tm= q' false' = inr λ ()
Tm= q' (if' _ _ _ _) = inr λ ()
Tm= q' (lam' _) = inr λ ()
Tm= q' (app' _) = inr λ ()
Tm= true' (_ [ _ ]t') = inr λ ()
Tm= true' q' = inr λ ()
Tm= (true' {_}{Γ'₀}) (true' {_}{Γ'₁}) with Con= Γ'₀ Γ'₁
... | inl refl = inl refl
... | inr ne   = inr λ { refl → ne refl }
Tm= true' false' = inr λ ()
Tm= true' (if' _ _ _ _) = inr λ ()
Tm= true' (lam' _) = inr λ ()
Tm= true' (app' _) = inr λ ()
Tm= false' (_ [ _ ]t') = inr λ ()
Tm= false' q' = inr λ ()
Tm= false' true' = inr λ ()
Tm= (false' {_}{Γ'₀}) (false' {_}{Γ'₁}) with Con= Γ'₀ Γ'₁
... | inl refl = inl refl
... | inr ne   = inr λ { refl → ne refl }
Tm= false' (if' _ _ _ _) = inr λ ()
Tm= false' (lam' _) = inr λ ()
Tm= false' (app' _) = inr λ ()
Tm= (if' _ _ _ _) (_ [ _ ]t') = inr λ ()
Tm= (if' _ _ _ _) q' = inr λ ()
Tm= (if' _ _ _ _) true' = inr λ ()
Tm= (if' _ _ _ _) false' = inr λ ()
Tm= (if' {_}{Γ'₀} C'₀ u'₀ v'₀ t'₀) (if' {_}{Γ'₁} C'₁ u'₁ v'₁ t'₁) with Con= Γ'₀ Γ'₁
Tm= (if' {_}{Γ'₀} C'₀ u'₀ v'₀ t'₀) (if' {_}{Γ'₁} C'₁ u'₁ v'₁ t'₁) | inl refl with Ty= C'₀ C'₁
Tm= (if' {_}{Γ'₀} C'₀ u'₀ v'₀ t'₀) (if' {_}{Γ'₁} C'₁ u'₁ v'₁ t'₁) | inl refl | inl refl with Tm= u'₀ u'₁
Tm= (if' {_}{Γ'₀} C'₀ u'₀ v'₀ t'₀) (if' {_}{Γ'₁} C'₁ u'₁ v'₁ t'₁) | inl refl | inl refl | inl refl with Tm= v'₀ v'₁
Tm= (if' {_}{Γ'₀} C'₀ u'₀ v'₀ t'₀) (if' {_}{Γ'₁} C'₁ u'₁ v'₁ t'₁) | inl refl | inl refl | inl refl | inl refl with Tm= t'₀ t'₁
Tm= (if' {_}{Γ'₀} C'₀ u'₀ v'₀ t'₀) (if' {_}{Γ'₁} C'₁ u'₁ v'₁ t'₁) | inl refl | inl refl | inl refl | inl refl | inl refl = inl refl
Tm= (if' {_}{Γ'₀} C'₀ u'₀ v'₀ t'₀) (if' {_}{Γ'₁} C'₁ u'₁ v'₁ t'₁) | inl refl | inl refl | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (if' {_}{Γ'₀} C'₀ u'₀ v'₀ t'₀) (if' {_}{Γ'₁} C'₁ u'₁ v'₁ t'₁) | inl refl | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (if' {_}{Γ'₀} C'₀ u'₀ v'₀ t'₀) (if' {_}{Γ'₁} C'₁ u'₁ v'₁ t'₁) | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (if' {_}{Γ'₀} C'₀ u'₀ v'₀ t'₀) (if' {_}{Γ'₁} C'₁ u'₁ v'₁ t'₁) | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (if' {_}{Γ'₀} C'₀ u'₀ v'₀ t'₀) (if' {_}{Γ'₁} C'₁ u'₁ v'₁ t'₁) | inr ne   = inr λ { refl → ne refl }
Tm= (if' _ _ _ _) (lam' _) = inr λ ()
Tm= (if' _ _ _ _) (app' _) = inr λ ()
Tm= (lam' _) (_ [ _ ]t') = inr λ ()
Tm= (lam' _) q' = inr λ ()
Tm= (lam' _) true' = inr λ ()
Tm= (lam' _) false' = inr λ ()
Tm= (lam' _) (if' _ _ _ _) = inr λ ()
Tm= (lam' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (lam' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) with Con= Γ'₀ Γ'₁
Tm= (lam' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (lam' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl with Ty= A'₀ A'₁
Tm= (lam' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (lam' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inl refl with Ty= B'₀ B'₁
Tm= (lam' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (lam' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inl refl | inl refl with Tm= t'₀ t'₁
Tm= (lam' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (lam' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inl refl | inl refl | inl refl = inl refl
Tm= (lam' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (lam' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (lam' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (lam' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (lam' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (lam' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (lam' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (lam' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inr ne   = inr λ { refl → ne refl }
Tm= (lam' _) (app' _) = inr λ ()
Tm= (app' _) (_ [ _ ]t') = inr λ ()
Tm= (app' _) q' = inr λ ()
Tm= (app' _) true' = inr λ ()
Tm= (app' _) false' = inr λ ()
Tm= (app' _) (if' _ _ _ _) = inr λ ()
Tm= (app' _) (lam' _) = inr λ ()
Tm= (app' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (app' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) with Con= Γ'₀ Γ'₁
Tm= (app' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (app' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl with Ty= A'₀ A'₁
Tm= (app' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (app' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inl refl with Ty= B'₀ B'₁
Tm= (app' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (app' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inl refl | inl refl with Tm= t'₀ t'₁
Tm= (app' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (app' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inl refl | inl refl | inl refl = inl refl
Tm= (app' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (app' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (app' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (app' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (app' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (app' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inl refl | inr ne   = inr λ { refl → ne refl }
Tm= (app' {_}{Γ'₀}{_}{A'₀}{_}{B'₀} t'₀) (app' {_}{Γ'₁}{_}{A'₁}{_}{B'₁} t'₁) | inr ne   = inr λ { refl → ne refl }
