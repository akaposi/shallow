{-# OPTIONS --no-pattern-matching #-}

module Renamings where

-- The category of renamings. Sub is renamings, Tm is variables.

open import Agda.Primitive
open import Renamings.Lib
import Standard.Lib as m
import Lib as l

import WrappedStandard.Lib as w
import WrappedStandard as s

infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_

Sub : ∀{i}(Γ : s.Con i){j}(Δ : s.Con j) → Set (i ⊔ j)
Sub Γ Δ = Wrap (w.∣ Γ ∣C → w.∣ Δ ∣C)

id : ∀{i}{Γ : s.Con i} → Sub Γ Γ
id = mk λ γ → γ

_∘_ : ∀{i}{Θ : s.Con i}{j}{Δ : s.Con j}(σ : Sub Θ Δ){k}{Γ : s.Con k}(δ : Sub Γ Θ) →
  Sub Γ Δ
σ ∘ δ = mk λ γ → ∣ σ ∣ (∣ δ ∣ γ)

ε : ∀{i}{Γ : s.Con i} → Sub Γ s.∙
ε = mk λ γ → m.tt

⌜_⌝ : ∀{i}{Γ : s.Con i}{j}{Δ : s.Con j} → Sub Γ Δ → s.Sub Γ Δ
⌜ δ ⌝ = w.mks ∣ δ ∣

Tm : ∀{i}(Γ : s.Con i){j}(A : s.Ty Γ j) → Set (i ⊔ j)
Tm Γ A = Wrap' ((γ : w.∣ Γ ∣C) → w.∣ A ∣T γ)

⌜_⌝t : ∀{i}{Γ : s.Con i}{j}{A : s.Ty Γ j} → Tm Γ A → s.Tm Γ A
⌜ t ⌝t = w.mkt ∣ t ∣

_[_]t : ∀{i}{Δ : s.Con i}{j}{A : s.Ty Δ j}(t : Tm Δ A){k}{Γ : s.Con k}(σ : Sub Γ Δ) → Tm Γ (A s.[ ⌜ σ ⌝ ]T)
t [ σ ]t = mk λ γ → ∣ t ∣ (∣ σ ∣ γ)

_,_ : ∀{i}{Γ : s.Con i}{j}{Δ : s.Con j}(σ : Sub Γ Δ){k}{A : s.Ty Δ k}(t : Tm Γ (A s.[ ⌜ σ ⌝ ]T)) → Sub Γ (Δ s.▷ A)
σ , t = mk λ γ → (∣ σ ∣ γ m.,Σ ∣ t ∣ γ)

p : ∀{i}{Γ : s.Con i}{j}{A : s.Ty Γ j} → Sub (Γ s.▷ A) Γ
p = mk m.fst

⌜p⌝≡sp : ∀{i}{Γ : s.Con i}{j}{A : s.Ty Γ j} → ⌜ p {A = A} ⌝ l.≡ s.p
⌜p⌝≡sp = l.refl

q : ∀{i}{Γ : s.Con i}{j}{A : s.Ty Γ j} → Tm (Γ s.▷ A) (A s.[ s.p ]T)
q = mk m.snd

-- abbreviations

p² :
  ∀{i}{Γ : s.Con i}
   {j}{A : s.Ty Γ j}
   {k}{B : s.Ty (Γ s.▷ A) k} →
   Sub (Γ s.▷ A s.▷ B) Γ
p² = p ∘ p

p³ :
  ∀{i}{Γ : s.Con i}
   {j}{A : s.Ty Γ j}
   {k}{B : s.Ty (Γ s.▷ A) k}
   {l}{C : s.Ty (Γ s.▷ A s.▷ B) l} →
   Sub (Γ s.▷ A s.▷ B s.▷ C) Γ
p³ = p ∘ p ∘ p

p⁴ :
  ∀{i}{Γ : s.Con i}
   {j}{A : s.Ty Γ j}
   {k}{B : s.Ty (Γ s.▷ A) k}
   {l}{C : s.Ty (Γ s.▷ A s.▷ B) l}
   {m}{D : s.Ty (Γ s.▷ A s.▷ B s.▷ C) m} →
   Sub (Γ s.▷ A s.▷ B s.▷ C s.▷ D) Γ
p⁴ = p ∘ p ∘ p ∘ p

p⁵ :
  ∀{i}{Γ : s.Con i}
   {j}{A : s.Ty Γ j}
   {k}{B : s.Ty (Γ s.▷ A) k}
   {l}{C : s.Ty (Γ s.▷ A s.▷ B) l}
   {m}{D : s.Ty (Γ s.▷ A s.▷ B s.▷ C) m}
   {n}{E : s.Ty (Γ s.▷ A s.▷ B s.▷ C s.▷ D) n} →
   Sub (Γ s.▷ A s.▷ B s.▷ C s.▷ D s.▷ E) Γ
p⁵ = p ∘ p ∘ p ∘ p ∘ p

p⁶ :
  ∀{i}{Γ : s.Con i}
   {j}{A : s.Ty Γ j}
   {k}{B : s.Ty (Γ s.▷ A) k}
   {l}{C : s.Ty (Γ s.▷ A s.▷ B) l}
   {m}{D : s.Ty (Γ s.▷ A s.▷ B s.▷ C) m}
   {n}{E : s.Ty (Γ s.▷ A s.▷ B s.▷ C s.▷ D) n}
   {o}{F : s.Ty (Γ s.▷ A s.▷ B s.▷ C s.▷ D s.▷ E) o} →
   Sub (Γ s.▷ A s.▷ B s.▷ C s.▷ D s.▷ E s.▷ F) Γ
p⁶ = p ∘ p ∘ p ∘ p ∘ p ∘ p

v⁰ : ∀{i}{Γ : s.Con i}{j}{A : s.Ty Γ j} → Tm (Γ s.▷ A) (A s.[ ⌜ p ⌝ ]T)
v⁰ = q

v¹ :
  ∀{i}{Γ : s.Con i}
   {j}{A : s.Ty Γ j}
   {k}{B : s.Ty (Γ s.▷ A) k} →
   Tm (Γ s.▷ A s.▷ B) (A s.[ ⌜ p² ⌝ ]T)
v¹ = v⁰ [ p ]t

v² :
  ∀{i}{Γ : s.Con i}
   {j}{A : s.Ty Γ j}
   {k}{B : s.Ty (Γ s.▷ A) k}
   {l}{C : s.Ty (Γ s.▷ A s.▷ B) l} →
   Tm (Γ s.▷ A s.▷ B s.▷ C) (A s.[ ⌜ p³ ⌝ ]T)
v² = v⁰ [ p² ]t

v³ :
  ∀{i}{Γ : s.Con i}
   {j}{A : s.Ty Γ j}
   {k}{B : s.Ty (Γ s.▷ A) k}
   {l}{C : s.Ty (Γ s.▷ A s.▷ B) l}
   {m}{D : s.Ty (Γ s.▷ A s.▷ B s.▷ C) m} →
   Tm (Γ s.▷ A s.▷ B s.▷ C s.▷ D) (A s.[ ⌜ p⁴ ⌝ ]T)
v³ = v⁰ [ p³ ]t

v⁴ :
  ∀{i}{Γ : s.Con i}
   {j}{A : s.Ty Γ j}
   {k}{B : s.Ty (Γ s.▷ A) k}
   {l}{C : s.Ty (Γ s.▷ A s.▷ B) l}
   {m}{D : s.Ty (Γ s.▷ A s.▷ B s.▷ C) m}
   {n}{E : s.Ty (Γ s.▷ A s.▷ B s.▷ C s.▷ D) n} →
   Tm (Γ s.▷ A s.▷ B s.▷ C s.▷ D s.▷ E) (A s.[ ⌜ p⁵ ⌝ ]T)
v⁴ = v⁰ [ p⁴ ]t

v⁵ :
  ∀{i}{Γ : s.Con i}
   {j}{A : s.Ty Γ j}
   {k}{B : s.Ty (Γ s.▷ A) k}
   {l}{C : s.Ty (Γ s.▷ A s.▷ B) l}
   {m}{D : s.Ty (Γ s.▷ A s.▷ B s.▷ C) m}
   {n}{E : s.Ty (Γ s.▷ A s.▷ B s.▷ C s.▷ D) n}
   {o}{F : s.Ty (Γ s.▷ A s.▷ B s.▷ C s.▷ D s.▷ E) o} →
   Tm (Γ s.▷ A s.▷ B s.▷ C s.▷ D s.▷ E s.▷ F) (A s.[ ⌜ p⁶ ⌝ ]T)
v⁵ = v⁰ [ p⁵ ]t

_^_ : ∀{i}{Γ : s.Con i}{j}{Δ : s.Con j}(σ : Sub Γ Δ){k}(A : s.Ty Δ k) →
  Sub (Γ s.▷ A s.[ ⌜ σ ⌝ ]T) (Δ s.▷ A)
_^_ {i}{Γ}{j}{Δ} σ {k} A = σ ∘ p , v⁰ {i}{Γ}{_}{A s.[ ⌜ σ ⌝ ]T}
