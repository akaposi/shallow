module Glue where

open import Agda.Primitive
import Lib as m
import WrappedStandard as S
import WrappedStandardDeepCopy as T
import Pseudomorphism as F
import Pseudomorphism.Maps as F

-- in this module we define gluing of the pseudomorphism F : S → T,
-- see the paper "Gluing for type theory"

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixr 6 _⇒_
infixl 6 _,Σ_

-- substitution calculus

Con : ∀ i → S.Con i → Set (lsuc i)
Con i Γˢ = T.Ty (F.Con Γˢ) i

Ty : ∀{i}{Γˢ : S.Con i}(Γ : Con i Γˢ) j (Aˢ : S.Ty Γˢ j) → Set (i ⊔ lsuc j)
Ty {i}{Γˢ} Γ j Aˢ = T.Ty (F.Con Γˢ T.▷ Γ T.▷ F.Ty Aˢ T.[ T.p ]T) j

Sub   : ∀ {i}{Γˢ : S.Con i}(Γ : Con i Γˢ)
          {j}{Δˢ : S.Con j}(Δ : Con j Δˢ)
          → S.Sub Γˢ Δˢ → Set (i ⊔ j)
Sub {i}{Γˢ} Γ {j}{Δˢ} Δ σˢ = T.Tm (F.Con Γˢ T.▷ Γ) (Δ T.[ F.Sub σˢ T.∘ T.p ]T)

Tm  : ∀ {i}{Γˢ : S.Con i}(Γ : Con i Γˢ)
          {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ j Aˢ) → S.Tm Γˢ Aˢ → Set (i ⊔ j)
Tm {i}{Γˢ} Γ {j}{Aˢ} A tˢ = T.Tm (F.Con Γˢ T.▷ Γ) (A T.[ T.id T., F.Tm tˢ T.[ T.p ]t ]T)

id : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ} → Sub Γ Γ S.id
id = T.q

_∘_ :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con i Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con j Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} (σ : Sub Θ Δ σˢ)
   {k}{Γˢ : S.Con k}     {Γ : Con k Γˢ}
      {δˢ : S.Sub Γˢ Θˢ} (δ : Sub Γ Θ δˢ)
  → Sub Γ Δ (σˢ S.∘ δˢ)
σ ∘ δ = σ T.[ F.Sub _ T.∘ T.p T., δ ]t

ass :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con i Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con j Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
   {k}{Ξˢ : S.Con k}     {Ξ : Con k Ξˢ}
      {δˢ : S.Sub Ξˢ Θˢ} {δ : Sub Ξ Θ δˢ}
   {l}{Γˢ : S.Con l}     {Γ : Con l Γˢ}
      {νˢ : S.Sub Γˢ Ξˢ} {ν : Sub Γ Ξ νˢ}
   → m._≡_ {A = Sub Γ Δ ((σˢ S.∘ δˢ) S.∘ νˢ)}
           (_∘_ {Θ = Ξ}{Δ = Δ}{σˢ = σˢ S.∘ δˢ}(_∘_ {Θ = Θ}{Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ} δ){δˢ = νˢ} ν)
           (_∘_ {Θ = Θ}{Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ S.∘ νˢ}(_∘_ {Θ = Ξ}{Δ = Θ}{σˢ = δˢ} δ {δˢ = νˢ} ν))
ass = m.refl

idl :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
  → m._≡_ {A = Sub Γ Δ σˢ} (_∘_ {Θ = Δ}{Δ = Δ}{σˢ = S.id}(id {Γ = Δ}){δˢ = σˢ} σ) σ
idl = m.refl

idr :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
  → m._≡_ {A = Sub Γ Δ σˢ} (_∘_ {Θ = Γ}{Δ = Δ}{σˢ = σˢ} σ {δˢ = S.id} (id {Γˢ = Γˢ}{Γ = Γ})) σ
idr = m.refl

_[_]T :
  ∀ {i}{Δˢ : S.Con i}   {Δ : Con i Δˢ}
    {j}{Aˢ : S.Ty Δˢ j} (A : Ty Δ j Aˢ)
    {k}{Γˢ : S.Con k}   {Γ : Con k Γˢ}
    {σˢ : S.Sub Γˢ Δˢ}  (σ : Sub Γ Δ σˢ)
    → Ty Γ j (Aˢ S.[ σˢ ]T)
A [ σ ]T = A T.[ F.Sub _ T.∘ T.p² T., σ T.[ T.p ]t T., T.q ]T

_[_]t :
  ∀{i}{Δˢ : S.Con i}     {Δ : Con i Δˢ}
   {j}{Aˢ : S.Ty Δˢ j}   {A : Ty Δ j Aˢ}
      {tˢ : S.Tm Δˢ Aˢ}  (t : Tm Δ A tˢ)
   {k}{Γˢ : S.Con k}     {Γ : Con k Γˢ}
      {σˢ : S.Sub Γˢ Δˢ} (σ : Sub Γ Δ σˢ)
  → Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) (tˢ S.[ σˢ ]t)
t [ σ ]t = t T.[ F.Sub _ T.∘ T.p T., σ ]t

[id]T :
  ∀{i}{Γˢ : S.Con i}   {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j} {A : Ty Γ j Aˢ}
  → m._≡_ {A = Ty Γ j Aˢ} (_[_]T {Aˢ = Aˢ} A {σˢ = S.id} (id {Γ = Γ})) A
[id]T = m.refl

[∘]T :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con i Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con j Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
   {k}{Γˢ : S.Con k}     {Γ : Con k Γˢ}
      {δˢ : S.Sub Γˢ Θˢ} {δ : Sub Γ Θ δˢ}
   {l}{Aˢ : S.Ty Δˢ l}   {A : Ty Δ l Aˢ}
   → m._≡_ {A = Ty Γ l (Aˢ S.[ σˢ S.∘ δˢ ]T)}
          (_[_]T {Aˢ = Aˢ S.[ σˢ ]T} (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) {σˢ = δˢ} δ)
          (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ S.∘ δˢ} (_∘_ {Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ} δ))
[∘]T = m.refl

[id]t :
  ∀{i}{Γˢ : S.Con i}   {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j} {A : Ty Γ j Aˢ}
   {tˢ : S.Tm Γˢ Aˢ}{t : Tm Γ A tˢ}
  → m._≡_ {A = Tm Γ A tˢ}
          (_[_]t {A = A} {tˢ = tˢ} t {σˢ = S.id} (id {Γ = Γ}))
          t
[id]t = m.refl

[∘]t :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con i Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con j Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
   {k}{Γˢ : S.Con k}     {Γ : Con k Γˢ}
      {δˢ : S.Sub Γˢ Θˢ} {δ : Sub Γ Θ δˢ}
   {l}{Aˢ : S.Ty Δˢ l}   {A : Ty Δ l Aˢ}
   {tˢ : S.Tm Δˢ Aˢ}{t : Tm Δ A tˢ}
   → m._≡_ {A = Tm Γ (_[_]T {Aˢ = Aˢ S.[ σˢ ]T}(_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) {σˢ = δˢ} δ) (tˢ S.[ σˢ ]t S.[ δˢ ]t)}
          (_[_]t {Aˢ = Aˢ S.[ σˢ ]T}{A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}
                 {tˢ = tˢ S.[ σˢ ]t}(_[_]t {Aˢ = Aˢ}{A = A} {tˢ = tˢ} t {σˢ = σˢ} σ)
                 {σˢ = δˢ} δ)
          (_[_]t {Aˢ = Aˢ}{A = A}
                 {tˢ = tˢ} t
                 {σˢ = σˢ S.∘ δˢ} (_∘_ {Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ} δ))
[∘]t = m.refl

∙ : Con lzero S.∙
∙ = T.⊤

ε : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
    → Sub Γ ∙ S.ε
ε = T.tt

•η :
   ∀  {i}{Γˢ : S.Con i}      {Γ : Con i Γˢ}
         {σˢ : S.Sub Γˢ S.∙} {σ : Sub Γ ∙ σˢ}
   → m._≡_ {A = Sub Γ ∙ σˢ} σ (ε {Γ = Γ})
•η = m.refl

_▷_   : ∀ {i}{Γˢ : S.Con i}(Γ : Con i Γˢ)
          {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ j Aˢ)
          → Con (i ⊔ j) (Γˢ S.▷ Aˢ)
Γ ▷ A = T.Σ (Γ T.[ T.p T.∘ F.▷ F.₁ ]T) (A T.[ T.p T.∘ F.▷ F.₁ T.∘ T.p T., T.v⁰ T., T.q T.[ F.▷ F.₁ T.∘ T.p ]t ]T)

_,_ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            (σ : Sub Γ Δ σˢ)
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ k Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} (t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ)
   → Sub Γ (_▷_ Δ {Aˢ = Aˢ} A) (σˢ S., tˢ)
σ , t = σ T.,Σ t

p :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ k Aˢ} →
   Sub (_▷_ Γ {Aˢ = Aˢ} A) Γ S.p
p = T.fst T.q

q :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ k Aˢ} →
   Tm (_▷_ Γ {Aˢ = Aˢ} A)
      {Aˢ = Aˢ S.[ S.p {A = Aˢ} ]T}
      (_[_]T {Aˢ = Aˢ} A {Γ = _▷_ Γ {Aˢ = Aˢ} A}{σˢ = S.p} (p {Aˢ = Aˢ}{A = A}))
      S.q
q = T.snd T.q

▷β₁ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ k Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ}
  → m._≡_ {A = Sub Γ Δ σˢ}
          (_∘_ {Θ = _▷_ Δ {Aˢ = Aˢ} A}{Δ = Δ}
               {σˢ = S.p}(p {Aˢ = Aˢ}{A = A})
               {δˢ = σˢ S., tˢ}(_,_ σ {Aˢ = Aˢ}{A = A}{tˢ = tˢ} t))
          σ
▷β₁ = m.refl

▷β₂ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ k Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ}
  → m._≡_ {A = Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ}
          (_[_]t {A = _[_]T {Aˢ = Aˢ} A {Γ = _▷_ Δ {Aˢ = Aˢ} A} {σˢ = S.p {A = Aˢ}} (p {Aˢ = Aˢ}{A = A})}
                 {tˢ = S.q {A = Aˢ}}(q {Aˢ = Aˢ}{A = A})
                 {σˢ = σˢ S., tˢ}(_,_ σ {Aˢ = Aˢ}{A = A}{tˢ = tˢ} t))
          t
▷β₂ = m.refl

▷η :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ k Aˢ}
  → m._≡_ {A = Sub (_▷_ Γ {Aˢ = Aˢ} A) (_▷_ Γ {Aˢ = Aˢ} A) (S.p S., S.q)}
          (_,_ {σˢ = S.p}(p {Aˢ = Aˢ}{A = A}){Aˢ = Aˢ}{A = A}{tˢ = S.q}(q {Aˢ = Aˢ}{A = A}))
          (id {Γ = _▷_ Γ {Aˢ = Aˢ} A})
▷η = m.refl

,∘ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ k Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) tˢ}
   {l}{Θˢ : S.Con l}                {Θ : Con l Θˢ}
      {δˢ : S.Sub Θˢ Γˢ}            {δ : Sub Θ Γ δˢ} →
  m._≡_ {A = Sub Θ (_▷_ Δ {Aˢ = Aˢ} A) ((σˢ S., tˢ) S.∘ δˢ)}
        (_∘_ {Θ = Γ}{Δˢ = Δˢ S.▷ Aˢ}{Δ = Δ ▷ A}{σˢ = σˢ S., tˢ}(_,_ {σˢ = σˢ} σ {Aˢ = Aˢ}{A = A}{tˢ = tˢ} t){δˢ = δˢ} δ)
        (_,_ (_∘_ {Δ = Δ}{σˢ = σˢ} σ {δˢ = δˢ} δ){Aˢ = Aˢ}{A = A}{tˢ = tˢ S.[ δˢ ]t}(_[_]t {A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}{tˢ = tˢ} t {σˢ = δˢ} δ))
,∘ = m.refl

-- abbreviations

p² :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ} →
   Sub (_▷_ (_▷_ Γ {Aˢ = Aˢ} A){Aˢ = Bˢ} B) Γ S.p²
p² {Γˢ = Γˢ}{Γ}{Aˢ = Aˢ}{A}{Bˢ = Bˢ}{B} =
  _∘_ {Θˢ = Γˢ S.▷ Aˢ}{Θ = _▷_ Γ {Aˢ = Aˢ} A}{Δˢ = Γˢ}{Δ = Γ}
      {σˢ = S.p {Γ = Γˢ}{A = Aˢ}}(p {Γˢ = Γˢ}{Γ}{Aˢ = Aˢ}{A})
      {Γˢ = Γˢ S.▷ Aˢ S.▷ Bˢ}{Γ = _▷_ (_▷_ Γ {Aˢ = Aˢ} A){Aˢ = Bˢ} B}
      {δˢ = S.p {Γ = Γˢ S.▷ Aˢ}{A = Bˢ}}(p {Γˢ = Γˢ S.▷ Aˢ}{Γ = _▷_ Γ {Aˢ = Aˢ} A}{Aˢ = Bˢ}{A = B})

v⁰ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ k Aˢ}
  → Tm (_▷_ Γ {Aˢ = Aˢ} A)
       ((_[_]T {Aˢ = Aˢ} A {Γ = _▷_ Γ {Aˢ = Aˢ} A}{σˢ = S.p {A = Aˢ}}
       (p {Γ = Γ}{Aˢ = Aˢ}{A = A}))) S.v⁰
v⁰ {Γˢ = Γˢ}{Γ}{Aˢ = Aˢ}{A} = q {Aˢ = Aˢ}{A = A}

_^_ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            (σ : Sub Γ Δ σˢ)
   {k}{Aˢ : S.Ty Δˢ k}              (A : Ty Δ k Aˢ)
  → Sub (Γ ▷ _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ) (Δ ▷ A) (σˢ S.^ Aˢ)
_^_ {i} {Γˢ} {Γ} {j} {Δˢ} {Δ} {σˢ} σ {k} {Aˢ} A =
  _,_ {Γˢ = Γˢ S.▷ Aˢ S.[ σˢ ]T}{_▷_ Γ {Aˢ = Aˢ S.[ σˢ ]T}(_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ)}
      {σˢ = σˢ S.∘ S.p}(_∘_ {Θˢ = Γˢ}{Θ = Γ}{Δˢ = Δˢ}{Δ = Δ}{σˢ = σˢ} σ
                             {Γˢ = Γˢ S.▷ Aˢ S.[ σˢ ]T}{Γ ▷ _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}
                             {δˢ = S.p {Γ = Γˢ}{A = Aˢ S.[ σˢ ]T}}
                             (p {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ S.[ σˢ ]T}{A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}))
      {Aˢ = Aˢ}{A = A}
      {tˢ = S.v⁰}(v⁰ {Γˢ = Γˢ}{Γ}{Aˢ = Aˢ S.[ σˢ ]T}{A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ})

-- Π
--------------------------------------------------------------------------------

Π :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          (A : Ty Γ j Aˢ)
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} (B : Ty (Γ ▷ A) k Bˢ)
   → Ty Γ (j ⊔ k) (S.Π Aˢ Bˢ)
Π {Aˢ = Aˢ} A {Bˢ = Bˢ} B =
  T.Π (F.Ty Aˢ T.[ T.p² ]T)
      (T.Π (A T.[ T.p² T., T.q ]T)
           (B T.[ F.▷ F.₂ T.∘ (T.p⁴ T., T.v¹) T., (T.v³ T.,Σ T.v⁰) T., F.Π₁ T.[ T.p⁴ T., T.v² ]t T.$ T.v¹ ]T))

lam :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ}
      {tˢ : S.Tm (Γˢ S.▷ Aˢ) Bˢ}(t : Tm (Γ ▷ A) B tˢ)
  → Tm Γ (Π {Aˢ = Aˢ} A {Bˢ = Bˢ} B) (S.lam tˢ)
lam t = T.lam (T.lam (t T.[ F.▷ F.₂ T.∘ (T.p³ T., T.v¹) T., (T.v² T.,Σ T.v⁰) ]t))

app :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ}
      {tˢ : S.Tm Γˢ (S.Π Aˢ Bˢ)}(t : Tm Γ (Π {Aˢ = Aˢ} A {Bˢ = Bˢ} B) tˢ)
  → Tm (Γ ▷ A) B (S.app tˢ)
app t = (T.app (T.app t)) T.[ T.p T.∘ F.▷ F.₁ T.∘ T.p T., T.fst T.v⁰ T., T.v⁰ T.[ F.▷ F.₁ T.∘ T.p ]t T., T.snd T.v⁰ ]t

Πβ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ}
      {tˢ : S.Tm (Γˢ S.▷ Aˢ) Bˢ}{t : Tm (Γ ▷ A) B tˢ}
  → m._≡_ {A = Tm (Γ ▷ A) B tˢ}
         (app {Bˢ = Bˢ}{B}{tˢ = S.lam tˢ}(lam {Aˢ = Aˢ}{A}{Bˢ = Bˢ}{B}{tˢ = tˢ} t))
         t
Πβ = m.refl

Πη :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ}
      {tˢ : S.Tm Γˢ (S.Π Aˢ Bˢ)}{t : Tm Γ (Π A {Bˢ = Bˢ} B) tˢ}
  → m._≡_ {A = Tm Γ (Π A {Bˢ = Bˢ} B) tˢ}
         (lam {Aˢ = Aˢ}{A}{Bˢ = Bˢ}{B}{tˢ = S.app tˢ}(app {Aˢ = Aˢ}{A}{Bˢ = Bˢ}{B}{tˢ = tˢ} t))
         t
Πη = m.refl

Π[] :
  ∀{i}{Γˢ : S.Con i}           {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}         {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k}{B : Ty (Γ ▷ A) k Bˢ}
   {l}{Θˢ : S.Con l}           {Θ : Con l Θˢ}
      {σˢ : S.Sub Θˢ Γˢ}       {σ : Sub Θ Γ σˢ}
  → m._≡_ {A = Ty Θ _ (S.Π Aˢ Bˢ S.[ σˢ ]T)}
         (_[_]T {Δ = Γ}{Aˢ = S.Π Aˢ Bˢ}(Π A {Bˢ = Bˢ} B){σˢ = σˢ} σ)
         (Π (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ)
            {Bˢ = Bˢ S.[ σˢ S.^ Aˢ ]T}
            (_[_]T {Aˢ = Bˢ} B {σˢ = σˢ S.^ Aˢ}(_^_ {σˢ = σˢ} σ {Aˢ = Aˢ} A)))
Π[] = m.refl

lam[] :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {l}{Δˢ : S.Con l}            {Δ : Con l Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}        {σ : Sub Γ Δ σˢ}
   {j}{Aˢ : S.Ty Δˢ j}          {A : Ty Δ j Aˢ}
   {k}{Bˢ : S.Ty (Δˢ S.▷ Aˢ) k} {B : Ty (Δ ▷ A) k Bˢ}
      {tˢ : S.Tm (Δˢ S.▷ Aˢ) Bˢ}{t : Tm (_▷_ Δ {Aˢ = Aˢ} A) {Aˢ = Bˢ} B tˢ}
  → m._≡_ {A = Tm Γ
                  {Aˢ = S.Π Aˢ Bˢ S.[ σˢ ]T}
                  (_[_]T {Aˢ = S.Π Aˢ Bˢ} (Π {Aˢ = Aˢ} A {Bˢ = Bˢ} B) {σˢ = σˢ} σ)
                  (S.lam tˢ S.[ σˢ ]t)}
         (_[_]t {A = Π {Aˢ = Aˢ} A {Bˢ = Bˢ} B}{tˢ = S.lam tˢ}(lam {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{tˢ = tˢ} t){σˢ = σˢ} σ)
         (lam {Aˢ = Aˢ S.[ σˢ ]T}
              {_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}
              {Bˢ = Bˢ S.[ σˢ S.^ Aˢ ]T}
              {_[_]T {Aˢ = Bˢ} B {σˢ = σˢ S.^ Aˢ}(_^_ {σˢ = σˢ} σ {Aˢ = Aˢ} A)}
              {tˢ = tˢ S.[ σˢ S.^ Aˢ ]t}
              (_[_]t {Aˢ = Bˢ}{A = B}{tˢ = tˢ} t {σˢ = σˢ S.^ Aˢ} (_^_ {σˢ = σˢ} σ {Aˢ = Aˢ} A)))
lam[] = m.refl

-- abbreviations

_⇒_ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          (A : Ty Γ j Aˢ)
   {k}{Bˢ : S.Ty Γˢ k}          (B : Ty Γ k Bˢ) →
   Ty Γ _ (Aˢ S.⇒ Bˢ)
_⇒_ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B =
  Π {Aˢ = Aˢ} A
    {Bˢ = Bˢ S.[ S.p {A = Aˢ} ]T}
    (_[_]T {Aˢ = Bˢ} B
           {Γˢ = Γˢ S.▷ Aˢ}{_▷_ Γ {Aˢ = Aˢ} A}
           {σˢ = S.p {A = Aˢ}} (p {Aˢ = Aˢ}{A = A}))

-- Σ
--------------------------------------------------------------------------------

Σ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          (A : Ty Γ j Aˢ)
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} (B : Ty (Γ ▷ A) k Bˢ)
   → Ty Γ _ (S.Σ Aˢ Bˢ)
Σ A B =
  T.Σ (A T.[ T.p T., T.fst (F.Σ F.₁ T.[ T.p² T., T.q ]t) ]T)
      (B T.[ F.▷ F.₂ T.∘ (T.p³ T., T.fst (F.Σ F.₁ T.[ T.p³ T., T.v¹ ]t)) T., T.v² T.,Σ T.v⁰ T., T.snd (F.Σ F.₁ T.[ T.p³ T., T.v¹ ]t) ]T)
-- this is wrong in the paper!

_,Σ_ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}(u : Tm Γ A uˢ)
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      (v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aˢ = Bˢ} B {σˢ = S.id S., uˢ}(_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = uˢ} u)) vˢ)
   → Tm Γ (Σ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B) (S._,Σ_ {B = Bˢ} uˢ vˢ)
u ,Σ v = u T.,Σ v

fst :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ}
      {tˢ : S.Tm Γˢ (S.Σ Aˢ Bˢ)}(t : Tm Γ (Σ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B) tˢ)
   → Tm Γ A (S.fst tˢ)
fst = T.fst

snd :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ}
      {tˢ : S.Tm Γˢ (S.Σ Aˢ Bˢ)}(t : Tm Γ (Σ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B) tˢ)
   → Tm Γ (_[_]T {Δ = Γ ▷ A}
                 {Aˢ = Bˢ}
                 B {σˢ = S.id S., S.fst tˢ}
                 (_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = S.fst tˢ} (fst {A = A}{Bˢ = Bˢ}{B = B}{tˢ = tˢ} t)))
          (S.snd tˢ)
snd = T.snd

Σβ₁ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      {v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aˢ = Bˢ} B {σˢ = S.id S., uˢ}(_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = uˢ} u)) vˢ} →
  m._≡_ {A = Tm Γ A uˢ}
        (fst {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{tˢ = uˢ S.,Σ vˢ}
             (_,Σ_ {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{uˢ = uˢ} u {vˢ = vˢ} v))
        u
Σβ₁ = m.refl

Σβ₂ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      {v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aˢ = Bˢ} B {σˢ = S.id S., uˢ}(_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = uˢ} u)) vˢ} →
  m._≡_ {A = Tm Γ (_[_]T {Δ = Γ ▷ A}{Aˢ = Bˢ} B {σˢ = S.id S., uˢ}(_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = uˢ} u)) vˢ}
        (snd {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{tˢ = uˢ S.,Σ vˢ}
             (_,Σ_ {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{uˢ = uˢ} u {vˢ = vˢ} v))
        v
Σβ₂ = m.refl

Ση :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ}
      {tˢ : S.Tm Γˢ (S.Σ Aˢ Bˢ)}{t : Tm Γ (Σ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B) tˢ} →
  m._≡_ {A = Tm Γ (Σ {Γˢ = Γˢ}{Γ = Γ}{Aˢ = Aˢ} A {Bˢ = Bˢ} B) tˢ}
        (_,Σ_ {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}
              {uˢ = S.fst tˢ}(fst {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{tˢ = tˢ} t)
              {vˢ = S.snd tˢ}(snd {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{tˢ = tˢ} t))
        t
Ση = m.refl

Σ[] :
  ∀{i}{Γˢ : S.Con i}           {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}         {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k}{B : Ty (Γ ▷ A) k Bˢ}
   {l}{Θˢ : S.Con l}           {Θ : Con l Θˢ}
      {σˢ : S.Sub Θˢ Γˢ}       {σ : Sub Θ Γ σˢ}
  → m._≡_ {A = Ty Θ _ (S.Σ Aˢ Bˢ S.[ σˢ ]T)}
         (_[_]T {Δ = Γ}{Aˢ = S.Σ Aˢ Bˢ}(Σ A {Bˢ = Bˢ} B){σˢ = σˢ} σ)
         (Σ (_[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ)
            {Bˢ = Bˢ S.[ σˢ S.^ Aˢ ]T}
            (_[_]T {Aˢ = Bˢ} B {σˢ = σˢ S.^ Aˢ}(_^_ {σˢ = σˢ} σ {Aˢ = Aˢ} A)))
Σ[] = m.refl

,Σ[] :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ j Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) k Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      {v : Tm Γ (_[_]T {Δ = Γ ▷ A}{Aˢ = Bˢ} B {σˢ = S.id S., uˢ}(_,_ {σˢ = S.id} (id {Γ = Γ}) {A = A}{tˢ = uˢ} u)) vˢ}
   {l}{Θˢ : S.Con l}            {Θ : Con l Θˢ}
      {σˢ : S.Sub Θˢ Γˢ}        {σ : Sub Θ Γ σˢ} →
  m._≡_ {A = Tm Θ
                {Aˢ = S.Σ Aˢ Bˢ S.[ σˢ ]T}
                (_[_]T {Aˢ = S.Σ Aˢ Bˢ} (Σ {Aˢ = Aˢ} A {Bˢ = Bˢ} B) {σˢ = σˢ} σ)
                ((S._,Σ_ {A = Aˢ}{B = Bˢ} uˢ vˢ) S.[ σˢ ]t)}
        (_[_]t {Aˢ = S.Σ Aˢ Bˢ}{A = Σ {Aˢ = Aˢ} A {Bˢ = Bˢ} B}{tˢ = S._,Σ_ {A = Aˢ}{B = Bˢ} uˢ vˢ}(_,Σ_ {Aˢ = Aˢ}{A = A}{Bˢ = Bˢ}{B = B}{uˢ = uˢ} u {vˢ = vˢ} v) {σˢ = σˢ} σ)
        (_,Σ_ {Aˢ = Aˢ S.[ σˢ ]T}
              {A = _[_]T {Aˢ = Aˢ} A {σˢ = σˢ} σ}
              {Bˢ = Bˢ S.[ σˢ S.^ Aˢ ]T}
              {B = _[_]T {Aˢ = Bˢ} B {σˢ = σˢ S.^ Aˢ} (_^_ {σˢ = σˢ} σ {Aˢ = Aˢ} A)}
              {uˢ = uˢ S.[ σˢ ]t}
              (_[_]t {Aˢ = Aˢ}{A = A}{tˢ = uˢ} u {σˢ = σˢ} σ)
              {vˢ = vˢ S.[ σˢ ]t}
              (_[_]t {Aˢ = Bˢ S.[ S._,_ S.id {A = Aˢ} uˢ ]T}{A = _[_]T {Aˢ = Bˢ} B {σˢ = S.id S., uˢ} (_,_ (id {Γˢ = Γˢ}{Γ = Γ}){Aˢ = Aˢ}{A = A}{tˢ = uˢ} u)}{tˢ = vˢ} v {σˢ = σˢ} σ))
,Σ[] = m.refl

-- unit
--------------------------------------------------------------------------------

⊤ : ∀ {i}{Γˢ : S.Con i}{Γ : Con i Γˢ} → Ty Γ lzero S.⊤
⊤ = T.⊤

tt : ∀ {i}{Γˢ : S.Con i}{Γ : Con i Γˢ} → Tm Γ (⊤ {Γ = Γ}) S.tt
tt = T.tt

⊤η : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ}{tˢ : S.Tm Γˢ S.⊤}{t : Tm Γ (⊤ {Γˢ = Γˢ}{Γ = Γ}) tˢ} →
  m._≡_ {A = Tm Γ (⊤ {Γˢ = Γˢ}{Γ = Γ}) tˢ} t (tt {Γˢ = Γˢ}{Γ = Γ})
⊤η = m.refl

⊤[] : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ}{j}{Δˢ : S.Con j}{Δ : Con j Δˢ}{σˢ : S.Sub Γˢ Δˢ}{σ : Sub Γ Δ σˢ} →
  m._≡_ {A = Ty Γ _ S.⊤} (_[_]T {Δ = Δ}{Aˢ = S.⊤}(⊤ {Γˢ = Δˢ}{Γ = Δ}){σˢ = σˢ} σ) (⊤ {Γˢ = Γˢ}{Γ = Γ})
⊤[] = m.refl

tt[] : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ}{j}{Δˢ : S.Con j}{Δ : Con j Δˢ}{σˢ : S.Sub Γˢ Δˢ}{σ : Sub Γ Δ σˢ} →
  m._≡_ {A = Tm Γ (⊤ {Γˢ = Γˢ}{Γ = Γ}) S.tt}
        (_[_]t {Δ = Δ}{Aˢ = S.⊤}{A = ⊤ {Γˢ = Δˢ}{Γ = Δ}}{tˢ = S.tt}(tt {Γˢ = Δˢ}{Γ = Δ}){σˢ = σˢ} σ)
        (tt {Γˢ = Γˢ}{Γ = Γ})
tt[] = m.refl

-- U
--------------------------------------------------------------------------------

U :
  ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ} j → Ty Γ (lsuc j) (S.U j)
U i = T.El (F.U₁ T.[ T.p² T., T.q ]t) T.⇒ T.U i

El :
  ∀{i}{Γˢ : S.Con i}         {Γ : Con i Γˢ}
   {j}{aˢ : S.Tm Γˢ (S.U j)} (a : Tm Γ (U {Γ = Γ} j) aˢ)
   → Ty Γ _ (S.El aˢ)
El a = T.El (T.app a)

c :
  ∀{i}{Γˢ : S.Con i}  {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ j Aˢ)
  → Tm Γ (U {Γ = Γ} j) (S.c Aˢ)
c A = T.lam (T.c A)

Uβ :
  ∀{i}{Γˢ : S.Con i}   {Γ : Con i Γˢ}
   {j}{Aˢ : S.Ty Γˢ j} {A : Ty Γ j Aˢ}
   → m._≡_ {A = Ty Γ _ Aˢ}(El {aˢ = S.c Aˢ}(c {Aˢ = Aˢ} A)) A
Uβ = m.refl

Uη :
  ∀{i}{Γˢ : S.Con i} {Γ : Con i Γˢ}
   {j}{aˢ : S.Tm Γˢ (S.U j)}{a : Tm Γ (U {Γ = Γ} j) aˢ}
   → m._≡_ {A = Tm Γ (U {Γ = Γ} j) aˢ}(c {Aˢ = S.El aˢ}(El {aˢ = aˢ} a)) a
Uη = m.refl

U[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   {k} → m._≡_ {A = Ty Γ _ (S.U k)}
              (_[_]T {Δ = Δ}{Aˢ = S.U k}(U {Γ = Δ} k){σˢ = σˢ} σ)
              (U {Γ = Γ} k)
U[] = m.refl

El[] :
  ∀{i}{Γˢ : S.Con i}        {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}        {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}    {σ : Sub Γ Δ σˢ}
   {k}{aˢ : S.Tm Δˢ (S.U k)}{a : Tm Δ (U {Γ = Δ} k) aˢ}
  → m._≡_ {A = Ty Γ _ (S.El aˢ S.[ σˢ ]T)}
         (_[_]T {Aˢ = S.El aˢ}(El {aˢ = aˢ} a){σˢ = σˢ} σ)
         (El {aˢ = aˢ S.[ σˢ ]t}(_[_]t {Aˢ = S.U k}{A = U {Γ = Δ} k}{tˢ = aˢ} a {σˢ = σˢ} σ))
El[] = m.refl

-- Bool
--------------------------------------------------------------------------------

Bool : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ} → Ty Γ lzero S.Bool
Bool = T.Σ T.Bool (T.Id (F.Ty S.Bool T.[ T.p³ ]T) (F.Bool₂ T.[ T.p³ T., T.q ]t) T.v¹)

true :
  ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
  → Tm Γ (Bool {Γ = Γ}) S.true
true = T.true T.,Σ T.refl (F.Tm S.true T.[ T.p ]t)

false :
  ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
  → Tm Γ (Bool {Γ = Γ}) S.false
false = T.false T.,Σ T.refl (F.Tm S.false T.[ T.p ]t)

if :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {j}{Cˢ : S.Ty (Γˢ S.▷ S.Bool) j} (C : Ty (Γ ▷ Bool {Γ = Γ}) j Cˢ)
      {uˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.true ]T)}
      (u : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.true}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.true} (true {Γ = Γ})))
                uˢ)
      {vˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.false ]T)}
      (v : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.false}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.false} (false {Γ = Γ})))
                vˢ)
      {tˢ : S.Tm Γˢ S.Bool}
      (t : Tm Γ (Bool {Γ = Γ}) tˢ)
  → Tm Γ
       (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
              {Aˢ = Cˢ} C
              {Γ = Γ}
              {σˢ = S.id S., tˢ}
              (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = tˢ} t))
       (S.if Cˢ uˢ vˢ tˢ)
if {Γ = Γ}{Cˢ = Cˢ} C {uˢ = uˢ} u {vˢ = vˢ} v {tˢ = tˢ} t =
  T.J (C T.[ F.▷ F.₂ T.∘ (T.p³ T., T.v¹) T.,
             (T.v² T.,Σ (T.fst t T.[ T.p² ]t T.,Σ T.v⁰)) T.,
             F.Tm (S.if (Cˢ S.[ S.p² S., S.q ]T) (uˢ S.[ S.p ]t) (vˢ S.[ S.p ]t) S.q) T.[ F.▷ F.₂ T.∘ (T.p³ T., T.v¹) ]t ]T)
      (T.if (C T.[ F.▷ F.₂ T.∘ (T.p² T., w) T., (T.v¹ T.,Σ (T.v⁰ T.,Σ T.refl w)) T., F.Tm (S.if (Cˢ S.[ S.p² S., S.q ]T) (uˢ S.[ S.p ]t) (vˢ S.[ S.p ]t) S.q) T.[ F.▷ F.₂ T.∘ (T.p² T., w) ]t ]T)
            u
            v
            (T.fst t))
      (T.snd t)
  where
    w = T.if (F.Ty S.Bool T.[ T.p² ]T) (F.Tm S.true T.[ T.p² ]t) (F.Tm S.false T.[ T.p² ]t) T.v⁰

Boolβ₁ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {j}{Cˢ : S.Ty (Γˢ S.▷ S.Bool) j} {C : Ty (Γ ▷ Bool {Γ = Γ}) j Cˢ}
      {uˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.true ]T)}
      {u : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.true}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.true} (true {Γ = Γ})))
                uˢ}
      {vˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.false ]T)}
      {v : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.false}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.false} (false {Γ = Γ})))
                vˢ}
   → m._≡_ {A = Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                           {Aˢ = Cˢ} C
                           {Γ = Γ}
                           {σˢ = S.id S., S.true}
                           (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.true} (true {Γ = Γ})))
                  uˢ}
          (if {Γ = Γ}{Cˢ = Cˢ} C {uˢ = uˢ} u {vˢ = vˢ} v {tˢ = S.true} (true {Γ = Γ}))
          u
Boolβ₁ = m.refl

Boolβ₂ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con i Γˢ}
   {j}{Cˢ : S.Ty (Γˢ S.▷ S.Bool) j} {C : Ty (Γ ▷ Bool {Γ = Γ}) j Cˢ}
      {uˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.true ]T)}
      {u : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.true}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.true} (true {Γ = Γ})))
                uˢ}
      {vˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.false ]T)}
      {v : Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                       {Aˢ = Cˢ} C
                       {Γ = Γ}
                       {σˢ = S.id S., S.false}
                       (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.false} (false {Γ = Γ})))
                vˢ}
   → m._≡_ {A = Tm Γ (_[_]T {Δ = _▷_ Γ {Aˢ = S.Bool} (Bool {Γ = Γ})}
                           {Aˢ = Cˢ} C
                           {Γ = Γ}
                           {σˢ = S.id S., S.false}
                           (_,_ {Γ = Γ}{Δ = Γ}{σˢ = S.id} (id {Γ = Γ}) {A = Bool {Γ = Γ}}{tˢ = S.false} (false {Γ = Γ})))
                  vˢ}
          (if {Γ = Γ}{Cˢ = Cˢ} C {uˢ = uˢ} u {vˢ = vˢ} v {tˢ = S.false} (false {Γ = Γ}))
          v
Boolβ₂ = m.refl

Bool[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   → m._≡_ {A = Ty Γ _ S.Bool}
          (_[_]T {Δ = Δ} (Bool {Γ = Δ}){σˢ = σˢ} σ)
          (Bool {Γ = Γ})
Bool[] = m.refl

true[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   → m._≡_ {A = Tm Γ (Bool {Γ = Γ}) S.true}
          (_[_]t {Δ = Δ}{A = Bool {Γ = Δ}}{tˢ = S.true} (true {Γ = Δ}) {σˢ = σˢ} σ)
          (true {Γ = Γ})
true[] = m.refl

false[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con i Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con j Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   → m._≡_ {A = Tm Γ (Bool {Γ = Γ}) S.false}
          (_[_]t {Δ = Δ}{A = Bool {Γ = Δ}}{tˢ = S.false} (false {Γ = Δ}) {σˢ = σˢ} σ)
          (false {Γ = Γ})
false[] = m.refl
