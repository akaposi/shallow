{-# OPTIONS --prop #-}

module Poset where

open import Lib

record Poset : Set₁ where
  field
    ∣_∣   : Set
    _∶_⇒_ : (Ω : ∣_∣)(Ψ : ∣_∣) → Prop
    idc   : (Ψ : ∣_∣) → _∶_⇒_ Ψ Ψ
    _∶_∘_ : {Ω : ∣_∣}{Ψ : ∣_∣}(β : _∶_⇒_ Ω Ψ)
      {Ω' : ∣_∣}(β' : _∶_⇒_ Ω' Ω) → _∶_⇒_ Ω' Ψ
  infix 5 _∶_⇒_
  infix 6 _∶_∘_
open Poset public
