{-# OPTIONS --prop #-}

module Readme where

{-
This file has been checked with Agda 2.6.1.
-}

import Lib                   -- standard lib


-- standard model variants
--------------------------------------------------------------------------------

import Standard               -- the standard model

import WrappedStandard        -- the wrapped standard model: importing
                              -- this only exposes the CwF+extra
                              -- interface

import Termification          -- termification of a model

import UnwrappedTermification -- unwrapped version of this (for
                              -- importing in other files, only for
                              -- efficiency)


-- case studies
--------------------------------------------------------------------------------
import Param                 -- Bernardy's parametricity translation

import Canon                 -- canonicity proof

import Inj                   -- unfinished formalization of the injectivity of
                             -- termification


-- formalisation of the paper "Gluing for type theory"
--------------------------------------------------------------------------------

import WrappedStandardDeepCopy -- a copy of the wrapped standard model

import Pseudomorphism          -- shallow embedding of a pseudomorphism

import Glue                    -- the glued model


-- template files
--------------------------------------------------------------------------------
{-
Note: these are only used for just a single purpose: to compute fully explicit
unfolded type signatures for the identity type structure in Canon. The issue
is that there is a massive number of implicit parameters, and a priori we have
no idea which ons are inferable. It takes a very long time to figure out manually
the correct configuration of explicit expressions.

Instead, we create these template files, and use rewriting to get the types for
Id that we would get in strict models. Then, we copy & paste fully explicit
types into other (displayed) models. The fully explicit types are humongous and
unreadable and it should be clear that they are not supposed to be read.

-}

import ModelTemplate
import DisplayedTemplate


-- everything as above, but adding a strict Prop universe
--------------------------------------------------------------------------------

import Lib.Props
import Standard.Props
import WrappedStandard.Props
import Termification.Props


-- everything as above, but adding new type and term sorts for strict
-- propositions
--------------------------------------------------------------------------------

import Standard.PropsNewSorts
import WrappedStandard.PropsNewSorts


-- other (unfinished) things
--------------------------------------------------------------------------------
import Poset
import Kripke                  -- Kripke model almost strict
import Definability
import GlueAdv
import Setoidification
import Slice
import StandardInterpretation
import StandardStandard        -- product of standard model with itself, canonicity is not true


import Pseudomorphism.NF       -- shallow embedding of a pseudomorphism

import Nf                      -- unquotiented terms for normal forms, decidability of equality
