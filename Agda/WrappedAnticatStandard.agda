{-# OPTIONS --no-pattern-matching #-}

module WrappedAnticatStandard where

-- The "wrapped" anticategorified standard model

open import Agda.Primitive
import Standard.Lib as m
open import WrappedAnticatStandard.Lib
import Lib as l

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^
infixr 6 _⇒_
infixl 7 _$_
infixl 6 _,Σ_

-- substitution calculus

Con : (i : Level) → Set (lsuc i)
Con i = Wrap 0 (m.⊤ → Set i)

Ty : ∀{i}(Γ : Con i)(j : Level) → Set (i ⊔ lsuc j)
Ty Γ j = Wrap 0 (∣ Γ ∣ _ → Set j)

Sub : ∀{i}(Γ : Con i){j}(Δ : Con j) → Set (i ⊔ j)
Sub Γ Δ = Wrap 1 (∣ Γ ∣ _ → ∣ Δ ∣ _)

Tm : ∀{i}(Γ : Con i){j}(A : Ty Γ j) → Set (i ⊔ j)
Tm Γ A = Wrap 1 ((γ : ∣ Γ ∣ _) → ∣ A ∣ γ)

id : ∀{i}{Γ : Con i} → Sub Γ Γ
id = mk λ γ → γ

_∘_ : ∀{i}{Θ : Con i}{j}{Δ : Con j}(σ : Sub Θ Δ){k}{Γ : Con k}(δ : Sub Γ Θ) →
  Sub Γ Δ
σ ∘ δ = mk λ γ → ∣ σ ∣ (∣ δ ∣ γ)

ass : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Ξ : Con k}{δ : Sub Ξ Θ}{l}{Γ : Con l}{ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν l.≡ σ ∘ (δ ∘ ν)
ass = l.refl

idl : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ l.≡ σ
idl = l.refl

idr : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id l.≡ σ
idr = l.refl

_[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
A [ σ ]T = mk λ γ → ∣ A ∣ (∣ σ ∣ γ)

_[_]t : ∀{i}{Δ : Con i}{j}{A : Ty Δ j}(t : Tm Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = mk λ γ → ∣ t ∣ (∣ σ ∣ γ)

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T l.≡ A
[id]T = l.refl

[∘]T : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
  {l}{A : Ty Δ l} → A [ σ ]T [ δ ]T l.≡ A [ σ ∘ δ ]T
[∘]T = l.refl

[id]t : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{t : Tm Γ A} → t [ id ]t l.≡ t
[id]t = l.refl

[∘]t : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
  {l}{A : Ty Δ l}{t : Tm Δ A} → t [ σ ]t [ δ ]t l.≡ t [ σ ∘ δ ]t
[∘]t = l.refl

∙ : Con lzero
∙ = mk (λ _ → m.⊤)

ε : ∀{i}{Γ : Con i} → Sub Γ ∙
ε = mk λ γ → m.tt

∙η : ∀{i}{Γ : Con i}{σ : Sub Γ ∙} → σ l.≡ ε
∙η = l.refl

_▷_   : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j)
Γ ▷ A = mk (λ _ → m.Σ (∣ Γ ∣ _) ∣ A ∣)

_,_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : Ty Δ k}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▷ A)
σ , t = mk λ γ → (∣ σ ∣ γ m.,Σ ∣ t ∣ γ)

p : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Sub (Γ ▷ A) Γ
p = mk m.fst

q : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
q = mk m.snd

▷β₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → p ∘ (_,_ σ {A = A} t) l.≡ σ
▷β₁ = l.refl

▷β₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → q [ _,_ σ {A = A} t ]t l.≡ t
▷β₂ = l.refl

▷η : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → (p {A = A} , q {A = A}) l.≡ id
▷η = l.refl

,∘ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)}{l}{θ : Con l}{δ : Sub θ Γ} →
  (_,_ σ {A = A} t) ∘ δ l.≡ (σ ∘ δ) , (t [ δ ]t)
,∘ = l.refl

-- abbreviations

p² :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k} →
   Sub (Γ ▷ A ▷ B) Γ
p² = p ∘ p

p³ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l} →
   Sub (Γ ▷ A ▷ B ▷ C) Γ
p³ = p ∘ p ∘ p

p⁴ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D) Γ
p⁴ = p ∘ p ∘ p ∘ p

p⁵ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) Γ
p⁵ = p ∘ p ∘ p ∘ p ∘ p

p⁶ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n}
   {o}{F : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) o} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F) Γ
p⁶ = p ∘ p ∘ p ∘ p ∘ p ∘ p

p⁷ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n}
   {o}{F : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) o}
   {p}{G : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F) p} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G) Γ
p⁷ = p ∘ p ∘ p ∘ p ∘ p ∘ p ∘ p

p⁸ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n}
   {o}{F : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) o}
   {p}{G : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F) p}
   {q}{H : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G) q} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H) Γ
p⁸ = p ∘ p ∘ p ∘ p ∘ p ∘ p ∘ p ∘ p

p⁹ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n}
   {o}{F : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) o}
   {p}{G : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F) p}
   {q}{H : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G) q}
   {r}{I : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H) r} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H ▷ I) Γ
p⁹ = p ∘ p ∘ p ∘ p ∘ p ∘ p ∘ p ∘ p ∘ p

p¹⁰ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n}
   {o}{F : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) o}
   {p}{G : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F) p}
   {q}{H : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G) q}
   {r}{I : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H) r}
   {s}{J : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H ▷ I) s} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H ▷ I ▷ J) Γ
p¹⁰ = p ∘ p ∘ p ∘ p ∘ p ∘ p ∘ p ∘ p ∘ p ∘ p

v⁰ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
v⁰ = q

v¹ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k} →
   Tm (Γ ▷ A ▷ B) (A [ p² ]T)
v¹ = v⁰ [ p ]t

v² :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l} →
   Tm (Γ ▷ A ▷ B ▷ C) (A [ p³ ]T)
v² = v⁰ [ p² ]t

v³ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
   Tm (Γ ▷ A ▷ B ▷ C ▷ D) (A [ p⁴ ]T)
v³ = v⁰ [ p³ ]t

v⁴ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n} →
   Tm (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) (A [ p⁵ ]T)
v⁴ = v⁰ [ p⁴ ]t

v⁵ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n}
   {o}{F : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) o} →
   Tm (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F) (A [ p⁶ ]T)
v⁵ = v⁰ [ p⁵ ]t

v⁶ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n}
   {o}{F : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) o}
   {p}{G : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F) p} →
   Tm (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G) (A [ p⁷ ]T)
v⁶ = v⁰ [ p⁶ ]t

v⁷ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n}
   {o}{F : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) o}
   {p}{G : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F) p}
   {q}{H : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G) q} →
   Tm (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H) (A [ p⁸ ]T)
v⁷ = v⁰ [ p⁷ ]t

v⁸ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n}
   {o}{F : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) o}
   {p}{G : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F) p}
   {q}{H : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G) q}
   {r}{I : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H) r} →
   Tm (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H ▷ I) (A [ p⁹ ]T)
v⁸ = v⁰ [ p⁸ ]t

v⁹ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n}
   {o}{F : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) o}
   {p}{G : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F) p}
   {q}{H : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G) q}
   {r}{I : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H) r}
   {s}{J : Ty (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H ▷ I) s} →
   Tm (Γ ▷ A ▷ B ▷ C ▷ D ▷ E ▷ F ▷ G ▷ H ▷ I ▷ J) (A [ p¹⁰ ]T)
v⁹ = v⁰ [ p⁹ ]t

_^ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : Ty Δ k} →
  Sub (Γ ▷ A [ σ ]T) (Δ ▷ A)
σ ^ = σ ∘ p , q

-- Π

Π : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▷ A) k) → Ty Γ (j ⊔ k)
Π A B = mk (λ γ → Wrap 2 ((α : ∣ A ∣ γ) → ∣ B ∣ (γ m.,Σ α)))

lam : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}(t : Tm (Γ ▷ A) B) → Tm Γ (Π A B)
lam t = mk λ γ → mk λ α → ∣ t ∣ (γ m.,Σ α)

app : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}(t : Tm Γ (Π A B)) → Tm (Γ ▷ A) B
app t = mk λ γ → ∣ ∣ t ∣ (m.fst γ) ∣ (m.snd γ)

Πβ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{t : Tm (Γ ▷ A) B} → app (lam t) l.≡ t
Πβ = l.refl

Πη : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{t : Tm Γ (Π A B)} → lam (app t) l.≡ t
Πη = l.refl

Π[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{l}{Θ : Con l}{σ : Sub Θ Γ} →
  Π A B [ σ ]T l.≡ Π (A [ σ ]T) (B [ σ ^ ]T)
Π[] = l.refl

lam[] :
  ∀{i}{Γ : Con i}{l}{Δ : Con l}{σ : Sub Γ Δ}{j}{A : Ty Δ j}{k}{B : Ty (Δ ▷ A) k}{t : Tm (Δ ▷ A) B} →
  lam t [ σ ]t l.≡ lam (t [ σ ^ ]t)
lam[] = l.refl

-- abbreviations

_⇒_ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty Γ k) → Ty Γ (j ⊔ k)
A ⇒ B = Π A (B [ p ]T)

_$_ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}(t : Tm Γ (Π A B))(u : Tm Γ A) → Tm Γ (B [ id , u ]T)
t $ u = app t [ id , u ]t

-- Σ

Σ : {i j k : Level}{Γ : Con i}(A : Ty Γ j)(B : Ty (Γ ▷ A) k) → Ty Γ (j ⊔ k)
Σ A B = mk λ γ → (m.Σ (∣ A ∣ γ) λ α → ∣ B ∣ (γ m.,Σ α))

_,Σ_ : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}(u : Tm Γ A)(v : Tm Γ (B [ id , u ]T)) → Tm Γ (Σ A B)
u ,Σ v = mk λ γ → (∣ u ∣ γ m.,Σ ∣ v ∣ γ)

fst : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k} → Tm Γ (Σ A B) → Tm Γ A
fst t = mk λ γ → m.fst (∣ t ∣ γ)

snd : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}(t : Tm Γ (Σ A B)) → Tm Γ (B [ id , fst t ]T)
snd t = mk λ γ → m.snd (∣ t ∣ γ)

Σβ₁ : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} →
  fst (_,Σ_ {A = A}{B = B} u v) l.≡ u
Σβ₁ = l.refl

Σβ₂ : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} →
  snd (_,Σ_ {A = A}{B = B} u v) l.≡ v
Σβ₂ = l.refl

Ση : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{t : Tm Γ (Σ A B)} →
  fst t ,Σ snd t l.≡ t
Ση = l.refl

Σ[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{l}{B : Ty (Δ ▷ A) l} →
  Σ A B [ σ ]T l.≡ Σ (A [ σ ]T) (B [ σ ^ ]T)
Σ[] = l.refl

,Σ[] : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{u : Tm Γ A}{v : Tm Γ (B [ _,_ id {A = A} u ]T)}{l}{Ω : Con l}{ν : Sub Ω Γ} →
  (_,Σ_ {A = A}{B = B} u v) [ ν ]t l.≡ _,Σ_ {A = A [ ν ]T}{B = B [ ν ^ ]T} (u [ ν ]t) (v [ ν ]t)
,Σ[] = l.refl

-- unit

⊤ : ∀{i}{Γ : Con i} → Ty Γ lzero
⊤ = mk λ _ → m.⊤

tt : ∀{i}{Γ : Con i} → Tm Γ ⊤
tt = mk λ _ → m.tt

⊤η : ∀{i}{Γ : Con i}{t : Tm Γ ⊤} → t l.≡ tt
⊤η = l.refl

⊤[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → ⊤ [ σ ]T l.≡ ⊤
⊤[] = l.refl

tt[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → tt [ σ ]t l.≡ tt
tt[] = l.refl
-- U

U : ∀{i}{Γ : Con i} j → Ty Γ (lsuc j)
U j = mk λ γ → Set j

El : ∀{i}{Γ : Con i}{j}(a : Tm Γ (U j)) → Ty Γ j
El a = mk ∣ a ∣

c : ∀{i}{Γ : Con i}{j}(A : Ty Γ j) → Tm Γ (U j)
c A = mk ∣ A ∣

Uβ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → El (c A) l.≡ A
Uβ = l.refl

Uη : ∀{i}{Γ : Con i}{j}{a : Tm Γ (U j)} → c (El a) l.≡ a
Uη = l.refl

U[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} {k} → U k [ σ ]T l.≡ U k
U[] = l.refl

El[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{a : Tm Δ (U k)}
       → El a [ σ ]T l.≡ El (a [ σ ]t)
El[] = l.refl

-- Bool

Bool    : ∀{i}{Γ : Con i} → Ty Γ lzero
Bool = mk λ γ → m.Bool

true    : ∀{i}{Γ : Con i} → Tm Γ Bool
true = mk λ γ → m.true

false   : ∀{i}{Γ : Con i} → Tm Γ Bool
false = mk λ γ → m.false

if : ∀{i}{Γ : Con i}{j}(C : Ty (Γ ▷ Bool) j)
  → Tm Γ (C [ (id , true) ]T)
  → Tm Γ (C [ (id , false) ]T)
  → (t : Tm Γ Bool)
  → Tm Γ (C [ (id , t) ]T)
if C u v t = mk λ γ → m.if (λ b → ∣ C ∣ (γ m.,Σ b)) (∣ u ∣ γ) (∣ v ∣ γ) (∣ t ∣ γ)

Boolβ₁ : ∀{i}{Γ : Con i}{j}{C : Ty (Γ ▷ Bool) j}
  → {u : Tm Γ (C [ (id , true) ]T)}
  → {v : Tm Γ (C [ (id , false) ]T)}
  → if C u v true l.≡ u
Boolβ₁ = l.refl

Boolβ₂ : ∀{i}{Γ : Con i}{j}{C : Ty (Γ ▷ Bool) j}
  → {u : Tm Γ (C [ (id , true) ]T)}
  → {v : Tm Γ (C [ (id , false) ]T)}
  → if C u v false l.≡ v
Boolβ₂ = l.refl

Bool[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → Bool [ σ ]T l.≡ Bool
Bool[] = l.refl

true[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → true [ σ ]t l.≡ true
true[] = l.refl

false[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → false [ σ ]t l.≡ false
false[] = l.refl

if[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}
  → {C  : Ty (Δ ▷ Bool) j}
  → {u : Tm Δ (C [ (id , true) ]T)}
  → {v : Tm Δ (C [ (id , false) ]T)}
  → {t  : Tm Δ Bool}
  → if C u v t [ σ ]t l.≡ if (C [ σ ^ ]T) (u [ σ ]t) (v [ σ ]t) (t [ σ ]t)
if[] = l.refl

-- Identity

Id : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(u v : Tm Γ A) → Ty Γ j
Id A u v = mk λ γ → ∣ u ∣ γ m.≡ ∣ v ∣ γ

refl : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}(u : Tm Γ A) → Tm Γ (Id A u u)
refl u = mk λ γ → m.refl

J :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
   {k}(C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k)
   (w : Tm Γ (C [ id , u , refl u ]T))
   {v : Tm Γ A}(t : Tm Γ (Id A u v)) → Tm Γ (C [ id , v , t ]T)
J C w t = mk λ γ → m.J (λ e → ∣ C ∣ (γ m.,Σ _ m.,Σ e)) (∣ w ∣ γ) (∣ t ∣ γ)

Idβ :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
   {k}{C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k}
   {w : Tm Γ (C [ id , u , refl u ]T)} →
   J C w (refl u) l.≡ w
Idβ = l.refl

Id[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u v : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
  Id A u v [ σ ]T l.≡ Id (A [ σ ]T) (u [ σ ]t) (v [ σ ]t)
Id[] = l.refl

refl[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
  refl u [ σ ]t l.≡ refl (u [ σ ]t)
refl[] = l.refl

J[] :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
   {k}{C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k}
   {w : Tm Γ (C [ id , u , refl u ]T)}
   {v : Tm Γ A}{t : Tm Γ (Id A u v)}{l}{Θ : Con l}{σ : Sub Θ Γ} →
   J C w t [ σ ]t l.≡ J (C [ σ ^ ^ ]T) (w [ σ ]t) (t [ σ ]t)
J[] = l.refl

-- abbreviations

tr :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}
   {k}(C : Ty (Γ ▷ A) k)
   {u v : Tm Γ A}(t : Tm Γ (Id A u v))
   (w : Tm Γ (C [ id , u ]T)) → Tm Γ (C [ id , v ]T)
tr C t w = J (C [ p ]T) w t

-- constant types (not very interesting because of Con = Ty ∙)

K : ∀{i}{Γ : Con i}{j} → Con j → Ty Γ j
K Δ = mk (λ γ → Wrap 3 (∣ Δ ∣ _))

K[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{Θ : Con k}{σ : Sub Θ Γ} → K Δ [ σ ]T l.≡ K Δ
K[] = l.refl

mkK : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ) → Tm Γ (K Δ)
mkK σ = mk λ γ → mk (∣ σ ∣ γ)

mkK[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{Θ : Con k}{ν : Sub Θ Γ} → mkK σ [ ν ]t l.≡ mkK (σ ∘ ν)
mkK[] = l.refl

unK : ∀{i}{Γ : Con i}{j}{Δ : Con j}(t : Tm Γ (K Δ)) → Sub Γ Δ
unK t = mk λ γ → ∣ ∣ t ∣ γ ∣

unK∘ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{t : Tm Γ (K Δ)}{k}{Θ : Con k}{ν : Sub Θ Γ} → unK t ∘ ν l.≡ unK (t [ ν ]t)
unK∘ = l.refl

Kβ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → unK (mkK σ) l.≡ σ
Kβ = l.refl

Kη : ∀{i}{Γ : Con i}{j}{Δ : Con j}{t : Tm Γ (K Δ)} → mkK (unK t) l.≡ t
Kη = l.refl

-- extra "anticategory" equalities

Con= : ∀{i} → Con i l.≡ Ty ∙ i
Sub= : ∀{i j}{Γ : Con i}{Δ : Con j} → Sub Γ Δ l.≡ Tm Γ (Δ [ ε ]T)
∘=   : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ} → σ ∘ δ l.≡ σ [ δ ]t
∙=   : ∙ l.≡ ⊤
ε=   : ∀{i}{Γ : Con i} → ε {i}{Γ} l.≡ tt
▷=   : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Γ ▷ A l.≡ Σ Γ (A [ q ]T)
,=   : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → _,_ σ {_}{A} t l.≡ σ ,Σ t
p=   : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → p {i}{Γ}{j}{A} l.≡ fst id
q=   : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → q {i}{Γ}{j}{A} l.≡ snd id

Con= = l.refl
Sub= = l.refl
∘= = l.refl
∙= = l.refl
ε= = l.refl
▷= = l.refl
,= = l.refl
p= = l.refl
q= = l.refl
