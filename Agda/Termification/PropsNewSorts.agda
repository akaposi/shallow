{-# OPTIONS --no-pattern-matching --prop #-}

module Termification.PropsNewSorts where

open import Agda.Primitive
import Lib.Props as m
import WrappedStandard.PropsNewSorts as S
open import Termification public

infixl 5 _▷P_
infixl 7 _[_]TP
infixl 5 _,P_
infixl 8 _[_]tP
infixl 5 _^P_
infixr 6 _⇒P_
infixl 7 _$P_
infixl 6 _,ΣP_
infixl 6 _,ΣSP_

-- two new sorts

TyP : ∀{i}(Γ : Con i)(j : Level) → Set (i ⊔ lsuc j)
TyP Γ j = S.Tm S.∙ (S.El Γ S.⇒ S.P j)
--        Γ → Prop j

TmP : ∀{i}(Γ : Con i){j}(A : TyP Γ j) → Prop (i ⊔ j)
TmP Γ A = S.TmP S.∙ (S.ΠP (S.El Γ) (S.ElP (S.app A)))
--        (γ : Γ) → A γ

irr : ∀{i}{Γ : Con i}{j}(A : TyP Γ j)(u v : TmP Γ A) → u m.≡P v
irr a u v = m.reflP
--          l.reflP

-- new substitutions for propositions

_[_]TP : ∀ {i}{Δ : Con i}{j}(A : TyP Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → TyP Γ j
A [ σ ]TP = S.lam (S.app A S.[ S.p S., S.app σ ]t)
--          λ γ → A (σ γ)

-- TODO: a bug in Agda? I cannot write S.ε instead of S.p above

_[_]tP : ∀{i}{Δ : Con i}{j}{A : TyP Δ j}(t : TmP Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → TmP Γ (A [ σ ]TP)
t [ σ ]tP = S.lamP (S.appP t S.[ S.p S., S.app σ ]tP)
--          λ γ → t (σ γ)

[id]TP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → A [ id ]TP m.≡ A
[id]TP = m.refl

[∘]TP : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
  {l}{A : TyP Δ l} → A [ σ ]TP [ δ ]TP m.≡ A [ σ ∘ δ ]TP
[∘]TP = m.refl

[id]tP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{t : TmP Γ A} → t [ id ]tP m.≡P t
[id]tP = m.reflP

[∘]tP : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
  {l}{A : TyP Δ l}{t : TmP Δ A} → t [ σ ]tP [ δ ]tP m.≡P t [ σ ∘ δ ]tP
[∘]tP = m.reflP

-- new comprehension for propositions

_▷P_   : ∀ {i}(Γ : Con i){j}(A : TyP Γ j) → Con (i ⊔ j)
Γ ▷P A = S.c (S.ΣSP (S.El Γ) (S.ElP (S.app A)))
--       m.ΣSP Γ λ γ → A γ

_,P_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : TyP Δ k}(t : TmP Γ (A [ σ ]TP)) → Sub Γ (Δ ▷P A)
σ ,P t = S.lam (S.app σ S.,ΣSP S.appP t)
--       λ γ → (σ γ m.,ΣSP t γ)

pP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → Sub (Γ ▷P A) Γ
pP = S.lam (S.fstSP S.v⁰)
--   m.fstSP

qP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → TmP (Γ ▷P A) (A [ pP ]TP)
qP = S.lamP (S.sndSP S.v⁰)
--   m.sndSP

▷Pβ₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : TyP Δ k}{t : TmP Γ (A [ σ ]TP)} → pP ∘ (_,P_ σ {A = A} t) m.≡ σ
▷Pβ₁ = m.refl

▷Pβ₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : TyP Δ k}{t : TmP Γ (A [ σ ]TP)} → qP [ _,P_ σ {A = A} t ]tP m.≡P t
▷Pβ₂ = m.reflP

▷Pη : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → (pP {A = A} ,P qP {A = A}) m.≡ id
▷Pη = m.refl

,P∘ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)}{l}{θ : Con l}{δ : Sub θ Γ} →
  (_,_ σ {A = A} t) ∘ δ m.≡ (σ ∘ δ) , (t [ δ ]t)
,P∘ = m.refl

pP² : 
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : TyP (Γ ▷P A) k} →
   Sub (Γ ▷P A ▷P B) Γ
pP² = pP ∘ pP

pP³ : 
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : TyP (Γ ▷P A) k}
   {l}{C : TyP (Γ ▷P A ▷P B) l} →
   Sub (Γ ▷P A ▷P B ▷P C) Γ
pP³ = pP ∘ pP ∘ pP

pP⁴ : 
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : TyP (Γ ▷P A) k}
   {l}{C : TyP (Γ ▷P A ▷P B) l}
   {m}{D : TyP (Γ ▷P A ▷P B ▷P C) m} →
   Sub (Γ ▷P A ▷P B ▷P C ▷P D) Γ
pP⁴ = pP ∘ pP ∘ pP ∘ pP

pP⁵ : 
  ∀{i}{Γ : Con i}
   {j}{A : TyP Γ j}
   {k}{B : TyP (Γ ▷P A) k}
   {l}{C : TyP (Γ ▷P A ▷P B) l}
   {m}{D : TyP (Γ ▷P A ▷P B ▷P C) m}
   {n}{E : TyP (Γ ▷P A ▷P B ▷P C ▷P D) n} →
   Sub (Γ ▷P A ▷P B ▷P C ▷P D ▷P E) Γ
pP⁵ = pP ∘ pP ∘ pP ∘ pP ∘ pP

_^P_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}(A : TyP Δ k) →
  Sub (Γ ▷P A [ σ ]TP) (Δ ▷P A)
_^P_ {i}{Γ}{j}{Δ} σ {k} A = σ ∘ pP ,P qP {i}{Γ}{_}{A [ σ ]TP}

-- Π

ΠP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : TyP (Γ ▷ A) k) → TyP Γ (j ⊔ k)
ΠP A B = S.lam (S.cP (S.ΠP (S.El (S.app A)) (S.ElP (S.app B) S.[ S.ε S., S.v¹ S.,Σ S.v⁰ ]TP)))
--       λ γ → (x : A γ) → (B (γ m.,Σ x))

lamP : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : TyP (Γ ▷ A) k} → TmP (Γ ▷ A) B → TmP Γ (ΠP A B)
lamP t = S.lamP (S.lamP (S.appP t S.[ S.ε S., S.v¹ S.,Σ S.v⁰ ]tP))
--       λ γ α → t (γ m.,Σ α)

appP : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : TyP (Γ ▷ A) k} → TmP Γ (ΠP A B) → TmP (Γ ▷ A) B
appP t = S.lamP (t S.[ S.ε ]tP S.$P S.fst S.v⁰ S.$P S.snd S.v⁰)
--       λ γ → t (m.fst γ) (m.snd γ)

ΠP[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : TyP (Γ ▷ A) k}{l}{Θ : Con l}{σ : Sub Θ Γ} →
  ΠP A B [ σ ]TP m.≡ ΠP (A [ σ ]T) (B [ σ ^ A ]TP)
ΠP[] = m.refl

-- abbreviations

_⇒P_ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : TyP Γ k) → TyP Γ (j ⊔ k)
A ⇒P B = ΠP A (B [ p ]TP)

_$P_ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : TyP (Γ ▷ A) k}(t : TmP Γ (ΠP A B))(u : Tm Γ A) → TmP Γ (B [ id , u ]TP)
t $P u = appP t [ id , u ]tP

-- Σ

ΣP : ∀{i}{Γ : Con i}{j}(A : TyP Γ j){k}(B : TyP (Γ ▷P A) k) → TyP Γ (j ⊔ k)
ΣP A B = S.lam (S.cP (S.ΣP (S.ElP (S.app A)) (S.ElP (S.app B S.[ S.ε S., (S.q S.[ S.pP ]t S.,ΣSP S.qP) ]t))))
--       λ γ → m.ΣP (A γ) λ α → B (γ m.,ΣSP α)

_,ΣP_ : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{B : TyP (Γ ▷P A) k}(u : TmP Γ A)(v : TmP Γ (B [ id ,P u ]TP)) → TmP Γ (ΣP A B)
u ,ΣP v = S.lamP (S.appP u S.,ΣP S.appP v)
--        λ γ → (u γ m.,ΣP v γ)

fstP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{B : TyP (Γ ▷P A) k}
  (t : TmP Γ (ΣP A B)) → TmP Γ A
fstP t = S.lamP (S.fstP (S.appP t))
--       λ γ → m.fstP (t γ)

sndP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{B : TyP (Γ ▷P A) k}
  (t : TmP Γ (ΣP A B)) → TmP Γ (B [ id ,P fstP t ]TP)
sndP t = S.lamP (S.sndP (S.appP t))
--       λ γ → m.sndP (t γ)

ΣP[] : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{B : TyP (Γ ▷P A) k}{l}{Θ : Con l}{σ : Sub Θ Γ} →
  ΣP A B [ σ ]TP m.≡ ΣP (A [ σ ]TP) (B [ σ ^P A ]TP)
ΣP[] = m.refl

-- another Σ

ΣSP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : TyP (Γ ▷ A) k) → Ty Γ (j ⊔ k)
ΣSP A B = S.lam (S.c (S.ΣSP (S.El (S.app A)) (S.ElP (S.app B S.[ S.ε S., S.v¹ S.,Σ S.v⁰ ]t))))
--        λ γ → m.ΣSP (A γ) λ α → B (γ m.,Σ α)

_,ΣSP_ : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}(u : Tm Γ A)(v : TmP Γ (B [ id , u ]TP)) → Tm Γ (ΣSP A B)
u ,ΣSP v = S.lam (S.app u S.,ΣSP S.appP v)
--         λ γ → u γ m.,ΣSP v γ

fstSP : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k} → Tm Γ (ΣSP A B) → Tm Γ A
fstSP t = S.lam (S.fstSP (S.app t))
--        λ γ → m.fstSP (t γ)

sndSP : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}(t : Tm Γ (ΣSP A B)) → TmP Γ (B [ id , fstSP t ]TP)
sndSP t = S.lamP (S.sndSP (S.app t))
--        λ γ → m.sndSP (t γ)

ΣSPβ : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}{u : Tm Γ A}{v : TmP Γ (B [ id , u ]TP)} →
  fstSP (_,ΣSP_ {A = A}{B = B} u v) m.≡ u
ΣSPβ = m.refl

ΣSPη : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}{t : Tm Γ (ΣSP A B)} →
  fstSP t ,ΣSP sndSP t m.≡ t
ΣSPη = m.refl

ΣSP[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{l}{B : TyP (Δ ▷ A) l} →
  ΣSP A B [ σ ]T m.≡ ΣSP (A [ σ ]T) (B [ σ ^ A ]TP)
ΣSP[] = m.refl

,ΣSP[] : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : TyP (Γ ▷ A) k}{u : Tm Γ A}{v : TmP Γ (B [ _,_ id {A = A} u ]TP)}{l}{Ω : Con l}{ν : Sub Ω Γ} →
  (_,ΣSP_ {A = A}{B = B} u v) [ ν ]t m.≡ _,ΣSP_ {A = A [ ν ]T}{B = B [ ν ^ A ]TP} (u [ ν ]t) (v [ ν ]tP)
,ΣSP[] = m.refl

-- unit

⊤P : ∀{i}{Γ : Con i} → TyP Γ lzero
⊤P = S.lam (S.cP S.⊤P)
-- λ _ → m.⊤P

⊤P[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ} → ⊤P [ σ ]TP m.≡ ⊤P
⊤P[] = m.refl

ttP : ∀{i}{Γ : Con i} → TmP Γ ⊤P
ttP = S.lamP S.ttP
--    λ _ → m.ttP

-- universe of propositions

P : ∀{i}{Γ : Con i} j → Ty Γ (lsuc j)
P j = S.lam (S.c (S.P j))
--    λ _ → Prop j

ElP : ∀{i}{Γ : Con i}{j}(a : Tm Γ (P j)) → TyP Γ j
ElP a = a
--      a

cP : ∀{i}{Γ : Con i}{j}(A : TyP Γ j) → Tm Γ (P j)
cP A = A
--     A

Pβ : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → ElP (cP A) m.≡ A
Pβ = m.refl

Pη : ∀{i}{Γ : Con i}{j}{a : Tm Γ (P j)} → cP (ElP a) m.≡ a
Pη = m.refl

P[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} {k} → U k [ σ ]T m.≡ U k
P[] = m.refl

ElP[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{a : Tm Δ (P k)}
       → ElP a [ σ ]TP m.≡ ElP (a [ σ ]t)
ElP[] = m.refl

-- extra equalities

RussellP : ∀{i}{Γ : Con i}{j} → Tm Γ (P j) m.≡ TyP Γ j
RussellP = m.refl

[]TtP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{Θ : Con k}{σ : Sub Θ Γ} → A [ σ ]TP m.≡ A [ σ ]t
[]TtP = m.refl

-- empty

⊥P : ∀ {i}{Γ : Con i} → TyP Γ lzero
⊥P = S.lam (S.cP S.⊥P)
--   λ _ → m.⊥P

⊥P[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ} → ⊥P [ σ ]TP m.≡ ⊥P
⊥P[] = m.refl

exfalsoP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(t : TmP Γ ⊥P) → Tm Γ A
exfalsoP A t = S.lam (S.exfalsoP (S.El (S.app A)) (S.appP t))
--             λ γ → m.exfalsoP (t γ)

exfalsoP[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ}{k}{A : Ty Γ k}{t : TmP Γ ⊥P} →
  exfalsoP A t [ σ ]t m.≡ exfalsoP (A [ σ ]T) (t [ σ ]tP)
exfalsoP[] = m.refl
