{-# OPTIONS --no-pattern-matching --prop #-}

module Termification.Props where

open import Agda.Primitive
import Lib as m
import WrappedStandard.Props as S
open import Termification public

infixr 6 _⇒P_
infixl 7 _$P_
infixl 6 _,ΣP_

-- Prop

P : ∀{i}{Γ : Con i} j → Ty Γ (lsuc j)
P j = S.lam (S.c (S.P j))
--    λ _ → Prop j

ElP : ∀{i}{Γ : Con i}{j}(a : Tm Γ (P j)) → Ty Γ j
ElP a = S.lam (S.c (S.ElP (S.app a)))
--      λ γ → m.ElP (a γ)

P[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} {k} → U k [ σ ]T m.≡ U k
P[] = m.refl

ElP[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{a : Tm Δ (P k)}
       → ElP a [ σ ]T m.≡ ElP (a [ σ ]t)
ElP[] = m.refl

irr : ∀{i}{Γ : Con i}{j}(a : Tm Γ (P j))(u v : Tm Γ (ElP a)) → u m.≡ v
irr a u v = m.refl

⊤P : ∀{i}{Γ : Con i} → Tm Γ (P lzero)
⊤P = S.lam S.⊤P
--   λ _ → m.⊤P

⊤P[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ} → ⊤P [ σ ]t m.≡ ⊤P
⊤P[] = m.refl

ttP : ∀{i}{Γ : Con i} → Tm Γ (ElP ⊤P)
ttP = S.lam S.ttP
--    λ _ → m.mkElP m.ttP

ttP[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ} → ttP [ σ ]t m.≡ ttP
ttP[] = m.refl

⊥P : ∀ {i}{Γ : Con i} → Tm Γ (P lzero)
⊥P = S.lam S.⊥P
--   λ _ → m.⊥P

⊥P[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ} → ⊥P [ σ ]t m.≡ ⊥P
⊥P[] = m.refl

exfalsoP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(t : Tm Γ (ElP ⊥P)) → Tm Γ A
exfalsoP A t = S.lam (S.exfalsoP (S.El (S.app A)) (S.app t))
--             λ γ → m.exfalsoP (m.unElP (t γ))

exfalsoP[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{σ : Sub Θ Γ}{k}{A : Ty Γ k}{t : Tm Γ (ElP ⊥P)} →
  exfalsoP A t [ σ ]t m.≡ exfalsoP (A [ σ ]T) (t [ σ ]t)
exfalsoP[] = m.refl

ΠP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(b : Tm (Γ ▷ A) (P k)) → Tm Γ (P (j ⊔ k))
ΠP A b = S.lam (S.ΠP (S.El (S.app A)) (b S.[ S.ε ]t S.$ (S.v¹ S.,Σ S.v⁰)))
--       λ γ → (x : A γ) → (b (γ m.,Σ x))

lamp : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ A) (P k)} → Tm (Γ ▷ A) (ElP b) → Tm Γ (ElP (ΠP A b))
lamp t = S.lam (S.lamp (t S.[ S.ε ]t S.$ (S.v¹ S.,Σ S.v⁰)))
--       λ γ → m.mkElP λ α → m.unElP (t (γ m.,Σ α))

appp : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ A) (P k)} → Tm Γ (ElP (ΠP A b)) → Tm (Γ ▷ A) (ElP b)
appp t = S.lam ((t S.[ S.ε ]t S.$ S.fst S.v⁰) S.$P S.snd S.v⁰)
--       λ γ → m.mkElP (m.unElP (t (m.fst γ)) (m.snd γ))

ΠP[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ A) (P k)}{l}{Θ : Con l}{σ : Sub Θ Γ} →
  ΠP A b [ σ ]t m.≡ ΠP (A [ σ ]T) (b [ σ ^ A ]t)
ΠP[] = m.refl

_⇒P_ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(b : Tm Γ (P k)) → Tm Γ (P (j ⊔ k))
A ⇒P b = ΠP A (b [ p ]t)

_$P_ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ A) (P k)}(t : Tm Γ (ElP (ΠP A b)))(u : Tm Γ A) → Tm Γ (ElP b [ id , u ]T)
t $P u = appp t [ id , u ]t

ΣP : ∀{i}{Γ : Con i}{j}(a : Tm Γ (P j)){k}(b : Tm (Γ ▷ ElP a) (P k)) → Tm Γ (P (j ⊔ k))
ΣP a b = S.lam (S.ΣP (S.app a) (b S.[ S.ε ]t S.$ (S.v¹ S.,Σ S.v⁰)))
--       λ γ → m.ΣP (a γ) λ α → b (γ m.,Σ m.mkElP α)

_,ΣP_ : ∀{i}{Γ : Con i}{j}{a : Tm Γ (P j)}{k}{b : Tm (Γ ▷ ElP a) (P k)}(u : Tm Γ (ElP a))(v : Tm Γ (ElP b [ id , u ]T)) → Tm Γ (ElP (ΣP a b))
_,ΣP_ {a = a}{b = b} u v = S.lam (S._,ΣP_ {a = S.app a}
                                          {b = S.app b S.[ S.ε S., S.v¹ S.,Σ S.v⁰ ]t}
                                          (S.app u)
                                          (S.app v))
--        λ γ → m.mkElP (m.unElP (u γ) m.,ΣP m.unElP (v γ))

fstP : ∀{i}{Γ : Con i}{j}{a : Tm Γ (P j)}{k}{b : Tm (Γ ▷ ElP a) (P k)}
  (t : Tm Γ (ElP (ΣP a b))) → Tm Γ (ElP a)
fstP {Γ = Γ}{a = a}{b = b} t = S.lam (S.fstP {a = S.app a}{b = S.app b S.[ S.ε S., S.v¹ S.,Σ S.v⁰ ]t} (S.app {A = S.El Γ}{B = S.El (S.app (ElP (ΣP a b)))} t))
--       λ γ → m.mkElP (m.fstP (m.unElP (t γ)))

sndP : ∀{i}{Γ : Con i}{j}{a : Tm Γ (P j)}{k}{b : Tm (Γ ▷ ElP a) (P k)}
  (t : Tm Γ (ElP (ΣP a b))) → Tm Γ (ElP b [ id , fstP {a = a}{b = b} t ]T)
sndP t = S.lam (S.sndP (S.app t))
--       λ γ → m.mkElP (m.sndP (m.unElP (t γ)))

ΣP[] : ∀{i}{Γ : Con i}{j}{a : Tm Γ (P j)}{k}{b : Tm (Γ ▷ ElP a) (P k)}{l}{Θ : Con l}{σ : Sub Θ Γ} →
  ΣP a b [ σ ]t m.≡ ΣP (a [ σ ]t) (b [ σ ^ ElP a ]t)
ΣP[] = m.refl
