{-# OPTIONS --no-pattern-matching #-}

module Termification where

-- The syntactic set model
-- We start from the syntax and create a model whose
-- components are sets of syntactic terms.

-- The structure mirrors the structure of the standard model
-- (module Standard.agda)

open import Agda.Primitive
import Lib as m
import WrappedStandard as S

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixr 6 _⇒_
infixl 7 _$_
infixl 6 _,Σ_

Con : (i : Level) → Set (lsuc i)
Con i = S.Tm S.∙ (S.U i)
--      Set i

Ty : ∀{i}(Γ : Con i)(j : Level) → Set (i ⊔ lsuc j)
Ty Γ j = S.Tm S.∙ ((S.El Γ) S.⇒ (S.U j))
--       Γ → Set j

Sub : ∀{i}(Γ : Con i){j}(Δ : Con j) → Set (i ⊔ j)
Sub Γ Δ = S.Tm S.∙ ((S.El Γ) S.⇒ (S.El Δ))
--        Γ → Δ

Tm : ∀{i}(Γ : Con i){j}(A : Ty Γ j) → Set (i ⊔ j)
Tm Γ A = S.Tm S.∙ (S.Π (S.El Γ) (S.El (S.app A)))
--       (γ : Γ) → A γ

id : ∀{i}{Γ : Con i} → Sub Γ Γ
id = S.lam S.v⁰
--   λ γ → γ

_∘_ : ∀{i}{Θ : Con i}{j}{Δ : Con j}(σ : Sub Θ Δ){k}{Γ : Con k}(δ : Sub Γ Θ) →
  Sub Γ Δ
σ ∘ δ = S.lam (σ S.[ S.ε ]t S.$ (δ S.[ S.ε ]t S.$ S.v⁰))
--      λ γ → σ (δ γ)

ass : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Ξ : Con k}{δ : Sub Ξ Θ}{l}{Γ : Con l}{ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν m.≡ σ ∘ (δ ∘ ν)
ass = m.refl

idl : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ m.≡ σ
idl = m.refl

idr : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id m.≡ σ
idr = m.refl

_[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
A [ σ ]T = S.lam (S.app A S.[ S.ε S., (S.app (σ S.[ S.ε ]t)) ]t)
--         λ γ → A (σ γ)

_[_]t : ∀{i}{Δ : Con i}{j}{A : Ty Δ j}(t : Tm Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = S.lam (S.app t S.[ S.ε S., S.app (σ S.[ S.ε ]t) ]t)
--         λ γ → t (σ γ)

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T m.≡ A
[id]T = m.refl

[∘]T : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
        {l}{A : Ty Δ l} → A [ σ ]T [ δ ]T m.≡ A [ σ ∘ δ ]T
[∘]T = m.refl

[id]t : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{t : Tm Γ A} → t [ id ]t m.≡ t
[id]t = m.refl

[∘]t : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
  {l}{A : Ty Δ l}{t : Tm Δ A} → t [ σ ]t [ δ ]t m.≡ t [ σ ∘ δ ]t
[∘]t = m.refl

∙ : Con lzero
∙ = S.c (S.⊤)
--  m.⊤

ε : ∀{i}{Γ : Con i} → Sub Γ ∙
ε = S.lam S.tt
--  λ γ → m.tt

∙η : ∀{i}{Γ : Con i}{σ : Sub Γ ∙} → σ m.≡ ε
∙η = m.refl

_▷_   : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j)
Γ ▷ A = S.c (S.Σ (S.El Γ) (S.El (S.app A)))
--      m.Σ Γ A

_,_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : Ty Δ k}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▷ A)
σ , t = S.lam (S.app σ S.,Σ S.app t)
--      λ γ → (σ γ m.,Σ t γ)

p : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Sub (Γ ▷ A) Γ
p = S.lam (S.fst S.v⁰)
--  m.fst

q : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
q = S.lam (S.snd S.v⁰)
--  m.snd

▷β₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → p ∘ (_,_ σ {A = A} t) m.≡ σ
▷β₁ = m.refl

▷β₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → q [ _,_ σ {A = A} t ]t m.≡ t
▷β₂ = m.refl

▷η : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → (p {A = A} , q {A = A}) m.≡ id
▷η = m.refl

,∘ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)}{l}{θ : Con l}{δ : Sub θ Γ} →
  (_,_ σ {A = A} t) ∘ δ m.≡ (σ ∘ δ) , (t [ δ ]t)
,∘ = m.refl

-- abbreviations

p² : 
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k} →
   Sub (Γ ▷ A ▷ B) Γ
p² = p ∘ p

p³ : 
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l} →
   Sub (Γ ▷ A ▷ B ▷ C) Γ
p³ = p ∘ p ∘ p

p⁴ : 
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D) Γ
p⁴ = p ∘ p ∘ p ∘ p

p⁵ : 
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) Γ
p⁵ = p ∘ p ∘ p ∘ p ∘ p

v⁰ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
v⁰ = q

v¹ : 
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k} →
   Tm (Γ ▷ A ▷ B) (A [ p² ]T)
v¹ = v⁰ [ p ]t

v² : 
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l} →
   Tm (Γ ▷ A ▷ B ▷ C) (A [ p³ ]T)
v² = v⁰ [ p² ]t

v³ : 
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
   Tm (Γ ▷ A ▷ B ▷ C ▷ D) (A [ p⁴ ]T)
v³ = v⁰ [ p³ ]t

_^_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}(A : Ty Δ k) →
  Sub (Γ ▷ A [ σ ]T) (Δ ▷ A)
_^_ {i}{Γ}{j}{Δ} σ {k} A = σ ∘ p , v⁰ {i}{Γ}{_}{A [ σ ]T}

-- Π

Π : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▷ A) k) → Ty Γ (j ⊔ k)
Π A B = S.lam (S.c (S.Π (S.El (S.app A)) (S.El (B S.[ S.ε ]t S.$ (S.v¹ S.,Σ S.v⁰)))))
--      λ γ → (α : A γ) → B (γ ,Σ' α)

lam : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}(t : Tm (Γ ▷ A) B) → Tm Γ (Π A B)
lam t = S.lam (S.lam (t S.[ S.ε ]t S.$ (S.v¹ S.,Σ S.v⁰)))
--      λ γ α → t (γ m.,Σ α)

app : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}(t : Tm Γ (Π A B)) → Tm (Γ ▷ A) B
app t = S.lam (t S.[ S.ε ]t S.$ S.fst S.v⁰ S.$ S.snd S.v⁰)
--      λ γ → t (m.fst γ) (m.snd γ)

Πβ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{t : Tm (Γ ▷ A) B} → app (lam t) m.≡ t
Πβ = m.refl

Πη : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{t : Tm Γ (Π A B)} → lam (app t) m.≡ t
Πη = m.refl

Π[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{l}{Θ : Con l}{σ : Sub Θ Γ} →
  Π A B [ σ ]T m.≡ Π (A [ σ ]T) (B [ σ ^ A ]T)
Π[] = m.refl

lam[] :
  ∀{i}{Γ : Con i}{l}{Δ : Con l}{σ : Sub Γ Δ}{j}{A : Ty Δ j}{k}{B : Ty (Δ ▷ A) k}{t : Tm (Δ ▷ A) B} →
  lam t [ σ ]t m.≡ lam (t [ σ ^ A ]t)
lam[] = m.refl

-- abbreviations

_⇒_ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty Γ k) → Ty Γ (j ⊔ k)
A ⇒ B = Π A (B [ p ]T)

_$_ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}(t : Tm Γ (Π A B))(u : Tm Γ A) → Tm Γ (B [ id , u ]T)
t $ u = app t [ id , u ]t

-- Σ

Σ : {i j k : Level}{Γ : Con i}(A : Ty Γ j)(B : Ty (Γ ▷ A) k) → Ty Γ (j ⊔ k)
Σ A B = S.lam (S.c (S.Σ (S.El (S.app A)) (S.El (S.app B S.[ S.ε S., (S.v¹ S.,Σ S.v⁰) ]t))))
--      λ γ → m.Σ (A γ) λ α → B (γ m.,Σ α)

_,Σ_ : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}(u : Tm Γ A)(v : Tm Γ (B [ id , u ]T)) → Tm Γ (Σ A B)
u ,Σ v = S.lam (S.app u S.,Σ S.app v)
--     = λ γ → u γ m.,Σ v γ

fst : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k} → Tm Γ (Σ A B) → Tm Γ A
fst t = S.lam (S.fst (S.app t))
--      λ γ → m.fst (t γ)

snd : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}(t : Tm Γ (Σ A B)) → Tm Γ (B [ id , fst t ]T)
snd t = S.lam (S.snd (S.app t))
--      λ γ → m.snd (t γ)

Σβ₁ : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} →
  fst (_,Σ_ {A = A}{B = B} u v) m.≡ u
Σβ₁ = m.refl

Σβ₂ : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} →
  snd (_,Σ_ {A = A}{B = B} u v) m.≡ v
Σβ₂ = m.refl

Ση : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{t : Tm Γ (Σ A B)} →
  fst t ,Σ snd t m.≡ t
Ση = m.refl

Σ[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{l}{B : Ty (Δ ▷ A) l} →
  Σ A B [ σ ]T m.≡ Σ (A [ σ ]T) (B [ σ ^ A ]T)
Σ[] = m.refl

,Σ[] : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{u : Tm Γ A}{v : Tm Γ (B [ _,_ id {A = A} u ]T)}{l}{Ω : Con l}{ν : Sub Ω Γ} →
  (_,Σ_ {A = A}{B = B} u v) [ ν ]t m.≡ _,Σ_ {A = A [ ν ]T}{B = B [ ν ^ A ]T} (u [ ν ]t) (v [ ν ]t)
,Σ[] = m.refl

-- unit

⊤ : ∀{i}{Γ : Con i} → Ty Γ lzero
⊤ = S.lam (S.c S.⊤)
--   λ _ → m.⊤

tt : ∀{i}{Γ : Con i} → Tm Γ ⊤
tt = S.lam S.tt
--   λ _ → m.tt

⊤η : ∀{i}{Γ : Con i}{t : Tm Γ ⊤} → t m.≡ tt
⊤η = m.refl

⊤[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → ⊤ [ σ ]T m.≡ ⊤
⊤[] = m.refl

tt[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → tt [ σ ]t m.≡ tt
tt[] = m.refl

-- U

U : ∀{i}{Γ : Con i} j → Ty Γ (lsuc j)
U j = S.lam (S.c (S.U j))
--    λ γ → Set j

El : ∀{i}{Γ : Con i}{j}(a : Tm Γ (U j)) → Ty Γ j
El a = a
--     a

c : ∀{i}{Γ : Con i}{j}(A : Ty Γ j) → Tm Γ (U j)
c A = A
--    A

Uβ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → El (c A) m.≡ A
Uβ = m.refl

Uη : ∀{i}{Γ : Con i}{j}{a : Tm Γ (U j)} → c (El a) m.≡ a
Uη = m.refl

U[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} {k} → U k [ σ ]T m.≡ U k
U[] = m.refl

El[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{a : Tm Δ (U k)}
       → El a [ σ ]T m.≡ El (a [ σ ]t)
El[] = m.refl

-- extra equalities

Russell : ∀{i}{Γ : Con i}{j} → Tm Γ (U j) m.≡ Ty Γ j
Russell = m.refl

[]Tt : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{Θ : Con k}{σ : Sub Θ Γ} → A [ σ ]T m.≡ A [ σ ]t
[]Tt = m.refl

-- Bool

Bool    : ∀{i}{Γ : Con i} → Ty Γ lzero
Bool = S.lam (S.c S.Bool)
--     λ γ → m.Bool

true    : ∀{i}{Γ : Con i} → Tm Γ Bool
true = S.lam S.true
--     λ γ → m.true

false   : ∀{i}{Γ : Con i} → Tm Γ Bool
false = S.lam S.false
--      λ γ → m.false

if : ∀{i}{Γ : Con i}{j}(C : Ty (Γ ▷ Bool) j)
  → Tm Γ (C [ (id , true) ]T)
  → Tm Γ (C [ (id , false) ]T)
  → (t : Tm Γ Bool)
  → Tm Γ (C [ (id , t) ]T)
if C u v t = S.lam (S.if (S.El (C S.[ S.ε ]t S.$ (S.v¹ S.,Σ S.v⁰))) (S.app u) (S.app v) (S.app t))
--           λ γ → m.if (λ b → C (γ m.,Σ b)) (u γ) (v γ) (t γ)

Boolβ₁ : ∀{i}{Γ : Con i}{j}{C : Ty (Γ ▷ Bool) j}
  → {u : Tm Γ (C [ (id , true) ]T)}
  → {v : Tm Γ (C [ (id , false) ]T)}
  → if C u v true m.≡ u
Boolβ₁ = m.refl

Boolβ₂ : ∀{i}{Γ : Con i}{j}{C : Ty (Γ ▷ Bool) j}
  → {u : Tm Γ (C [ (id , true) ]T)}
  → {v : Tm Γ (C [ (id , false) ]T)}
  → if C u v false m.≡ v
Boolβ₂ = m.refl

Bool[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → Bool [ σ ]T m.≡ Bool
Bool[] = m.refl

true[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → true [ σ ]t m.≡ true
true[] = m.refl

false[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → false [ σ ]t m.≡ false
false[] = m.refl

if[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}
  → {C  : Ty (Δ ▷ Bool) j}
  → {u : Tm Δ (C [ (id , true) ]T)}
  → {v : Tm Δ (C [ (id , false) ]T)}
  → {t  : Tm Δ Bool}
  → if C u v t [ σ ]t m.≡ if (C [ σ ^ Bool ]T) (u [ σ ]t) (v [ σ ]t) (t [ σ ]t)
if[] = m.refl

-- Identity

Id : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(u v : Tm Γ A) → Ty Γ j
Id A u v = S.lam (S.c (S.Id (S.El (S.app A)) (S.app u) (S.app v)))
--         λ γ → u γ m.≡ v γ

refl : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}(u : Tm Γ A) → Tm Γ (Id A u u)
refl u = S.lam (S.refl (S.app u))
--       λ γ → m.refl

J :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
   {k}(C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k)
   (w : Tm Γ (C [ id , u , refl u ]T))
   {v : Tm Γ A}(t : Tm Γ (Id A u v)) → Tm Γ (C [ id , v , t ]T)
J C w t = S.lam (S.J (S.El (C S.[ S.ε ]t S.$ (S.v² S.,Σ S.v¹ S.,Σ S.v⁰))) (S.app w) (S.app t))
--        λ γ → m.J (λ e → C (γ m.,Σ _ m.,Σ e)) (w γ) (t γ)

Idβ : 
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
   {k}{C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k}
   {w : Tm Γ (C [ id , u , refl u ]T)} →
   J C w (refl u) m.≡ w
Idβ = m.refl

Id[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u v : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
  Id A u v [ σ ]T m.≡ Id (A [ σ ]T) (u [ σ ]t) (v [ σ ]t)
Id[] = m.refl

refl[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
  refl u [ σ ]t m.≡ refl (u [ σ ]t)
refl[] = m.refl

J[] :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
   {k}{C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k}
   {w : Tm Γ (C [ id , u , refl u ]T)}
   {v : Tm Γ A}{t : Tm Γ (Id A u v)}{l}{Θ : Con l}{σ : Sub Θ Γ} →
   J C w t [ σ ]t m.≡ J (C [ σ ^ A ^ Id (A [ p ]T) (u [ p ]t) v⁰ ]T) (w [ σ ]t) (t [ σ ]t)
J[] = m.refl

-- abbreviations

tr :
  ∀{i}{Γ : Con i}{j}{A : Ty Γ j}
   {k}(C : Ty (Γ ▷ A) k)
   {u v : Tm Γ A}(t : Tm Γ (Id A u v))
   (w : Tm Γ (C [ id , u ]T)) → Tm Γ (C [ id , v ]T)
tr C t w = J (C [ p ]T) w t
