module Pseudomorphism.Maps where

open import Agda.Primitive
import Lib as l
import WrappedStandard as S
import WrappedStandardDeepCopy as T
open import Pseudomorphism

infix 4 _≅T_

record _≅T_ {i}{Γ : T.Con i}{j}(A : T.Ty Γ j){k}(B : T.Ty Γ k) : Set (i ⊔ j ⊔ k) where
  field
    _₁  : T.Tm (Γ T.▷ A) (B T.[ T.p ]T)
    _₂  : T.Tm (Γ T.▷ B) (A T.[ T.p ]T)
    _₁₂ : (_₁ T.[ T.p T., _₂ ]t) l.≡ T.q
    _₂₁ : (_₂ T.[ T.p T., _₁ ]t) l.≡ T.q
open _≅T_ public

Π₁ : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j}{k}{B : S.Ty (Γ S.▷ A) k} →
  T.Tm (Con Γ T.▷ Ty (S.Π A B)) (T.Π (Ty A) (Ty B T.[ ▷ ₂ ]T) T.[ T.p ]T)
Π₁ = T.lam (Tm (S.app S.q) T.[ ▷ ₂ T.∘ (▷ ₂ T.^ _) ]t)

U₁ : ∀{i}{Γ : S.Con i}{j} → T.Tm (Con Γ T.▷ Ty (S.U j)) (T.U j)
U₁ = T.c (Ty (S.El S.q) T.[ ▷ ₂ ]T)

Bool₂ : ∀{i}{Γ : S.Con i} → T.Tm (Con Γ T.▷ T.Bool) (Ty S.Bool T.[ T.p ]T)
Bool₂ = T.if _ (Tm S.true T.[ T.p ]t) (Tm S.false T.[ T.p ]t) T.q

Σ : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j}{k}{B : S.Ty (Γ S.▷ A) k} →
  Ty (S.Σ A B) ≅T T.Σ (Ty A) (Ty B T.[ ▷ ₂ ]T)
Σ = record {
  _₁ = (Tm (S.fst S.q) T.,Σ Tm (S.snd S.q)) T.[ ▷ ₂ ]t ;
  _₂ = Tm (S.v¹ S.,Σ S.v⁰) T.[ ▷ ₂ T.∘ (▷ ₂ T.∘ (T.p T., T.fst T.q) T., T.snd T.q) ]t ;
  _₁₂ = l.refl ;
  _₂₁ = l.refl }

⊤ : ∀{i}{Γ : S.Con i} → Ty (S.⊤ {Γ = Γ}) ≅T T.⊤
⊤ = record {
  _₁ = T.tt ;
  _₂ = Tm S.tt T.[ T.p ]t ;
  _₁₂ = l.refl ;
  _₂₁ = l.refl }

K : ∀{i}{Γ : S.Con i}{j}{Δ : S.Con j} →
  Ty (S.K {_}{Γ} Δ) ≅T T.K {_}{Con Γ} (Con Δ)
K = record {
  _₁ = T.mkK (Sub (S.unK S.q) T.∘ ▷ ₂) ;
  _₂ = Tm (S.mkK S.id) T.[ T.unK T.q ]t ;
  _₁₂ = l.refl ;
  _₂₁ = l.refl }

