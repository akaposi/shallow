module Pseudomorphism.NF where

open import Agda.Primitive
import Lib as l
import WrappedStandard as S
import WrappedStandardDeepCopy as T
open import Pseudomorphism
open import Pseudomorphism.Maps

postulate
  VAR     : ∀{i}{Γ : S.Con i}{j}(A : S.Ty Γ j) → T.Ty (Con Γ T.▷ Ty A) j
  NE      : ∀{i}{Γ : S.Con i}{j}(A : S.Ty Γ j) → T.Ty (Con Γ T.▷ Ty A) j
  NF      : ∀{i}{Γ : S.Con i}{j}(A : S.Ty Γ j) → T.Ty (Con Γ T.▷ Ty A) j
  NEvar   : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j} →
            T.Tm (Con Γ T.▷ Ty A T.▷ VAR A) (NE A T.[ T.p ]T)
  NEapp   : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j}{k}{B : S.Ty (Γ S.▷ A) k} →
            T.Tm (Con Γ T.▷ Ty (S.Π A B) T.▷ NE (S.Π A B) T.▷ Ty A T.[ T.p² ]T T.▷ NF A T.[ T.p³ T., T.q ]T)
                 (NE B T.[ ▷ ₂ T.∘ (T.p⁴ T., T.v¹) T., T.app Π₁ T.[ T.p⁴ T., T.v³ T., T.v¹ ]t ]T)
  NEfst   : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j}{k}{B : S.Ty (Γ S.▷ A) k} →
            T.Tm (Con Γ T.▷ Ty (S.Σ A B) T.▷ NE (S.Σ A B))
                 (NE A T.[ T.p² T., T.fst (Σ ₁) T.[ T.p ]t ]T)
  NEsnd   : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j}{k}{B : S.Ty (Γ S.▷ A) k} →
            T.Tm (Con Γ T.▷ Ty (S.Σ A B) T.▷ NE (S.Σ A B))
                 (NE B T.[ ▷ ₂ T.∘ (T.p T., T.fst (Σ ₁)) T., T.snd (Σ ₁) ]T T.[ T.p ]T)
  NFneU   : ∀{i}{Γ : S.Con i}{j} →
            T.Tm (Con Γ T.▷ Ty (S.U j) T.▷ NE (S.U j)) (NF (S.U j) T.[ T.p ]T)
  NFneEl  : ∀{i}{Γ : S.Con i}{j} →
            T.Tm (Con Γ T.▷ Ty (S.U j) T.▷ NE (S.U j) T.▷ T.El U₁ T.[ T.p ]T T.▷ NE (S.El S.q) T.[ ▷ ₂ T.^ _ ]T T.[ T.p² T., T.q ]T)
                 (NF (S.El S.q) T.[ ▷ ₂ T.^ _ ]T T.[ (T.p² T., T.q) ]T T.[ T.p ]T )
  NFlam   : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j}{k}{B : S.Ty (Γ S.▷ A) k} →
            T.Tm (Con Γ T.▷ Ty (S.Π A B) T.▷ T.Π (Ty A T.[ T.p ]T) (T.Π (VAR A T.[ T.p² T., T.q ]T) (NF B T.[ ▷ ₂ T.∘ (T.p³ T., T.v¹) T., T.app Π₁ T.[ T.p² T., T.v¹ ]t ]T)))
                 (NF (S.Π A B) T.[ T.p ]T)
  NF,     : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j}{k}{B : S.Ty (Γ S.▷ A) k} →
            T.Tm (Con Γ T.▷ Ty A T.▷ NF A T.▷ Ty B T.[ ▷ ₂ T.∘ T.p ]T T.▷ NF B T.[ ▷ ₂ T.∘ T.p² T., T.q ]T)
                 (NF (S.Σ A B) T.[ T.p⁴ T., Σ ₂ T.[ T.p⁴ T., T.v³ T.,Σ T.v¹ ]t ]T)
  NFtt    : ∀{i}{Γ : S.Con i} →
            T.Tm (Con Γ) (NF S.⊤ T.[ T.id T., Tm S.tt ]T)

{-
VAR   : ∀ Γ A   → Ty (F Γ ▷ F A)
VAR   : ∀ Γ     → 

NEvar : ∀ Γ     → Tm (F Γ ▷ F U ▷ F (El q) ▷ Var ...)
NEvar : ∀ Γ A   → Tm (F Γ ▷ F A ▷ VAR A) (NE A [p])
NEvar : ∀ Γ A t → Tm (F Γ ▷ VAR A [  ])  (NE A [p])

-}
