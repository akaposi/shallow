{-# OPTIONS --no-pattern-matching --prop  #-}

module Setoidification where

open import Agda.Primitive
import Lib as m
import WrappedAnticatStandard.Props as S

variable
  i j k l : Level

{- Category -}
open import Setoidification.Category

{- Terminal Object -}
open import Setoidification.TerminalObject

{- Families -}
open import Setoidification.Families

-- {- Comprehension -}
-- open import experiment.Setoidification.Comprehension

-- {- Unit -}
-- open import experiment.Setoidification.Unit

-- {- Sigma -}
-- open import experiment.Setoidification.Sigma

-- {- Pi -}
-- open import experiment.Setoidification.Pi

{-
Π̂-T = U {Γ}
Π̂-B-α
Tm Π̂ᶜ Π̂ᴿ = Tm (Γ ▹ Conˢ ▹ U ▹ Π̂-B-α ) (Tyˢ [ wk² U (Π Π̂-B-α (Tyˢ [ wk² U Π̂-B-α ]T)) ])

(Γ:Con) =
  ∣Γ∣ : S.COn
  Γ~  : TyP (∣Γ∣ ▹ ∣Γ∣ [ε])
  RΓ  : TmP ∣Γ∣ (Γ~[id,id])
  SΓ  : TmP (∣Γ∣ ▹ ∣Γ∣ [ε] ▹ Γ~) (Γ~[swap∘p])            Tm Nat Vec  (Vec : Ty Nat),    Sub Nat Vec
  TΓ  : TmP (∣Γ∣ ▹ ∣Γ∣ [ε] ▹ Γ~ ▹ ∣Γ∣ ▹ Γ~' v² v⁰) (Γ~[swap∘p])          Sub (A▹B[p]) (B▹A[p])       Tm (Γ▹A▹B[p]) (Σ (B[p²]) (A[p²]))    Sub (Γ▹A▹B[p]) (Γ▹B▹A[p])        Tm Γ A = (σ:Sub Γ (Γ▹A))×(p∘σ=id)        t:Tm Γ A     (id,t)   p∘(id,t)=id

  Γ~' t₀ t₁ := Γ~[t₀,t₁]

(A:Ty Γ) =
  ∣A∣ : S.T
  y ∣Γ∣
  A~  : ...

https://bitbucket.org/akaposi/qiit/src/master/Setoid/Utils.agda
https://bitbucket.org/akaposi/qiit/src/master/ToS/Algebra/

Agda-ban CwF:                    egy tetsz.strict modellben belso CwF:     a standard modell: Con,Ty,Sub, ... Γ:Con a standard modellben
Con : Set                        Conˢ : Ty Γ
Ty : COn → Set                   Tyˢ  : Ty (Γ▹Conˢ)
Tm : (Γ:Con)→Ty Γ→Set            Tmˢ  : Ty (Γ▹Conˢ▹Tyˢ)
...
∙ : Con                          ∙ˢ   : Tm Γ Conˢ
_▷_ : (Γ:COn)→Ty Γ → Con         ▷ˢ   : Tm (Γ▹Conˢ▹Tyˢ) (Conˢ[p²])
...


p∘p     p²
-}

-- Sub Θ Δ      →   Sub Γ Θ  → Sub Γ Δ
-- Tm  Θ (Δ[ε]) →   Sub Γ Θ  → Tm  Γ (Δ[ε])
-- Tm  Θ A      →(σ:Sub Γ Θ) → Tm  Γ (A[σ])
-- _∘_ : (Θ→Δ)       →   (Γ→Θ) → (Γ    → Δ)
-- _∘_ : ((x:Θ)→A x) → (σ:Γ→Θ) → ((x:Γ)→ A (σ x))

-- Sub Δ Γ = Tm Δ (Γ[ε])
-- Ty ∙ = Con
-- Lift : TyP ∙ → Ty ∙
-- Lift : TyP ∙ → Con
-- TmP Γ (Δ[ε]) = Tm Γ (Lift Δ[ε]) = Sub Γ (Lift Δ)
{-

  Tm ⊃ Sub
     ⊃ TmP

┌──────────────────────────────────────────────────┐
│                                                  │
│                  ┌─────────────────────────┐     │
│                  │                         │     │
│                  │                         │     │
│                  │                         │     │
│      Sub         │                         │     │
│     ┌────────────┼───────┐                 │     │
│     │            │       │                 │     │
│     │            │       │                 │     │
│     │            └───────┼─────────────────┘     │
│     │                    │          TmP          │
│     │                    │                       │
│     │                    │                       │
│     └────────────────────┘                       │
│                                                  │
└──────────────────────────────────────────────────┘

-}
