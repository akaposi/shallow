{-# OPTIONS --no-pattern-matching #-}
-- {-# OPTIONS --type-in-type #-}

module Anticategorification where

-- model construction where Con becomes Ty and Sub becomes Tm

open import Agda.Primitive
import Lib as m
import WrappedStandard as S

infixl 5 _,_
infixl 5 _^_
infixr 6 _⇒_
infixl 7 _$_
infixl 6 _,Σ_

variable
  i j k l : Level

module Category where

  infixr 6 _∘_

  -- A.
  Con : (i : Level) → Set (lsuc i)
  Con i = S.Ty (S.∙ S.▷ S.⊤) i

  variable 
    Γ Δ Θ Ξ : Con i

  -- B.
  Sub : (Γ : Con i)(Δ : Con j) → Set (i ⊔ j)
  Sub Γ Δ = S.Tm (S.∙ S.▷ Γ S.[ S.ε S., S.tt ]T) (Δ S.[ S.ε S., S.tt ]T)
  
  variable
    σ δ ν : Sub Γ Δ

  -- a.
  _∘_ : (σ : Sub Δ Θ)(δ : Sub Γ Δ) → Sub Γ Θ
  σ ∘ δ = σ S.[ S.ε S., δ ]t

  -- b.
  id : Sub Γ Γ
  id = S.q

  -- 1.
  ass : (σ ∘ δ) ∘ ν m.≡ σ ∘ (δ ∘ ν)
  ass = m.refl

  -- 2.
  idl : id ∘ σ m.≡ σ
  idl = m.refl

  -- 3.
  idr : σ ∘ id m.≡ σ
  idr = m.refl

open Category

module TerminalObject where

  -- a.
  ∙ : Con lzero
  ∙ = S.⊤

  -- b.
  ε : Sub Γ ∙
  ε = S.tt

  -- 1.
  ∙η : {σ : Sub Γ ∙} → σ m.≡ ε
  ∙η = m.refl

open TerminalObject

module Families where -- TODO: Familyfication? :D
  
  infixl 7 _[_]T
  infixl 8 _[_]t

  -- A.
  Ty : (Γ : Con i)(j : Level) → Set (i ⊔ lsuc j)
  Ty Γ j = S.Ty (S.∙ S.▷ Γ S.[ S.ε S., S.tt ]T) j

  variable 
    A B : Ty Γ i
  
  -- B.
  Tm : (Γ : Con i)(A : Ty Γ j) → Set (i ⊔ j)
  Tm Γ A = S.Tm (S.∙ S.▷ Γ S.[ S.ε S., S.tt ]T) A

  variable
    t : Tm Γ A

  -- a.
  _[_]T : Ty Δ k → Sub Γ Δ → Ty Γ k
  A [ σ ]T = A S.[ S.ε S., σ ]T

  -- b.
  _[_]t : {A : Ty Δ k} → Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
  t [ σ ]t = t S.[ S.ε S., σ ]t

  -- 1.
  [∘]T : {A : Ty Θ l} → A [ δ ]T [ σ ]T m.≡ A [ δ ∘ σ ]T
  [∘]T = m.refl

  -- 2.
  [id]T : A [ id ]T m.≡ A
  [id]T = m.refl

  -- 3.
  [∘]t : {A : Ty Θ l}{t : Tm Θ A} → t [ δ ]t [ σ ]t m.≡ t [ δ ∘ σ ]t
  [∘]t = m.refl

  -- 4.
  [id]t : {A : Ty Γ l}{t : Tm Γ A} → t [ id ]t m.≡ t
  [id]t = m.refl

open Families

module Comprehensions where

  infixl 5 _▷_

  -- a.
  _▷_ : (Γ : Con i)(A : Ty Γ l) → Con (i ⊔ l)
  Γ ▷ A = S.Σ Γ (A S.[ S.ε S., S.q ]T) 
  
  -- b.
  _,_ : (σ : Sub Γ Δ){A : Ty Δ l}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▷ A)
  σ , t = σ S.,Σ t

  -- c.
  p : Sub (Γ ▷ A) Γ
  p = S.fst S.q

  -- d.
  q : Tm (Γ ▷ A) (A [ p ]T)
  q = S.snd S.q

  -- 1.
  ▷β₁ : {A : Ty Δ l}{t : Tm Γ (A [ σ ]T)} → p ∘ (_,_ σ {A = A} t) m.≡ σ
  ▷β₁ = m.refl

  -- 2.
  ▷β₂ : {A : Ty Δ l}{t : Tm Γ (A [ σ ]T)} → q [ _,_ σ {A = A} t ]t m.≡ t
  ▷β₂ = m.refl

  -- 3.
  ▷η : (p {A = A} , q {A = A}) m.≡ id
  ▷η = m.refl

  -- ?. (Derivable?) TOASK
  ,∘ : {A : Ty Θ l}{t : Tm Δ (A [ δ ]T)} → (_,_ δ {A = A} t) ∘ σ m.≡ (δ ∘ σ) , (t [ σ ]t)
  ,∘ = m.refl

open Comprehensions

module Unit where

  -- a.
  ⊤ : Ty Γ lzero
  ⊤ = S.⊤

  -- b.
  tt : Tm Γ ⊤
  tt = S.tt

  -- 1.
  ⊤η : {t : Tm Γ ⊤} → t m.≡ tt
  ⊤η = m.refl

  -- 2.
  ⊤[] : ⊤ [ σ ]T m.≡ ⊤
  ⊤[] = m.refl

  -- ?. TOASK
  tt[] : tt [ σ ]t m.≡ tt
  tt[] = m.refl

open Unit

module Sigma where

  -- test : Con (lsuc lzero)
  -- test = {! ∙  !}
  
  -- a.
  Σ : (A : Ty Γ k)(B : Ty (Γ ▷ A) l) → Ty Γ (k ⊔ l)
  Σ A B = S.Σ A (B S.[ S.ε S., (S.q S.[ S.p ]t S.,Σ S.q) ]T)

  -- b.
  _,Σ_ : {A : Ty Γ k}{B : Ty (Γ ▷ A) l}
         (u : Tm Γ A)(v : Tm Γ (B [ id , u ]T)) → Tm Γ (Σ A B)
  u ,Σ v = u S.,Σ v

  -- c.
  fst : Tm Γ (Σ A B) → Tm Γ A
  fst t = S.fst t

  -- d.
  snd : (t : Tm Γ (Σ A B)) → Tm Γ (B [ id , fst t ]T)
  snd t = S.snd t

  -- 1.
  Σβ₁ : {A : Ty Γ k}{B : Ty (Γ ▷ A) l}{u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} →
        fst (_,Σ_ {A = A}{B = B} u v) m.≡ u
  Σβ₁ = m.refl

  Σβ₂ : {A : Ty Γ k}{B : Ty (Γ ▷ A) l}{u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} →
        snd (_,Σ_ {A = A}{B = B} u v) m.≡ v
  Σβ₂ = m.refl

  -- Auxiliary operator for substitution behind context extension
  _^ : (σ : Sub Γ Δ) → Sub (Γ ▷ A [ σ ]T) (Δ ▷ A)
  σ ^ = (σ ∘ p) , q

  Ση : {A : Ty Γ k}{B : Ty (Γ ▷ A) l}{t : Tm Γ (Σ A B)} →
       fst t ,Σ snd t m.≡ t
  Ση = m.refl

  Σ[] : {A : Ty Δ k}{B : Ty (Δ ▷ A) l} →
        Σ A B [ σ ]T m.≡ Σ (A [ σ ]T) (B [ σ ^ ]T)
  Σ[] = m.refl

  ,Σ[] : {A : Ty Δ k}{B : Ty (Δ ▷ A) l}{u : Tm Δ A}{v : Tm Δ (B [ id , u ]T)} →
         (_,Σ_ {A = A}{B = B} u v) [ σ ]t m.≡ (u [ σ ]t) ,Σ (v [ σ ]t)
  ,Σ[] = m.refl

open Sigma

module Pi where

  Π : (A : Ty Γ k)(B : Ty (Γ ▷ A) l) → Ty Γ (k ⊔ l)
  Π A B = S.Π A ((B S.[ S.ε S., (S.q S.[ S.p ]t S.,Σ S.q) ]T))

  lam : (t : Tm (Γ ▷ A) B) → Tm Γ (Π A B)
  lam t = S.lam (t S.[ S.ε S., (S.q S.[ S.p ]t S.,Σ S.q) ]t)

  -- lam t = S.lam (S.lam (t S.[ S.ε ]t S.$ (S.v¹ S.,Σ S.v⁰)))
  -- --      λ γ α → t (γ m.,Σ α)

  app : (t : Tm Γ (Π A B)) → Tm (Γ ▷ A) B
  app t = (S.app t) S.[ (S.ε S., S.fst S.q) S., S.snd S.q ]t
  --        S.Sub (Γ ▷ A) (S.∙ S.▷ Γ S.[ S.ε S., S.tt ]T S.▷ A) 
  --        S.Sub (S.∙ S.▷ (S.Σ Γ (A S.[ S.ε S., S.q ]T)) S.[ S.ε S., S.tt ]T) (S.∙ S.▷ Γ S.[ S.ε S., S.tt ]T S.▷ A) 
  --        S.Sub (S.∙ S.▷ (S.Σ (Γ S.[ S.ε S., S.tt ]T) (A S.[ S.ε S., S.q ]T)))
  --              (S.∙ S.▷       Γ S.[ S.ε S., S.tt ]T S.▷ A) 

  Πβ : {B : Ty (Γ ▷ A) l}{t : Tm (Γ ▷ A) B} → app (lam t) m.≡ t
  Πβ = m.refl

  -- Πη : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{t : Tm Γ (Π A B)} → lam (app t) m.≡ t
  -- Πη = m.refl

  Π[] : Π A B [ σ ]T m.≡ Π (A [ σ ]T) (B [ σ ^ ]T)
  Π[] = m.refl

  lam[] : {B : Ty (Δ ▷ A) l}{t : Tm (Δ ▷ A) B} → lam t [ σ ]t m.≡ lam (t [ σ ^ ]t)
  lam[] = m.refl

  -- -- abbreviations

  -- _⇒_ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty Γ k) → Ty Γ (j ⊔ k)
  -- A ⇒ B = Π A (B [ p ]T)

  -- _$_ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}(t : Tm Γ (Π A B))(u : Tm Γ A) → Tm Γ (B [ id , u ]T)
  -- t $ u = app t [ id , u ]t

open Pi

module Universe where
  -- -- U

  U : (j : Level) → Ty Γ (lsuc j)
  U j = S.U j

  El : (a : Tm Γ (U j)) → Ty Γ j
  El a = S.El a

  c : (A : Ty Γ j) → Tm Γ (U j)
  c A = S.c A

  Uβ : {A : Ty Γ j} → El (c A) m.≡ A
  Uβ = m.refl

  Uη : {a : Tm Γ (U j)} → c (El a) m.≡ a
  Uη = m.refl

  U[] : U k [ σ ]T m.≡ U k
  U[] = m.refl

  El[] : {a : Tm Δ (U k)} → El a [ σ ]T m.≡ El (a [ σ ]t)
  El[] = m.refl

open Universe

module AMLTT where

  Con= : ∀{i} → Con i m.≡ Ty ∙ i
  Con= = m.refl

  Sub= : ∀{i j}{Γ : Con i}{Δ : Con j} → Sub Γ Δ m.≡ Tm Γ (Δ [ ε ]T)
  Sub= = m.refl

  ∘= : ∀{i j k}{Θ : Con i}{Δ : Con j}{Γ : Con k}{δ : Sub Γ Θ}{σ : Sub Θ Δ} →
       σ ∘ δ m.≡ σ [ δ ]t
  ∘= = m.refl

  ∙= : ∙ m.≡ S.⊤
  ∙= = m.refl

  ε= : ∀{i}{Γ : Con i} → ε {i} {Γ} m.≡ tt {i} {Γ}
  ε= = m.refl

  ▹= : ∀{i j}{Γ : Con i}{A : Ty Γ j} → Γ ▷ A m.≡ Σ Γ (A [ q ]T)
  ▹= = m.refl

  ,= : σ , t m.≡ σ ,Σ t
  ,= = m.refl  

  p= : p {Γ = Γ}{A = A} m.≡ fst id
  p= = m.refl 

  q= : q {Γ = Γ}{A = A} m.≡ snd id
  q= = m.refl 

open AMLTT
