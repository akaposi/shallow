module NormalForm.Lib where

open import Agda.Primitive

private variable
  i : Level

record NTy' (A : Set i) : Set i where
  constructor mk
  field ∣_∣ : A
open NTy' public

record Nf' (A : Set i) : Set i where
  constructor mk
  field ∣_∣ : A
open Nf' public

record Ne' (A : Set i) : Set i where
  constructor mk
  field ∣_∣ : A
open Ne' public
