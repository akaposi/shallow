{-# OPTIONS --no-pattern-matching --prop  #-}

module Setoidification.CategoryModules where

open import Agda.Primitive
import Lib as m
import WrappedAnticatStandard.Props as S

variable
  i j k l : Level

module Con {i : Level} where

  ∣Γ∣ : Set (lsuc i)
  ∣Γ∣ = S.Con i

  Γ~ : ∣Γ∣ → Set (lsuc i)
  Γ~ ∣ = S.TyP (∣ S.▷ ∣ S.[ S.ε ]T) i

  Γ⟳ : {∣ : ∣Γ∣} → (~ : Γ~ ∣) → Set i
  Γ⟳ {∣} ~ = S.TmP ∣ (~ S.[ S.id S., S.id ]TP)

  Γ↔ : {∣ : ∣Γ∣} → (~ : Γ~ ∣) → Set i
  Γ↔ {∣} ~ = S.TmP (∣ S.▷ ∣ S.[ S.ε ]T S.▷P ~) (~ S.[ S.v¹ S., S.p² ]TP)

  Γ» : {∣ : ∣Γ∣} → (~ : Γ~ ∣) → Set i
  Γ» {∣} ~ = S.TmP (∣ S.▷ ∣ S.[ S.ε ]T S.▷ ∣ S.[ S.ε ]T S.▷P ~ S.[ S.p ]TP S.▷P ~ S.[ S.v² S., S.v¹ ]TP) (~ S.[ S.p⁴ S., S.v² ]TP)

open Con

open Con renaming
  ( ∣Γ∣ to ∣_∣
  ; Γ~ to [_]~
  ; Γ⟳ to [_]⟳
  ; Γ↔ to [_]↔
  ; Γ» to [_]»
  ) public

variable
  Γ Δ Θ Ω : ∣_∣ {i}

module Sub (Γ : ∣_∣ {i}) (Δ : ∣_∣ {i}) where

  ∣σ∣ : Set i
  ∣σ∣ = S.Sub Γ Δ

  σ≈ : [ Γ ]~ → [ Δ ]~ → ∣σ∣ → Set i
  σ≈ Γ~ Δ~ ∣ = S.TmP (Γ S.▷ Γ S.[ S.ε ]T S.▷P Γ~) (Δ~ S.[ ∣ S.∘ S.p² S., ∣ S.∘ S.v¹ ]TP)

open Sub renaming
  ( ∣σ∣ to ∣_,_∣
  ; σ≈ to [_,_]≈
  )

variable
  σ δ ν : ∣ Γ , Δ ∣

module _∘_
  {Γ : ∣_∣ {i}} {Δ : ∣_∣ {i}} {Θ : ∣_∣ {i}}
  (σ : ∣ Δ , Θ ∣) (δ : ∣ Γ , Δ ∣)
  where

  ∣_∘_∣ : ∣ Γ , Θ ∣
  ∣_∘_∣ = σ S.∘ δ

  ∘≈ :
    (Γ~ : [ Γ ]~) → (Δ~ : [ Δ ]~) → (Θ~ : [ Θ ]~) →
    [ Δ , Θ ]≈ Δ~ Θ~ σ → [ Γ , Δ ]≈ Γ~ Δ~ δ →
    [ Γ , Θ ]≈ Γ~ Θ~ ∣_∘_∣
  ∘≈ Γ~ Δ~ Θ~ σ≈ δ≈ = σ≈ S.[ δ S.∘ S.p² S., δ S.∘ S.v¹ S.,P δ≈ ]tP

open _∘_

module id {Γ : ∣_∣ {i}} where

  ∣id∣ : ∣ Γ , Γ ∣
  ∣id∣ = S.id

  id≈ : (Γ~ : [ Γ ]~) → [ Γ , Γ ]≈ Γ~ Γ~ ∣id∣
  id≈ Γ~ = S.q

open id

ass : ∣ ∣ σ ∘ δ ∣ ∘ ν ∣ m.≡ ∣ σ ∘ ∣ δ  ∘ ν ∣ ∣
ass = m.refl

idl : ∣ ∣id∣ ∘ σ ∣ m.≡ σ
idl = m.refl

idr : ∣ σ ∘ ∣id∣ ∣ m.≡ σ
idr = m.refl
