{-# OPTIONS --no-pattern-matching --prop  #-}

module Setoidification.TerminalObject where

open import Agda.Primitive
import Lib as m
import WrappedAnticatStandard.Props as S

open import Setoidification.Category

∙ : Con lzero
∙ = record
  { ¦Γ¦ = S.∙
  ; Γ~ = S.⊤P
  ; Γ⟳ = S.ttP
  ; Γ↔ = S.ttP
  ; Γ» = S.ttP
  }

ε : Sub Γ ∙
ε = record
  { ¦σ¦ = S.ε
  ; σ≈ = S.ttP
  }

∙η : {σ : Sub Γ ∙} → σ m.≡ ε
∙η = m.refl
