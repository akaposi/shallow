{-# OPTIONS --no-pattern-matching --prop  #-}

module Setoidification.Category where

open import Agda.Primitive
import Lib as m
import WrappedAnticatStandard.Props as S

variable
  i j k l : Level

record Con (i : Level) : Set (lsuc i) where
  field
    ∣Γ∣ : S.Con i
    Γ~ : S.TyP (∣Γ∣ S.▷ ∣Γ∣ S.[ S.ε ]T) i
    
  Γ~' : ∀{j}{Ω : S.Con j} → S.Sub Ω ∣Γ∣ → S.Sub Ω ∣Γ∣ → S.TyP Ω i
  Γ~'  γ₀ γ₁ = Γ~ S.[ γ₀ S., γ₁ ]TP

  Γline = ∣Γ∣ S.▷ ∣Γ∣ S.[ S.ε ]T S.▷P Γ~' S.p S.q
  Γ»cxt = Γline S.▷ ∣Γ∣ S.[ S.ε ]T S.▷P Γ~' S.v² S.v⁰
  
  field
    Γ⟳ : S.TmP ∣Γ∣ (Γ~' S.id S.id)
    Γ↔ : S.TmP Γline (Γ~' S.v¹ S.p²)
    Γ» : S.TmP Γ»cxt (Γ~' S.p⁴ S.v¹)

  Γ⟳' : ∀{j}{Ω : S.Con j}(γ : S.Sub Ω ∣Γ∣) → S.TmP Ω (Γ~' γ γ)
  Γ⟳' γ = Γ⟳ S.[ γ ]tP

  Γ↔' : ∀{j}{Ω : S.Con j}{γ₀ γ₁ : S.Sub Ω ∣Γ∣} → S.TmP Ω (Γ~' γ₀ γ₁) → S.TmP Ω (Γ~' γ₁ γ₀)
  Γ↔' {γ₀ = γ₀}{γ₁} γ₀₁ = Γ↔ S.[ γ₀ S., γ₁ S., γ₀₁ ]tP

  Γ»' : ∀{j}{Ω : S.Con j}{γ₀ γ₁ : S.Sub Ω ∣Γ∣} → S.TmP Ω (Γ~' γ₀ γ₁) → {γ₂ : S.Sub Ω ∣Γ∣} → S.TmP Ω (Γ~' γ₁ γ₂) → S.TmP Ω (Γ~' γ₀ γ₂)
  Γ»' {γ₀ = γ₀}{γ₁} γ₀₁ {γ₂} γ₁₂ = Γ» S.[ γ₀ S., γ₁ S., γ₀₁ S., γ₂ S., γ₁₂ ]tP

open Con renaming
  ( ∣Γ∣ to ∣_∣
  ; Γ~ to [_]~
  ; Γ~' to [_]~'
  ; Γ⟳ to [_]⟳
  ; Γ⟳' to [_]⟳'
  ; Γ↔ to [_]↔
  ; Γ↔' to [_]↔'
  ; Γ» to [_]»
  ; Γ»' to [_]»'
  ; Γline to [_]line
  ; Γ»cxt to [_]»cxt
  ) public

variable
  Γ Δ Θ Ω : Con i

record Sub (Γ : Con i) (Δ : Con j) : Set (i ⊔ j) where
  field
    ∣σ∣ : S.Sub ∣ Γ ∣ ∣ Δ ∣
    σ≈ : S.TmP (∣ Γ ∣ S.▷ ∣ Γ ∣ S.[ S.ε ]T S.▷P [ Γ ]~' S.p S.q) ([ Δ ]~' (∣σ∣ S.∘ S.p²) (∣σ∣ S.∘ S.v¹))
  σ≈' : ∀{j}{Ω : S.Con j}{γ₀ γ₁ : S.Sub Ω ∣ Γ ∣} → S.TmP Ω ([ Γ ]~' γ₀ γ₁) → S.TmP Ω ([ Δ ]~' (∣σ∣ S.∘ γ₀) (∣σ∣ S.∘ γ₁))
  σ≈' {γ₀ = γ₀}{γ₁} γ₀₁ = σ≈ S.[ γ₀ S., γ₁ S., γ₀₁ ]tP
open Sub renaming
  ( ∣σ∣ to ∣_∣
  ; σ≈ to [_]≈
  ; σ≈' to [_]≈'
  )

variable
  σ δ ν : Sub Γ Δ

infixr 6 _∘_
_∘_ : Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
σ ∘ δ = record
  { ∣σ∣ = ∣ σ ∣ S.∘ ∣ δ ∣
  ; σ≈ = [ σ ]≈' ([ δ ]≈' S.v⁰)
  }

id : Sub Γ Γ
id = record
  { ∣σ∣ = S.id
  ; σ≈ = S.q
  }

ass : (σ ∘ δ) ∘ ν m.≡ σ ∘ (δ ∘ ν)
ass = m.refl

idl : id ∘ σ m.≡ σ
idl = m.refl

idr : σ ∘ id m.≡ σ
idr = m.refl
