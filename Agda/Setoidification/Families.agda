{-# OPTIONS --no-pattern-matching --prop  #-}

module Setoidification.Families where

open import Agda.Primitive
import Lib as m
import WrappedAnticatStandard.Props as S

open import Setoidification.Category

module _ (Γ : Con i) (j : Level) where

  ∣A∣ᵀ : Set _
  ∣A∣ᵀ = S.Ty ∣ Γ ∣ j
  
  A~ᵀ : ∣A∣ᵀ → Set _
  A~ᵀ ∣A∣ = S.TyP ([ Γ ]line S.▷ ∣A∣ S.[ S.p² ]T S.▷ ∣A∣ S.[ S.v² ]T) j

  A~' : (∣A∣ : ∣A∣ᵀ) → A~ᵀ ∣A∣ →
        ∀{k}{Ω : S.Con k}{γ₀ γ₁ : S.Sub Ω ∣ Γ ∣} → S.TmP Ω ([ Γ ]~' γ₀ γ₁) → S.Tm Ω (∣A∣ S.[ γ₀ ]T) → S.Tm Ω (∣A∣ S.[ γ₁ ]T) → S.TyP Ω j
  A~' ∣A∣ A~ γ₀₁ a₀ a₁ = A~ S.[ _ S., _ S., γ₀₁ S., a₀ S., a₁ ]TP

  Aline : (∣A∣ : ∣A∣ᵀ) → A~ᵀ ∣A∣ → S.Con _
  Aline ∣A∣ A~ = [ Γ ]line S.▷ ∣A∣ S.[ S.p² ]T S.▷ ∣A∣ S.[ S.v² ]T S.▷P (A~' ∣A∣ A~) S.v² S.v¹ S.v⁰

  A»cxt : (∣A∣ : ∣A∣ᵀ) → A~ᵀ ∣A∣ → S.Con _
  A»cxt ∣A∣ A~ = [ Γ ]»cxt S.▷ ∣A∣ S.[ S.p⁴ ]T S.▷ ∣A∣ S.[ S.v⁴ ]T S.▷P (A~' ∣A∣ A~) S.v⁴ S.v¹ S.v⁰ S.▷ ∣A∣ S.[ S.v⁴ ]T S.▷P (A~' ∣A∣ A~) S.v⁴ S.v² S.v⁰

  A⟳ᵀ : (∣A∣ : ∣A∣ᵀ) → A~ᵀ ∣A∣ → Set _
  A⟳ᵀ ∣A∣ A~ = S.TmP (∣ Γ ∣ S.▷ ∣A∣) ((A~' ∣A∣ A~) ([ Γ ]⟳' S.p) S.v⁰ S.v⁰)

  A↔ᵀ : (∣A∣ : ∣A∣ᵀ) → A~ᵀ ∣A∣ → Set _
  A↔ᵀ ∣A∣ A~ = S.TmP (Aline ∣A∣ A~) ((A~' ∣A∣ A~) ([ Γ ]↔' S.v³) S.v¹ S.v²)

  A»ᵀ : (∣A∣ : ∣A∣ᵀ) → A~ᵀ ∣A∣ → Set _
  A»ᵀ ∣A∣ A~ = S.TmP (A»cxt ∣A∣ A~) ((A~' ∣A∣ A~) ([ Γ ]» S.[ S.p⁵ ]tP) S.v⁴ S.v¹)

module _ (Γ : Con i){j : Level}(∣A∣ : ∣A∣ᵀ Γ j)(A~ : A~ᵀ Γ j ∣A∣)  where
  ∣t∣ᵀ : Set _
  ∣t∣ᵀ = S.Tm ∣ Γ ∣ ∣A∣
  t~ᵀ : ∣t∣ᵀ → Set _
  t~ᵀ ∣t∣ = S.TmP ([ Γ ]line) (A~ S.[ S.id S., (∣t∣ S.[ S.p² ]t) S., ∣t∣ S.[ S.v¹ ]t ]TP)

{-
record Ty (Γ : Con i) (j : Level) : Set (lsuc (i ⊔ j)) where
  field
    ∣A∣ : S.Ty ∣ Γ ∣ j
    A~  : S.TyP ([ Γ ]line S.▷ ∣A∣ S.[ S.p² ]T S.▷ ∣A∣ S.[ S.v² ]T) j
    
  A~' : ∀{k}{Ω : S.Con k}{γ₀ γ₁ : S.Sub Ω ∣ Γ ∣} → S.TmP Ω ([ Γ ]~' γ₀ γ₁) → S.Tm Ω (∣A∣ S.[ γ₀ ]T) → S.Tm Ω (∣A∣ S.[ γ₁ ]T) → S.TyP Ω j
  A~' γ₀₁ a₀ a₁ = A~ S.[ _ S., _ S., γ₀₁ S., a₀ S., a₁ ]TP

  Aline = [ Γ ]line S.▷ ∣A∣ S.[ S.p² ]T S.▷ ∣A∣ S.[ S.v² ]T S.▷P A~' S.v² S.v¹ S.v⁰
  A»cxt = [ Γ ]»cxt S.▷ ∣A∣ S.[ S.p⁴ ]T S.▷ ∣A∣ S.[ S.v⁴ ]T S.▷P A~' S.v⁴ S.v¹ S.v⁰ S.▷ ∣A∣ S.[ S.v⁴ ]T S.▷P A~' S.v⁴ S.v² S.v⁰

  field
    A⟳ : S.TmP (∣ Γ ∣ S.▷ ∣A∣) (A~' ([ Γ ]⟳' S.p) S.v⁰ S.v⁰)
    A↔ : S.TmP Aline (A~' ([ Γ ]↔' S.v³) S.v¹ S.v²)
    A» : S.TmP A»cxt (A~' ([ Γ ]» S.[ S.p⁵ ]tP) S.v⁴ S.v¹)

    A↷ : S.Tm (line Γ S.▷ ¦A¦ S.[ start Γ ]T) (¦A¦ S.[ end Γ S.∘ S.p ]T)
    A≈ : S.TmP (line Γ S.▷ ¦A¦ S.[ start Γ ]T) (A~ S.[ S.id S., A↷ ]TP)
open Ty renaming
  ( ¦A¦ to ¦_¦
  ; A~ to [_]~
  ; A⟳ to [_]⟳
  ; A↔ to [_]↔
--   ; A» to [_]»
  ; A↷ to [_]↷
  ; A≈ to [_]≈
  ) public

variable
  A : Ty Γ i

record Tm (Γ : Con i) (A : Ty Γ j) : Set (lsuc (i ⊔ j)) where
  field
    ¦A¦ : S.Tm ¦ Γ ¦ ¦ A ¦
    A~ : S.TmP (line Γ) ([ A ]~ S.[ S.id S., ¦A¦ S.[ start Γ ]t S., ¦A¦ S.[ end Γ ]t ]TP)
open Tm renaming
  ( ¦A¦ to ¦_¦
  ; A~ to [_]≈_
  )

variable
  a : Tm Γ A
-}
