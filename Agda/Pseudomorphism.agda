module Pseudomorphism where

open import Agda.Primitive
import Lib as l
import WrappedStandard as S
open import WrappedStandard.Lib
open import Standard.Lib
import WrappedStandardDeepCopy as T
open import WrappedStandardDeepCopy.Lib
open import WrappedStandardDeepCopy.LibCopy

infix 4 _≅_

record _≅_ {i}(Γ : T.Con i){j}(Δ : T.Con j) : Set (i ⊔ j) where
  field
    _₁  : T.Sub Γ Δ
    _₂  : T.Sub Δ Γ
    _₁₂ : _₁ T.∘ _₂ l.≡ T.id
    _₂₁ : _₂ T.∘ _₁ l.≡ T.id
open _≅_ public

Con : {i : Level}(Γ : S.Con i) → T.Con i
Con Γ = mkC ∣ Γ ∣C

Ty : {i : Level}{Γ : S.Con i}{j : Level}(A : S.Ty Γ j) → T.Ty (Con Γ) j
Ty A = mkT ∣ A ∣T

Sub : ∀{i}{Γ : S.Con i}{j}{Δ : S.Con j}(σ : S.Sub Γ Δ) → T.Sub (Con Γ) (Con Δ)
Sub σ = mks ∣ σ ∣s

Tm : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j}(t : S.Tm Γ A) → T.Tm (Con Γ) (Ty A)
Tm t = mkt ∣ t ∣t

id : ∀{i}{Γ : S.Con i} → Sub (S.id {Γ = Γ}) l.≡ T.id
id = l.refl

∘ :  ∀{i}{Θ : S.Con i}{j}{Δ : S.Con j}{σ : S.Sub Θ Δ}{k}{Γ : S.Con k}{δ : S.Sub Γ Θ} →
  Sub (σ S.∘ δ) l.≡ Sub σ T.∘ Sub δ
∘ = l.refl

[]T : ∀{i}{Δ : S.Con i}{j}{A : S.Ty Δ j}{k}{Γ : S.Con k}{σ : S.Sub Γ Δ} →
  Ty (A S.[ σ ]T) l.≡ Ty A T.[ Sub σ ]T
[]T = l.refl

[]t : ∀{i}{Δ : S.Con i}{j}{A : S.Ty Δ j}{t : S.Tm Δ A}{k}{Γ : S.Con k}{σ : S.Sub Γ Δ} →
  Tm (t S.[ σ ]t) l.≡ Tm t T.[ Sub σ ]t
[]t = l.refl

∙ : Con S.∙ ≅ T.∙
∙ = record { _₁₂ = l.refl ; _₂₁ = l.refl }

ε : ∀{i}{Γ : S.Con i} → Sub (S.ε {Γ = Γ}) l.≡ ∙ ₂ T.∘ T.ε
ε = l.refl

▷ : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j} → Con (Γ S.▷ A) ≅ Con Γ T.▷ Ty A
▷ = record {
  _₁ = mks λ γ → fst γ ,Σ snd γ ;
  _₂ = mks λ γ → fst γ ,Σ snd γ ;
  _₁₂ = l.refl ;
  _₂₁ = l.refl }

▷₁∘ : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j}{k}{Θ : S.Con k}{σ : S.Sub Θ Γ} →
  ▷ ₁ T.∘ Sub (σ S.^ A) l.≡ (Sub σ T.^ Ty A) T.∘ ▷ ₁
▷₁∘ = l.refl

, : ∀{i}{Γ : S.Con i}{j}{Δ : S.Con j}{σ : S.Sub Γ Δ}{k}{A : S.Ty Δ k}{t : S.Tm Γ (A S.[ σ ]T)} →
  Sub (σ S., t) l.≡ ▷ {Γ = Δ}{A = A} ₂ T.∘ (Sub σ T., Tm t)
, = l.refl

p : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j} → Sub (S.p {A = A}) l.≡ T.p T.∘ ▷ ₁
p = l.refl

q : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j} → Tm (S.q {A = A}) l.≡ T.q T.[ ▷ ₁ ]t
q = l.refl
