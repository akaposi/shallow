module NormalForm where

open import Agda.Primitive
import Standard.Lib as m
open import WrappedStandard.Lib
open import WrappedStandard as s using (Con ; id ; Ty ; _[_]T ; Tm ; _▷_ ; _,_)
open import Renamings.Lib
import Renamings as r
open import NormalForm.Lib

private variable
  i j k : Level
  Γ : Con i
  A B : Ty Γ i

NTy : Con i → ∀ j → Set (i ⊔ lsuc j)
NTy Γ j = NTy' (∣ Γ ∣C → Set j)

⌜_⌝NTy : NTy Γ j → Ty Γ j
∣ ⌜ A ⌝NTy ∣T = ∣ A ∣

Nf : (Γ : Con i) (A : Ty Γ j) → Set (i ⊔ j)
Nf Γ A = Nf' ((γ : ∣ Γ ∣C) → ∣ A ∣T γ)

⌜_⌝Nf : Nf Γ A → Tm Γ A
∣ ⌜ t ⌝Nf ∣t = ∣ t ∣

Ne : (Γ : Con i) (A : Ty Γ j) → Set (i ⊔ j)
Ne Γ A = Ne' ((γ : ∣ Γ ∣C) → ∣ A ∣T γ)

⌜_⌝Ne : Ne Γ A → Tm Γ A
∣ ⌜ t ⌝Ne ∣t = ∣ t ∣

var : r.Tm Γ A → Ne Γ A
∣ var t ∣ = ∣ t ∣

Π : (A : NTy Γ i) → NTy (Γ ▷ ⌜ A ⌝NTy) j → NTy Γ (i ⊔ j)
∣ Π A B ∣ γ = Π' (∣ A ∣ γ) λ α → ∣ B ∣ (γ m.,Σ α)

lam :
  {A : NTy Γ i} {B : NTy (Γ ▷ ⌜ A ⌝NTy) j} →
  Nf (Γ ▷ ⌜ A ⌝NTy) ⌜ B ⌝NTy → Nf Γ ⌜ Π A B ⌝NTy
∣ lam t ∣ γ = lam' λ α → ∣ t ∣ (γ m.,Σ α)

infixl 7 _$_
_$_ :
  {A : NTy Γ i} {B : NTy (Γ ▷ ⌜ A ⌝NTy) j} →
  Ne Γ ⌜ Π A B ⌝NTy → (u : Nf Γ ⌜ A ⌝NTy) →
  Ne Γ (⌜ B ⌝NTy [ id , ⌜ u ⌝Nf ]T)
∣ t $ u ∣ γ = app' (∣ t ∣ γ) (∣ u ∣ γ)

Σ : (A : NTy Γ i) → NTy (Γ ▷ ⌜ A ⌝NTy) j → NTy Γ (i ⊔ j)
∣ Σ A B ∣ γ = m.Σ (∣ A ∣ γ) λ α → ∣ B ∣ (γ m.,Σ α)

infixl 6 _,Σ_
_,Σ_ :
  {A : NTy Γ i} {B : NTy (Γ ▷ ⌜ A ⌝NTy) j} →
  (u : Nf Γ ⌜ A ⌝NTy) → Nf Γ (⌜ B ⌝NTy [ id , ⌜ u ⌝Nf ]T) → Nf Γ ⌜ Σ A B ⌝NTy
∣ u ,Σ v ∣ γ = ∣ u ∣ γ m.,Σ ∣ v ∣ γ

fst :
  {A : NTy Γ i} {B : NTy (Γ ▷ ⌜ A ⌝NTy) j} →
  Ne Γ ⌜ Σ A B ⌝NTy → Ne Γ ⌜ A ⌝NTy
∣ fst t ∣ γ = m.fst (∣ t ∣ γ)

snd :
  {Γ : Con i} {A : NTy Γ j} {B : NTy (Γ ▷ ⌜ A ⌝NTy) k} →
  (t : Ne Γ ⌜ Σ A B ⌝NTy) → Ne Γ (⌜ B ⌝NTy [ id , ⌜ fst t ⌝Ne ]T)
∣ snd t ∣ γ = m.snd (∣ t ∣ γ)

⊤ : NTy Γ lzero
∣ ⊤ ∣ γ = m.⊤

tt : Nf Γ s.⊤
∣ tt ∣ γ = m.tt

U : ∀ i → NTy Γ (lsuc i)
∣ U i ∣ γ = Set i

El : Ne Γ (s.U i) → NTy Γ i
∣ El a ∣ = ∣ a ∣

c : NTy Γ i → Nf Γ (s.U i)
∣ c A ∣ = ∣ A ∣

Bool : NTy Γ lzero
∣ Bool ∣ γ = m.Bool

neBool : Ne Γ s.Bool → Nf Γ s.Bool
∣ neBool t ∣ γ = ∣ t ∣ γ

true : Nf Γ s.Bool
∣ true ∣ γ = m.true

false : Nf Γ s.Bool
∣ false ∣ γ = m.false

if :
  (C : NTy (Γ ▷ s.Bool) j) →
  Nf Γ (⌜ C ⌝NTy [ id , ⌜ true ⌝Nf ]T) →
  Nf Γ (⌜ C ⌝NTy [ id , ⌜ false ⌝Nf ]T) →
  (t : Ne Γ s.Bool) →
  Ne Γ (⌜ C ⌝NTy [ id , ⌜ t ⌝Ne ]T)
∣ if C u v t ∣ γ = m.if (λ b → ∣ C ∣ (γ m.,Σ b)) (∣ u ∣ γ) (∣ v ∣ γ) (∣ t ∣ γ)
