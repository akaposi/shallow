module StandardInterpretation where

open import Agda.Primitive
import Lib as l
import WrappedStandard as S
open import WrappedStandard.Lib
open import Standard.Lib
import Standard as SET

Con : {i : Level}(Γ : S.Con i) → SET.Con i
Con Γ = ∣ Γ ∣C

Ty : {i : Level}{Γ : S.Con i}{j : Level}(A : S.Ty Γ j) → SET.Ty (Con Γ) j
Ty A = ∣ A ∣T

Sub : ∀{i}{Γ : S.Con i}{j}{Δ : S.Con j}(σ : S.Sub Γ Δ) → SET.Sub (Con Γ) (Con Δ)
Sub σ = ∣ σ ∣s

Tm : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j}(t : S.Tm Γ A) → SET.Tm (Con Γ) (Ty A)
Tm t = ∣ t ∣t

id : ∀{i}{Γ : S.Con i} → Sub (S.id {Γ = Γ}) l.≡ SET.id
id = l.refl

∘ :  ∀{i}{Θ : S.Con i}{j}{Δ : S.Con j}{σ : S.Sub Θ Δ}{k}{Γ : S.Con k}{δ : S.Sub Γ Θ} →
  Sub (σ S.∘ δ) l.≡ Sub σ SET.∘ Sub δ
∘ = l.refl

[]T : ∀{i}{Δ : S.Con i}{j}{A : S.Ty Δ j}{k}{Γ : S.Con k}{σ : S.Sub Γ Δ} →
  Ty (A S.[ σ ]T) l.≡ Ty A SET.[ Sub σ ]T
[]T = l.refl

[]t : ∀{i}{Δ : S.Con i}{j}{A : S.Ty Δ j}{t : S.Tm Δ A}{k}{Γ : S.Con k}{σ : S.Sub Γ Δ} →
  Tm (t S.[ σ ]t) l.≡ Tm t SET.[ Sub σ ]t
[]t = l.refl

∙ : Con S.∙ l.≡ SET.∙
∙ = l.refl

ε : ∀{i}{Γ : S.Con i} → Sub (S.ε {Γ = Γ}) l.≡ SET.ε
ε = l.refl

▷ : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j} → Con (Γ S.▷ A) l.≡ Con Γ SET.▷ Ty A
▷ = l.refl

-- TODO
-- , : ∀{i}{Γ : S.Con i}{j}{Δ : S.Con j}{σ : S.Sub Γ Δ}{k}{A : S.Ty Δ k}{t : S.Tm Γ (A S.[ σ ]T)} →
--   Sub (σ S., t) l.≡ Sub σ SET., Tm t
-- , = l.refl

p : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j} → Sub (S.p {A = A}) l.≡ SET.p
p = l.refl

q : ∀{i}{Γ : S.Con i}{j}{A : S.Ty Γ j} → Tm (S.q {A = A}) l.≡ SET.q
q = l.refl

-- TODO: lot more components
