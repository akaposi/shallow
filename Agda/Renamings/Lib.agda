{-# OPTIONS --without-K #-}

module Renamings.Lib where

open import Agda.Primitive

-- wrapper

record Wrap {i}(A : Set i) : Set i where
  constructor mk
  field
    ∣_∣ : A
open Wrap public

record Wrap' {i}(A : Set i) : Set i where
  constructor mk
  field
    ∣_∣ : A
open Wrap' public
