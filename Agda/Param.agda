{-# OPTIONS --no-pattern-matching #-}

module Param where

open import Agda.Primitive
import Lib as m
import WrappedStandard as S

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixr 6 _⇒_
infixl 7 _$_
infixl 6 _,Σ_

-- substitution calculus

Con : ∀ i → S.Con i → Set (lsuc i)
Con i Γˢ = S.Ty Γˢ i

Ty  : ∀ {j}{Γˢ : S.Con j}(Γ : Con j Γˢ) i (Aˢ : S.Ty Γˢ i) → Set (lsuc i ⊔ j)
Ty {j}{Γˢ} Γ i Aˢ = S.Ty (Γˢ S.▷ Γ S.▷ Aˢ S.[ S.p ]T) i

Sub   : ∀ {i}{Γˢ : S.Con i}(Γ : Con i Γˢ)
          {j}{Δˢ : S.Con j}(Δ : Con j Δˢ)
          → S.Sub Γˢ Δˢ → Set (i ⊔ j)
Sub {i}{Γˢ} Γ {j}{Δˢ} Δ σˢ = S.Tm (Γˢ S.▷ Γ) (Δ S.[ σˢ S.∘ S.p ]T)

Tm  : ∀ {i}{Γˢ : S.Con i}(Γ : Con i Γˢ)
          {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ j Aˢ) → S.Tm Γˢ Aˢ → Set (i ⊔ j)
Tm {i}{Γˢ} Γ {j}{Aˢ} A tˢ = S.Tm (Γˢ S.▷ Γ) (A S.[ S.id S., tˢ S.[ S.p ]t ]T)

id : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ} → Sub Γ Γ S.id
id = S.v⁰

_∘_ :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con _ Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con _ Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} (σ : Sub Θ Δ σˢ)
   {k}{Γˢ : S.Con k}     {Γ : Con _ Γˢ}
      {δˢ : S.Sub Γˢ Θˢ} (δ : Sub Γ Θ δˢ)
  → Sub Γ Δ (σˢ S.∘ δˢ)
_∘_ {i}{Θˢ}{Θ}{j}{Δˢ}{Δ}{σˢ} σ {k}{Γˢ}{Γ}{δˢ} δ = σ S.[ ( δˢ S.∘ S.p S., δ ) ]t

ass :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con _ Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con _ Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
   {k}{Ξˢ : S.Con k}     {Ξ : Con _ Ξˢ}
      {δˢ : S.Sub Ξˢ Θˢ} {δ : Sub Ξ Θ δˢ}
   {l}{Γˢ : S.Con l}     {Γ : Con _ Γˢ}
      {νˢ : S.Sub Γˢ Ξˢ} {ν : Sub Γ Ξ νˢ}
   → _∘_ {Θ = Ξ}{Δ = Δ}(_∘_ {Θ = Θ}{Δ = Δ} σ δ) ν m.≡
     _∘_ {Θ = Θ}{Δ = Δ} σ (_∘_ {Θ = Ξ}{Δ = Θ} δ ν)
ass = m.refl

idl :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
  → _∘_ {Θ = Δ}{Δ = Δ} id σ m.≡ σ
idl = m.refl

idr :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
  → _∘_ {Θ = Γ}{Δ = Δ} σ id m.≡ σ
idr = m.refl

_[_]T :
  ∀ {i}{Δˢ : S.Con i}   {Δ : Con i Δˢ}
    {j}{Aˢ : S.Ty Δˢ j} (A : Ty Δ j Aˢ)
    {k}{Γˢ : S.Con k}   {Γ : Con k Γˢ}
    {σˢ : S.Sub Γˢ Δˢ}  (σ : Sub Γ Δ σˢ)
    → Ty Γ j (Aˢ S.[ σˢ ]T)
_[_]T {i}{Δˢ}{Δ}{j}{Aˢ} A {k}{Γˢ}{Γ}{σˢ} σ =
  A S.[ (σˢ S.∘ S.p S., σ) S.^ _ ]T

_[_]t :
  ∀{i}{Δˢ : S.Con i}     {Δ : Con _ Δˢ}
   {j}{Aˢ : S.Ty Δˢ j}   {A : Ty Δ _ Aˢ}
      {tˢ : S.Tm Δˢ Aˢ}  (t : Tm Δ A tˢ)
   {k}{Γˢ : S.Con k}     {Γ : Con _ Γˢ}
      {σˢ : S.Sub Γˢ Δˢ} (σ : Sub Γ Δ σˢ)
  → Tm Γ (A [ σ ]T) (tˢ S.[ σˢ ]t)
_[_]t t {σˢ = σˢ} σ = t S.[ σˢ S.∘ S.p S., σ ]t

[id]T :
  ∀{i}{Γˢ : S.Con i}   {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j} {A : Ty Γ _ Aˢ}
  → A [ id ]T m.≡ A
[id]T = m.refl

[∘]T :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con _ Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con _ Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
   {k}{Γˢ : S.Con k}     {Γ : Con _ Γˢ}
      {δˢ : S.Sub Γˢ Θˢ} {δ : Sub Γ Θ δˢ}
   {l}{Aˢ : S.Ty Δˢ l}   {A : Ty Δ _ Aˢ}
   → A [ _∘_ {Δ = Δ} σ δ ]T m.≡ A [ σ ]T [ δ ]T
[∘]T = m.refl

[id]t :
  ∀{i}{Γˢ : S.Con i}   {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j} {A : Ty Γ _ Aˢ}
   {tˢ : S.Tm Γˢ Aˢ}{t : Tm Γ A tˢ}
  → _[_]t {A = A} t id m.≡ t
[id]t = m.refl

[∘]t :
  ∀{i}{Θˢ : S.Con i}     {Θ : Con _ Θˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con _ Δˢ}
      {σˢ : S.Sub Θˢ Δˢ} {σ : Sub Θ Δ σˢ}
   {k}{Γˢ : S.Con k}     {Γ : Con _ Γˢ}
      {δˢ : S.Sub Γˢ Θˢ} {δ : Sub Γ Θ δˢ}
   {l}{Aˢ : S.Ty Δˢ l}   {A : Ty Δ _ Aˢ}
   {tˢ : S.Tm Δˢ Aˢ}{t : Tm Δ A tˢ}
   → _[_]t {A = A} t (_∘_ {Δ = Δ} σ δ) m.≡ _[_]t {A = A [ σ ]T} (_[_]t {A = A} t σ) δ
[∘]t = m.refl

∙ : Con lzero S.∙
∙ = S.⊤

ε : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
    → Sub Γ ∙ S.ε
ε = S.tt

•η :
   ∀  {i}{Γˢ : S.Con i}      {Γ : Con _ Γˢ}
         {σˢ : S.Sub Γˢ S.∙} {σ : Sub Γ ∙ σˢ}
   → σ m.≡ ε
•η = m.refl

_▷_   : ∀ {i}{Γˢ : S.Con i}(Γ : Con i Γˢ)
          {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ j Aˢ)
          → Con (i ⊔ j) (Γˢ S.▷ Aˢ)
_▷_ {i} Γ {j} {Aˢ} A =
  S.Σ (Γ S.[ S.p ]T) (A S.[ S.p S.∘ S.p S., S.v⁰ S., S.v¹ ]T)

_,_ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            (σ : Sub Γ Δ σˢ)
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ _ Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} (t : Tm Γ (A [ σ ]T) tˢ)
   → Sub Γ (Δ ▷ A) (σˢ S., tˢ)
_,_ {i}{Γˢ}{Γ}{j}{Δˢ}{Δ}{σˢ} σ {k}{Aˢ}{A}{tˢ} t = σ S.,Σ t

p :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ _ Aˢ} →
   Sub (Γ ▷ A) Γ S.p
p = S.fst S.v⁰

q :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ _ Aˢ} →
   Tm (Γ ▷ A) (A [ p ]T) S.q
q = S.snd S.v⁰

▷β₁ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ _ Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (A [ σ ]T) tˢ}
  → _∘_ {Θ = Δ ▷ A}{Δ = Δ} p (_,_ σ {A = A} t) m.≡ σ
▷β₁ = m.refl

▷β₂ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ _ Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (A [ σ ]T) tˢ}
  → _[_]t {Δ = Δ ▷ A}{A = A [ p ]T} q (_,_ σ {A = A} t) m.≡ t
▷β₂ = m.refl

▷η :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ _ Aˢ}
  → _,_ p {A = A} (q {A = A}) m.≡ id
▷η = m.refl

,∘ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            {σ : Sub Γ Δ σˢ}
   {k}{Aˢ : S.Ty Δˢ k}              {A : Ty Δ _ Aˢ}
      {tˢ : S.Tm Γˢ (Aˢ S.[ σˢ ]T)} {t : Tm Γ (A [ σ ]T) tˢ}
   {l}{Θˢ : S.Con l}                {Θ : Con _ Θˢ}
      {δˢ : S.Sub Θˢ Γˢ}            {δ : Sub Θ Γ δˢ}
  → _∘_ {Θ = Γ}{Δ = Δ ▷ A}(_,_ σ {A = A} t) δ m.≡
    _,_ (_∘_ {Θ = Γ}{Δ = Δ} σ δ) {A = A} (_[_]t {A = A [ σ ]T} t δ)
,∘ = m.refl

-- abbreviations

p² :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ} →
   Sub (Γ ▷ A ▷ B) Γ S.p²
p² {Γ = Γ}{A = A} = _∘_ {Θ = Γ ▷ A}{Δ = Γ} p p

v⁰ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {k}{Aˢ : S.Ty Γˢ k}              {A : Ty Γ _ Aˢ}
  → Tm (Γ ▷ A) (A [ p ]T) S.v⁰
v⁰ {A = A} = q {A = A}

_^_ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}                {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}            (σ : Sub Γ Δ σˢ)
   {k}{Aˢ : S.Ty Δˢ k}              (A : Ty Δ k Aˢ)
  → Sub (Γ ▷ A [ σ ]T) (Δ ▷ A) (σˢ S.^ Aˢ)
_^_ {i} {Γˢ} {Γ} {j} {Δˢ} {Δ} {σˢ} σ {k} {Aˢ} A =
  _,_ (_∘_ {Θ = Γ}{Δ = Δ} σ p) {A = A} (v⁰ {A = A [ σ ]T})

-- Π

Π :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          (A : Ty Γ _ Aˢ)
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} (B : Ty (Γ ▷ A) _ Bˢ)
   → Ty Γ (j ⊔ k) (S.Π Aˢ Bˢ)
Π {Aˢ = Aˢ} A B =
  S.Π (Aˢ S.[ S.p² ]T)
      (S.Π (A S.[ S.p S.^ Aˢ S.[ S.p ]T ]T)
           (B S.[ S.p⁴ S., S.v¹ S., (S.v³ S.,Σ S.v⁰) S., (S.v² S.$ S.v¹) ]T))

lam :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {tˢ : S.Tm (Γˢ S.▷ Aˢ) Bˢ}(t : Tm (Γ ▷ A) B tˢ)
  → Tm Γ (Π A B) (S.lam tˢ)
lam t = S.lam (S.lam (t S.[ S.p³ S., S.v¹ S., (S.v² S.,Σ S.v⁰) ]t))

app :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {tˢ : S.Tm Γˢ (S.Π Aˢ Bˢ)}(t : Tm Γ (Π A B) tˢ)
  → Tm (Γ ▷ A) B (S.app tˢ)
app t = t S.[ S.p² S., S.fst S.v⁰ ]t S.$ S.v¹ S.$ S.snd S.v⁰

Πβ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {tˢ : S.Tm (Γˢ S.▷ Aˢ) Bˢ}{t : Tm (Γ ▷ A) B tˢ}
  → app {B = B} (lam {B = B} t) m.≡ t
Πβ = m.refl

Πη :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {tˢ : S.Tm Γˢ (S.Π Aˢ Bˢ)}{t : Tm Γ (Π A B) tˢ}
  → lam {B = B} (app {B = B} t) m.≡ t
Πη = m.refl

Π[] :
  ∀{i}{Γˢ : S.Con i}           {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}         {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k}{B : Ty (Γ ▷ A) _ Bˢ}
   {l}{Θˢ : S.Con l}           {Θ : Con _ Θˢ}
      {σˢ : S.Sub Θˢ Γˢ}       {σ : Sub Θ Γ σˢ}
  → Π A B [ σ ]T m.≡ Π (A [ σ ]T) (B [ σ ^ A ]T)
Π[] = m.refl

lam[] :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {l}{Δˢ : S.Con l}            {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}        {σ : Sub Γ Δ σˢ}
   {j}{Aˢ : S.Ty Δˢ j}          {A : Ty Δ _ Aˢ}
   {k}{Bˢ : S.Ty (Δˢ S.▷ Aˢ) k} {B : Ty (Δ ▷ A) _ Bˢ}
      {tˢ : S.Tm (Δˢ S.▷ Aˢ) Bˢ}{t : Tm (Δ ▷ A) B tˢ}
  → _[_]t {A = Π A B}(lam {B = B} t) σ m.≡ lam {B = B [ σ ^ A ]T} (_[_]t {A = B} t (σ ^ A))
lam[] = m.refl

-- abbreviations

_⇒_ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          (A : Ty Γ _ Aˢ)
   {k}{Bˢ : S.Ty Γˢ k}          (B : Ty Γ _ Bˢ) →
   Ty Γ (j ⊔ k) (Aˢ S.⇒ Bˢ)
A ⇒ B = Π A (B [ p ]T)

_$_ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {tˢ : S.Tm Γˢ (S.Π Aˢ Bˢ)}(t : Tm Γ (Π A B) tˢ)
      {uˢ : S.Tm Γˢ Aˢ}         (u : Tm Γ A uˢ) →
   Tm Γ (B [ _,_ id {A = A} u ]T) (tˢ S.$ uˢ)
_$_ {A = A}{B = B} t u =
  _[_]t {A = B} (app {A = A}{B = B} t) (_,_ id {A = A} u)

-- Σ

Σ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          (A : Ty Γ _ Aˢ)
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} (B : Ty (Γ ▷ A) _ Bˢ)
   → Ty Γ (j ⊔ k) (S.Σ Aˢ Bˢ)
Σ A B =
  S.Σ (A S.[ S.p S., S.fst S.v⁰ ]T)
      (B S.[ S.p³ S., S.fst S.v¹ S., (S.v² S.,Σ S.v⁰) S., S.snd S.v¹ ]T)

_,Σ_ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}(u : Tm Γ A uˢ)
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      (v : Tm Γ (_[_]T {Δ = Γ ▷ A} B (_,_ id {A = A} u)) vˢ)
   → Tm Γ (Σ A B) (uˢ S.,Σ vˢ)
u ,Σ v = u S.,Σ v

fst :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {tˢ : S.Tm Γˢ (S.Σ Aˢ Bˢ)}(t : Tm Γ (Σ A B) tˢ)
   → Tm Γ A (S.fst tˢ)
fst t = S.fst t

snd :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {tˢ : S.Tm Γˢ (S.Σ Aˢ Bˢ)}(t : Tm Γ (Σ A B) tˢ)
   → Tm Γ (_[_]T {Δ = Γ ▷ A} B (_,_ id {A = A} (fst {B = B}{tˢ} t))) (S.snd tˢ)
snd t = S.snd t

Σβ₁ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      {v : Tm Γ (_[_]T {Δ = Γ ▷ A} B (_,_ id {A = A} u)) vˢ} →
  fst {B = B}(_,Σ_ {A = A}{B = B} u v) m.≡ u
Σβ₁ = m.refl

Σβ₂ :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      {v : Tm Γ (_[_]T {Δ = Γ ▷ A} B (_,_ id {A = A} u)) vˢ} →
  snd {B = B}(_,Σ_ {A = A}{B = B} u v) m.≡ v
Σβ₂ = m.refl

Ση :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {tˢ : S.Tm Γˢ (S.Σ Aˢ Bˢ)}{t : Tm Γ (Σ A B) tˢ} →
  (_,Σ_ {B = B} (fst {B = B} t) (snd {B = B} t)) m.≡ t
Ση = m.refl

Σ[] :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
   {l}{Θˢ : S.Con l}            {Θ : Con _ Θˢ}
      {σˢ : S.Sub Θˢ Γˢ}        {σ : Sub Θ Γ σˢ} →
  Σ A B [ σ ]T m.≡ Σ (A [ σ ]T) (B [ σ ^ A ]T)
Σ[] = m.refl

,Σ[] :
  ∀{i}{Γˢ : S.Con i}            {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}          {A : Ty Γ _ Aˢ}
   {k}{Bˢ : S.Ty (Γˢ S.▷ Aˢ) k} {B : Ty (Γ ▷ A) _ Bˢ}
      {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
      {vˢ : S.Tm Γˢ (Bˢ S.[ S.id S., uˢ ]T)}
      {v : Tm Γ (_[_]T {Δ = Γ ▷ A} B (_,_ id {A = A} u)) vˢ}
   {l}{Θˢ : S.Con l}            {Θ : Con _ Θˢ}
      {σˢ : S.Sub Θˢ Γˢ}        {σ : Sub Θ Γ σˢ} →
  _[_]t {A = Σ A B} (_,Σ_ {B = B} u v) σ m.≡
  _,Σ_ {B = B [ σ ^ A ]T}
       (_[_]t {A = A} u σ)
       (_[_]t {A = B [ _,_ id {A = A} u ]T} v σ)
,Σ[] = m.refl

-- unit

⊤ : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ} → Ty Γ lzero S.⊤
⊤ = S.⊤

tt : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ} → Tm Γ ⊤ S.tt
tt = S.tt

⊤η : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ}{tˢ : S.Tm Γˢ S.⊤}{t : Tm Γ ⊤ tˢ} → t m.≡ tt
⊤η = m.refl

⊤[] : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ}{j}{Δˢ : S.Con j}{Δ : Con j Δˢ}{σˢ : S.Sub Γˢ Δˢ}{σ : Sub Γ Δ σˢ} →
  ⊤ {Γ = Δ} [ σ ]T m.≡ ⊤
⊤[] = m.refl

tt[] : ∀{i}{Γˢ : S.Con i}{Γ : Con i Γˢ}{j}{Δˢ : S.Con j}{Δ : Con j Δˢ}{σˢ : S.Sub Γˢ Δˢ}{σ : Sub Γ Δ σˢ} →
  _[_]t {A = ⊤} (tt {Γ = Δ}) σ m.≡ tt
tt[] = m.refl

-- U

U :
  ∀{i}{Γˢ : S.Con i}{Γ : Con _ Γˢ} j → Ty Γ (lsuc j) (S.U j)
U j = S.Π (S.El S.v⁰) (S.U j)

El :
  ∀{i}{Γˢ : S.Con i}         {Γ : Con _ Γˢ}
   {j}{aˢ : S.Tm Γˢ (S.U j)} (a : Tm Γ (U j) aˢ)
   → Ty Γ j (S.El aˢ)
El a = S.El (S.app a)

c :
  ∀{i}{Γˢ : S.Con i}  {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ _ Aˢ)
  → Tm Γ (U j) (S.c Aˢ)
c A = S.lam (S.c A)

Uβ :
  ∀{i}{Γˢ : S.Con i}   {Γ : Con _ Γˢ}
   {j}{Aˢ : S.Ty Γˢ j} {A : Ty Γ _ Aˢ}
   → El (c A) m.≡ A
Uβ = m.refl

Uη :
  ∀{i}{Γˢ : S.Con i} {Γ : Con _ Γˢ}
   {j}{aˢ : S.Tm Γˢ (S.U j)}{a : Tm Γ (U j) aˢ}
   → c (El a) m.≡ a
Uη = m.refl

U[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   {k} → _[_]T {Δ = Δ} (U k) σ m.≡ U k
U[] = m.refl

El[] :
  ∀{i}{Γˢ : S.Con i}        {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}        {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ}    {σ : Sub Γ Δ σˢ}
   {k}{aˢ : S.Tm Δˢ (S.U k)}{a : Tm Δ (U k) aˢ}
  → El a [ σ ]T m.≡ El (_[_]t {A = U k} a σ)
El[] = m.refl

-- not Russell

-- Bool

Bool :
  ∀{i}{Γˢ : S.Con i}{Γ : Con _ Γˢ}
  → Ty Γ lzero S.Bool
Bool = S.Σ S.Bool (S.Id S.Bool S.v⁰ S.v¹)

true :
  ∀{i}{Γˢ : S.Con i}{Γ : Con _ Γˢ}
  → Tm Γ Bool S.true
true = S.true S.,Σ S.refl S.true

false :
  ∀{i}{Γˢ : S.Con i}{Γ : Con _ Γˢ}
  → Tm Γ Bool S.false
false = S.false S.,Σ S.refl S.false

if :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {j}{Cˢ : S.Ty (Γˢ S.▷ S.Bool) j} (C : Ty (Γ ▷ Bool) j Cˢ)
      {uˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.true ]T)}
      (u : Tm Γ (C [ _,_ id {A = Bool {Γ = Γ}} true ]T) uˢ)
      {vˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.false ]T)}
      (v : Tm Γ (C [ _,_ id {A = Bool {Γ = Γ}} false ]T) vˢ)
      {tˢ : S.Tm Γˢ S.Bool}
      (t : Tm Γ Bool tˢ)
  → Tm Γ (C [ _,_ id {A = Bool {Γ = Γ}} t ]T) (S.if Cˢ uˢ vˢ tˢ)
if {Cˢ = Cˢ} C {uˢ = uˢ} u {vˢ = vˢ} v t =
  S.J (C S.[ S.p³ S., S.v¹ S., (S.v² S.,Σ (S.fst t S.[ S.p² ]t S.,Σ S.v⁰)) S., S.if (Cˢ S.[ S.p⁴ S., S.v⁰ ]T) (uˢ S.[ S.p³ ]t) (vˢ S.[ S.p³ ]t) S.v¹ ]T)
      ((S.if (C S.[ S.p² S., S.v⁰ S., (S.v¹ S.,Σ (S.v⁰ S.,Σ S.refl S.v⁰)) S., S.if (Cˢ S.[ S.p³ S., S.v⁰ ]T) (uˢ S.[ S.p² ]t) (vˢ S.[ S.p² ]t) S.v⁰ ]T)
             u
             v
             (S.fst t)))
      (S.snd t)

Boolβ₁ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {j}{Cˢ : S.Ty (Γˢ S.▷ S.Bool) j} {C : Ty (Γ ▷ Bool) j Cˢ}
      {uˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.true ]T)}
         {u : Tm Γ (C [ _,_ id {A = Bool {Γ = Γ}} true ]T) uˢ}
      {vˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.false ]T)}
         {v : Tm Γ (C [ _,_ id {A = Bool {Γ = Γ}} false ]T) vˢ}
   → if C u v true m.≡ u
Boolβ₁ = m.refl

Boolβ₂ :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {j}{Cˢ : S.Ty (Γˢ S.▷ S.Bool) j} {C : Ty (Γ ▷ Bool) j Cˢ}
      {uˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.true ]T)}
         {u : Tm Γ (C [ _,_ id {A = Bool {Γ = Γ}} true ]T) uˢ}
      {vˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.false ]T)}
         {v : Tm Γ (C [ _,_ id {A = Bool {Γ = Γ}} false ]T) vˢ}
   → if C u v false m.≡ v
Boolβ₂ = m.refl

Bool[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   → _[_]T {Δ = Δ} Bool σ m.≡ Bool
Bool[] = m.refl

true[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   → _[_]t {A = Bool {Γ = Δ}} true σ m.≡ true
true[] = m.refl

false[] :
  ∀{i}{Γˢ : S.Con i}     {Γ : Con _ Γˢ}
   {j}{Δˢ : S.Con j}     {Δ : Con _ Δˢ}
      {σˢ : S.Sub Γˢ Δˢ} {σ : Sub Γ Δ σˢ}
   → _[_]t {A = Bool {Γ = Δ}} false σ m.≡ false
false[] = m.refl

if[] :
  ∀{i}{Γˢ : S.Con i}                {Γ : Con _ Γˢ}
   {j}{Cˢ : S.Ty (Γˢ S.▷ S.Bool) j} {C : Ty (Γ ▷ Bool) j Cˢ}
      {uˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.true ]T)}{u : Tm Γ (C [ _,_ id {A = Bool {Γ = Γ}} true ]T) uˢ}
      {vˢ : S.Tm Γˢ (Cˢ S.[ S.id S., S.false ]T)}{v : Tm Γ (C [ _,_ id {A = Bool {Γ = Γ}} false ]T) vˢ}
      {tˢ : S.Tm Γˢ S.Bool}{t : Tm Γ Bool tˢ}
   {k}{Θˢ : S.Con k}     {Θ : Con _ Θˢ}
      {σˢ : S.Sub Θˢ Γˢ} {σ : Sub Θ Γ σˢ} →
   _[_]t {A = _[_]T {Aˢ = Cˢ} C {σˢ = S.id S., tˢ} (_,_ {σˢ = S.id} id {A = Bool} t)}
         (if C u v t)
         σ m.≡
   if {Γˢ = Θˢ}{Γ = Θ}{Cˢ = Cˢ S.[ σˢ S.^ S.Bool ]T}
      (C [ σ ^ Bool {Γ = Γ} ]T)
      (_[_]t {A = C [ _,_ id {A = Bool {Γ = Γ}} (true {Γ = Γ}) ]T} u σ)
      (_[_]t {A = C [ _,_ id {A = Bool {Γ = Γ}} (false {Γ = Γ}) ]T} v σ)
      (_[_]t {A = Bool {Γ = Γ}} t σ)
if[] = m.refl

-- Identity

Id :
  ∀ {i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
    {j}{Aˢ : S.Ty Γˢ j}(A : Ty Γ j Aˢ)
       {uˢ : S.Tm Γˢ Aˢ}(u : Tm Γ A uˢ)
       {vˢ : S.Tm Γˢ Aˢ}(v : Tm Γ A vˢ) →
    Ty Γ j (S.Id Aˢ uˢ vˢ)
Id A u {vˢ} v =
  S.Id (A S.[ S.p S., vˢ S.[ S.p² ]t ]T)
       (S.tr (A S.[ S.p² S., S.v⁰ ]T) S.v⁰ (u S.[ S.p ]t))
       (v S.[ S.p ]t)

refl :
  ∀ {i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
    {j}{Aˢ : S.Ty Γˢ j}{A : Ty Γ j Aˢ}
       {uˢ : S.Tm Γˢ Aˢ}(u : Tm Γ A uˢ) →
    Tm Γ (Id A u u) (S.refl uˢ)
refl {uˢ = uˢ} u = S.refl u

J :
  ∀ {i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
    {j}{Aˢ : S.Ty Γˢ j}{A : Ty Γ j Aˢ}
       {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
    {k}{Cˢ : S.Ty (Γˢ S.▷ Aˢ S.▷ S.Id (Aˢ S.[ S.p ]T) (uˢ S.[ S.p ]t) S.v⁰) k}
    (C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (_[_]t {A = A} u p) v⁰) k Cˢ)
    {wˢ : S.Tm Γˢ (Cˢ S.[ S.id S., uˢ S., S.refl uˢ ]T)}
    (w : Tm Γ (C [ _,_ (_,_ id {A = A} u) {A = Id (_[_]T A {Γ = Γ ▷ A} p) (_[_]t {A = A} u p) v⁰} (refl {A = A} u) ]T) wˢ)
    {vˢ : S.Tm Γˢ Aˢ}{v : Tm Γ A vˢ}
    {tˢ : S.Tm Γˢ (S.Id Aˢ uˢ vˢ)}(t : Tm Γ (Id A u v) tˢ) →
    Tm Γ
       (C [ _,_ (_,_ id {A = A} v)
                {A = Id (_[_]T A {Γ = Γ ▷ A} p) (_[_]t {A = A} u p) v⁰}
                t ]T)
       (S.J Cˢ wˢ tˢ)
J {i}{Γˢ}{Γ}{j}{Aˢ}{A}{uˢ}{u}{k}{Cˢ} C {wˢ} w {vˢ}{v}{tˢ} t =
  S.J (C S.[ (S.id S., vˢ S., tˢ) S.∘ S.p³ S., ((S.v² S.,Σ S.v¹) S.,Σ S.v⁰) S., S.J Cˢ wˢ tˢ S.[ S.p³ ]t ]T)
      (S.J (C S.[ S.p³ S.,
                  S.v¹ S.,
                  S.v⁰ S.,
                  (S.v² S.,Σ
                   S.tr (A S.[ S.p³ S., S.v⁰ ]T) S.v⁰ (u S.[ S.p² ]t) S.,Σ
                   S.refl _) S.,
                  S.J (Cˢ S.[ S.p⁵ S., S.v¹ S., S.v⁰ ]T) (wˢ S.[ S.p³ ]t) S.v⁰ ]T)
           w
           (tˢ S.[ S.p ]t))
      t

Idβ :
  ∀ {i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
    {j}{Aˢ : S.Ty Γˢ j}{A : Ty Γ j Aˢ}
       {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
    {k}{Cˢ : S.Ty (Γˢ S.▷ Aˢ S.▷ S.Id (Aˢ S.[ S.p ]T) (uˢ S.[ S.p ]t) S.v⁰) k}
    {C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (_[_]t {A = A} u p) v⁰) k Cˢ}
    {wˢ : S.Tm Γˢ (Cˢ S.[ S.id S., uˢ S., S.refl uˢ ]T)}
    {w : Tm Γ (C [ _,_ (_,_ id {A = A} u) {A = Id (_[_]T A {Γ = Γ ▷ A} p) (_[_]t {A = A} u p) v⁰} (refl {A = A} u) ]T) wˢ} →
    J C w (refl {A = A} u) m.≡ w
Idβ = m.refl

Id[] :
  ∀ {i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
    {j}{Aˢ : S.Ty Γˢ j}{A : Ty Γ j Aˢ}
       {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
       {vˢ : S.Tm Γˢ Aˢ}{v : Tm Γ A vˢ}
   {k}{Θˢ : S.Con k}     {Θ : Con _ Θˢ}
      {σˢ : S.Sub Θˢ Γˢ} {σ : Sub Θ Γ σˢ} →
  Id A u v [ σ ]T m.≡ Id (A [ σ ]T) (_[_]t {A = A} u σ) (_[_]t {A = A} v σ)
Id[] = m.refl

refl[] :
  ∀ {i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
    {j}{Aˢ : S.Ty Γˢ j}{A : Ty Γ j Aˢ}
       {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
    {k}{Θˢ : S.Con k}     {Θ : Con _ Θˢ}
       {σˢ : S.Sub Θˢ Γˢ} {σ : Sub Θ Γ σˢ} →
  _[_]t {A = Id A u u} (refl {A = A} u) σ m.≡ refl {A = A [ σ ]T} (_[_]t {A = A} u σ)
refl[] = m.refl

J[] :
  ∀ {i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
    {j}{Aˢ : S.Ty Γˢ j}{A : Ty Γ j Aˢ}
       {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
    {k}{Cˢ : S.Ty (Γˢ S.▷ Aˢ S.▷ S.Id (Aˢ S.[ S.p ]T) (uˢ S.[ S.p ]t) S.v⁰) k}
    {C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (_[_]t {A = A} u p) v⁰) k Cˢ}
    {wˢ : S.Tm Γˢ (Cˢ S.[ S.id S., uˢ S., S.refl uˢ ]T)}
    {w : Tm Γ (C [ _,_ (_,_ id {A = A} u) {A = Id (_[_]T A {Γ = Γ ▷ A} p) (_[_]t {A = A} u p) v⁰} (refl {A = A} u) ]T) wˢ}
    {vˢ : S.Tm Γˢ Aˢ}{v : Tm Γ A vˢ}
    {tˢ : S.Tm Γˢ (S.Id Aˢ uˢ vˢ)}{t : Tm Γ (Id A u v) tˢ}
    {k}{Θˢ : S.Con k}     {Θ : Con _ Θˢ}
       {σˢ : S.Sub Θˢ Γˢ} {σ : Sub Θ Γ σˢ} →
  _[_]t {A = C [ _,_ (_,_ id {A = A} v) {A = Id (_[_]T A (p {A = A})) (_[_]t {A = A} u p) v⁰} t ]T} (J C w t) σ m.≡
   J (C [ (σ ^ A) ^ Id (A [ p ]T) (_[_]t {A = A} u (p {A = A})) (v⁰ {A = A}) ]T)
     (_[_]t {A = C [ _,_ (_,_ id {A = A} u) {A = Id (_[_]T A {Γ = Γ ▷ A} p) (_[_]t {A = A} u p) v⁰} (refl {A = A} u) ]T} w σ)
     (_[_]t {A = Id A u v} t σ)
J[] = m.refl

-- abbreviations

tr :
  ∀ {i}{Γˢ : S.Con i}{Γ : Con i Γˢ}
    {j}{Aˢ : S.Ty Γˢ j}{A : Ty Γ j Aˢ}
       {uˢ : S.Tm Γˢ Aˢ}{u : Tm Γ A uˢ}
    {k}{Cˢ : S.Ty (Γˢ S.▷ Aˢ) k}(C : Ty (Γ ▷ A) k Cˢ)
    {vˢ : S.Tm Γˢ Aˢ}{v : Tm Γ A vˢ}
    {tˢ : S.Tm Γˢ (S.Id Aˢ uˢ vˢ)}(t : Tm Γ (Id A u v) tˢ) →
    {wˢ : S.Tm Γˢ (Cˢ S.[ S.id S., uˢ ]T)}
    (w : Tm Γ (C [ _,_ id {A = A} u ]T) wˢ) →
  Tm Γ (C [ _,_ id {A = A} v ]T) (S.tr Cˢ tˢ wˢ)
tr C t w = J (C [ p ]T) w t
