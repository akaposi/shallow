{-# OPTIONS --rewriting #-}

module ModelTemplate where

open import Agda.Primitive
open import Lib using (_≡_)
import Lib as l

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_

{-# BUILTIN REWRITE l._≡_ #-}

postulate

  Con : (i : Level) → Set (lsuc i)

  Ty : ∀{i}(Γ : Con i)(j : Level) → Set (i ⊔ lsuc j)

  Sub : ∀{i}(Γ : Con i){j}(Δ : Con j) → Set (i ⊔ j)

  Tm : ∀{i}(Γ : Con i){j}(A : Ty Γ j) → Set (i ⊔ j)

  id : ∀{i}{Γ : Con i} → Sub Γ Γ

  _∘_ : ∀{i}{Θ : Con i}{j}{Δ : Con j}(σ : Sub Θ Δ){k}{Γ : Con k}(δ : Sub Γ Θ) →
    Sub Γ Δ

  ass : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Ξ : Con k}{δ : Sub Ξ Θ}{l}{Γ : Con l}{ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν l.≡ σ ∘ (δ ∘ ν)

  idl : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ l.≡ σ

  idr : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id l.≡ σ

  _[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j

  _[_]t : ∀{i}{Δ : Con i}{j}{A : Ty Δ j}(t : Tm Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)

  [id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T l.≡ A

  [∘]T : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
    {l}{A : Ty Δ l} → A [ σ ]T [ δ ]T l.≡ A [ σ ∘ δ ]T
{-# REWRITE [id]T [∘]T ass idl idr #-}

postulate
  [id]t : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{t : Tm Γ A} → t [ id ]t l.≡ t
  [∘]t : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
    {l}{A : Ty Δ l}{t : Tm Δ A} → t [ σ ]t [ δ ]t l.≡ t [ σ ∘ δ ]t
  ∙ : Con lzero
  ε : ∀{i}{Γ : Con i} → Sub Γ ∙
  ∙η : ∀{i}{Γ : Con i}{σ : Sub Γ ∙} → σ l.≡ ε
  _▷_   : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j)
  _,_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : Ty Δ k}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▷ A)
  p : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Sub (Γ ▷ A) Γ
  q : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
  ▷β₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → p ∘ (_,_ σ {A = A} t) l.≡ σ
{-# REWRITE [id]t [∘]t ▷β₁  #-}

postulate
  ▷β₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → q [ _,_ σ {A = A} t ]t l.≡ t
  ▷η : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → (p {A = A} , q {A = A}) l.≡ id
  ,∘ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)}{l}{θ : Con l}{δ : Sub θ Γ} →
    (_,_ σ {A = A} t) ∘ δ l.≡ (σ ∘ δ) , (t [ δ ]t)
{-# REWRITE ▷β₂ ,∘ ▷η #-}

-- abbreviations
p² :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k} →
   Sub (Γ ▷ A ▷ B) Γ
p² = p ∘ p

p³ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l} →
   Sub (Γ ▷ A ▷ B ▷ C) Γ
p³ = p ∘ p ∘ p

p⁴ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D) Γ
p⁴ = p ∘ p ∘ p ∘ p

p⁵ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) Γ
p⁵ = p ∘ p ∘ p ∘ p ∘ p

v⁰ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
v⁰ = q

v¹ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k} →
   Tm (Γ ▷ A ▷ B) (A [ p² ]T)
v¹ = v⁰ [ p ]t

v² :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l} →
   Tm (Γ ▷ A ▷ B ▷ C) (A [ p³ ]T)
v² = v⁰ [ p² ]t

v³ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
   Tm (Γ ▷ A ▷ B ▷ C ▷ D) (A [ p⁴ ]T)
v³ = v⁰ [ p³ ]t

_^_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}(A : Ty Δ k) →
  Sub (Γ ▷ A [ σ ]T) (Δ ▷ A)
_^_ {i}{Γ}{j}{Δ} σ {k} A = σ ∘ p , v⁰ {i}{Γ}{_}{A [ σ ]T}

-- Identity
postulate
  Id : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(u v : Tm Γ A) → Ty Γ j
  Id[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u v : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
    Id A u v [ σ ]T l.≡ Id (A [ σ ]T) (u [ σ ]t) (v [ σ ]t)
{-# REWRITE Id[] #-}

postulate
  refl : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}(u : Tm Γ A) → Tm Γ (Id A u u)
  refl[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
    refl u [ σ ]t l.≡ refl (u [ σ ]t)

  J :
    ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
     {k}(C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k)
     (w : Tm Γ (C [ id , u , refl u ]T))
     {v : Tm Γ A}(t : Tm Γ (Id A u v)) → Tm Γ (C [ id , v , t ]T)

  Idβ :
    ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
     {k}{C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k}
     {w : Tm Γ (C [ id , u , refl u ]T)} →
     J C w (refl u) l.≡ w
{-# REWRITE refl[] Idβ #-}

-- postulate
--   J[] :
--     ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
--      {k}{C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k}
--      {w : Tm Γ (C [ id , u , refl u ]T)}
--      {v : Tm Γ A}{t : Tm Γ (Id A u v)}{l}{θ : Con l}{σ : Sub θ Γ} →
--      l.coe {!!}
--        (J C w t [ σ ]t)
--      ≡ J {l}{θ}{j}{A [ σ ]T}{u [ σ ]t}{k}
--          (C [ σ ^ A ^ Id (A [ p ]T) (u [ p ]t) v⁰ ]T)
--          {!!}
--          {!!}
