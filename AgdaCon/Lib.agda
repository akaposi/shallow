{-# OPTIONS --without-K #-}

module Lib where

open import Agda.Primitive

infix 4 _≡_ _≡ω_
infix 5 _⁻¹

data _≡_ {ℓ}{A : Set ℓ} (x : A) : A → Set ℓ where
  refl : x ≡ x

data _≡ω_ {A : Setω} (x : A) : A → Setω where
  refl : x ≡ω x

transpω : {A : Setω}(P : A → Setω){a₀ a₁ : A} → a₀ ≡ω a₁ → P a₀ → P a₁
transpω P refl p = p

_⁻¹ : {A : Setω}{x y : A} → x ≡ω y → y ≡ω x
refl ⁻¹ = refl
