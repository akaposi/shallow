{-# OPTIONS --no-pattern-matching --without-K #-}

module Tel.Lib where

open import Agda.Primitive

infixl 5 _,_

record ⊤ : Setω where
  constructor tt

record Σ (A : Setω)(B : A → Setω) : Setω where
  constructor _,_
  field
    fst : A
    snd : B fst
open Σ public
