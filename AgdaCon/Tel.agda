{-# OPTIONS --without-K #-}

module Tel where

-- Telescopes

open import Agda.Primitive
import Lib as l
import Tel.Lib as m
open import Standard

infixl 5 _▸_
infixl 4 _++_
infixl 7 _[_]Ts
infixl 5 _,,_
infixl 8 _[_]ts

data Tys (Γ : Con) : Setω
_++_ : (Γ : Con)(Ω : Tys Γ) → Con

data Tys Γ where
  ◆   : Tys Γ                               -- \di
  _▸_ : (Ω : Tys Γ) → Ty (Γ ++ Ω) i → Tys Γ -- \t5

variable Ω : Tys Γ

Γ ++ ◆       = Γ
Γ ++ (Ω ▸ A) = (Γ ++ Ω) ▷ A

ππ₁ : Sub Δ (Γ ++ Ω) → Sub Δ Γ
ππ₁ {Ω = ◆} γ = γ
ππ₁ {Ω = Ω ▸ A} γω = ππ₁ {Ω = Ω} (π₁ γω)

Tms : (Γ : Con) → Tys Γ → Setω
_[_]Ts : Tys Γ → Sub Δ Γ → Tys Δ
[id]Ts : Ω [ id ]Ts l.≡ω Ω
[∘]Ts : Ω [ γ ∘ δ ]Ts l.≡ω Ω [ γ ]Ts [ δ ]Ts
_,,_ : (γ : Sub Δ Γ) → Tms Δ (Ω [ γ ]Ts) → Sub Δ (Γ ++ Ω)
ππ₂ : (γω : Sub Δ (Γ ++ Ω)) → Tms Δ (Ω [ ππ₁ γω ]Ts)

Tms Γ ◆ = m.⊤
Tms Γ (Ω ▸ A) = m.Σ (Tms Γ Ω) λ ω → Tm Γ (A [ _,,_ {Ω = Ω} id (l.transpω (Tms Γ) ([id]Ts l.⁻¹) ω) ]T)

◆ [ γ ]Ts = ◆
(Ω ▸ A) [ γ ]Ts = Ω [ γ ]Ts ▸ A [ γ ∘ ππ₁ id ,, l.transpω (Tms (_ ++ Ω [ γ ]Ts)) ([∘]Ts l.⁻¹) (ππ₂ {Ω = Ω [ γ ]Ts} id) ]T

_,,_ {Ω = ◆}     γ _    = γ
_,,_ {Ω = Ω ▸ A} γ (ω m., a) = _,,_ {Ω = Ω} γ ω , {!a!}

[id]Ts = {!!}

[∘]Ts = {!!}

ππ₂ = {!!}
