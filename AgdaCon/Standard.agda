{-# OPTIONS --no-pattern-matching --without-K #-}

module Standard where

-- The standard model.

open import Agda.Primitive
import Lib as l
import Standard.Lib as m

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_

-- category

Con = Setω

Sub : Con → Con → Setω
Sub Δ Γ = Δ → Γ

variable
  Γ Δ Θ Ξ : Con
  γ δ θ ξ γa : Sub Δ Γ

_∘_ : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
γ ∘ δ = λ g → γ (δ g)

id : Sub Γ Γ
id = λ g → g

ass : (γ ∘ δ) ∘ θ l.≡ω γ ∘ (δ ∘ θ)
ass = l.refl

idl : id ∘ γ l.≡ω γ
idl = l.refl

idr : γ ∘ id l.≡ω γ
idr = l.refl

-- terminal object

∙ : Con
∙ = m.⊤

ε : Sub Γ ∙
ε = λ _ → m.tt

∙η : γ l.≡ω ε
∙η = l.refl

-- families

variable i j k : Level

Ty : Con → (i : Level) → Setω
Ty Γ i = Γ → Set i

variable A B C : Ty Γ i

_[_]T : Ty Γ i → Sub Δ Γ → Ty Δ i
A [ γ ]T = λ g → A (γ g)

[∘]T : A [ γ ∘ δ ]T l.≡ω A [ γ ]T [ δ ]T
[∘]T = l.refl

[id]T : A [ id ]T l.≡ω A
[id]T = l.refl

Tm : (Γ : Con) → Ty Γ i → Setω
Tm Γ A = (g : Γ) → A g

variable a b c : Tm Γ A

_[_]t : Tm Γ A → (γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)
a [ γ ]t = λ g → a (γ g)

[∘]t : a [ γ ∘ δ ]t l.≡ω a [ γ ]t [ δ ]t
[∘]t = l.refl

[id]t : a [ id ]t l.≡ω a
[id]t = l.refl

-- Tm is locally representable

_▷_ : (Γ : Con) → Ty Γ i → Con
Γ ▷ A = m.Σ Γ A

π₁ : Sub Δ (Γ ▷ A) → Sub Δ Γ
π₁ γa = λ d → m.fst (γa d)

π₂ : (γa : Sub Δ (Γ ▷ A)) → Tm Δ (A [ π₁ γa ]T)
π₂ γa = λ d → m.snd (γa d)

_,_ : (γ : Sub Δ Γ) → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▷ A)
γ , a = λ d → γ d m., a d

▷β₁ : π₁ (γ , a) l.≡ω γ
▷β₁ = l.refl

▷β₂ : π₂ (γ , a) l.≡ω a
▷β₂ = l.refl

▷η  : π₁ γa , π₂ γa l.≡ω γa
▷η = l.refl

,∘  : (γ , a) ∘ δ l.≡ω γ ∘ δ , a [ δ ]t
,∘ = l.refl
