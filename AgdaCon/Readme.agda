{-# OPTIONS --no-pattern-matching --without-K #-}

module Readme where

-- in this folder we use the same shallow embedding trick to define
-- the syntax as a strict model, but we do not index contexts by
-- universe levels (Con lives in Setω)

open import Standard   -- the standard model
