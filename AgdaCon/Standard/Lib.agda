{-# OPTIONS --no-pattern-matching --without-K #-}

module Standard.Lib where

open import Agda.Primitive

infixl 5 _,_

record ⊤ : Setω where
  constructor tt

record Σ (A : Setω){i}(B : A → Set i) : Setω where
  constructor _,_
  field
    fst : A
    snd : B fst
open Σ public
