{-# OPTIONS --no-pattern-matching --without-K #-}

module Param where

-- The standard model.

open import Agda.Primitive
import Lib as l
import Standard.Lib as m
open import Standard

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_

record Con∙ (Γ : Con) : Setω₁ where
  field
    PΓ : Con
    pΓ : Sub PΓ Γ
open Con∙ renaming (PΓ to P_; pΓ to p_)

record Sub∙ {Δ}(Δ∙ : Con∙ Δ){Γ}(Γ∙ : Con∙ Γ)(γ : Sub Δ Γ) : Setω₁ where
  field
    Pγ : Sub (P Δ∙) (P Γ∙)
    pγ : ∀{Θ}(x : Sub Θ (P Δ∙))(y : Sub Θ Δ) → p Δ∙ ∘ x l.≡ω y → p Γ∙ ∘ Pγ ∘ x l.≡ω γ ∘ y
open Sub∙ renaming (Pγ to P_; pγ to p_)

_∘∙_ : ∀{Γ}{Γ∙ : Con∙ Γ}{Δ}{Δ∙ : Con∙ Δ}{γ} → Sub∙ Δ∙ Γ∙ γ → ∀{Θ}{Θ∙ : Con∙ Θ}{δ} → Sub∙ Θ∙ Δ∙ δ → Sub∙ Θ∙ Γ∙ (γ ∘ δ)
γ∙ ∘∙ δ∙ = record { Pγ = P γ∙ ∘ P δ∙ ; pγ = λ x y e → (p γ∙) _ _ ((p δ∙) x y e) }

Ty∙ : ∀{Γ}(Γ∙ : Con∙ Γ) i (A : Ty Γ i) → Setω
Ty∙ Γ∙ i A = Ty (P Γ∙ ▷ A [ p Γ∙ ]T) i

_^_ : (γ : Sub Δ Γ) → (A ≡ B [ γ ]) → Sub (Δ ▷ A) (Γ ▷ B)


_[_]∙ : ∀{Γ}{Γ∙ : Con∙ Γ}{i}{A}(A∙ : Ty∙ Γ∙ i A){Δ}{Δ∙ : Con∙ Δ}{γ : Sub Δ Γ}(γ∙ : Sub∙ Δ∙ Γ∙ γ) → Ty∙ Δ∙ i (A [ γ ]T)
A∙ [ γ∙ ]∙ = A∙ [ {!P γ∙!} ]T
