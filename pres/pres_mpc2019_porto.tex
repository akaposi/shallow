\documentclass{beamer}
\usetheme{default}
\useoutertheme{infolines}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{tikz}
\usepackage[utf8]{inputenc}
\usepackage{pmboxdraw}
\usepackage{graphicx} % for \scalebox
\usepackage{scrextend}
\usepackage{setspace} % for \setstretch
\usepackage{stmaryrd} % for \llbracket, \llbracket
\changefontsizes{12pt}

\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\setbeamerfont{itemize/enumerate subsubbody}{size=\normalsize}

\input{abbrevs.tex}

\begin{document}

%---------------------------------------------------------------------
{
\logo{\hspace{4em}\includegraphics[width=26em]{efop.jpg}}
%---------------------------------------------------------------------

\begin{frame}
  \vspace{0.5cm}
  \begin{center}

    \textcolor{blue}{\Large{
        Shallow embedding of type theory \\
        is morally correct}
    }
    
    \vspace{0.7cm}

    Ambrus Kaposi \\
    \vspace{0.2cm}

    E{\"o}tv{\"o}s Lor{\'a}nd University, Budapest \\
    \vspace{0.2cm}

    j.w.w.\ Andr{\'a}s Kov{\'acs} and Nicolai Kraus \\
    \vspace{0.7cm}

    MPC 2019 \\
    \vspace{0.2cm}

    Porto, 8 October 2019
    
  \end{center}
\end{frame}
}

% --------------------------------------------------------------------

\begin{frame}{Overview}
\tableofcontents
\end{frame}

\AtBeginSection[]
  {
     \begin{frame}<beamer>
     \frametitle{Plan}
     \tableofcontents[currentsection,currentsubsection]
     \end{frame}
  }

% --------------------------------------------------------------------

\section{Shallow embedding of monoids}

%--------------------------------------------------------------------

\begin{frame}{Deep embedding of monoids}
  Assume there is a monoid:
  \begin{alignat*}{5}
    & \s M && : \Set \\
    & \s u && : \s M \\
    & \blank\otimes\blank && : \s M \ra \s M \ra \s M \\
    & \s{ass} && : (a\,b\,c :\s M)\ra (a\otimes b)\otimes c \equiv a\otimes(b\otimes c) \\
    & \s{idl} && : (a:\s M)\ra \s u\otimes a \equiv a \\
    & \s{idr} && : (a:\s M)\ra a\otimes \s u \equiv a
  \end{alignat*}
  A theorem of monoids:
  \begin{alignat*}{5}
    & \s{thm} : (a:\s M)\ra a\otimes(\s u\otimes \s u) \equiv a \\
    & \s{thm} := \lambda a.\s{trans}\,(\s{ap}\,(a\otimes\blank)\,(\s{idl}\,\s u))\,(\s{idr}\,a)
  \end{alignat*}
%  Notion of homomorphism from $(\N,0,+)$ to $\s M$:
%  \[
%    (f : \N\ra\s M)\times(f\,0 \equiv \s u)\times((x\,y:\N)\ra f\,(x+y)\equiv f\,x\otimes f\,y)
%  \]
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{A shallow embedding of monoids (i)}
  Let's work with a concrete monoid:
  \begin{alignat*}{5}
    & \s M && := \top\ra\top \\
    & \s u && := \lambda x.x \\
    & a\otimes b && := \lambda x.a\,(b\,x) \\
    & \s{ass}\,a\,b\,c && : \underbrace{(a\otimes b)\otimes c}_{= \lambda x.a\,(b\,(c\,x))} \equiv \underbrace{a\otimes(b\otimes c)}_{= \lambda x.a\,(b\,(c\,x))} \\
    & \s{ass}\,a\,b\,c && := \refl_{\lambda x.a\,(b\,(c\,x))} \\
    & \s{idl}\,a && := \refl_a \\
    & \s{idr}\,a && := \refl_a
  \end{alignat*}
  Theorem:
  \begin{alignat*}{5}
    & \s{thm} : (a:\s M)\ra a\otimes(\s u\otimes \s u) \equiv a \\
    & \s{thm} := \lambda a.\refl_a
  \end{alignat*}
  \pause
  Problem: there are too few elements, i.e.\ $\forall a. a \equiv \s u$.\vphantom{y}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{A shallow embedding of monoids (ii)}
  Another concrete monoid:
  \begin{alignat*}{5}
    & \s M && := \Bool\ra\Bool \\
    & \s u && := \lambda x.x \\
    & a\otimes b && := \lambda x.a\,(b\,x) \\
    & \s{ass}\,a\,b\,c && : \underbrace{(a\otimes b)\otimes c}_{= \lambda x.a\,(b\,(c\,x))} \equiv \underbrace{a\otimes(b\otimes c)}_{= \lambda x.a\,(b\,(c\,x))} \\
    & \s{ass}\,a\,b\,c && := \refl_{\lambda x.a\,(b\,(c\,x))} \\
    & \s{idl}\,a && := \refl_a \\
    & \s{idr}\,a && := \refl_a
  \end{alignat*}
  Theorem:
  \begin{alignat*}{5}
    & \s{thm} : (a:\s M)\ra a\otimes(\s u\otimes \s u) \equiv a \\
    & \s{thm} := \lambda a.\refl_a
  \end{alignat*}
  \pause
  Problem: there are too many elements, e.g.\ $(\lambda a.\s{true})\not\equiv\s{u}$.
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Implementation hiding}

  \vspace{-1em}
  {\footnotesize
  \begin{alignat*}{5}
    & \text{Secret.agda:}\hspace{2em} && \rlap{$\s{module\ Secret\ where}$} \\
    & && \hspace{1.5em} && \s{record\ M} && := \s{mkM}\,\{ \s{unM} : \Bool\ra\Bool \} \\
%    & && && \s u && : \s M \\
    & && && \s u && := \s{mkM}\,(\lambda x.x) \\
%    & && && \blank\otimes\blank && : \s M\ra\s M\ra\s M \\
    & && && a\otimes b && := \s{mkM}\,(\lambda x.\s{unM}\,a\,(\s{unM}\,b\,x)) \\
    & \text{Monoid.agda: } && \rlap{$\s{module\ Monoid\ where}$} \\
    & && && \rlap{$\s{import\ Secret}$} \\
    & && && \s M && := \s{Secret}.\s{M} \\
    & && && \s u && := \s{Secret}.\s{u} \\
    & && && \blank\otimes\blank && := \s{Secret}.{\blank\otimes\blank} \\
    & && && \s{ass}\,a\,b\,c && := \refl \\
    & && && \s{idl}\,a && := \refl \\
    & && && \s{idr}\,a && := \refl \\
    & \text{Usage.agda: } && \rlap{$\s{module\ Usage\ where}$} \\
    & && && \rlap{$\s{open\ import\ Monoid}$}
  \end{alignat*}
  }
\end{frame}

% ---------------------------------------------------------------------

\begin{frame}{Check how this works for associativity}

  \begin{alignat*}{5}
    & \rlap{$a\otimes b := \s{mkM}\,(\lambda x.\s{unM}\,a\,(\s{unM}\,b\,x))$} \\
    & \\
    & && (a\otimes b)\otimes c = \\
    & && \s{mkM}\,(\lambda x.\s{unM}\,(a\otimes b)\,(\s{unM}\,c\,x)) =\\
    & && \s{mkM}\,(\lambda x.\s{unM}\,(\s{mkM}\,(\lambda x.\s{unM}\,a\,(\s{unM}\,b\,x))\,(\s{unM}\,c\,x))) = \\
    & && \s{mkM}\,(\lambda x.(\lambda x.\s{unM}\,a\,(\s{unM}\,b\,x))\,(\s{unM}\,c\,x)) = \\
    & && \s{mkM}\,(\lambda x.\s{unM}\,a\,(\s{unM}\,b\,(\s{unM}\,c\,x))) = \\
    & && \dots \\
    & && a\otimes(b\otimes c)
  \end{alignat*}
\end{frame}
    
% ---------------------------------------------------------------------

\begin{frame}{Summary}
  \begin{itemize}
  \item Shallow embedding of monoids:
    \begin{itemize}
    \item Convenient reasoning because equalities are definitional
    \item Moral correctness: we can transfer every construction on the
      shallow embedded monoid to an arbitrary monoid
    \end{itemize}
  \item Rest of this talk:
    \begin{itemize}
    \item Do the same for another algebraic theory
    \item Show moral correctness
    \item Applications
    \end{itemize}
  \end{itemize}
\end{frame}

% ---------------------------------------------------------------------

\section{Shallow embedding of type theory}

% ---------------------------------------------------------------------

\begin{frame}{Martin-L{\"o}f's type theory as an algebraic theory}
  4 sorts:
  \begin{alignat*}{5}
    & \Con && : \Set \\
    & \Ty && : \Con\ra\Set \\
    & \Sub && : \Con\ra\Con\ra\Set \\
    & \Tm && : (\Gamma:\Con)\ra\Ty\,\Gamma\ra\Set
  \end{alignat*}
  The operators and equations come in groups:
  \begin{itemize}
  \item substitution calculus (CwF), e.g.\ $\blank[\blank] :\Ty\,\Delta\ra\Sub\,\Gamma\,\Delta\ra\Ty\,\Gamma$
  \item rules for $\Pi$ types: $\lam : \Tm\,(\Gamma\ext A)\,B\ra\Tm\,\Gamma\,(\Pi\,A\,B)$, $\app$, $\beta$, $\eta$, ${\lam[]} : (\lam\,t)[\sigma] = \lam\,(t^\uparrow)$
  \item rules for $\s{Nat}$: $\zero$, $\suc$, induction, $\beta$ laws, \dots
  \item rules for $\s{Id}$: reflexivity, $\J$, \dots
  \item \dots
  \end{itemize}
\end{frame}

% ---------------------------------------------------------------------

\begin{frame}{Size}
  \begin{center}
  \includegraphics[height=8cm]{tt.png}
  \end{center}
\end{frame}

% ---------------------------------------------------------------------

\begin{frame}{Explain traditional syntax}
  \begin{center}
  \begin{tikzpicture}
    \node (qiit) at (0,0) {quotient inductive-inductive type with 4 sorts};
    \node (iit) at (0,-2) {inductive-inductive type with 8 sorts};
    \node (it) at (0,-4) {inductive types of precontexts, preterms etc.\ and typing relations};
    \draw[->] (qiit) edge node[right] {setoid interpretation} (iit);
    \draw[->] (iit) edge node[right] {Streicher's construction} (it);
  \end{tikzpicture}
  \end{center}
\end{frame}

% ---------------------------------------------------------------------

\begin{frame}{Difficulty of a deep embedding}
  Equations depend on previous equations:
  \begin{alignat*}{5}
    & \hspace{2em} && \id && : \Sub\,\Gamma\,\Gamma \hspace{100em} \\
    & && \blank[\blank] && :\Ty\,\Delta\ra\Sub\,\Gamma\,\Delta\ra\Ty\,\Gamma \\
    & && \blank[\blank] && :\Tm\,\Delta\,A\ra(\sigma:\Sub\,\Gamma\,\Delta)\ra\Tm\,\Gamma\,(A[\sigma]) \\
    & && {\s{idlTy}} && : (A : \Ty\,\Gamma)\ra A[\id] \equiv A \\
    & && {\s{idlTm}} && : (t : \Tm\,\Gamma\,A)\ra \underbrace{t[\id]}_{:\Tm\,\Gamma\,(A[\id])} \equiv \underbrace{t}_{:\Tm\,\Gamma\,A} \\
  \end{alignat*}
\end{frame}

% ---------------------------------------------------------------------

\begin{frame}{Difficulty of a deep embedding}
  Equations depend on previous equations:
  \begin{alignat*}{5}
    & \hspace{2em} && \id && : \Sub\,\Gamma\,\Gamma \hspace{100em} \\
    & && \blank[\blank] && :\Ty\,\Delta\ra\Sub\,\Gamma\,\Delta\ra\Ty\,\Gamma \\
    & && \blank[\blank] && :\Tm\,\Delta\,A\ra(\sigma:\Sub\,\Gamma\,\Delta)\ra\Tm\,\Gamma\,(A[\sigma]) \\
    & && {\s{idlTy}} && : (A : \Ty\,\Gamma)\ra A[\id] \equiv A \\
    & && {\s{idlTm}} && : (t : \Tm\,\Gamma\,A)\ra \transp\,(\Tm\,\Gamma)\,(\s{idlTy}\,A)\,(t[\id]) \equiv t \\
    & \\
    &
  \end{alignat*}
\end{frame}

% ---------------------------------------------------------------------

\begin{frame}{Difficulty of a deep embedding}
  It is challenging even to write down the signature.

  Natural numbers:
  
  \vspace{0.5em}
  \includegraphics[height=6.5cm]{nat.png}
\end{frame}

% ---------------------------------------------------------------------

\begin{frame}{A model of type theory where every equation is $\refl$}
  The ``set'' model:
  \begin{alignat*}{5}
    & \Con && := \Set \\
    & \Ty\,\Gamma && := \Gamma\ra\Set \\
    & \Sub\,\Gamma\,\Delta && := \Gamma\ra\Delta \\
    & \Tm\,\Gamma\,A && := (x:\Gamma)\ra A\,x \\
  \end{alignat*}
  Substitutions:
  \begin{alignat*}{5}
    & A[\sigma]\,x && := A\,(\sigma\,x) \\
    & \id && := \lambda x.x \\
    & \s{idlTy}\,A && : \underbrace{A[\id]}_{= \lambda x.A\,x} \equiv A \\
    & \s{idlTy}\,A && := \refl
  \end{alignat*}
  We use this as the shallow embedding of the syntax.
\end{frame}

%---------------------------------------------------------------------

\section{Moral correctness}

%---------------------------------------------------------------------

\begin{frame}{Moral correctness}
  We check in Agda that we have \emph{enough} equations.
  
  \vspace{0.5em}
  We make sure using implementation hiding that we can only use the
  operators.

  \vspace{0.5em}
  We need to check that we don't have \emph{too many} equalities.
  
  \vspace{0.5em}
  \pause
  $\ll\blank\rr$ is the interpretation into the set model. We want to
  show: if $\ll t\rr \equiv \ll t'\rr$, then $t \equiv t'$.

  \vspace{0.5em}
  \pause
  This certainly doesn't hold internally:

  \vspace{0.5em}

  If we have funext, then
  \[
    \ll \lam\,(\s{if}\,\s{v^0}\,\s{then}\,\true\,\s{else}\,\false)\rr \equiv \ll\lam\,\s{v^0} \rr,
  \]
  however the two terms are not equal.
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Moral correctness externally}
  The set model externally:
  \begin{alignat*}{5}
    & \Con && := \Tm\,\bullet\,\U \\
    & \Ty\,\Gamma && := \Tm\,\bullet\,(\El\,\Gamma\Ra\U) \\
    & \Sub\,\Gamma\,\Delta && := \Tm\,\bullet\,(\El\,\Gamma\Ra\El\,\Delta) \\
    & \Tm\,\Gamma\,A && := \Tm\,\bullet\,\big(\Pi\,(\El\,\Gamma)\, (\El\,(A\oldapp\s{v^0}))\big)
  \end{alignat*}
%  Substitutions:
%  \begin{alignat*}{5}
%    & A[\sigma] && := \lam\,(A\oldapp (\sigma\oldapp\s{v^0})) \\
%    & \id && := \lam\,\s{v^0} \\
%    & \s{idlTy}\,A && : \underbrace{A[\id]}_{= \lam\,(A\oldapp \s{v^0})} \equiv A \\
%    & \s{idlTy}\,A && := \refl
%  \end{alignat*}
  \pause
  If $t, t':\Tm\,\Gamma\,A$, and $\ll\blank\rr$ is
  interpretation into the above model, then
  \[
    \ll t\rr = \ll t'\rr \text{ implies } t = t'.
  \]
  \pause
  Proof. Induction on the syntax:
  \begin{alignat*}{5}
    & \mathsf{I}_{(\Gamma:\Con)} && : \Gamma\simeq(\bullet\ext\El\,\ll\Gamma\rr) \\
    & \mathsf{I}_{(A:\Ty\,\Gamma)} && : A = \El\,(\app\,\ll A\rr[\mathsf{I}_\Gamma]) \\
    & \mathsf{I}_{(t:\Tm\,\Gamma\,A)} && : t = \El\,(\app\,\ll t\rr[\mathsf{I}_\Gamma])
  \end{alignat*}
  \pause
  Now: $t = \El\,(\app\,\ll t\rr[\mathsf{I}_\Gamma]) = \El\,(\app\,\ll t'\rr[\mathsf{I}_\Gamma]) = t'$.
\end{frame}

%---------------------------------------------------------------------

\section{Applications}

%---------------------------------------------------------------------

\begin{frame}{Usage}
  We formalised the following displayed models:
  \begin{itemize}
  \item Bernardy style syntactic parametricity shallow vs.\ deep
    \begin{itemize}
    \item 322 vs.\ 1682 loc
    \item 1 day vs 2 months
    \end{itemize}
  \item Canonicity
  \item Gluing (generalisation of the above two)
  \end{itemize}
  Available online: \url{https://bitbucket.org/akaposi/shallow}
\end{frame}

%---------------------------------------------------------------------

\section{Related work}

%---------------------------------------------------------------------

\begin{frame}{Range from shallow to deep}
  \begin{itemize}
  \item this paper
  \item terms deep but indexing shallow: McBride (Outrageous but
    meaningful coincidences)
  \item HOAS (not dependent types): Chlipala,
    Despeyroux-Felty-Hirschowitz, Pientka-Dunfield-Cave,
    Pientka-Thibodeau-Abel-Ferreira-Zucchini
  \item QIITs: Altenkirch-Kaposi
  \item using IITs: Chapman, Danielsson
  \item using preterms and typing relations:
    \begin{itemize}
    \item Agda: Abel-{\"O}hmann-Vezzosi, Devriese-Piessens,
    \item Coq: Wieczorek-Biernacki
    \item MetaCoq: Anand-Boulier-Cohen-Sozeau-Tabareau
    \end{itemize}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Usages of shallow embedding}
  \begin{itemize}
  \item metatheory of type theory: Boulier-Pédrot-Tabareau,
    Tabareau-Tanter-Sozeau, Kaposi-Kovács, Kaposi-Kovács-Altenkirch
  \item synthethic homotopy theory: e.g.\ Favonia-Finster-Licata-Lumsdaine
  \item internal language of CwF model: Kaposi-Kovács-Altenkirch
  \item internal language of presheaf model: Orton-Pitts,
    Coquand-Huber-Sattler
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\end{document}
