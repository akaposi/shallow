module lib where

infix 3 _≡_

data Bool : Set where
  true false : Bool

data _≡_ {A : Set}(a : A) : A → Set where
  refl : a ≡ a
