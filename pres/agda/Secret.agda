module Secret where

open import lib

record M : Set where
  constructor mkM
  field
    unM : Bool → Bool

u : M
u = mkM λ x → x

_⊗_ : M → M → M
a ⊗ b = mkM λ x → M.unM a (M.unM b x)

ass : (a b c : M) → (a ⊗ b) ⊗ c ≡ a ⊗ (b ⊗ c)
ass a b c = refl

idl : (a : M) → u ⊗ a ≡ a
idl a = refl

idr : (a : M) → a ⊗ u ≡ a
idr a = refl
