module b where

open import lib

module Monoid where

  module Secret where
    record M : Set where
      constructor mkM
      field
        unM : Bool → Bool

    u : M
    u = mkM λ x → x

    _⊗_ : M → M → M
    a ⊗ b = mkM λ x → M.unM a (M.unM b x)

    ass : (a b c : M) → (a ⊗ b) ⊗ c ≡ a ⊗ (b ⊗ c)
    ass a b c = refl

    idl : (a : M) → u ⊗ a ≡ a
    idl a = refl

    idr : (a : M) → a ⊗ u ≡ a
    idr a = refl

  M : Set
  M = Secret.M

  u : M
  u = Secret.u

  _⊗_ : M → M → M
  _⊗_ = Secret._⊗_

open Monoid

evil : M
evil = {!!}

