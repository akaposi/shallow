module Monoid where

import Secret

M : Set
M = Secret.M

u : M
u = Secret.u

_⊗_ : M → M → M
_⊗_ = Secret._⊗_
